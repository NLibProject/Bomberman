#ifndef PROJET_CLIENT_BCLIENT_PROTECT
#define PROJET_CLIENT_BCLIENT_PROTECT

/*
	@author SOARES Lucas
*/

// ------------------------------
// struct Projet::Client::BClient
// ------------------------------

typedef struct BClient
{
	// Est en cours?
	NBOOL m_estEnCours;

	// Est h�te?
	NBOOL m_estHote;

	// Menus
	BFondEtoileMenu *m_etoileMenu[ BNOMBRE_COUCHES_ETOILES_MENU ];
	BMenuPrincipal *m_menuPrincipal;
	BMenuCreer *m_menuCreer;
	BMenuRejoindre *m_menuRejoindre;

	// Configuration
	BConfiguration *m_configuration;

	// Serveur
	BServeur *m_serveur;

	// Client
	NClient *m_client;

	// Ressources
	BRessource *m_ressource;

	// Ensemble carte
	BEnsembleCarte *m_ensembleCarte;

	// Cache clients
	BCacheClient *m_cacheClient;

	// Monde
	BMonde *m_monde;
	BMondeClient *m_mondeClient;
	BConfigurationMonde *m_configurationMonde;

	// Ping
	NPing *m_ping;

	// Fenetre
	NFenetre *m_fenetre;
} BClient;

/* Construire */
__ALLOC BClient *Projet_Client_BClient_Construire( void );

/* Detruire */
void Projet_Client_BClient_Detruire( BClient** );

/* Lancer le client */
NBOOL Projet_Client_BClient_Lancer( BClient* );

/* Mettre a jour */
NBOOL Projet_Client_BClient_MettreAJour( BClient* );

/* Se connecter */
NBOOL Projet_Client_BClient_Connecter( BClient*,
	const char *ip,
	NU32 port );

/* Se deconnecter */
NBOOL Projet_Client_BClient_Deconnecter( BClient* );

/* Obtenir configuration */
const BConfiguration *Projet_Client_BClient_ObtenirConfiguration( const BClient* );

/* Obtenir nombre carte */
NU32 Projet_Client_BClient_ObtenirNombreCarte( const BClient* );

/* Obtenir nombre joueurs */
NU32 Projet_Client_BClient_ObtenirNombreJoueur( const BClient* );

/* Obtenir carte */
const BCarte *Projet_Client_BClient_ObtenirCarte( const BClient*,
	NU32 );

/* Obtenir etat carte */
const BEtatCarte *Projet_Client_BClient_ObtenirEtatCarte( const BClient* );

/* Obtenir configuration monde */
BConfigurationMonde *Projet_Client_BClient_ObtenirConfigurationMonde( BClient* );

/* Obtenir le serveur */
BServeur *Projet_Client_BClient_ObtenirServeur( BClient* );

/* Obtenir ressources */
const BRessource *Projet_Client_BClient_ObtenirRessource( const BClient* );

/* Obtenir fenetre */
const NFenetre *Projet_Client_BClient_ObtenirFenetre( const BClient* );

/* Obtenir joueur (le cache clients doit etre protege) */
const BEtatClient *Projet_Client_BClient_ObtenirJoueur( const BClient*,
	NU32 index );
const BEtatClient *Projet_Client_BClient_ObtenirJoueurCourant( const BClient* );

/* Obtenir l'identifiant du joueur courant */
NU32 Projet_Client_BClient_ObtenirIndexJoueurCourant( const BClient* );

/* Obtenir touche */
SDL_Keycode Projet_Client_BClient_ObtenirTouche( const BClient*,
	BTouche );
BTouche Projet_Client_BClient_ObtenirToucheInverse( const BClient*,
	SDL_Keycode );

/* Obtenir monde */
const BMonde *Projet_Client_BClient_ObtenirMonde( const BClient* );

/* Obtenir cache clients */
const BCacheClient *Projet_Client_BClient_ObtenirCacheClient( const BClient* );

/* Proteger cache clients */
NBOOL Projet_Client_BClient_ProtegerCacheClient( BClient* );

/* Ne plus proteger le cache clients */
NBOOL Projet_Client_BClient_NePlusProtegerCacheClient( BClient* );

/* Obtenir identifiant joueur courant */
NU32 Projet_Client_BClient_ObtenirIdentifiantJoueurCourant( const BClient* );

/* Obtenir checksum ressources */
NU32 Projet_Client_BClient_ObtenirChecksumRessource( const BClient* );

/* Obtenir identifiant derniere modification */
NU32 Projet_Client_BClient_ObtenirIdentifiantDerniereModificationCacheClient( BClient* );

/* Est connecte? */
NBOOL Projet_Client_BClient_EstConnecte( const BClient* );

/* Est h�te? */
NBOOL Projet_Client_BClient_EstHote( const BClient* );

/* Est pret a lancer */
NBOOL Projet_Client_BClient_EstPretLancer( const BClient* );

/* Jouer musique */
NBOOL Projet_Client_BClient_JouerMusique( const BClient*,
	BListeMusique );

/* Lire effet sonore */
void Projet_Client_BClient_JouerEffetSonore( const BClient*,
	BListeEffetSonore );

/* Arreter musique */
NBOOL Projet_Client_BClient_ArreterMusique( const BClient* );

/* Traiter un packet */
NBOOL Projet_Client_BClient_TraiterPacket( BClient*,
	void *donnee,
	BTypePacket );

/* Traiter la deconnexion */
NBOOL Projet_Client_BClient_TraiterDeconnexion( BClient* );

/* Envoyer un packet */
NBOOL Projet_Client_BClient_EnvoyerPacket( BClient*,
	__WILLBEOWNED NPacket* );

#endif // !PROJET_CLIENT_BCLIENT_PROTECT

