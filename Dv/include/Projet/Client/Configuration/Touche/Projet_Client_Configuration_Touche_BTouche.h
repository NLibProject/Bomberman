#ifndef PROJET_CLIENT_CONFIGURATION_TOUCHE_BTOUCHE_PROTECT
#define PROJET_CLIENT_CONFIGURATION_TOUCHE_BTOUCHE_PROTECT

// ---------------------------------------------------
// enum Projet::Client::Configuration::Touche::BTouche
// ---------------------------------------------------

typedef enum BTouche
{
	BTOUCHE_HAUT,
	BTOUCHE_BAS,
	BTOUCHE_GAUCHE,
	BTOUCHE_DROITE,

	BTOUCHE_BOMBE,

	BTOUCHES,

	BTOUCHE_AUCUNE = NERREUR
} BTouche;

/* Obtenir touche par defaut */
SDL_Keycode Projet_Client_Configuration_Touche_BTouche_ObtenirToucheDefaut( BTouche );

#ifdef PROJET_CLIENT_CONFIGURATION_TOUCHE_BTOUCHE_INTERNE
static const SDL_Keycode BToucheDefaut[ BTOUCHES ] =
{
	SDLK_z,
	SDLK_s,
	SDLK_q,
	SDLK_d,
	
	SDLK_SPACE
};
#endif // PROJET_CLIENT_CONFIGURATION_TOUCHE_BTOUCHE_INTERNE

#endif // !PROJET_CLIENT_CONFIGURATION_TOUCHE_BTOUCHE_PROTECT

