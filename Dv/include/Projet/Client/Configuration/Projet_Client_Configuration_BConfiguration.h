#ifndef PROJET_CLIENT_CONFIGURATION_BCONFIGURATION_PROTECT
#define PROJET_CLIENT_CONFIGURATION_BCONFIGURATION_PROTECT

// ----------------------------------------------------
// struct Projet::Client::Configuration::BConfiguration
// ----------------------------------------------------

typedef struct BConfiguration
{
	// Resolution
	NUPoint m_resolution;

	// Port
	NU32 m_port;

	// Configuration des touches
	BConfigurationTouche *m_touche;

	// Parametres par defaut
	char *m_nomDefaut;
	NU32 m_charsetDefaut;
	NU32 m_couleurCharsetDefaut;
} BConfiguration;

/* Construire */
__ALLOC BConfiguration *Projet_Client_Configuration_BConfiguration_Construire( const char* );

/* Detruire */
void Projet_Client_Configuration_BConfiguration_Detruire( BConfiguration** );

/* Obtenir la resolution */
const NUPoint *Projet_Client_Configuration_BConfiguration_ObtenirResolution( const BConfiguration* );

/* Obtenir le port */
NU32 Projet_Client_Configuration_BConfiguration_ObtenirPort( const BConfiguration* );

/* Obtenir le nom par defaut */
const char *Projet_Client_Configuration_BConfiguration_ObtenirNomDefaut( const BConfiguration* );

/* Obtenir le charset defaut */
NU32 Projet_Client_Configuration_BConfiguration_ObtenirCharsetDefaut( const BConfiguration* );

/* Obtenir couleur charset defaut */
NU32 Projet_Client_Configuration_BConfiguration_ObtenirCouleurCharsetDefaut( const BConfiguration* );

/* Obtenir configuration touches */
BConfigurationTouche *Projet_Client_Configuration_BConfiguration_ObtenirConfigurationTouche( const BConfiguration* );

#endif // !PROJET_CLIENT_CONFIGURATION_BCONFIGURATION_PROTECT

