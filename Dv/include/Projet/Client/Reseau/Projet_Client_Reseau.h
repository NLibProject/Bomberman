#ifndef PROJET_CLIENT_RESEAU_PROTECT
#define PROJET_CLIENT_RESEAU_PROTECT

// --------------------------------
// namespace Projet::Client::Reseau
// --------------------------------

/* Callback reception packet */
__CALLBACK NBOOL Projet_Client_Reseau_CallbackReceptionPacket( const NPacket*,
	const NClient* );

/* Callback deconnexion */
NBOOL Projet_Client_Reseau_CallbackDeconnexion( const NClient* );

#endif // !PROJET_CLIENT_RESEAU_PROTECT

