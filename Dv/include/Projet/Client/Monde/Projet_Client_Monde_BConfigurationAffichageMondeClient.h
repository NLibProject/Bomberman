#ifndef PROJET_CLIENT_MONDE_BCONFIGURATIONAFFICHAGEMONDECLIENT_PROTECT
#define PROJET_CLIENT_MONDE_BCONFIGURATIONAFFICHAGEMONDECLIENT_PROTECT

// ----------------------------------------------------------------
// struct Projet::Client::Monde::BConfigurationAffichageMondeClient
// ----------------------------------------------------------------

typedef struct BConfigurationAffichageMondeClient
{
	// Position initiale affichage
	NSPoint m_baseAffichage;

	// Facteur de zoom
	NU32 m_zoom;
} BConfigurationAffichageMondeClient;

/* Construire */
__ALLOC BConfigurationAffichageMondeClient *Projet_Client_Monde_BConfigurationAffichageMondeClient_Construire( const NFenetre*,
	const BCarte* );

/* Detruire */
void Projet_Client_Monde_BConfigurationAffichageMondeClient_Detruire( BConfigurationAffichageMondeClient** );

/* Obtenir base affichage */
const NSPoint *Projet_Client_Monde_BConfigurationAffichageMondeClient_ObtenirBaseAffichage( const BConfigurationAffichageMondeClient* );

/* Obtenir facteur de zoom */
NU32 Projet_Client_Monde_BConfigurationAffichageMondeClient_ObtenirFacteurZoom( const BConfigurationAffichageMondeClient* );

#endif // !PROJET_CLIENT_MONDE_BCONFIGURATIONAFFICHAGEMONDECLIENT_PROTECT

