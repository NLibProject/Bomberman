#ifndef PROJET_CLIENT_MONDE_BMONDECLIENT_PROTECT
#define PROJET_CLIENT_MONDE_BMONDECLIENT_PROTECT

// ------------------------------------------
// struct Projet::Client::Monde::BMondeClient
// ------------------------------------------

typedef struct BMondeClient
{
	// Est en cours?
	NBOOL m_estEnCours;

	// Est partie en cours?
	NBOOL m_estPartieEnCours;

	// Fin de partie
	BTypeFinPartie m_typeFinPartie;
	NU32 m_identifiantVainqueur;

	// Code retour
	BCodeRetourMondeClient m_codeRetour;

	// Configuration affichage
	BConfigurationAffichageMondeClient *m_configurationAffichage;

	// Gestion messages
	BMessageMondeClient *m_message;

	// Etat deplacement
	BEtatDeplacement *m_etatDeplacement;

	// Etat bombe
	BEtatPoseBombe *m_etatPoseBombe;

	// Client
	struct BClient *m_client;

	// Cache clients
	const BCacheClient *m_cacheClient;

	// Monde
	const BMonde *m_monde;

	// Carte
	const BCarte *m_carte;
} BMondeClient;

/* Construire */
__ALLOC BMondeClient *Projet_Client_Monde_BMondeClient_Construire( struct BClient *client );

/* Detruire */
void Projet_Client_Monde_BMondeClient_Detruire( BMondeClient** );

/* Lancer */
BCodeRetourMondeClient Projet_Client_Monde_BMondeClient_Lancer( BMondeClient* );

/* Demarrer la partie (sur ordre du serveur) */
void Projet_Client_Monde_BMondeClient_DemarrerPartie( BMondeClient* );

/* Afficher un message */
NBOOL Projet_Client_Monde_BMondeClient_AfficherMessage( BMondeClient*,
	const char*,
	BPoliceMessageClient,
	NU32 duree,
	NCouleur,
	BPositionAffichageMessageClient,
	NBOOL estDoitAfficherCadre,
	NCouleur couleurCadre );

/* Enregistrer reponse suite a un deplacement */
void Projet_Client_Monde_BMondeClient_EnregistrerReponseServeurDeplacement( BMondeClient* );

/* Enregistrer reponse pose bombe */
void Projet_Client_Monde_BMondeClient_EnregistrerReponseServeurPoseBombe( BMondeClient* );

/* Poser bombe */
NBOOL Projet_Client_Monde_BMondeClient_PoserBombe( BMondeClient*,
	NSPoint position,
	NU32 identifiantBombe,
	NU32 identifiantJoueur );

/* Poser bonus */
NBOOL Projet_Client_Monde_BMondeClient_PoserBonus( BMondeClient*,
	NSPoint position,
	BListeBonus type,
	NU32 duree,
	NU32 identifiant );

/* Supprimer bonus */
NBOOL Projet_Client_Monde_BMondeClient_SupprimerBonus( BMondeClient*,
	NU32 identifiant );

/* Lire son bonus */
void Projet_Client_Monde_BMondeClient_JouerSonBonus( BMondeClient*,
	NBOOL estJoueurCourant,
	BListeBonus );

/* Tuer un joueur */
NBOOL Projet_Client_Monde_BMondeClient_TuerJoueur( BMondeClient*,
	NU32 identifiant );

/* Arreter la partie */
void Projet_Client_Monde_BMondeClient_ArreterPartie( BMondeClient*,
	BTypeFinPartie,
	NU32 identifiantVainqueur );

#endif // !PROJET_CLIENT_MONDE_BMONDECLIENT_PROTECT

