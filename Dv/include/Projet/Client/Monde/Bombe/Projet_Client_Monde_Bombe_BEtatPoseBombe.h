#ifndef PROJET_CLIENT_MONDE_BOMBE_BETATPOSEBOMBE_PROTECT
#define PROJET_CLIENT_MONDE_BOMBE_BETATPOSEBOMBE_PROTECT

// ---------------------------------------------------
// struct Projet::Client::Monde::Bombe::BEtatPoseBombe
// ---------------------------------------------------

typedef struct BEtatPoseBombe
{
	// Est attente reponse serveur?
	NBOOL m_estAttenteReponseServeur;

	// Est doit locker la pose (en attente de relāchement?)
	NBOOL m_estPoseLock;

	// Client
	struct BClient *m_client;
} BEtatPoseBombe;

/* Construire */
__ALLOC BEtatPoseBombe *Projet_Client_Monde_Bombe_BEtatPoseBombe_Construire( const struct BClient *client );

/* Detruire */
void Projet_Client_Monde_Bombe_BEtatPoseBombe_Detruire( BEtatPoseBombe** );

/* Poser bombe */
NBOOL Projet_Client_Monde_Bombe_BEtatPoseBombe_PoserBombe( BEtatPoseBombe* );

/* Finir de poser bombe */
void Projet_Client_Monde_Bombe_BEtatPoseBombe_FinirPoseBombe( BEtatPoseBombe* );

/* Enregister reponse serveur */
void Projet_Client_Monde_Bombe_BEtatPoseBombe_EnregistrerReponseServeur( BEtatPoseBombe* );

#endif // !PROJET_CLIENT_MONDE_BOMBE_BETATPOSEBOMBE_PROTECT

