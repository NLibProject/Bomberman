#ifndef PROJET_CLIENT_MONDE_PROTECT
#define PROJET_CLIENT_MONDE_PROTECT

// -------------------------------
// namespace Projet::Client::Monde
// -------------------------------

// Effets sonore
static const BListeEffetSonore BSON_EXPLOSION_BOMBE_NORMALE = BLISTE_EFFET_SONORE_BM_BOMB1;
static const BListeEffetSonore BSON_EXPLOSION_BOMBE_FORTE	= BLISTE_EFFET_SONORE_BM_BOMB2;
static const BListeEffetSonore BEFFET_SONORE_MORT_CLIENT	= BLISTE_EFFET_SONORE_BM_JEU_MORT;

// enum Projet::Client::Monde::BCodeRetourMondeClient
#include "Projet_Client_Monde_BCodeRetourMondeClient.h"

// struct Projet::Client::Monde::BConfigurationAffichageMondeClient
#include "Projet_Client_Monde_BConfigurationAffichageMondeClient.h"

// namespace Projet::Client::Monde::Message
#include "Message/Projet_Client_Monde_Message.h"

// namespace Projet::Client::Monde::Deplacement
#include "Deplacement/Projet_Client_Monde_Deplacement.h"

// namespace Projet::Client::Monde::Bombe
#include "Bombe/Projet_Client_Monde_Bombe.h"

// struct Projet::Client::Monde::BMondeClient
#include "Projet_Client_Monde_BMondeClient.h"

#endif // !PROJET_CLIENT_MONDE_PROTECT

