#ifndef PROJET_CLIENT_MONDE_DEPLACEMENT_BETATDEPLACEMENT_PROTECT
#define PROJET_CLIENT_MONDE_DEPLACEMENT_BETATDEPLACEMENT_PROTECT

// -----------------------------------------------------------
// struct Projet::Client::Monde::Deplacement::BEtatDeplacement
// -----------------------------------------------------------

typedef struct BEtatDeplacement
{
	// Est deplacement en cours?
	NBOOL m_estEnCours;

	// Est attente reponse deplacement?
	NBOOL m_estAttenteReponseDeplacement;

	// Debut pression touche
	NU32 m_tempsDebutPressionTouche;

	// Direction
	NDirection m_direction;

	// Client
	const struct BClient *m_client;
} BEtatDeplacement;

/* Construire */
__ALLOC BEtatDeplacement *Projet_Client_Monde_Deplacement_BEtatDeplacement_Construire( const struct BClient* );

/* Detruire */
void Projet_Client_Monde_Deplacement_BEtatDeplacement_Detruire( BEtatDeplacement** );

/* Deplacer */
void Projet_Client_Monde_Deplacement_BEtatDeplacement_Deplacer( BEtatDeplacement*,
	NDirection direction );

/* Stopper deplacement */
void Projet_Client_Monde_Deplacement_BEtatDeplacement_StopperDeplacement( BEtatDeplacement*,
	NDirection direction );

/* Reponse deplacement serveur */
void Projet_Client_Monde_Deplacement_BEtatDeplacement_EnregistrerReponseDeplacementServeur( BEtatDeplacement* );

/* Est en attente de la reponse pour le deplacement? */
NBOOL Projet_Client_Monde_Deplacement_BEtatDeplacement_EstAttenteReponseServeur( const BEtatDeplacement* );

/* Update */
void Projet_Client_Monde_Deplacement_BEtatDeplacement_Update( BEtatDeplacement* );

#endif // !PROJET_CLIENT_MONDE_DEPLACEMENT_BETATDEPLACEMENT_PROTECT

