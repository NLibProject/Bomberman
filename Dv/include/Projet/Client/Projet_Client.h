#ifndef PROJET_CLIENT_PROTECT
#define PROJET_CLIENT_PROTECT

/*
	Definitions relatives au client

	@author SOARES Lucas
*/

// ------------------------
// namespace Projet::Client
// ------------------------

// Titre fenetre
static const char BTITRE_FENETRE_CLIENT[ 32 ] = "Bomberman";

/* Main client */
NS32 Projet_Client_Main( void );

// namespace Projet::Client::Menu
#include "Menu/Projet_Client_Menu.h"

// namespace Projet::Client::Configuration
#include "Configuration/Projet_Client_Configuration.h"

// enum Projet::Client::BEtapeConstruction
#include "Projet_Client_BEtapeConstructionClient.h"

// namespace Projet::Client::Monde
#include "Monde/Projet_Client_Monde.h"

// struct Projet::Client::BClient
#include "Projet_Client_BClient.h"

// namespace Projet::Client::Reseau
#include "Reseau/Projet_Client_Reseau.h"

// namespace Projet::Client::TraitementPacket
#include "TraitementPacket/Projet_Client_TraitementPacket.h"

#endif // !PROJET_CLIENT_PROTECT

