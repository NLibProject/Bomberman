#ifndef PROJET_CLIENT_MENU_PRINCIPAL_BCODEMENUPRINCIPAL_PROTECT
#define PROJET_CLIENT_MENU_PRINCIPAL_BCODEMENUPRINCIPAL_PROTECT

// --------------------------------------------------------
// enum Projet::Client::Menu::Principal::BCodeMenuPrincipal
// --------------------------------------------------------

typedef enum BCodeMenuPrincipal
{
	BCODE_MENU_PRINCIPAL_MENU_CREER,
	BCODE_MENU_PRINCIPAL_MENU_REJOINDRE,
	BCODE_MENU_PRINCIPAL_QUITTER,

	BCODES_MENU_PRINCIPAL
} BCodeMenuPrincipal;

/* Obtenir texte code */
const char *Projet_Client_Menu_Principal_BCodeMenuPrincipal_ObtenirTexte( BCodeMenuPrincipal );

#ifdef PROJET_CLIENT_MENU_PRINCIPAL_BCODEMENUPRINCIPAL_INTERNE
static const char BCodeMenuPrincipalTexte[ BCODES_MENU_PRINCIPAL ][ 32 ] =
{
	"Creer une partie",
	"Rejoindre une partie",
	"Quitter"
};
#endif // PROJET_CLIENT_MENU_PRINCIPAL_BCODEMENUPRINCIPAL_INTERNE

#endif // !PROJET_CLIENT_MENU_PRINCIPAL_BCODEMENUPRINCIPAL_PROTECT

