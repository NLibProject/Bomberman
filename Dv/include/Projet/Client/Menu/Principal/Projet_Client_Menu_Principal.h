#ifndef PROJET_CLIENT_MENU_PRINCIPAL_PROTECT
#define PROJET_CLIENT_MENU_PRINCIPAL_PROTECT

// -----------------------------------------
// namespace Projet::Client::Menu::Principal
// -----------------------------------------

// Epaisseur bouton
static const NU32 BEPAISSEUR_BOUTON_MENU_PRINCIPAL					= 2;

// Couleurs
static const NCouleur BCOULEUR_BOUTON_MENU_PRINCIPAL_CONTOUR		= { 0xFF, 0xFF, 0xFF, 0xFF };
static const NCouleur BCOULEUR_BOUTON_MENU_PRINCIPAL_FOND			= { 0x00, 0x00, 0x00, 0xFF };
static const NCouleur BCOULEUR_BOUTON_MENU_PRINCIPAL_CONTOUR_SURVOL = { 0xFF, 0xFF, 0xFF, 0xFF };
static const NCouleur BCOULEUR_BOUTON_MENU_PRINCIPAL_FOND_SURVOL	= { 0x7F, 0x7F, 0x7F, 0xFF };
static const NCouleur BCOULEUR_BOUTON_MENU_PRINCIPAL_CONTOUR_CLIC	= { 0xFF, 0x00, 0x00, 0xFF };
static const NCouleur BCOULEUR_BOUTON_MENU_PRINCIPAL_FOND_CLIC		= { 0x7F, 0x7F, 0x7F, 0xFF };
static const NCouleur BCOULEUR_TITRE_MENU_PRINCIPAL					= { 0xFF, 0xFF, 0xFF, 0xFF };
static const NCouleur BCOULEUR_OPTIONS_MENU_PRINCIPAL				= { 0xFF, 0xFF, 0xFF, 0xFF };

// Marging/Padding
static const NS32 BPADDING_BOUTON_MENU_PRINCIPAL[ NDIRECTIONS ]		= { 15, 15, 15, 15 };
static const NS32 BMARGING_BOUTON_MENU_PRINCIPAL[ NDIRECTIONS ]		= { 10, 10, 10, 10 };
static const NU32 BESPACE_IMAGE_LOGO_TITRE_MENU_PRINCIPAL			= 10;

// Police
static const NU32 BPOLICE_TITRE_MENU_PRINCIPAL						= BLISTE_POLICE_LAZY_SUNDAY;
static const NU32 BPOLICE_MENU_PRINCIPAL							= BLISTE_POLICE_BUBBLEGUM;
static const NU32 BTAILLE_POLICE_TITRE_OPTIONS_MENU_PRINCIPAL		= 38;
static const NU32 BTAILLE_POLICE_OPTIONS_MENU_PRINCIPAL				= 24;

// Base affichage premiere option
static const NSPoint BPOSITION_PREMIER_BOUTON_MENU_PRINCIPAL		= { 20, 20 };

// Titre
static const char BTITRE_MENU_PRINCIPAL[ 32 ]						= "Bomberman";

// enum Projet::Client::Menu::Principal::BCodeMenuPrincipal
#include "Projet_Client_Menu_Principal_BCodeMenuPrincipal.h"

// struct Projet::Client::Menu::Principal::BMenuPrincipal
#include "Projet_Client_Menu_Principal_BMenuPrincipal.h"

#endif // !PROJET_CLIENT_MENU_PRINCIPAL_PROTECT

