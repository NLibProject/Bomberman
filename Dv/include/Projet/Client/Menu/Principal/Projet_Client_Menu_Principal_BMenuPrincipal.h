#ifndef PROJET_CLIENT_MENU_PRINCIPAL_BMENUPRINCIPAL_PROTECT
#define PROJET_CLIENT_MENU_PRINCIPAL_BMENUPRINCIPAL_PROTECT

// ------------------------------------------------------
// struct Projet::Client::Menu::Principal::BMenuPrincipal
// ------------------------------------------------------

typedef struct BMenuPrincipal
{
	// Est en cours?
	NBOOL m_estEnCours;

	// Surface bomberman
	NSurface *m_surfaceBomberman;

	// Surface titre
	NSurface *m_surfaceTitre;

	// Boutons
	NBouton *m_bouton[ BCODES_MENU_PRINCIPAL ];
	NSurface *m_texteBouton[ BCODES_MENU_PRINCIPAL ];

	// Position souris
	NSPoint m_positionSouris;

	// Etoiles
	const BFondEtoileMenu **m_etoile;

	// Fenetre
	const NFenetre *m_fenetre;
} BMenuPrincipal;

/* Construire */
__ALLOC BMenuPrincipal *Projet_Client_Menu_Principal_BMenuPrincipal_Construire( const NFenetre*,
	const BFondEtoileMenu *etoiles[ BNOMBRE_COUCHES_ETOILES_MENU ] );

/* Detruire */
void Projet_Client_Menu_Principal_BMenuPrincipal_Detruire( BMenuPrincipal** );

/* Lancer le menu */
BCodeMenuPrincipal Projet_Client_Menu_Principal_BMenuPrincipal_Lancer( BMenuPrincipal* );


#endif // !PROJET_CLIENT_MENU_PRINCIPAL_BMENUPRINCIPAL_PROTECT

