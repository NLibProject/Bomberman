#ifndef PROJET_CLIENT_MENU_CREER_BMENUCREER_PROTECT
#define PROJET_CLIENT_MENU_CREER_BMENUCREER_PROTECT

// ----------------------------------------------
// struct Projet::Client::Menu::Creer::BMenuCreer
// ----------------------------------------------

typedef struct BMenuCreer
{
	// Est en cours?
	NBOOL m_estEnCours;

	// Position souris
	NSPoint m_positionSouris;

	// Fenetre
	NFenetre *m_fenetre;
} BMenuCreer;

/* Construire */
__ALLOC BMenuCreer *Projet_Client_Menu_Creer_BMenuCreer_Construire( const NFenetre* );

/* Detruire */
void Projet_Client_Menu_Creer_BMenuCreer_Detruire( BMenuCreer** );

/* Lancer */
BCodeMenuCreer Projet_Client_Menu_Creer_BMenuCreer_Lancer( BMenuCreer* );

#endif // !PROJET_CLIENT_MENU_CREER_BMENUCREER_PROTECT

