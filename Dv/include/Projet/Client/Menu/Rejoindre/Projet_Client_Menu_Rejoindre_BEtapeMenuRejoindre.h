#ifndef PROJET_CLIENT_MENU_REJOINDRE_BETAPEMENUREJOINDRE_PROTECT
#define PROJET_CLIENT_MENU_REJOINDRE_BETAPEMENUREJOINDRE_PROTECT

// ---------------------------------------------------------
// enum Projet::Client::Menu::Rejoindre::BEtapeMenuRejoindre
// ---------------------------------------------------------

typedef enum BEtapeMenuRejoindre
{
	BETAPE_MENU_REJOINDRE_CONNEXION,
	BETAPE_MENU_REJOINDRE_ATTENTE_REPONSE_CONNEXION,
	BETAPE_MENU_REJOINDRE_CHOIX_DETAILS,

	BETAPES_MENU_REJOINDRE
} BEtapeMenuRejoindre;

#endif // !PROJET_CLIENT_MENU_REJOINDRE_BETAPEMENUREJOINDRE_PROTECT

