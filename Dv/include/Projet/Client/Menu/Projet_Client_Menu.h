#ifndef PROJET_CLIENT_MENU_PROTECT
#define PROJET_CLIENT_MENU_PROTECT

// ------------------------------
// namespace Projet::Client::Menu
// ------------------------------

// Etoiles
#define BNOMBRE_ETOILES_MENU							500
#define BNOMBRE_COUCHES_ETOILES_MENU					4

// Proprietes couches etoiles
static const NCouleur BCOULEUR_ETOILES_MENU[ BNOMBRE_COUCHES_ETOILES_MENU ] =
{
	{ 0x7F, 0x7F, 0x00, 0xFF },
	{ 0xDD, 0xDD, 0xDD, 0xFF },
	{ 0x00, 0x00, 0xFF, 0x7F },
	{ 0x00, 0xFF, 0x7F, 0xFF }
};

static const NU32 BDELAI_ENTRE_UPDATE_ETOILES_MENU[ BNOMBRE_COUCHES_ETOILES_MENU ] =
{
	0,
	50,
	25,
	100
};

// struct Projet::Client::Menu:BFondEtoileMenu
#include "Projet_Client_Menu_BFondEtoileMenu.h"

// namespace Projet::Client::Menu::Principal
#include "Principal/Projet_Client_Menu_Principal.h"

// namespace Projet::Client::Menu::Creer
#include "Creer/Projet_Client_Menu_Creer.h"

// namespace Projet::Client::Menu::Rejoindre
#include "Rejoindre/Projet_Client_Menu_Rejoindre.h"

#endif // !PROJET_CLIENT_MENU_PROTECT

