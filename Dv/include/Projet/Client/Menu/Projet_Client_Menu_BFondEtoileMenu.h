#ifndef PROJET_CLIENT_MENU_BFONDETOILEMENU_PROTECT
#define PROJET_CLIENT_MENU_BFONDETOILEMENU_PROTECT

// --------------------------------------------
// struct Projet::Client::Menu::BFondEtoileMenu
// --------------------------------------------

typedef struct BFondEtoileMenu
{
	// Fenetre
	const NFenetre *m_fenetre;

	// Etoiles
	NSPoint m_etoile[ BNOMBRE_ETOILES_MENU ];
	NSPoint m_etoileAffichee[ BNOMBRE_ETOILES_MENU ];
	NUPoint m_scrollEtoile;

	// Delai entre update
	NU32 m_delaiEntreUpdate;

	// Couleur
	NCouleur m_couleur;

	// Temps dernier update
	NU32 m_tempsDernierUpdate;
} BFondEtoileMenu;

/* Construire */
__ALLOC BFondEtoileMenu *Projet_Client_Menu_BFondEtoileMenu_Construire( const NFenetre*,
	NU32 delaiEntreUpdate,
	NCouleur couleur );

/* Detruire */
void Projet_Client_Menu_BFondEtoileMenu_Detruire( BFondEtoileMenu** );

/* Afficher */
void Projet_Client_Menu_BFondEtoileMenu_Afficher( const BFondEtoileMenu* );

/* Update */
void Projet_Client_Menu_BFondEtoileMenu_Update( BFondEtoileMenu* );

#endif // !PROJET_CLIENT_MENU_BFONDETOILEMENU_PROTECT

