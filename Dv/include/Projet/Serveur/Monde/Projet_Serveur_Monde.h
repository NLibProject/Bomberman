#ifndef PROJET_SERVEUR_MONDE_PROTECT
#define PROJET_SERVEUR_MONDE_PROTECT

// --------------------------------
// namespace Projet::Serveur::Monde
// --------------------------------

// Temps avant lancement partie (compte a rebours)
#define BTEMPS_AVANT_LANCEMENT_PARTIE_MONDE_SERVEUR					3

// Message debut partie
#define BMESSAGE_DEBUT_PARTIE_MONDE_SERVEUR							"C'est parti!"

// Temps avant explosion d'une bombe
#define BTEMPS_AVANT_EXPLOSION_BOMBE_DEFAUT							2000

// Temps additionnel aleatoire avant explosion
#define BTEMPS_ADDITIONNEL_MAXIMUM_EXPLOSION_BOMBE					3000

// Duree d'affichage du message de mort
static const NU32 BDUREE_MESSAGE_MORT_JOUEUR_SERVEUR				= 5000;

// Couleur message mort joueur
static const NCouleur BCOULEUR_MESSAGE_MORT_JOUEUR_SERVEUR			= { 0xFF, 0x22, 0x22, 0xFF };

// Couleur cadre message mort joueur
static const NCouleur BCOULEUR_CADRE_MESSAGE_MORT_JOUEUR_SERVEUR	= { 0x00, 0x00, 0x00, 200 };

// Message mort
static const char BMESSAGE_MORT_JOUEUR_SERVEUR[ 32 ]				= "%s a ete tue par %s";
static const char BMESSAGE_MORT_JOUEUR_SUICIDE_SERVEUR[ 32 ]		= "%s s'est suicide";

// struct Projet::Serveur::Monde::BMondeServeur
#include "Projet_Serveur_Monde_BMondeServeur.h"

#endif // !PROJET_SERVEUR_MONDE_PROTECT

