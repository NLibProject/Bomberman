#ifndef PROJET_SERVEUR_MONDE_BMONDESERVEUR_PROTECT
#define PROJET_SERVEUR_MONDE_BMONDESERVEUR_PROTECT

// --------------------------------------------
// struct Projet::Serveur::Monde::BMondeServeur
// friend Projet::Serveur::BServeur
// --------------------------------------------

typedef struct BMondeServeur
{
	// Serveur
	struct BServeur *m_serveur;

	// Carte
	const BCarte *m_carte;

	// Etat carte
	const BEtatCarte *m_etatCarte;

	// Monde
	const BMonde *m_monde;

	// Cache clients
	const BCacheClient *m_cacheClient;

	// Experience pour le spawn de bonus
	NExperience *m_experienceApparitionBonus;

	// Compte a rebours initial
		// Est effectue?
			NBOOL m_estCompteReboursEffectue;
		// Valeur actuelle
			NU32 m_valeurCompteRebours;
		// Dernier changement
			NU32 m_tempsDernierChangement;

	// Partie en cours?
	NBOOL m_estPartieEnCours;

	// Temps dernier envoi message aux clients
	NU32 m_tempsDernierEnvoiMessageClient;
} BMondeServeur;

/* Construire */
__ALLOC BMondeServeur *Projet_Serveur_Monde_BMondeServeur_Construire( struct BServeur *serveur );

/* Detruire */
void Projet_Serveur_Monde_BMondeServeur_Detruire( BMondeServeur** );

/* Est partie en cours? */
NBOOL Projet_Serveur_Monde_BMondeServeur_EstPartieEnCours( const BMondeServeur* );

/* Lancer partie */
void Projet_Serveur_Monde_BMondeServeur_LancerPartie( BMondeServeur* );

/* Obtenir carte */
const BCarte *Projet_Serveur_Monde_BMondeServeur_ObtenirCarte( BMondeServeur* );

/* Obtenir etat carte */
const BEtatCarte *Projet_Serveur_Monde_BMondeServeur_ObtenirEtatCarte( BMondeServeur* );

/* Poser une bombe */
NU32 Projet_Serveur_Monde_BMondeServeur_PoserBombe( BMondeServeur*,
	NSPoint position,
	NU32 identifiantJoueur );

/* Traiter la prise de bonus eventuelle */
NBOOL Projet_Serveur_Monde_BMondeServeur_TraiterPriseBonus( BMondeServeur*,
	const NClientServeur*,
	const BEtatClient* );

/* Update */
void Projet_Serveur_Monde_BMondeServeur_Update( BMondeServeur* );

#endif // !PROJET_SERVEUR_MONDE_BMONDESERVEUR_PROTECT

