#ifndef PROJET_SERVEUR_RESEAU_PROTECT
#define PROJET_SERVEUR_RESEAU_PROTECT

// ---------------------------------
// namespace Projet::Serveur::Reseau
// ---------------------------------

/* Callback connexion client */
__CALLBACK NBOOL Projet_Serveur_Reseau_CallbackConnexionClient( const NClientServeur* );

/* Callback reception packet */
__CALLBACK NBOOL Projet_Serveur_Reseau_CallbackReceptionPacket( const NClientServeur*,
	const NPacket* );

/* Callback deconnexion client */
__CALLBACK NBOOL Projet_Serveur_Reseau_CallbackDeconnexionClient( const NClientServeur* );

#endif // !PROJET_SERVEUR_RESEAU_PROTECT

