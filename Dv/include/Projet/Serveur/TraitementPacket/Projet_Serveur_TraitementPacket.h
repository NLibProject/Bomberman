#ifndef PROJET_SERVEUR_TRAITEMENTPACKET_PROTECT
#define PROJET_SERVEUR_TRAITEMENTPACKET_PROTECT

// -------------------------------------------
// namespace Projet::Serveur::TraitementPacket
// friend Projet::Serveur::BServeur
// -------------------------------------------

/* Notifier deconnexion joueur (le cache doit etre protege) */
NBOOL Projet_Serveur_TraitementPacket_NotifierDeconnexionJoueur( BServeur*,
	NClientServeur* );

/* Notifier un joueur de son identifiant */
NBOOL Projet_Serveur_TraitementPacket_NotifierIdentifiantJoueur( BServeur*,
	NClientServeur* );

/* Notifier un joueur des joueurs deja presents (le cache doit etre protege) */
NBOOL Projet_Serveur_TraitementPacket_NotifierJoueurConnecte( BServeur*,
	NClientServeur* );

/* Notifier un joueur de l'etat actuel du serveur */
NBOOL Projet_Serveur_TraitementPacket_NotifierEtatServeur( BServeur*,
	NClientServeur* );

/* Notifier un joueur de son kick pour CRC incorrect */
NBOOL Projet_Serveur_TraitementPacket_NotifierJoueurKickCRC( BServeur*,
	NClientServeur* );

/* Effectuer requete ping */
NBOOL Projet_Serveur_TraitementPacket_EnvoyerRequetePing( BServeur*,
	NClientServeur* );

/* Notifier changement de carte */
NBOOL Projet_Serveur_TraitementPacket_NotifierChangementCarte( BServeur*,
	NU32 carte );

/* Notifier lancement partie */
NBOOL Projet_Serveur_TraitementPacket_NotifierLancementPartie( BServeur* );

/* Notifier message */
NBOOL Projet_Serveur_TraitementPacket_NotifierMessageClientUnique( NClientServeur*,
	const char *message,
	BPoliceMessageClient,
	NCouleur,
	NU32 dureeAffichage,
	BPositionAffichageMessageClient,
	NBOOL estDoitAfficherCadre,
	NCouleur couleurCadre );
NBOOL Projet_Serveur_TraitementPacket_NotifierMessageClientTous( BServeur*,
	const char *message,
	BPoliceMessageClient,
	NCouleur,
	NU32 dureeAffichage,
	BPositionAffichageMessageClient,
	NBOOL estDoitAfficherCadre,
	NCouleur couleurCadre );

/* Notifier le debut de la partie */
NBOOL Projet_Serveur_TraitementPacket_NotifierDebutPartie( BServeur* );

/* Notifier un changement de direction de la part d'un joueur */
NBOOL Projet_Serveur_TraitementPacket_NotifierJoueurChangeDirection( BServeur*,
	const NClientServeur*,
	NDirection );

/* Notifier un changement de position de la part d'un joueur */
NBOOL Projet_Serveur_TraitementPacket_NotifierJoueurChangePosition( BServeur*,
	const NClientServeur*,
	NSPoint nouvellePosition,
	NDirection );

/* Notifier une pose de bombe */
NBOOL Projet_Serveur_TraitementPacket_NotifierPoseBombe( BServeur*,
	const NClientServeur*,
	const NSPoint*,
	NU32 identifiantBombe );

/* Notifier un refus de pose de bombe */
NBOOL Projet_Serveur_TraitementPacket_NotifierRefusPoseBombe( const NClientServeur* );

/* Notifier une explosion de bombe */
NBOOL Projet_Serveur_TraitementPacket_NotifierExplosionBombe( BServeur*,
	NU32 identifiantBombe,
	NU32 puissance,
	NU32 identifiantJoueur );

/* Notifier destruction de bloc rempli */
NBOOL Projet_Serveur_TraitementPacket_NotifierDestructionBlocRempli( BServeur*,
	NS32 x,
	NS32 y );

/* Notifier une apparition de bonus */
NBOOL Projet_Serveur_TraitementPacket_NotifierApparitionBonus( BServeur*,
	NSPoint position,
	BListeBonus,
	NU32 dureeBonus,
	NU32 identifiantBonus );

/* Notifier une disparition de bonus */
NBOOL Projet_Serveur_TraitementPacket_NotifierDisparitionBonus( BServeur*,
	NU32 identifiantBonus );

/* Notifier une prise de bonus */
NBOOL Projet_Serveur_TraitementPacket_NotifierPriseBonus( const NClientServeur*,
	NU32 typeBonus );

/* Notifier la mort d'un joueur */
NBOOL Projet_Serveur_TraitementPacket_NotifierMortJoueur( BServeur*,
	NU32 identifiant );

/* Notifier la fin de la partie */
NBOOL Projet_Serveur_TraitementPacket_NotifierFinPartie( BServeur*,
	BTypeFinPartie,
	NU32 identifiant );

// BTYPE_PACKET_CLIENT_SERVEUR_TRANSMET_INFORMATIONS_JOUEUR
NBOOL Projet_Serveur_TraitementPacket_TraiterTransmetInformationsJoueur( BServeur*,
	const NClientServeur*,
	const struct BPacketClientServeurTransmetInformationsJoueur* );

// BTYPE_PACKET_CLIENT_SERVEUR_TRANSMET_CHECKSUM
NBOOL Projet_Serveur_TraitementPacket_TraiterTransmetChecksum( BServeur*,
	const NClientServeur*,
	const struct BPacketClientServeurTransmetChecksum* );

// BTYPE_PACKET_CLIENT_SERVEUR_PING
NBOOL Projet_Serveur_TraitementPacket_TraiterPing( BServeur*,
	const NClientServeur*,
	const struct BPacketClientServeurTransmetChecksum* );

// BTYPE_PACKET_CLIENT_SERVEUR_PONG:
NBOOL Projet_Serveur_TraitementPacket_TraiterPong( BServeur*,
	const NClientServeur*,
	const struct BPacketClientServeurTransmetChecksum* );

// BTYPE_PACKET_CLIENT_SERVEUR_CHANGE_ETAT_PRET
NBOOL Projet_Serveur_TraitementPacket_TraiterChangeEtatPret( BServeur*,
	const NClientServeur*,
	const struct BPacketClientServeurChangeEtatPret* );

// BTYPE_PACKET_CLIENT_SERVEUR_CONFIRME_LANCEMENT
NBOOL Projet_Serveur_TraitementPacket_TraiterConfirmeLancement( BServeur*,
	const NClientServeur*,
	const struct BPacketClientServeurConfirmeLancement* );

// BTYPE_PACKET_CLIENT_SERVEUR_CHANGE_DIRECTION
NBOOL Projet_Serveur_TraitementPacket_TraiterChangeDirection( BServeur*,
	const NClientServeur*,
	const struct BPacketClientServeurChangeDirection* );
// BTYPE_PACKET_CLIENT_SERVEUR_CHANGE_POSITION
NBOOL Projet_Serveur_TraitementPacket_TraiterChangePosition( BServeur*,
	const NClientServeur*,
	const struct BPacketClientServeurChangePosition* );
// BTYPE_PACKET_CLIENT_SERVEUR_POSE_BOMBE
NBOOL Projet_Serveur_TraitementPacket_TraiterPoseBombe( BServeur*,
	const NClientServeur*,
	const struct BPacketClientServeurPoseBombe* );

#endif // !PROJET_SERVEUR_TRAITEMENTPACKET_PROTECT

