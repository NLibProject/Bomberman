#ifndef PROJET_SERVEUR_BETATSERVEUR_PROTECT
#define PROJET_SERVEUR_BETATSERVEUR_PROTECT

// ----------------------------------
// enum Projet::Serveur::BEtatServeur
// ----------------------------------

typedef enum BEtatServeur
{
	// En pause
	BETAT_SERVEUR_PAUSE,

	// Dans la salle d'attente
	BETAT_SERVEUR_SALLE_ATTENTE,

	// En attente de la confirmation des clients du lancement de la partie
	BETAT_SERVEUR_ATTENTE_CONFIRMATION_LANCEMENT,

	// En jeu
	BETAT_SERVEUR_EN_JEU,

	BETATS_SERVEUR
} BEtatServeur;

#endif // !PROJET_SERVEUR_BETATSERVEUR_PROTECT

