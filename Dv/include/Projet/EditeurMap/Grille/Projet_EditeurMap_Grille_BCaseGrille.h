#ifndef PROJET_EDITEURMAP_GRILLE_BCASEGRILLE_PROTECT
#define PROJET_EDITEURMAP_GRILLE_BCASEGRILLE_PROTECT

// ----------------------------------------------
// struct Projet::EditeurMap::Grille::BCaseGrille
// ----------------------------------------------

typedef struct BCaseGrille
{
	// Tileset
	NU32 m_idTileset;

	// Position dans le tileset
	NUPoint m_position;

	// Type de bloc
	BTypeBloc m_typeBloc;
} BCaseGrille;

#endif // !PROJET_EDITEURMAP_GRILLE_BCASEGRILLE_PROTECT

