#ifndef PROJET_EDITEURMAP_COMMANDE_GESTION_PROTECT
#define PROJET_EDITEURMAP_COMMANDE_GESTION_PROTECT

// -----------------------------------------------
// namespace Projet::EditeurMap::Commande::Gestion
// -----------------------------------------------

/* Gerer */
NBOOL Projet_EditeurMap_Commande_Gestion_Gerer( BEditeur* );

#endif // !PROJET_EDITEURMAP_COMMANDE_GESTION_PROTECT

