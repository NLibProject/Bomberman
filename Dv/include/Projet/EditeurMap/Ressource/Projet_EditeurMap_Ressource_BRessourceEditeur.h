#ifndef PROJET_EDITEURMAP_RESSOURCE_BRESSOURCEEDITEUR_PROTECT
#define PROJET_EDITEURMAP_RESSOURCE_BRESSOURCEEDITEUR_PROTECT

// ----------------------------------------------------------
// namespace Projet::EditeurMap::Ressource::BRessourceEditeur
// ----------------------------------------------------------

typedef enum BRessourceEditeur
{
	BRESSOURCE_EDITEUR_SURFACE_OUTIL_PINCEAU,
	BRESSOURCE_EDITEUR_SURFACE_OUTIL_GOMME,

	BRESSOURCES_EDITEUR
} BRessourceEditeur;

/* Obtenir lien vers ressource */
const char *Projet_EditeurMap_Ressource_BRessourceEditeur_ObtenirLienVersRessource( BRessourceEditeur );

#ifdef PROJET_EDITEURMAP_RESSOURCE_BRESSOURCEEDITEUR_INTERNE
static const char BRessourceEditeurTexte[ BRESSOURCES_EDITEUR ][ 64 ] =
{
	"Assets/Editeur/Pinceau.png",
	"Assets/Editeur/Gomme.png"
};
#endif // PROJET_EDITEURMAP_RESSOURCE_BRESSOURCEEDITEUR_INTERNE


#endif // !PROJET_EDITEURMAP_RESSOURCE_BRESSOURCEEDITEUR_PROTECT

