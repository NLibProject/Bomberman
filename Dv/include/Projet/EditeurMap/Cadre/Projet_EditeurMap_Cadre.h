#ifndef PROJET_EDITEURMAP_CADRE_PROTECT
#define PROJET_EDITEURMAP_CADRE_PROTECT

// -----------------------------------
// namespace Projet::EditeurMap::Cadre
// -----------------------------------

// Epaisseur des cadres
static const NU32 BEPAISSEUR_CADRE_EDITEUR_MAP		= 2;

// Dividende pour la repartition entre tileset et colision
static const NU32 BDIVIDENDE_TAILLE_TILESET_EDITEUR = 4;

// enum Projet::EditeurMap::Cadre::BListeCadre
#include "Projet_EditeurMap_Cadre_BListeCadre.h"

// namespace Projet::EditeurMap::Cadre::Carte
#include "Carte/Projet_EditeurMap_Cadre_Carte.h"

// namespace Projet::EditeurMap::Cadre::TypeBloc
#include "TypeBloc/Projet_EditeurMap_Cadre_TypeBloc.h"

// namespace Projet::EditeurMap::Cadre::Tileset
#include "Tileset/Projet_EditeurMap_Cadre_Tileset.h"


#endif // !PROJET_EDITEURMAP_CADRE_PROTECT

