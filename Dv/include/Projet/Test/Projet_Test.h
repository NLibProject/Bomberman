#ifndef PROJET_TEST_PROTECT
#define PROJET_TEST_PROTECT

/*
	Header du projet de test

	@author SOARES Lucas
*/

// ----------------------
// namespace Projet::Test
// ----------------------

/* Test */
NS32 Projet_Test_Main( void );

#endif // !PROJET_TEST_PROTECT

