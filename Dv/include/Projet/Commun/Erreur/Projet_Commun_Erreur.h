#ifndef PROJET_COMMUN_ERREUR_PROTECT
#define PROJET_COMMUN_ERREUR_PROTECT

/*
	Definition du callback pour gestion des erreurs via
	le systeme de notification de NLib

	@author SOARES Lucas
*/

// --------------------------------
// namespace Projet::Commun::Erreur
// --------------------------------

/* Callback notification erreurs */
__CALLBACK void Projet_Commun_Erreur_CallbackNotificationErreur( const NErreur* );

#endif // !PROJET_COMMUN_ERREUR_PROTECT

