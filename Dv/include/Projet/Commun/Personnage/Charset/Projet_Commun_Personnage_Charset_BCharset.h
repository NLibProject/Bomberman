#ifndef PROJET_COMMUN_PERSONNAGE_CHARSET_BCHARSET_PROTECT
#define PROJET_COMMUN_PERSONNAGE_CHARSET_BCHARSET_PROTECT

/*
	Definition d'un charset comprenant l'animation de deplacement
	(Nombre couleur*animatin) et l'animation de mort

	@author SOARES Lucas
*/

// ----------------------------------------------------
// struct Projet::Commun::Personnage::Charset::BCharset
// ----------------------------------------------------

// Nombre de frames
static const NU32 BNOMBRE_FRAMES_CHARSET_DEPLACEMENT		= 8;

// Nombre de position de deplacement
static const NU32 BNOMBRE_POSITIONS_CHARSET_DEPLACEMENT		= 5;

typedef struct
{
	// Animation deplacement [NB_COULEURS]
	NAnimation **m_animationDeplacement;

	// Nombre couleurs
	NU32 m_nombreCouleur;

	// Nombre couleurs
	char **m_nomCouleur;

	// Animation mort
	NAnimation *m_animationMort;
} BCharset;

/* Construire */
__ALLOC BCharset *Projet_Commun_Personnage_Charset_BCharset_Construire( const char *nomCharset,
	const NFenetre* );

/* Detruire */
void Projet_Commun_Personnage_Charset_BCharset_Detruire( BCharset** );

/* Definir position */
void Projet_Commun_Personnage_Charset_BCharset_DefinirPosition( BCharset*,
	NSPoint );

/* Afficher frame */
NBOOL Projet_Commun_Personnage_Charset_BCharset_AfficherFrame( BCharset*,
	NDirection,
	NU32 couleur,
	NU32 zoom );
NBOOL Projet_Commun_Personnage_Charset_BCharset_AfficherFrameMort( BCharset*,
	NU32 zoom,
	const NEtatAnimation* );

/* Obtenir taille frame */
NUPoint Projet_Commun_Personnage_Charset_BCharset_ObtenirTailleFrameDeplacement( const BCharset* );
NUPoint Projet_Commun_Personnage_Charset_BCharset_ObtenirTailleFrameMort( const BCharset* );

/* Obtenir le nombre de couleurs */
NU32 Projet_Commun_Personnage_Charset_BCharset_ObtenirNombreCouleur( const BCharset* );

/* Obtenir animation mort */
const NAnimation *Projet_Commun_Personnage_Charset_BCharset_ObtenirAnimationMort( const BCharset* );

/* Update */
void Projet_Commun_Personnage_Charset_BCharset_Update( BCharset* );

#endif // !PROJET_COMMUN_PERSONNAGE_CHARSET_BCHARSET_PROTECT

