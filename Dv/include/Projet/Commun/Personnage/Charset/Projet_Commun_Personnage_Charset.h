#ifndef PROJET_COMMUN_PERSONNAGE_CHARSET_PROTECT
#define PROJET_COMMUN_PERSONNAGE_CHARSET_PROTECT

/*
	Definitions relatives au charset

	@author SOARES Lucas
*/

// ---------------------------------------------
// namespace Projet::Commun::Personnage::Charset
// ---------------------------------------------

// Repertoire charset
static const char BREPERTOIRE_CHARSET[ 32 ]				= "Assets/Charset";

// Delai entre frame pour les animations
static const NU32 BDELAI_ENTRE_FRAME_ANIMATION_DEPLACEMENT	= 100;
static const NU32 BDELAI_ENTRE_FRAME_ANIMATION_MORT			= 150;

// Taille d'une frame de charset
static const NUPoint BTAILLE_CHARSET =
{
	21,
	30
};

// Base du nom pour les charsets de mort
static const char BBASE_NOM_CHARSET_MORT[ 32 ] = "Mort";

// struct Projet::Commun::Personnage::Charset::BCharset
#include "Projet_Commun_Personnage_Charset_BCharset.h"


#endif // !PROJET_COMMUN_PERSONNAGE_CHARSET_PROTECT

