#ifndef PROJET_COMMUN_PERSONNAGE_PROTECT
#define PROJET_COMMUN_PERSONNAGE_PROTECT

/*
	Definitions relatives au personnage

	@author SOARES Lucas
*/

// ------------------------------------
// namespace Projet::Commun::Personnage
// ------------------------------------

// Taille maximale nom joueur
static const NU32 BTAILLE_MAXIMALE_NOM_JOUEUR			= 16;

// Correction position personnage (pixels)
static const NSPoint BCORRECTION_POSITION_JOUEUR		= { 0, 3 };

// Nombre de bombe initial
static const NU32 BNOMBRE_BOMBE_INITIAL_JOUEUR			= 1;

// Nombre de bombe maximum
static const NU32 BNOMBRE_BOMBE_MAXIMUM_JOUEUR			= 5;

// Puissance minimale initiale de tous les personnages
static const NU32 BPUISSANCE_INITIALE_JOUEUR			= 2;

// Puissance minimale
static const NU32 BPUISSANCE_MINIMALE_JOUEUR			= 1;

// Puissance maximale
static const NU32 BPUISSANCE_MAXIMALE_JOUEUR			= 12;

// namespace Projet::Commun::Personnage::Charset
#include "Charset/Projet_Commun_Personnage_Charset.h"

// struct Projet::Commun::Personnage::Charset::BCharset
#include "Projet_Commun_Personnage_BPersonnage.h"

// namespace Projet::Commun::Personnage::Chargement
#include "Chargement/Projet_Commun_Personnage_Chargement.h"

// namespace Projet::Commun::Personnage::Affichage
#include "Affichage/Projet_Commun_Personnage_Affichage.h"

#endif // !PROJET_COMMUN_PERSONNAGE_PROTECT

