#ifndef PROJET_COMMUN_RESEAU_PROTECT
#define PROJET_COMMUN_RESEAU_PROTECT

/*
	Declarations du projet relatives au reseau
	(packet/client/serveur)

	@author SOARES Lucas
*/

// --------------------------------
// namespace Projet::Commun::Reseau
// --------------------------------

// namespace Projet::Commun::Reseau::Client
#include "Client/Projet_Commun_Reseau_Client.h"

// namespace Projet::Commun::Reseau::Serveur
#include "Serveur/Projet_Commun_Reseau_Serveur.h"

// namespace Projet::Commun::Reseau::Packet
#include "Packet/Projet_Commun_Reseau_Packet.h"

// Verifier
nassert( ( BNOMBRE_MAXIMUM_JOUEUR >= BNOMBRE_MINIMUM_JOUEUR ),
	"Projet::Commun::Reseau::BNOMBRE_MAXIMUM_JOUEUR" );

#endif // !PROJET_COMMUN_RESEAU_PROTECT

