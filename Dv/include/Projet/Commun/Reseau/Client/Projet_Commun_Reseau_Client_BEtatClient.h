#ifndef PROJET_COMMUN_RESEAU_BETATCLIENT_PROTECT
#define PROJET_COMMUN_RESEAU_BETATCLIENT_PROTECT

// --------------------------------------------------
// struct Projet::Commun::Reseau::Client::BEtatClient
// --------------------------------------------------

typedef struct BEtatClient
{
	/* Client */
	// Est client courant? [Le client qui joue ce personnage?]
	NBOOL m_estClientCourant;

	/* Serveur */
	// Donnees supplementaires [Si serveur: NClientServeur* associe, Si client: NULL]
	void *m_donneeSupplementaire;

	/* Commun */
		/* Etat */
			// Est client verifie? [Checksum ok?]
			NBOOL m_estChecksumVerifie;

			// Tick connexion
			NU32 m_tickConnexion;

			// Est pret?
			BEtatPret m_estPret;
	
			// A confirme le lancement?
			NBOOL m_estConfirmeLancement;

			// Est en train de jouer l'animation de mort?
			NBOOL m_estAnimationMortEnCours;

			// Etat animation mort
			NEtatAnimation *m_etatAnimationMort;
		/* Proprietes */
			// Nom
			char *m_nom;

			// Identifiant
			NU32 m_identifiant;

			// Charset
			NU32 m_charset;

			// Couleur
			NU32 m_couleurCharset;
		/* Etat dans monde */
			// Est en vie
			NBOOL m_estEnVie;

			// Position
			NSPoint m_position;

			// Direction
			NDirection m_direction;

			// Deplacement
			BEtatDeplacementClient *m_etatDeplacement;

			// Puissance
			NU32 m_puissance;

			// Nombre de bombes posees au maximum
			NU32 m_nombreBombeMaximum;

			// Nombre de bombes posees actuellement
			NU32 m_nombreBombePosee;
} BEtatClient;

/* Construire */
__ALLOC BEtatClient *Projet_Commun_Reseau_Client_BEtatClient_Construire( NU32 identifiant,
	NBOOL estClientCourant,
	void *donneeSupplementaire,
	const void *ressource );
__ALLOC BEtatClient *Projet_Commun_Reseau_Client_BEtatClient_Construire2( const BEtatClient* );

/* Detruire */
void Projet_Commun_Reseau_Client_BEtatClient_Detruire( BEtatClient** );

/* Obtenir nom */
const char *Projet_Commun_Reseau_Client_BEtatClient_ObtenirNom( const BEtatClient* );

/* Obtenir identifiant */
NU32 Projet_Commun_Reseau_Client_BEtatClient_ObtenirIdentifiant( const BEtatClient* );

/* Obtenir charset */
NU32 Projet_Commun_Reseau_Client_BEtatClient_ObtenirCharset( const BEtatClient* );

/* Obtenir couleur charset */
NU32 Projet_Commun_Reseau_Client_BEtatClient_ObtenirCouleurCharset( const BEtatClient* );

/* Obtenir position */
const NSPoint *Projet_Commun_Reseau_Client_BEtatClient_ObtenirPosition( const BEtatClient* );

/* Obtenir etat deplacement */
const BEtatDeplacementClient *Projet_Commun_Reseau_Client_BEtatClient_ObtenirEtatDeplacement( const BEtatClient* );

/* Obtenir etat animation */
const NEtatAnimation *Projet_Commun_Reseau_Client_BEtatClient_ObtenirEtatAnimationMort( const BEtatClient* );

/* Obtenir nombre bombes posees */
NU32 Projet_Commun_Reseau_Client_BEtatClient_ObtenirNombreBombePosee( const BEtatClient* );

/* Obtenir nombre maximum bombes */
NU32 Projet_Commun_Reseau_Client_BEtatClient_ObtenirNombreMaximumBombe( const BEtatClient* );

/* Est confirme le lancement? */
NBOOL Projet_Commun_Reseau_Client_BEtatClient_EstConfirmeLancement( const BEtatClient* );

/* Est pret? */
BEtatPret Projet_Commun_Reseau_Client_BEtatClient_EstPret( const BEtatClient* );

/* Est client verifie? */
NBOOL Projet_Commun_Reseau_Client_BEtatClient_EstVerifie( const BEtatClient* );

/* On doit afficher ce client? */
NBOOL Projet_Commun_Reseau_Client_BEtatClient_EstDoitAfficher( const BEtatClient* );

/* Verifier etat client */
NBOOL Projet_Commun_Reseau_Client_BEtatClient_EstClientEtatCorrect( const BEtatClient* );

/* Obtenir tick connexion */
NU32 Projet_Commun_Reseau_Client_BEtatClient_ObtenirTickConnexion( const BEtatClient* );

/* Confirmer le lancement */
void Projet_Commun_Reseau_Client_BEtatClient_ConfirmerLancement( BEtatClient* );

/* Definir nom */
NBOOL Projet_Commun_Reseau_Client_BEtatClient_DefinirNom( BEtatClient*,
	const char* );

/* Definir charset */
void Projet_Commun_Reseau_Client_BEtatClient_DefinirCharset( BEtatClient*,
	NU32 );

/* Definir couleur charset */
void Projet_Commun_Reseau_Client_BEtatClient_DefinirCouleurCharset( BEtatClient*,
	NU32 );

/* Definir etat verification */
void Projet_Commun_Reseau_Client_BEtatClient_DefinirEstVerifie( BEtatClient*,
	NBOOL );

/* Definir est pret */
void Projet_Commun_Reseau_Client_BEtatClient_DefinirEstPret( BEtatClient*,
	BEtatPret );

/* Definir position */
void Projet_Commun_Reseau_Client_BEtatClient_DefinirPosition( BEtatClient*,
	NSPoint );

/* Definir direction */
void Projet_Commun_Reseau_Client_BEtatClient_DefinirDirection( BEtatClient*,
	NDirection );

/* Definir puissance */
void Projet_Commun_Reseau_Client_BEtatClient_DefinirPuissance( BEtatClient*,
	NU32 );

/* Definir est en vie */
void Projet_Commun_Reseau_Client_BEtatClient_DefinirEstEnVie( BEtatClient*,
	NBOOL );

/* Est en vie? */
NBOOL Projet_Commun_Reseau_Client_BEtatClient_EstEnVie( const BEtatClient* );

/* Obtenir direction */
NDirection Projet_Commun_Reseau_Client_BEtatClient_ObtenirDirection( const BEtatClient* );

/* Obtenir donnee supplementaire */
void *Projet_Commun_Reseau_Client_BEtatClient_ObtenirDonneeSupplementaire( const BEtatClient* );

/* Definir donnee supplementaire */
void Projet_Commun_Reseau_Client_BEtatClient_DefinirDonneeSupplementaire( BEtatClient*,
	void *data );

/* Obtenir puissance */
NU32 Projet_Commun_Reseau_Client_BEtatClient_ObtenirPuissance( const BEtatClient* );

/* Incrementer nombre bombes maximum */
void Projet_Commun_Reseau_Client_BEtatClient_IncrementerNombreBombeMaximum( BEtatClient* );

/* Incrementer/decrementer nombre bombes posees */
void Projet_Commun_Reseau_Client_BEtatClient_IncrementerNombreBombePosee( BEtatClient* );
void Projet_Commun_Reseau_Client_BEtatClient_DecrementerNombreBombePosee( BEtatClient* );

/* Lancer l'animation de mort */
void Projet_Commun_Reseau_Client_BEtatClient_LancerAnimationMort( BEtatClient* );

/* Update */
void Projet_Commun_Reseau_Client_BEtatClient_Update( BEtatClient* );

#endif // !PROJET_COMMUN_RESEAU_BETATCLIENT_PROTECT


