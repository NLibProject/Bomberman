#ifndef PROJET_COMMUN_RESEAU_CLIENT_MESSAGE_BPOLICEMESSAGECLIENT_PROTECT
#define PROJET_COMMUN_RESEAU_CLIENT_MESSAGE_BPOLICEMESSAGECLIENT_PROTECT

// ------------------------------------------------------------------
// enum Projet::Commun::Reseau::Client::Message::BPoliceMessageClient
// ------------------------------------------------------------------

typedef enum BPoliceMessageClient
{
	BPOLICE_MESSAGE_CLIENT_COMPTE_REBOURS,
	BPOLICE_MESSAGE_CLIENT_DEBUT_PARTIE,
	BPOLICE_MESSAGE_CLIENT_MORT_JOUEUR,
	BPOLICE_MESSAGE_CLIENT_FIN_PARTIE,

	BPOLICES_MESSAGE_CLIENT
} BPoliceMessageClient;

/* Obtenir police (BListePolice) */
BListePolice Projet_Commun_Reseau_Client_Message_BPoliceMessageClient_ObtenirPolice( BPoliceMessageClient );

/* Obtenir taille */
NU32 Projet_Commun_Reseau_Client_Message_BPoliceMessageClient_ObtenirTaille( BPoliceMessageClient );

#ifdef PROJET_COMMUN_RESEAU_CLIENT_MESSAGE_BPOLICEMESSAGECLIENT_INTERNE
static const BListePolice BPoliceMessageClientPolice[ BPOLICES_MESSAGE_CLIENT ] =
{
	// BPOLICE_MESSAGE_CLIENT_COMPTE_REBOURS
	BLISTE_POLICE_BUBBLEGUM,
	
	// BPOLICE_MESSAGE_CLIENT_DEBUT_PARTIE
	BLISTE_POLICE_BUBBLEGUM,

	// BPOLICE_MESSAGE_CLIENT_MORT_JOUEUR
	BLISTE_POLICE_TREBUCHET,

	// BPOLICE_MESSAGE_CLIENT_FIN_PARTIE
	BLISTE_POLICE_LAZY_SUNDAY
};

static const NU32 BPoliceMessageClientTaille[ BPOLICES_MESSAGE_CLIENT ] =
{
	// BPOLICE_MESSAGE_CLIENT_COMPTE_REBOURS
	240,
	
	// BPOLICE_MESSAGE_CLIENT_DEBUT_PARTIE
	120,

	// BPOLICE_MESSAGE_CLIENT_MORT_JOUEUR
	40,

	// BPOLICE_MESSAGE_CLIENT_FIN_PARTIE
	50
};
#endif // PROJET_COMMUN_RESEAU_CLIENT_MESSAGE_BPOLICEMESSAGECLIENT_INTERNE

// Verifier taille enum
nassert( sizeof( BPoliceMessageClient ) == sizeof( NU32 ),
	"Projet::Commun::Reseau::Client::Message::BPoliceMessageClient" );

#endif // !PROJET_COMMUN_RESEAU_CLIENT_MESSAGE_BPOLICEMESSAGECLIENT_PROTECT

