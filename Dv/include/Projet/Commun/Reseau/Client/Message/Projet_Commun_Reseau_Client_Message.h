#ifndef PROJET_COMMUN_RESEAU_CLIENT_MESSAGE_PROTECT
#define PROJET_COMMUN_RESEAU_CLIENT_MESSAGE_PROTECT

// -------------------------------------------------
// namespace Projet::Commun::Reseau::Client::Message
// -------------------------------------------------

// enum Projet::Commun::Reseau::Client::Message::BPositionAffichageMessageClient
#include "Projet_Commun_Reseau_Client_Message_BPositionAffichageMessageClient.h"

// enum Projet::Commun::Reseau::Client::Message::BPoliceMessageClient
#include "Projet_Commun_Reseau_Client_Message_BPoliceMessageClient.h"

#endif // !PROJET_COMMUN_RESEAU_CLIENT_MESSAGE_PROTECT

