#ifndef PROJET_COMMUN_RESEAU_CLIENT_PROTECT
#define PROJET_COMMUN_RESEAU_CLIENT_PROTECT

/*
	Declarations relatives au client

	@author SOARES Lucas
*/

// ----------------------------------------
// namespace Projet::Commun::Reseau::Client
// ----------------------------------------

// Temps entre update client pour deplacement
static const NU32 BTEMPS_ENTRE_UPDATE_DEPLACEMENT_CLIENT	= 5;

// enum Projet::Commun::Reseau::Client::BEtatPret
#include "Projet_Commun_Reseau_Client_BEtatPret.h"

// struct Projet::Commun::Reseau::Client::BEtatDeplacementClient
#include "Projet_Commun_Reseau_Client_BEtatDeplacementClient.h"

// struct Projet::Commun::Reseau::Client::BEtatClient
#include "Projet_Commun_Reseau_Client_BEtatClient.h"

// namespace Projet::Commun::Reseau::Client::Cache
#include "Cache/Projet_Commun_Reseau_Client_Cache.h"

// namespace Projet::Commun::Reseau::Client::Message
#include "Message/Projet_Commun_Reseau_Client_Message.h"

#endif // !PROJET_COMMUN_RESEAU_CLIENT_PROTECT

