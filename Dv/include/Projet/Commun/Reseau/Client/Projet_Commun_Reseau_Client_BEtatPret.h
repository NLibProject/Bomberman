#ifndef PROJET_COMMUN_RESEAU_CLIENT_BETATPRET_PROTECT
#define PROJET_COMMUN_RESEAU_CLIENT_BETATPRET_PROTECT

// ----------------------------------------------
// enum Projet::Commun::Reseau::Client::BEtatPret
// ----------------------------------------------

typedef enum BEtatPret
{
	BETAT_PRET_PAS_PRET,
	BETAT_PRET_PRET,

	BETATS_PRET
} BEtatPret;

/* Obtenir nom etat */
const char *Projet_Commun_Reseau_Client_BEtatPret_ObtenirNomEtat( BEtatPret );

#ifdef PROJET_COMMUN_RESEAU_CLIENT_BETATPRET_INTERNE
static const char BEtatPretTexte[ BETATS_PRET ][ 32 ] =
{
	"Pas pret",
	"Pret"
};
#endif // PROJET_COMMUN_RESEAU_CLIENT_BETATPRET_INTERNE

#endif // !PROJET_COMMUN_RESEAU_CLIENT_BETATPRET_PROTECT

