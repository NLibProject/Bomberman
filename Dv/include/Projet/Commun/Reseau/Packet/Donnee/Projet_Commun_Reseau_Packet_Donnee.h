#ifndef PROJET_COMMUN_RESEAU_PACKET_DONNEE_PROTECT
#define PROJET_COMMUN_RESEAU_PACKET_DONNEE_PROTECT

// ------------------------------------------------
// namespace Projet::Commun::Reseau::Packet::Donnee
// ------------------------------------------------

/* Liberer une structure de donnees de packet */
// Libere le conteneur
void Projet_Commun_Reseau_Packet_Donnee_Liberer( void *data,
	BTypePacket );
// Ne libere pas le conteneur
void Projet_Commun_Reseau_Packet_Donnee_Liberer2( void *data,
	BTypePacket );

// --------------------
// CLIENT VERS SERVEUR:
// --------------------
// struct BPacketClientServeurTransmetInformationsJoueur [BTYPE_PACKET_CLIENT_SERVEUR_TRANSMET_INFORMATIONS_JOUEUR]
struct BPacketClientServeurTransmetInformationsJoueur
{
	NU32 m_charset;
	NU32 m_couleurCharset;
	__MUSTBEFREED char *m_nom;
};

// struct BPacketClientServeurTransmetChecksum [BTYPE_PACKET_CLIENT_SERVEUR_TRANSMET_CHECKSUM]
struct BPacketClientServeurTransmetChecksum
{
	NU32 m_checksum;
};

// struct BPacketClientServeurPing [BTYPE_PACKET_CLIENT_SERVEUR_PING]
struct BPacketClientServeurPing
{
	NU32 m_identifiant;
};

// struct BPacketClientServeurPong [BTYPE_PACKET_CLIENT_SERVEUR_PONG]
struct BPacketClientServeurPong
{
	NU32 m_identifiant;
};

// struct BPacketClientServeurChangeEtatPret [BTYPE_PACKET_CLIENT_SERVEUR_CHANGE_ETAT_PRET]
struct BPacketClientServeurChangeEtatPret
{
	NU32 m_identifiant;
};

// struct BPacketClientServeurConfirmeLancement [BTYPE_PACKET_CLIENT_SERVEUR_CONFIRME_LANCEMENT]
struct BPacketClientServeurConfirmeLancement
{
	NU32 m_identifiant;
};

// struct BPacketClientServeurChangeDirection [BTYPE_PACKET_CLIENT_SERVEUR_CHANGE_DIRECTION]
struct BPacketClientServeurChangeDirection
{
	// Direction
	NDirection m_direction;
};

// struct BPacketClientServeurChangePosition [BTYPE_PACKET_CLIENT_SERVEUR_CHANGE_POSITION]
struct BPacketClientServeurChangePosition
{
	// Direction
	NDirection m_direction;
};

// struct BPacketClientServeurPoseBombe [BTYPE_PACKET_CLIENT_SERVEUR_POSE_BOMBE]
struct BPacketClientServeurPoseBombe
{
	// Identifiant
	NU32 m_identifiant;
};

// --------------------
// SERVEUR VERS CLIENT:
// --------------------
// struct BPacketServeurClientConnexionTransmetIdentifiant [BTYPE_PACKET_SERVEUR_CLIENT_CONNEXION_TRANSMET_IDENTIFIANT]
struct BPacketServeurClientConnexionTransmetIdentifiant
{
	NU32 m_identifiant;
};

// struct BPacketServeurClientReponseInformationsJoueur [BTYPE_PACKET_SERVEUR_CLIENT_REPONSE_INFORMATIONS_JOUEUR]
struct BPacketServeurClientReponseInformationsJoueur
{
	NU32 m_charset;
	NU32 m_couleurCharset;
	__MUSTBEFREED char *m_nom;
	NU32 m_identifiant;
};

// struct BPacketServeurClientDeconnexionJoueur [BTYPE_PACKET_SERVEUR_CLIENT_DECONNEXION_JOUEUR]
struct BPacketServeurClientDeconnexionJoueur
{
	NU32 m_identifiant;
};

// struct BPacketServeurClientChecksumIncorrect [BTYPE_PACKET_SERVEUR_CLIENT_CHECKSUM_INCORRECT]
struct BPacketServeurClientChecksumIncorrect
{
	NU32 m_identifiant;
};

// struct BPacketServeurClientPing [BTYPE_PACKET_SERVEUR_CLIENT_PING]
struct BPacketServeurClientPing
{
	NU32 m_identifiant;
};

// struct BPacketServeurClientPong [BTYPE_PACKET_SERVEUR_CLIENT_PONG]
struct BPacketServeurClientPong
{
	NU32 m_identifiant;
};

// struct BPacketServeurClientDiffuseEtatPret [BTYPE_PACKET_SERVEUR_CLIENT_DIFFUSE_ETAT_PRET]
struct BPacketServeurClientDiffuseEtatPret
{
	NU32 m_identifiant;
	BEtatPret m_etat;
};

// struct BPacketServeurClientDiffuseChangementCarte [BTYPE_PACKET_SERVEUR_CLIENT_DIFFUSE_CHANGEMENT_CARTE]
struct BPacketServeurClientDiffuseChangementCarte
{
	NU32 m_identifiant;
};

// struct BPacketServeurClientDiffuseLancement [BTYPE_PACKET_SERVEUR_CLIENT_DIFFUSE_LANCEMENT]
struct BPacketServeurClientDiffuseLancement
{
	// Joueurs
	NU32 m_nombreJoueur;
	NU32 *m_identifiantJoueur;
	NSPoint *m_positionJoueur;

	// Carte
	NU32 m_identifiantCarte;
	NUPoint m_tailleCarte;
	NBOOL **m_estCaseRemplie;
};

// struct BPacketServeurClientDiffuseMessageAfficher [BTYPE_PACKET_SERVEUR_CLIENT_DIFFUSE_MESSAGE_AFFICHER]
struct BPacketServeurClientDiffuseMessageAfficher
{
	// Message
	NU32 m_tailleMessage;
	char *m_message;

	// Duree d'affichage
	NU32 m_dureeAffichage;

	// Couleur
	NCouleur m_couleur;

	// Police
	BPoliceMessageClient m_police;

	// Position
	BPositionAffichageMessageClient m_position;

	// Est doit afficher cadre?
	NBOOL m_estDoitAfficherCadre;

	// Couleur cadre
	NCouleur m_couleurCadre;
};

// struct BPacketServeurClientDiffuseDebutPartie [BTYPE_PACKET_SERVEUR_CLIENT_DIFFUSE_DEBUT_PARTIE]
struct BPacketServeurClientDiffuseDebutPartie
{
	// Identifiant
	NU32 m_identifiant;
};

// struct BPacketServeurClientDiffuseChangementDirection [BTYPE_PACKET_SERVEUR_CLIENT_DIFFUSE_CHANGEMENT_DIRECTION]
struct BPacketServeurClientDiffuseChangementDirection
{
	// Identifiant
	NU32 m_identifiant;

	// Direction
	NDirection m_direction;
};

// struct BPacketServeurClientDiffuseChangementPosition [BTYPE_PACKET_SERVEUR_CLIENT_DIFFUSE_CHANGEMENT_POSITION]
struct BPacketServeurClientDiffuseChangementPosition
{
	// Identifiant
	NU32 m_identifiant;

	// Direction
	NDirection m_direction;

	// Nouvelle position
	NSPoint m_nouvellePosition;
};

// struct BPacketServeurClientDiffusePoseBombe [BTYPE_PACKET_SERVEUR_CLIENT_DIFFUSE_POSE_BOMBE]
struct BPacketServeurClientDiffusePoseBombe
{
	// Identifiant joueur
	NU32 m_identifiantJoueur;

	// Identifiant bombe
	NU32 m_identifiantBombe;

	// Position
	NSPoint m_position;
};

// struct BPacketServeurClientRefusePoseBombe [BTYPE_PACKET_SERVEUR_CLIENT_REFUSE_POSE_BOMBE]
struct BPacketServeurClientRefusePoseBombe
{
	// Identifiant joueur
	NU32 m_identifiant;
};

// struct BPacketServeurClientBombeExplose [BTYPE_PACKET_SERVEUR_CLIENT_BOMBE_EXPLOSE]
struct BPacketServeurClientBombeExplose
{
	// Identifiant bombe
	NU32 m_identifiant;

	// Puissance
	NU32 m_puissance;

	// Identifiant joueur
	NU32 m_identifiantJoueur;
};

// struct BPacketServeurClientBlocRempliDetruit [BTYPE_PACKET_SERVEUR_CLIENT_BLOC_REMPLI_DETRUIT]
struct BPacketServeurClientBlocRempliDetruit
{
	// Position
	NSPoint m_position;
};

// struct BPacketServeurClientDiffuseApparitionBonus [BTYPE_PACKET_SERVEUR_CLIENT_DIFFUSE_APPARITION_BONUS]
struct BPacketServeurClientDiffuseApparitionBonus
{
	// Position
	NSPoint m_position;

	// Type (BListeBonus)
	NU32 m_type;

	// Duree
	NU32 m_duree;

	// Identifiant bonus
	NU32 m_identifiant;
};

// struct BPacketServeurClientDiffuseDisparitionBonus [BTYPE_PACKET_SERVEUR_CLIENT_DIFFUSE_DISPARITION_BONUS]
struct BPacketServeurClientDiffuseDisparitionBonus
{
	// Identifiant bonus
	NU32 m_identifiant;
};

// struct BPacketServeurClientDistribueBonus [BTYPE_PACKET_SERVEUR_CLIENT_DISTRIBUE_BONUS]
struct BPacketServeurClientDistribueBonus
{
	// Type bonus (BListeBonus)
	NU32 m_typeBonus;

	// Identifiant joueur
	NU32 m_identifiant;
};

// struct BPacketServeurClientDiffuseMortJoueur [BTYPE_PACKET_SERVEUR_CLIENT_DIFFUSE_MORT_JOUEUR]
struct BPacketServeurClientDiffuseMortJoueur
{
	// Identifiant joueur
	NU32 m_identifiantJoueur;
};

// struct BPacketServeurClientAnnonceFinPartie [BTYPE_PACKET_SERVEUR_CLIENT_ANNONCE_FIN_PARTIE]
struct BPacketServeurClientAnnonceFinPartie
{
	// Type de fin de partie (BTypeFinPartie)
	NU32 m_type;

	// Identifiant du vainqueur
	NU32 m_identifiant;
};

#endif // !PROJET_COMMUN_RESEAU_PACKET_DONNEE_PROTECT

