#ifndef PROJET_COMMUN_RESEAU_PACKET_BTYPEPACKET
#define PROJET_COMMUN_RESEAU_PACKET_BTYPEPACKET

/*
	Les types de packet existants, et leurs details sont
	detailles ici.

	@author SOARES Lucas
*/

// ------------------------------------------------
// enum Projet::Commun::Reseau::Packet::BTypePacket
// ------------------------------------------------

// enum Projet::Commun::Reseau::Packet::BTypePacket
typedef enum BTypePacket
{
	// Packet nul
	BTYPE_PACKET_AUCUN,

	// -------------------
	// CLIENT vers SERVEUR
	// -------------------
	/*
		[Deuxieme etape de communication, le client transmet sa requete de connexion au serveur]

		Le client demande l'accession a la salle d'attente du serveur.

		Donnees transmises:
			// Le charset du personnage
			NU32 identifiantCharsetPersonnage;
			
			// La couleur du personnage parmis les couleurs disponibles
			// dans le charset
			NU32 identifiantCouleurPersonnage;

			// Taille nom
			NU32 tailleNomJoueur;

			// Le nom choisi par le joueur
			char *nomJoueur;
	*/
	BTYPE_PACKET_CLIENT_SERVEUR_TRANSMET_INFORMATIONS_JOUEUR,	

	/*
		[Le client donne son checksum de ressources]

		Le client donne le checksum de ses ressources pour etre compare
		a celui du serveur.

		Donnees transmises:
			// Checksum
			NU32 checksum;
	*/
	BTYPE_PACKET_CLIENT_SERVEUR_TRANSMET_CHECKSUM,
	
	/*
		[Le client demande le ping]

		Le client demande un ping au serveur

		Donnees transmises:
			// Identifiant
			NU32 identifiant;
	*/
	BTYPE_PACKET_CLIENT_SERVEUR_PING,

	/*
		[Le client repond a la demande de ping]

		Le client repond a la demande de ping emise par le serveur

		Donnees transmises:
			// Identifiant
			NU32 identifiant;
	*/
	BTYPE_PACKET_CLIENT_SERVEUR_PONG,

	/*
		[Le client informe un changement d'etat pret]

		Le client inverse son etat pret/pas pret dans le lobby

		Donnees transmises:
			// Identifiant
			NU32 identifiant;
	*/
	BTYPE_PACKET_CLIENT_SERVEUR_CHANGE_ETAT_PRET,

	/*
		[Le client annonce au serveur que le lancement a
		ete correctement effectue]

		Le client est pret a ce que la partie demarre.

		Donnees transmises:
			// Identifiant
			NU32 identifiant;
	*/
	BTYPE_PACKET_CLIENT_SERVEUR_CONFIRME_LANCEMENT,

	/*
		[Le joueur change de direction]

		Le joueur change la direction de son personnage.

		Donnees transmises:
			// Direction
			NDirection direction;
	*/
	BTYPE_PACKET_CLIENT_SERVEUR_CHANGE_DIRECTION,

	/*
		[Le joueur se deplace]

		Le joueur deplace son personnage

		Donnees transmises:
			// Direction
			NDirection direction;
	*/
	BTYPE_PACKET_CLIENT_SERVEUR_CHANGE_POSITION,

	/*
		[Le joueur pose une bombe]

		Le joueur appuie sur espace pour poser une bombe

		Donnees transmises
			// Identifiant
			NU32 identifiant;
	*/
	BTYPE_PACKET_CLIENT_SERVEUR_POSE_BOMBE,

	// -------------------
	// SERVEUR vers CLIENT
	// -------------------
	/*
		[Premiere etape de la communication]

		Le serveur envoie un identifiant unique au client venant de 
		se connecter.

		Donnees transmises:
			// L'identifiant attribue au client par le serveur
			NU32 identifiantJoueur;
	*/
	BTYPE_PACKET_SERVEUR_CLIENT_CONNEXION_TRANSMET_IDENTIFIANT,

	/*
		[Serveur donne ce qu'il autorise comme charset/couleur/nom
		au client]

		Le serveur valide en envoyant la meme info/modifie ce qu'il
		faut dans les informations que le client lui transmet.

		Donnees transmises:
			// Taille nom
			NU32 tailleNom;

			// Nom
			char *nom;

			// Charset
			NU32 charset;

			// Couleur charset
			NU32 couleurCharset
	*/
	BTYPE_PACKET_SERVEUR_CLIENT_REPONSE_INFORMATIONS_JOUEUR,

	/*
		[Serveur informe qu'un client se deconnecte]

		Le serveur a deconnecte un joueur et informe les
		autres joueurs

		Donnees transmises:
			// Identifiant
			NU32 identifiant;
	*/
	BTYPE_PACKET_SERVEUR_CLIENT_DECONNEXION_JOUEUR,

	/*
		[Serveur informe un client que son checksum est incorrect]

		Le serveur informe un client que son CRC ressource est incorrect

		Donnees transmises:
			// Identifiant
			NU32 identifiant;
	*/
	BTYPE_PACKET_SERVEUR_CLIENT_CHECKSUM_INCORRECT,

	/*
		[Serveur fait une demande de ping]

		Le serveur fait une demande de ping au client

		Donnees transmises:
			// Identifiant
			NU32 identifiant;
	*/
	BTYPE_PACKET_SERVEUR_CLIENT_PING,

	/*
		[Serveur repond au ping]

		Le serveur repond a la demande de ping du client

		Donnees transmises:
			// Identifiant
			NU32 identifiant;
	*/
	BTYPE_PACKET_SERVEUR_CLIENT_PONG,

	/*
		[Le serveur diffuse un changement d'etat pret/pas pret]

		Changement d'etat pret/pas pret dans le lobby diffuse

		Donnees transmises:
			// Identifiant
			NU32 identifiant;

			// Etat
			BEtatPret etat;
	*/
	BTYPE_PACKET_SERVEUR_CLIENT_DIFFUSE_ETAT_PRET,

	/*
		[Le serveur informe d'un changement de carte]

		L'h�te de la partie change de carte et en informe
		le(s) client(s)

		Donnees transmises:
			// Identifiant carte
			NU32 identifiant;
	*/
	BTYPE_PACKET_SERVEUR_CLIENT_DIFFUSE_CHANGEMENT_CARTE,

	/*
		[Le serveur informe du lancement de la partie]

		Le serveur donne le signal du debut de partie,
		et attribue une position a chaque joueur

		Donnees transmises:
			// Identifiant carte
			NU32 identifiantCarte;

			// Nombre joueurs
			NU32 nombreJoueurs;

			// Identifiants joueurs
			NU32 identifiantsJoueurs[ nombreJoueurs ];

			// Position joueur (o� l'indice correspond a l'identifiant dans identifiantsJoueurs)
			NSPoint position[ nombreJoueurs ];

			// Taille carte
			NUPoint tailleCarte;

			// Est case remplie?
			NBOOL estCaseRemplie[ tailleCarte.x ][ tailleCarte.y ];
	*/
	BTYPE_PACKET_SERVEUR_CLIENT_DIFFUSE_LANCEMENT,

	/*
		[Le serveur demande l'affichage d'un message sur
		l'ecran du client]

		Le serveur demande l'affichage d'un message comme
		un compte a rebours a l'ecran

		Donnees transmises:
			// Taille message
			NU32 tailleMessage;

			// Message
			char *message;

			// Duree d'affichage
			NU32 dureeAffichage;
	*/
	BTYPE_PACKET_SERVEUR_CLIENT_DIFFUSE_MESSAGE_AFFICHER,

	/*
		[Le serveur donne le top au joueur pour qu'ils puissent
		commencer a jouer]

		Debut de la partie

		Donnees transmises:
			// Identifiant
			NU32 m_identifiant;
	*/
	BTYPE_PACKET_SERVEUR_CLIENT_DIFFUSE_DEBUT_PARTIE,

	/*
		[Le serveur diffuse un changement de direction]

		Changement de direction d'un joueur

		Donnees transmises:
			// Identifiant
			NU32 m_identifiant;

			// Direction
			NDirection m_direction;
	*/
	BTYPE_PACKET_SERVEUR_CLIENT_DIFFUSE_CHANGEMENT_DIRECTION,

	/*
		[Le serveur diffuse un changement de position]

		Changement de position d'un joueur

		Donnees transmises:
			// Identifiant
			NU32 m_identifiant;

			// Direction
			NDirection m_direction;

			// Nouvelle position
			NSPoint m_nouvellePosition;
	*/
	BTYPE_PACKET_SERVEUR_CLIENT_DIFFUSE_CHANGEMENT_POSITION,

	/*
		[Le serveur diffuse une pose de bombe]

		Pose de bombe

		Donnees transmises:
			// Identifiant joueur
			NU32 m_identifiantJoueur;

			// Identifiant bombe
			NU32 m_identifiantBombe;

			// Position
			NSPoint m_position;
	*/
	BTYPE_PACKET_SERVEUR_CLIENT_DIFFUSE_POSE_BOMBE,

	/*
		[Le serveur refuse une pose de bombe a un client]

		Refus d'une pose de bombe

		Donnees transmises:
			// Identifiant joueur
			NU32 m_identifiant;
	*/
	BTYPE_PACKET_SERVEUR_CLIENT_REFUSE_POSE_BOMBE,

	/*
		[Le serveur previent qu'un bombe explose]

		Une bombe explose

		Donnees transmises:
			// Identifiant bombe
			NU32 m_identifiantBombe;

			// Puissance
			NU32 m_puissance;
	*/
	BTYPE_PACKET_SERVEUR_CLIENT_BOMBE_EXPLOSE,

	/*
		[Le serveur previent qu'un bloc rempli a ete detruit]

		Destruction d'un bloc rempli

		Donnees transmises:
			// Position du bloc
			NSPoint m_position;
	*/
	BTYPE_PACKET_SERVEUR_CLIENT_BLOC_REMPLI_DETRUIT,

	/*
		[Le serveur annonce l'apparition d'un bonus]

		Un bonus apparait

		Donnees transmises:
			// Position
			NSPoint m_position;

			// Type de bonus
			BListeBonus m_type;

			// Duree
			NU32 m_dureeBonus;

			// Identifiant bonus
			NU32 m_identifiantBonus;
	*/
	BTYPE_PACKET_SERVEUR_CLIENT_DIFFUSE_APPARITION_BONUS,

	/*
		[Le serveur annonce la disparition d'un bonus]

		Disparition d'un bonus

		Donnees transmises:
			// Identifiant bonus
			NU32 m_identifiantBonus;
	*/
	BTYPE_PACKET_SERVEUR_CLIENT_DIFFUSE_DISPARITION_BONUS,

	/*
		[Le serveur signale a un client qu'il a eut un bonus]

		Un joueur marche sur un bonus

		Donnees transmises:
			// Type bonus (BListeBonus)
			NU32 m_typeBonus;
	*/
	BTYPE_PACKET_SERVEUR_CLIENT_DISTRIBUE_BONUS,

	/*
		[Le serveur annonce la mort d'un joueur]

		Un joueur meurt

		Donnees transmises:
			// Identifiant joueur
			NU32 m_identifiant;
	*/
	BTYPE_PACKET_SERVEUR_CLIENT_DIFFUSE_MORT_JOUEUR,

	/*
		[Le serveur informe que la partie est finie]

		Fin de la partie

		Donnees transmises:
			// Type de fin de partie
			BTypeFinPartie m_type;

			// Identifiant du joueur gagnant
			NU32 m_identifiant;
	*/
	BTYPE_PACKET_SERVEUR_CLIENT_ANNONCE_FIN_PARTIE,

	// Fin
	BTYPES_PACKET
} BTypePacket;

// Verifier
nassert( sizeof( BTypePacket ) == sizeof( NU32 ),
	"Projet::Commun::Reseau::Packet::BTypePacket" );

#endif // !PROJET_COMMUN_RESEAU_PACKET_BTYPEPACKET

