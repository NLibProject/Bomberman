#ifndef PROJET_COMMUN_RESEAU_PACKET_FORGE_PROTECT
#define PROJET_COMMUN_RESEAU_PACKET_FORGE_PROTECT

// -----------------------------------------------
// namespace Projet::Commun::Reseau::Packet::Forge
// -----------------------------------------------

/* Creer un packet */
__ALLOC NPacket *Projet_Commun_Reseau_Packet_Forge_CreerPacket( BTypePacket );

/* Forge */
// Client vers serveur:
// BTYPE_PACKET_CLIENT_SERVEUR_TRANSMET_INFORMATIONS_JOUEUR
NBOOL Projet_Commun_Reseau_Packet_Forge_ForgerPacketClientServeurTransmetInformationsJoueur( NPacket*,
	const struct BPacketClientServeurTransmetInformationsJoueur* );
// BTYPE_PACKET_CLIENT_SERVEUR_TRANSMET_CHECKSUM
NBOOL Projet_Commun_Reseau_Packet_Forge_ForgerPacketClientServeurTransmetChecksum( NPacket*,
	const struct BPacketClientServeurTransmetChecksum* );
// BTYPE_PACKET_CLIENT_SERVEUR_PING
NBOOL Projet_Commun_Reseau_Packet_Forge_ForgerPacketClientServeurPing( NPacket*,
	const struct BPacketClientServeurPing* );
// BTYPE_PACKET_CLIENT_SERVEUR_PONG
NBOOL Projet_Commun_Reseau_Packet_Forge_ForgerPacketClientServeurPong( NPacket*,
	const struct BPacketClientServeurPong* );
// BTYPE_PACKET_CLIENT_SERVEUR_CHANGE_ETAT_PRET
NBOOL Projet_Commun_Reseau_Packet_Forge_ForgerPacketClientServeurChangeEtatPret( NPacket*,
	const struct BPacketClientServeurChangeEtatPret* );
// BTYPE_PACKET_CLIENT_SERVEUR_CONFIRME_LANCEMENT
NBOOL Projet_Commun_Reseau_Packet_Forge_ForgerPacketClientServeurConfirmeLancement( NPacket*,
	const struct BPacketClientServeurConfirmeLancement* );
// BTYPE_PACKET_CLIENT_SERVEUR_CHANGE_DIRECTION
NBOOL Projet_Commun_Reseau_Packet_Forge_ForgerPacketClientServeurChangeDirection( NPacket*,
	const struct BPacketClientServeurChangeDirection* );
// BTYPE_PACKET_CLIENT_SERVEUR_CHANGE_POSITION
NBOOL Projet_Commun_Reseau_Packet_Forge_ForgerPacketClientServeurChangePosition( NPacket*,
	const struct BPacketClientServeurChangePosition* );
// BTYPE_PACKET_CLIENT_SERVEUR_POSE_BOMBE
NBOOL Projet_Commun_Reseau_Packet_Forge_ForgerPacketClientServeurPoseBombe( NPacket*,
	const struct BPacketClientServeurPoseBombe* );

// Serveur vers client:
// BTYPE_PACKET_SERVEUR_CLIENT_CONNEXION_TRANSMET_IDENTIFIANT
NBOOL Projet_Commun_Reseau_Packet_Forge_ForgerPacketServeurClientConnexionTransmetIdentifiant( NPacket*,
	const struct BPacketServeurClientConnexionTransmetIdentifiant* );
// BTYPE_PACKET_SERVEUR_CLIENT_REPONSE_INFORMATIONS_JOUEUR
NBOOL Projet_Commun_Reseau_Packet_Forge_ForgerPacketServeurClientReponseInformationsJoueur( NPacket*,
	const struct BPacketServeurClientReponseInformationsJoueur* );
// BTYPE_PACKET_SERVEUR_CLIENT_DECONNEXION_JOUEUR
NBOOL Projet_Commun_Reseau_Packet_Forge_ForgerPacketServeurClientDeconnexionJoueur( NPacket*,
	const struct BPacketServeurClientDeconnexionJoueur* );
// BTYPE_PACKET_SERVEUR_CLIENT_CHECKSUM_INCORRECT
NBOOL Projet_Commun_Reseau_Packet_Forge_ForgerPacketServeurClientChecksumIncorrect( NPacket*,
	const struct BPacketServeurClientChecksumIncorrect* );
// BTYPE_PACKET_SERVEUR_CLIENT_PING
NBOOL Projet_Commun_Reseau_Packet_Forge_ForgerPacketServeurClientPing( NPacket*,
	const struct BPacketServeurClientPing* );
// BTYPE_PACKET_SERVEUR_CLIENT_PONG
NBOOL Projet_Commun_Reseau_Packet_Forge_ForgerPacketServeurClientPong( NPacket*,
	const struct BPacketServeurClientPong* );
// BTYPE_PACKET_SERVEUR_CLIENT_DIFFUSE_ETAT_PRET
NBOOL Projet_Commun_Reseau_Packet_Forge_ForgerPacketServeurClientDiffuseEtatPret( NPacket*,
	const struct BPacketServeurClientDiffuseEtatPret* );
// BTYPE_PACKET_SERVEUR_CLIENT_DIFFUSE_CHANGEMENT_CARTE
NBOOL Projet_Commun_Reseau_Packet_Forge_ForgerPacketServeurClientDiffuseChangementCarte( NPacket*,
	const struct BPacketServeurClientDiffuseChangementCarte* );
// BTYPE_PACKET_SERVEUR_CLIENT_DIFFUSE_LANCEMENT
NBOOL Projet_Commun_Reseau_Packet_Forge_ForgerPacketServeurClientDiffuseLancement( NPacket*,
	const struct BPacketServeurClientDiffuseLancement* );
// BTYPE_PACKET_SERVEUR_CLIENT_DIFFUSE_MESSAGE_AFFICHER
NBOOL Projet_Commun_Reseau_Packet_Forge_ForgerPacketServeurClientDiffuseMessageAfficher( NPacket*,
	const struct BPacketServeurClientDiffuseMessageAfficher* );
// BTYPE_PACKET_SERVEUR_CLIENT_DIFFUSE_DEBUT_PARTIE
NBOOL Projet_Commun_Reseau_Packet_Forge_ForgerPacketServeurClientDiffuseDebutPartie( NPacket*,
	const struct BPacketServeurClientDiffuseDebutPartie* );
// BTYPE_PACKET_SERVEUR_CLIENT_DIFFUSE_CHANGEMENT_DIRECTION
NBOOL Projet_Commun_Reseau_Packet_Forge_ForgerPacketServeurClientDiffuseChangementDirection( NPacket*,
	const struct BPacketServeurClientDiffuseChangementDirection* );
// BTYPE_PACKET_SERVEUR_CLIENT_DIFFUSE_CHANGEMENT_POSITION
NBOOL Projet_Commun_Reseau_Packet_Forge_ForgerPacketServeurClientDiffuseChangementPosition( NPacket*,
	const struct BPacketServeurClientDiffuseChangementPosition* );
// BTYPE_PACKET_SERVEUR_CLIENT_DIFFUSE_POSE_BOMBE
NBOOL Projet_Commun_Reseau_Packet_Forge_ForgerPacketServeurClientDiffusePoseBombe( NPacket*,
	const struct BPacketServeurClientDiffusePoseBombe* );
// BTYPE_PACKET_SERVEUR_CLIENT_REFUSE_POSE_BOMBE
NBOOL Projet_Commun_Reseau_Packet_Forge_ForgerPacketServeurClientRefusePoseBombe( NPacket*,
	const struct BPacketServeurClientRefusePoseBombe* );
// BTYPE_PACKET_SERVEUR_CLIENT_BOMBE_EXPLOSE
NBOOL Projet_Commun_Reseau_Packet_Forge_ForgerPacketServeurClientBombeExplose( NPacket*,
	const struct BPacketServeurClientBombeExplose* );
// BTYPE_PACKET_SERVEUR_CLIENT_BLOC_REMPLI_DETRUIT
NBOOL Projet_Commun_Reseau_Packet_Forge_ForgerPacketServeurClientBlocRempliDetruit( NPacket*,
	const struct BPacketServeurClientBlocRempliDetruit* );
// BTYPE_PACKET_SERVEUR_CLIENT_DIFFUSE_APPARITION_BONUS
NBOOL Projet_Commun_Reseau_Packet_Forge_ForgerPacketServeurClientDiffuseApparitionBonus( NPacket*,
	const struct BPacketServeurClientDiffuseApparitionBonus* );
// BTYPE_PACKET_SERVEUR_CLIENT_DIFFUSE_DISPARITION_BONUS
NBOOL Projet_Commun_Reseau_Packet_Forge_ForgerPacketServeurClientDiffuseDisparitionBonus( NPacket*,
	const struct BPacketServeurClientDiffuseDisparitionBonus* );
// BTYPE_PACKET_SERVEUR_CLIENT_DISTRIBUE_BONUS
NBOOL Projet_Commun_Reseau_Packet_Forge_ForgerPacketServeurClientDistribueBonus( NPacket*,
	const struct BPacketServeurClientDistribueBonus* );
// BTYPE_PACKET_SERVEUR_CLIENT_DIFFUSE_MORT_JOUEUR
NBOOL Projet_Commun_Reseau_Packet_Forge_ForgerPacketServeurClientDiffuseMortJoueur( NPacket*,
	const struct BPacketServeurClientDiffuseMortJoueur* );
// BTYPE_PACKET_SERVEUR_CLIENT_ANNONCE_FIN_PARTIE
NBOOL Projet_Commun_Reseau_Packet_Forge_ForgerPacketServeurClientAnnonceFinPartie( NPacket*,
	const struct BPacketServeurClientAnnonceFinPartie* );

#endif // !PROJET_COMMUN_RESEAU_PACKET_FORGE_PROTECT

