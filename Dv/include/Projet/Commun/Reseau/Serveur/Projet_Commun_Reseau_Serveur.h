#ifndef PROJET_COMMUN_RESEAU_SERVEUR_PROTECT
#define PROJET_COMMUN_RESEAU_SERVEUR_PROTECT

/*
	Definitions relative a la partie serveur du projet

	@author SOARES Lucas
*/

// -----------------------------------------
// namespace Projet::Commun::Reseau::Serveur
// -----------------------------------------

// 5 secondes avant d'etre kicke si le client ne transmet pas son checksum ressources
static const NU32 BDELAI_KICK_CLIENT_NON_VERIFIE = 5000;

// 3 secondes avant d'ignorer l'envoi du packet de kick
static const NU32 BTIMEOUT_ENVOI_PACKET_KICK_SERVEUR = 3000;
#endif // !PROJET_COMMUN_RESEAU_SERVEUR_PROTECT

