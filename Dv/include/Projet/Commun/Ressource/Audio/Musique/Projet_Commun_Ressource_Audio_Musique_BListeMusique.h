#ifndef PROJET_COMMUN_RESSOURCE_AUDIO_MUSIQUE_BLISTEMUSIQUE_PROTECT
#define PROJET_COMMUN_RESSOURCE_AUDIO_MUSIQUE_BLISTEMUSIQUE_PROTECT

// -------------------------------------------------------------
// enum Projet::Commun::Ressource::Audio::Musique::BListeMusique
// -------------------------------------------------------------

typedef enum BListeMusique
{
	// Animal crossing
	BLISTE_MUSIQUE_AC_RAIN,

	// Bomberman
	BLISTE_MUSIQUE_BM_5_ZONE_1,
	BLISTE_MUSIQUE_BM_5_ZONE_5B,
	BLISTE_MUSIQUE_BM_5_ZONE_5D,
	BLISTE_MUSIQUE_BM_BATTLE,
	BLISTE_MUSIQUE_BM_BATTLEMENU,
	BLISTE_MUSIQUE_BM_BATTLEREMIX,
	BLISTE_MUSIQUE_BM_BIGBOSS,
	BLISTE_MUSIQUE_BM_BOSSBATTLE,
	BLISTE_MUSIQUE_BM_BOSSBATTLE2,
	BLISTE_MUSIQUE_BM_BOSSBATTLE3,
	BLISTE_MUSIQUE_BM_CREDITS,
	BLISTE_MUSIQUE_BM_DRAW,
	BLISTE_MUSIQUE_BM_GAMEOVER,
	BLISTE_MUSIQUE_BM_LEVEL,
	BLISTE_MUSIQUE_BM_LEVEL1_3,
	BLISTE_MUSIQUE_BM_LEVEL2,
	BLISTE_MUSIQUE_BM_LEVEL3,
	BLISTE_MUSIQUE_BM_LEVEL3_2,
	BLISTE_MUSIQUE_BM_LEVEL4,
	BLISTE_MUSIQUE_BM_LEVEL6,
	BLISTE_MUSIQUE_BM_LEVELKANGAROOREMIX,
	BLISTE_MUSIQUE_BM_LEVELREMIX,
	BLISTE_MUSIQUE_BM_LEVELTHEME,
	BLISTE_MUSIQUE_BM_MAP,
	BLISTE_MUSIQUE_BM_MAP2,
	BLISTE_MUSIQUE_BM_MAPARRANGED,
	BLISTE_MUSIQUE_BM_MULTIPLAYER,
	BLISTE_MUSIQUE_BM_MULTIPLAYERBATTLEMENU,
	BLISTE_MUSIQUE_BM_PASSWORD,
	BLISTE_MUSIQUE_BM_STAGE_CLEAR,
	BLISTE_MUSIQUE_BM_STAGE_START,
	BLISTE_MUSIQUE_BM_TITLE_SCREEN,
	BLISTE_MUSIQUE_BM_TITLE_SCREEN2,
	BLISTE_MUSIQUE_BM_VICTORY,
	BLISTE_MUSIQUE_BM_ZONE1,
	BLISTE_MUSIQUE_BM_ZONE5B,
	BLISTE_MUSIQUE_BM_ZONE5D_MAGNETS,
	BLISTE_MUSIQUE_BM_5_TITLESCREEN,

	BLISTE_MUSIQUES,

	BLISTE_MUSIQUE_AUCUNE = (NU32)0xFFFFFFFF
} BListeMusique;

/* Obtenir lien */
const char *Projet_Commun_Ressource_Audio_Musique_BListeMusique_ObtenirLien( BListeMusique );

#ifdef PROJET_COMMUN_RESSOURCE_AUDIO_MUSIQUE_BLISTEMUSIQUE_INTERNE
static const char BListeMusiqueTexte[ BLISTE_MUSIQUES ][ 64 ] =
{
	"Assets/Audio/BGM/AC_Rain.mid",

	"Assets/Audio/BGM/BM_5_Zone_1.mid",
	"Assets/Audio/BGM/BM_5_Zone_5B.mid",
	"Assets/Audio/BGM/BM_5_Zone_5D.mid",
	"Assets/Audio/BGM/BM_Battle.mid",
	"Assets/Audio/BGM/BM_BattleMenu.mid",
	"Assets/Audio/BGM/BM_BattleRemix.mid",
	"Assets/Audio/BGM/BM_BigBoss.mid",
	"Assets/Audio/BGM/BM_BossBattle.mid",
	"Assets/Audio/BGM/BM_BossBattle2.mid",
	"Assets/Audio/BGM/BM_BossBattle3.mid",
	"Assets/Audio/BGM/BM_Credits.mid",
	"Assets/Audio/BGM/BM_Draw.mid",
	"Assets/Audio/BGM/BM_GameOver.mid",
	"Assets/Audio/BGM/BM_Level.mid",
	"Assets/Audio/BGM/BM_Level1_3.mid",
	"Assets/Audio/BGM/BM_Level2.mid",
	"Assets/Audio/BGM/BM_Level3.mid",
	"Assets/Audio/BGM/BM_Level3_2.mid",
	"Assets/Audio/BGM/BM_Level4.mid",
	"Assets/Audio/BGM/BM_Level6.mid",
	"Assets/Audio/BGM/BM_LevelKangarooRemix.mid",
	"Assets/Audio/BGM/BM_LevelRemix.mid",
	"Assets/Audio/BGM/BM_LevelTheme.mid",
	"Assets/Audio/BGM/BM_Map.mid",
	"Assets/Audio/BGM/BM_Map2.mid",
	"Assets/Audio/BGM/BM_MapArranged.mid",
	"Assets/Audio/BGM/BM_Multiplayer.mid",
	"Assets/Audio/BGM/BM_MultiplayerBattleMenu.mid",
	"Assets/Audio/BGM/BM_Password.mid",
	"Assets/Audio/BGM/BM_Stage_Clear.mid",
	"Assets/Audio/BGM/BM_Stage_Start.mid",
	"Assets/Audio/BGM/BM_Title_Screen.mid",
	"Assets/Audio/BGM/BM_Title_Screen2.mid",
	"Assets/Audio/BGM/BM_Victory.mid",
	"Assets/Audio/BGM/BM_Zone1.mid",
	"Assets/Audio/BGM/BM_Zone5B.mid",
	"Assets/Audio/BGM/BM_Zone5D_Magnets.mid",
	"Assets/Audio/BGM/BM_5_TitleScreen.mid"
};
#endif // PROJET_COMMUN_RESSOURCE_AUDIO_MUSIQUE_BLISTEMUSIQUE_INTERNE

// Verifier la taille
nassert( sizeof( BListeMusique ) == sizeof( NU32 ),
	"Projet::Comun::Ressource::Audio::Musique::BListeMusique" );

#endif // !PROJET_COMMUN_RESSOURCE_AUDIO_MUSIQUE_BLISTEMUSIQUE_PROTECT

