#ifndef PROJET_COMMUN_RESSOURCE_AUDIO_PROTECT
#define PROJET_COMMUN_RESSOURCE_AUDIO_PROTECT

// ------------------------------------------
// namespace Projet::Commun::Ressource::Audio
// ------------------------------------------

// namespace Projet::Commun::Ressource::Audio::Musique
#include "Musique/Projet_Commun_Ressource_Audio_Musique.h"

// namespace Projet::Commun::Ressource::Audio::Effet
#include "Effet/Projet_Commun_Ressource_Audio_Effet.h"

#endif // !PROJET_COMMUN_RESSOURCE_AUDIO_PROTECT

