#ifndef PROJET_COMMUN_RESSOURCE_AUDIO_EFFET_BEFFETSONORE_PROTECT
#define PROJET_COMMUN_RESSOURCE_AUDIO_EFFET_BEFFETSONORE_PROTECT

// ------------------------------------------------------------
// struct Projet::Commun::Ressource::Audio::Effet::BEffetSonore
// ------------------------------------------------------------

typedef struct BEffetSonore
{
	// Sons
	NSon *m_son[ BLISTE_EFFETS_SONORE ];
} BEffetSonore;

/* Construire */
__ALLOC BEffetSonore *Projet_Commun_Ressource_Audio_Effet_BEffetSonore_Construire( void );

/* Detruire */
void Projet_Commun_Ressource_Audio_Effet_BEffetSonore_Detruire( BEffetSonore** );

/* Lire son */
void Projet_Commun_Ressource_Audio_Effet_BEffetSonore_Lire( const BEffetSonore*,
	BListeEffetSonore );

#endif // !PROJET_COMMUN_RESSOURCE_AUDIO_EFFET_BEFFETSONORE_PROTECT

