#ifndef PROJET_COMMUN_RESSOURCE_BRESSOURCE_PROTECT
#define PROJET_COMMUN_RESSOURCE_BRESSOURCE_PROTECT

/*
	Ressource du projet (gestion externe des animations via
	Projet_Commun_Ressource_BRessource_Update)

	@author SOARES Lucas
*/

// --------------------------------------------
// struct Projet::Commun::Ressource::BRessource
// --------------------------------------------

typedef struct BRessource
{
	// Personnage
	BPersonnage **m_personnage;
	NU32 m_nombrePersonnage;

	// Tilesets
	BTileset **m_tileset;
	NU32 m_nombreTileset;

	// Animations
	BEnsembleAnimation *m_animation;

	// Audio
	BMusique *m_musique;
	BEffetSonore *m_effetSonore;

	// Ic�nes bonus
	BRessourceBonus *m_bonus;

	// Checksum ressources
	NU32 m_checksum;
} BRessource;

/* Construire */
__ALLOC BRessource *Projet_Commun_Ressource_BRessource_Construire( const NFenetre* );

/* Detruire */
void Projet_Commun_Ressource_BRessource_Detruire( BRessource** );

/* Obtenir le nombre de personnages */
NU32 Projet_Commun_Ressource_BRessource_ObtenirNombrePersonnage( const BRessource* );

/* Obtenir un personnage */
BPersonnage *Projet_Commun_Ressource_BRessource_ObtenirPersonnage( const BRessource*,
	NU32 personnage );
BPersonnage *Projet_Commun_Ressource_BRessource_ObtenirPersonnage2( const BRessource*,
	const char *nom );

/* Obtenir l'ensemble des personnages */
BPersonnage **Projet_Commun_Ressource_BRessource_ObtenirEnsemblePersonnage( const BRessource* );

/* Obtenir le nombre de tileset */
NU32 Projet_Commun_Ressource_BRessource_ObtenirNombreTileset( const BRessource* );

/* Obtenir les tilesets */
const BTileset **Projet_Commun_Ressource_BRessource_ObtenirEnsembleTileset( const BRessource* );

/* Obtenir un tileset */
BTileset *Projet_Commun_Ressource_BRessource_ObtenirTileset( const BRessource*,
	NU32 id );

/* Obtenir checksum */
NU32 Projet_Commun_Ressource_BRessource_ObtenirChecksum( const BRessource* );

/* Obtenir ensemble animations */
const BEnsembleAnimation *Projet_Commun_Ressource_BRessource_ObtenirEnsembleAnimation( const BRessource* );

/* Obtenir ic�ne bonus */
const NSurface *Projet_Commun_Ressource_BRessource_ObtenirIconeBonus( const BRessource*,
	BListeBonus );

/* Obtenir bonus */
const BRessourceBonus *Projet_Commun_Ressource_BRessource_ObtenirBonus( const BRessource* );

/* Lire musique */
NBOOL Projet_Commun_Ressource_BRessource_LireMusique( const BRessource*,
	BListeMusique );

/* Est lecture musique en cours? */
NBOOL Projet_Commun_Ressource_BRessource_EstLectureMusique( const BRessource* );

/* Arreter musique */
NBOOL Projet_Commun_Ressource_BRessource_ArreterMusique( const BRessource* );

/* Lire effet sonore */
void Projet_Commun_Ressource_BRessource_LireEffetSonore( const BRessource*,
	BListeEffetSonore );

/* Update */
void Projet_Commun_Ressource_BRessource_Update( BRessource* );


#endif // !PROJET_COMMUN_RESSOURCE_BRESSOURCE_PROTECT

