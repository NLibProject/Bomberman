#ifndef PROJET_COMMUN_RESSOURCE_ANIMATION_BLISTEANIMATION_PROTECT
#define PROJET_COMMUN_RESSOURCE_ANIMATION_BLISTEANIMATION_PROTECT

// ----------------------------------------------------------
// enum Projet::Commun::Ressource::Animation::BListeAnimation
// ----------------------------------------------------------

typedef enum BListeAnimation
{
	BLISTE_ANIMATION_BOMBE,

	BLISTE_ANIMATIONS
} BListeAnimation;

/* Obtenir lien vers animation */
const char *Projet_Commun_Ressource_Animation_BListeAnimation_ObtenirLien( BListeAnimation );

/* Obtenir taille frame animation */
const NUPoint *Projet_Commun_Ressource_Animation_BListeAnimation_ObtenirTailleFrame( BListeAnimation );

/* Obtenir delai entre frame animation */
NU32 Projet_Commun_Ressource_Animation_BListeAnimation_ObtenirDelaiFrame( BListeAnimation );

#ifdef PROJET_COMMUN_RESSOURCE_ANIMATION_BLISTEANIMATION_INTERNE
static const char BListeAnimationTexte[ BLISTE_ANIMATIONS ][ 32 ] =
{
	"Assets/Animation/Bombe.png"
};

static const NUPoint BListeAnimationTaille[ BLISTE_ANIMATIONS ] =
{
	{ 16, 16 }
};

static const NU32 BListeAnimationDelaiFrame[ BLISTE_ANIMATIONS ] =
{
	150
};
#endif // !PROJET_COMMUN_RESSOURCE_ANIMATION_BLISTEANIMATION_INTERNE

#endif // !PROJET_COMMUN_RESSOURCE_ANIMATION_BLISTEANIMATION_PROTECT

