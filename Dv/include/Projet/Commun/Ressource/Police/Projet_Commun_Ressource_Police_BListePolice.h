#ifndef PROJET_COMMUN_RESSOURCE_POLICE_BLISTEPOLICE_PROTECT
#define PROJET_COMMUN_RESSOURCE_POLICE_BLISTEPOLICE_PROTECT

// ----------------------------------------------------
// enum Projet::Commun::Ressource::Police::BListePolice
// ----------------------------------------------------

typedef enum BListePolice
{
	BLISTE_POLICE_TREBUCHET,
	BLISTE_POLICE_BUBBLEGUM,
	BLISTE_POLICE_LAZY_SUNDAY,

	BLISTE_POLICES
} BListePolice;

/* Obtenir lien police */
const char *Projet_Commun_Ressource_Police_BListePolice_ObtenirLien( BListePolice );

#ifdef PROJET_COMMUN_RESSOURCE_POLICE_BLISTEPOLICE_INTERNE
static const char BListePoliceTexte[ BLISTE_POLICES ][ 64 ] =
{
	"Assets/Police/trebuc.ttf",
	"Assets/Police/Bubblegum.ttf",
	"Assets/Police/LazySunday.ttf"
};
#endif // !PROJET_COMMUN_RESSOURCE_POLICE_BLISTEPOLICE_INTERNE

#endif // !PROJET_COMMUN_RESSOURCE_POLICE_BLISTEPOLICE_PROTECT

