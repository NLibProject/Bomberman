#ifndef PROJET_COMMUN_MONDE_BMONDE_PROTECT
#define PROJET_COMMUN_MONDE_BMONDE_PROTECT

// ------------------------------------
// struct Projet::Commun::Monde::BMonde
// ------------------------------------

typedef struct BMonde
{
	// Cache clients
	BCacheClient *m_cacheClient;

	// Configuration
	const BConfigurationMonde *m_configuration;

	// Carte
	const BCarte *m_carte;

	// Etat carte
	BEtatCarte *m_etatCarte;
} BMonde;

/* Construire */
__ALLOC BMonde *Projet_Commun_Monde_BMonde_Construire( BCacheClient *clients,
	const BConfigurationMonde *configuration,
	const BEnsembleCarte *ensembleCarte,
	const BRessource* );

/* Detruire */
void Projet_Commun_Monde_BMonde_Detruire( BMonde** );

/* Obtenir carte */
const BCarte *Projet_Commun_Monde_BMonde_ObtenirCarte( const BMonde* );

/* Obtenir etat carte */
const BEtatCarte *Projet_Commun_Monde_BMonde_ObtenirEtatCarte( const BMonde* );

/* Obtenir cases etat carte */
const BCaseEtatCarte **Projet_Commun_Monde_BMonde_ObtenirCaseEtatCarte( const BMonde* );

/* Generer blocs remplis */
NBOOL Projet_Commun_Monde_BMonde_GenererBloc( BMonde* );
NBOOL Projet_Commun_Monde_BMonde_GenererBloc2( BMonde*,
	const NBOOL **estCaseRemplie );

/* Update */
void Projet_Commun_Monde_BMonde_Update( BMonde* );

/* Exploser bombe */
NBOOL Projet_Commun_Monde_BMonde_ExploserBombe( BMonde*,
	NU32 identifiantBombe,
	NU32 puissance,
	NU32 identifiantJoueur );

/* Supprimer bloc rempli */
NBOOL Projet_Commun_Monde_BMonde_SupprimerBlocRempli( BMonde*,
	NSPoint );

#endif // !PROJET_COMMUN_MONDE_BMONDE_PROTECT

