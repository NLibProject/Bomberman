#ifndef PROJET_COMMUN_MONDE_FINPARTIE_BTYPEFINPARTIE_PROTECT
#define PROJET_COMMUN_MONDE_FINPARTIE_BTYPEFINPARTIE_PROTECT

// -----------------------------------------------------
// enum Projet::Commun::Monde::FinPartie::BTypeFinPartie
// -----------------------------------------------------

typedef enum BTypeFinPartie
{
	BTYPE_FIN_PARTIE_VICTOIRE,
	BTYPE_FIN_PARTIE_EGALITE,

	BTYPES_FIN_PARTIE
} BTypeFinPartie;

// S'assurer que la taille soit correcte
nassert( sizeof( BTypeFinPartie ) == sizeof( NU32 ),
	"Projet::Commun::Monde::FinPartie::BTypeFinPartie" );

#endif // !PROJET_COMMUN_MONDE_FINPARTIE_BTYPEFINPARTIE_PROTECT

