#ifndef PROJET_COMMUN_CARTE_BCOUCHECARTE_PROTECT
#define PROJET_COMMUN_CARTE_BCOUCHECARTE_PROTECT

// ----------------------------------------
// enum Projet::Commun::Carte::BCoucheCarte
// ----------------------------------------

typedef enum BCoucheCarte
{
	BCOUCHE_CARTE_UN,
	BCOUCHE_CARTE_DEUX,

	BCOUCHES_CARTE
} BCoucheCarte;


#endif // !PROJET_COMMUN_CARTE_BCOUCHECARTE_PROTECT

