#ifndef PROJET_COMMUN_CARTE_PROTECT
#define PROJET_COMMUN_CARTE_PROTECT

/*
	Definition des elements necessaires a la declaration
	d'une carte

	@author SOARES Lucas
*/

// -------------------------------
// namespace Projet::Commun::Carte
// -------------------------------

// Taille conseillee au maximum pour une carte
static const NUPoint BTAILLE_MAXIMUM_CARTE = { 25, 25 };

// namespace Projet::Commun::Carte::Tileset
#include "Tileset/Projet_Commun_Carte_Tileset.h"

// namespace Projet::Commun::Carte::Bonus
#include "Bonus/Projet_Commun_Carte_Bonus.h"

// enum Projet::Commun::Carte::BCoucheCarte
#include "Projet_Commun_Carte_BCoucheCarte.h"

// namespace Projet::Commun::Carte::Bloc
#include "Bloc/Projet_Commun_Carte_Bloc.h"

// struct Projet::Commun::Carte::BCarte
#include "Projet_Commun_Carte_BCarte.h"

// namespace Projet::Commun::Carte::Affichage
#include "Affichage/Projet_Commun_Carte_Affichage.h"

// namespace Projet::Commun::Carte::Ensemble
#include "Ensemble/Projet_Commun_Carte_Ensemble.h"

// namespace Projet::Commun::Carte::Etat
#include "Etat/Projet_Commun_Carte_Etat.h"

#endif // !PROJET_COMMUN_CARTE_PROTECT

