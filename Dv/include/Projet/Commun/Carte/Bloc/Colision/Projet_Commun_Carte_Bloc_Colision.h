#ifndef PROJET_COMMUN_CARTE_BLOC_COLISION_PROTECT
#define PROJET_COMMUN_CARTE_BLOC_COLISION_PROTECT

/*
	Toutes les definitions relatives aux colisions

	@author SOARES Lucas
*/

// -----------------------------------------------
// namespace Projet::Commun::Carte::Bloc::Colision
// -----------------------------------------------

// enum Projet::Commun::Carte::Bloc::Colision::BColision
#include "Projet_Commun_Carte_Bloc_Colision_BColision.h"

#endif // !PROJET_COMMUN_CARTE_BLOC_COLISION_PROTECT

