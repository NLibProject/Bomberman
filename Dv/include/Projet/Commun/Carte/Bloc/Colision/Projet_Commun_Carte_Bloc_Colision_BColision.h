#ifndef PROJET_COMMUN_CARTE_BLOC_COLISION_BCOLISION_PROTECT
#define PROJET_COMMUN_CARTE_BLOC_COLISION_BCOLISION_PROTECT

/*
	Liste des colisions sur un bloc

	@author SOARES Lucas
*/

// -----------------------------------------------------
// enum Projet::Commun::Carte::Bloc::Colision::BColision
// -----------------------------------------------------

typedef enum BColision
{
	BCOLISION_AUCUNE,
	BCOLISION_COLISION,

	BCOLISIONS
} BColision;

// Verifier la taille
nassert( sizeof( BColision ) == sizeof( NU32 ),
	"Projet_Commun_Carte_Bloc_Colision_BColision" );

#endif // !PROJET_COMMUN_CARTE_BLOC_COLISION_BCOLISION_PROTECT

