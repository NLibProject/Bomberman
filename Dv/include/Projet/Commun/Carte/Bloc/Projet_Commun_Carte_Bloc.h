#ifndef PROJET_COMMUN_CARTE_BLOC_PROTECT
#define PROJET_COMMUN_CARTE_BLOC_PROTECT

/*
	Tous les elements relatifs et necessaires a la declaration
	d'un bloc

	@author SOARES Lucas
*/

// -------------------------------------
// namespace Projet::Commun::Carte::Bloc
// -------------------------------------

// namespace Projet::Commun::Carte::Bloc::Colision
#include "Colision/Projet_Commun_Carte_Bloc_Colision.h"

// enum Projet::Commun::Carte::Bloc::BTypeBloc
#include "Projet_Commun_Carte_Bloc_BTypeBloc.h"

// struct Projet::Commun::Carte::Bloc::BBloc
#include "Projet_Commun_Carte_Bloc_BBloc.h"

#endif // !PROJET_COMMUN_CARTE_BLOC_PROTECT

