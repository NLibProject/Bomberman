#ifndef PROJET_COMMUN_CARTE_AFFICHAGE_PROTECT
#define PROJET_COMMUN_CARTE_AFFICHAGE_PROTECT

// ------------------------------------------
// namespace Projet::Commun::Carte::Affichage
// ------------------------------------------

// Nombre de blocs supplementaires affiches pour la bordure
static const NU32 BAFFICHAGE_NOMBRE_CASE_SUPPLEMENTAIRE_BLOC_BORDURE	=	2;

/* Obtenir la taille a afficher */
NUPoint Projet_Commun_Carte_Affichage_ObtenirTailleAAfficher( const BCarte*,
	NU32 zoom );

/* Afficher bordure */
void Projet_Commun_Carte_Affichage_AfficherBordure( const BCarte*,
	const BTileset **tileset,
	NSPoint positionInitiale,
	NU32 zoom,
	const NFenetre* );

/* Afficher */
void Projet_Commun_Carte_Affichage_Afficher( const BCarte*,
	NU32 couche,
	const BTileset **tileset,
	NSPoint positionInitiale,
	NU32 zoom );

/* Afficher remplissage */
void Projet_Commun_Carte_Affichage_AfficherRemplissageComplet( const BCarte*,
	const BTileset **tileset,
	NSPoint positionInitiale,
	NU32 zoom );
void Projet_Commun_Carte_Affichage_AfficherRemplissageSchema( const BCarte*,
	const BTileset **tileset,
	NSPoint positionInitiale,
	NU32 zoom,
	const void* );

/* Afficher bombes */
void Projet_Commun_Carte_Affichage_AfficherBombe( const BCarte*,
	const void*,
	NSPoint positionInitiale,
	NU32 zoom,
	const void* );

/* Afficher explosions */
void Projet_Commun_Carte_Affichage_AfficherExplosion( const BCarte*,
	NSPoint positionInitiale,
	NU32 zoom,
	const void* );

/* Afficher bonus */
void Projet_Commun_Carte_Affichage_AfficherBonus( const BCarte*,
	const void*,
	NSPoint positionInitiale,
	NU32 zoom,
	const void* );

#endif // !PROJET_COMMUN_CARTE_AFFICHAGE_PROTECT

