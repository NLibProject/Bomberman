#ifndef PROJET_COMMUN_CARTE_BONUS_BLISTEBONUS_PROTECT
#define PROJET_COMMUN_CARTE_BONUS_BLISTEBONUS_PROTECT

// ----------------------------------------------
// enum Projet::Commun::Carte::Bonus::BListeBonus
// ----------------------------------------------

typedef enum BListeBonus
{
	BLISTE_BONUS_AUGMENTE_PUISSANCE,
	BLISTE_BONUS_DIMINUE_PUISSANCE,
	BLISTE_BONUS_PUISSANCE_MAXIMALE,
	BLISTE_BONUS_AUGMENTE_NOMBRE_BOMBE,

	BLISTE_BONUS,

	BLISTE_BONUS_ERREUR = NERREUR
} BListeBonus;

/* Obtenir ensemble probabilite bonus */
const NU32 *Projet_Commun_carte_Bonus_BListeBonus_ObtenirEnsembleProbabilite( void );

#ifdef PROJET_COMMUN_CARTE_BONUS_BLISTEBONUS_INTERNE
static const NU32 BPROBABILITE_SPAWN_BONUS[ BLISTE_BONUS ] =
{
	15,
	8,
	2,
	10
};
#endif // PROJET_COMMUN_CARTE_BONUS_BLISTEBONUS_INTERNE

// Verifier taille enum
nassert( sizeof( BListeBonus ) == sizeof( NU32 ),
	"Projet_Commun_Carte_Bonus_BListeBonus" );

#endif // !PROJET_COMMUN_CARTE_BONUS_BLISTEBONUS_PROTECT

