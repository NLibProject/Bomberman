#ifndef PROJET_COMMUN_CARTE_ETAT_BETATCARTE_PROTECT
#define PROJET_COMMUN_CARTE_ETAT_BETATCARTE_PROTECT

// ----------------------------------------------
// struct Projet::Commun::Carte::Etat::BEtatCarte
// ----------------------------------------------

typedef struct BEtatCarte
{
	// Mutex
	NMutex *m_mutex;

	// Carte
	const BCarte *m_carte;

	// Cases etat
	BCaseEtatCarte **m_case;

	// Dernier identifiant attribue
	NU32 m_dernierIdentifiant;
} BEtatCarte;

/* Construire */
__ALLOC BEtatCarte *Projet_Commun_Carte_Etat_BEtatCarte_Construire( const BCarte*,
	const struct BRessource *ressources );

/* Detruire */
void Projet_Commun_Carte_Etat_BEtatCarte_Detruire( BEtatCarte** );

/* Obtenir carte */
const BCarte *Projet_Commun_Carte_Etat_BEtatCarte_ObtenirCarte( const BEtatCarte* );

/* Obtenir taille */
const NUPoint *Projet_Commun_Carte_Etat_BEtatCarte_ObtenirTaille( const BEtatCarte* );

/* Obtenir cases */
const BCaseEtatCarte **Projet_Commun_Carte_Etat_BEtatCarte_ObtenirCase( const BEtatCarte* );

/* Generer bloc */
NBOOL Projet_Commun_Carte_Etat_BEtatCarte_GenererBloc( BEtatCarte* );
NBOOL Projet_Commun_Carte_Etat_BEtatCarte_GenererBloc2( BEtatCarte*,
	const NBOOL **estCaseRemplie );

/* Poser bombe */
NU32 Projet_Commun_Carte_Etat_BEtatCarte_PoserBombe( BEtatCarte*,
	NSPoint position,
	NU32 identifiantJoueur,
	NU32 dureeVie,
	NU32 tempsPose,
	NU32 puissance,
	const void* );
NU32 Projet_Commun_Carte_Etat_BEtatCarte_PoserBombe2( BEtatCarte*,
	NSPoint position,
	NU32 identifiantJoueur,
	NU32 dureeVie,
	NU32 tempsPose,
	NU32 puissance,
	const void*,
	NU32 identifiantBombe );

/* Poser bonus */
NU32 Projet_Commun_Carte_Etat_BEtatCarte_PoserBonus( BEtatCarte*,
	NSPoint position,
	BListeBonus type,
	NU32 duree,
	NU32 tempsPose );
NU32 Projet_Commun_Carte_Etat_BEtatCarte_PoserBonus2( BEtatCarte *this,
	NSPoint position,
	BListeBonus type,
	NU32 duree,
	NU32 tempsPose,
	NU32 identifiant );

/* Supprimer bombe (doit etre protege) */
NBOOL Projet_Commun_Carte_Etat_BEtatCarte_SupprimerBombe( BEtatCarte*,
	NU32 identifiantBombe );

/* Supprimer bonus (doit etre protege) */
NBOOL Projet_Commun_Carte_Etat_BEtatCarte_DisparaitreBonus( BEtatCarte*,
	NU32 identifiantBonus );

/* Obtenir position bombe */
NBOOL Projet_Commun_Carte_Etat_BEtatCarte_ObtenirPositionBombe( const BEtatCarte*,
	NU32 identifiantBombe,
	__OUTPUT NSPoint* );

/* Update */
void Projet_Commun_Carte_Etat_BEtatCarte_Update( BEtatCarte* );

/* Proteger etat */
NBOOL Projet_Commun_Carte_Etat_BEtatCarte_Proteger( BEtatCarte* );

/* Ne plus proteger */
void Projet_Commun_Carte_Etat_BEtatCarte_NePlusProteger( BEtatCarte* );

#endif // !PROJET_COMMUN_CARTE_ETAT_BETATCARTE_PROTECT

