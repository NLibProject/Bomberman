#ifndef PROJET_COMMUN_CARTE_ETAT_PROTECT
#define PROJET_COMMUN_CARTE_ETAT_PROTECT

// -------------------------------------
// namespace Projet::Commun::Carte::Etat
// -------------------------------------

// Facteur de reduction du nombre de blocs a generer
#define BFACTEUR_REDUCTION_BLOC_REMPLISSABLE_CARTE				2

// namespace Projet::Commun::Carte::Etat::Case
#include "Case/Projet_Commun_Carte_Etat_Case.h"

// struct Projet::Commun::Carte::Etat::BEtatCarte
#include "Projet_Commun_Carte_Etat_BEtatCarte.h"

#endif // !PROJET_COMMUN_CARTE_ETAT_PROTECT

