#ifndef PROJET_COMMUN_CARTE_ETAT_CASE_BONUS_BBONUSCASE_PROTECT
#define PROJET_COMMUN_CARTE_ETAT_CASE_BONUS_BBONUSCASE_PROTECT

// -----------------------------------------------------------
// struct Projet::Commun::Carte::Etat::Case::Bonus::BBonusCase
// -----------------------------------------------------------

typedef struct BBonusCase
{
	// Type de bonus sur la case
	BListeBonus m_bonus;

	// Temps d'apparition du bonus
	NU32 m_tempsApparition;

	// Duree
	NU32 m_dureeBonus;

	// Identifiant
	NU32 m_identifiant;

	/* Signalement disparition */
	// Niveau alpha
	NU32 m_alpha;

	// Sens alpha
	NBOOL m_sensAlpha;
} BBonusCase;

/* Construire */
__ALLOC BBonusCase *Projet_Commun_Carte_Etat_Case_Bonus_BBonusCase_Construire( BListeBonus,
	NU32 tempsApparition,
	NU32 dureeBonus,
	NU32 identifiant );

/* Detruire */
void Projet_Commun_Carte_Etat_Case_Bonus_BBonusCase_Detruire( BBonusCase** );

/* Obtenir type */
BListeBonus Projet_Commun_Carte_Etat_Case_Bonus_BBonusCase_ObtenirType( const BBonusCase* );

/* Obtenir temps apparition */
NU32 Projet_Commun_Carte_Etat_Case_Bonus_BBonusCase_ObtenirTempsApparition( const BBonusCase* );

/* Obtenir duree bonus */
NU32 Projet_Commun_Carte_Etat_Case_Bonus_BBonusCase_ObtenirDureeBonus( const BBonusCase* );

/* Obtenir modificateur alpha */
NU32 Projet_Commun_Carte_Etat_Case_Bonus_BBonusCase_ObtenirModificateurAlpha( const BBonusCase* );

/* Obtenir identifiant */
NU32 Projet_Commun_Carte_Etat_Case_Bonus_BBonusCase_ObtenirIdentifiant( const BBonusCase* );

/* Update */
void Projet_Commun_Carte_Etat_Case_Bonus_BBonusCase_Update( BBonusCase* );

#endif // !PROJET_COMMUN_CARTE_ETAT_CASE_BONUS_BBONUSCASE_PROTECT

