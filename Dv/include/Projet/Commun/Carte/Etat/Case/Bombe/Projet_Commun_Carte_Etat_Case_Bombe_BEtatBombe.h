#ifndef PROJET_COMMUN_CARTE_ETAT_CASE_BOMBE_BETATBOMBE_PROTECT
#define PROJET_COMMUN_CARTE_ETAT_CASE_BOMBE_BETATBOMBE_PROTECT

// -----------------------------------------------------------
// struct Projet::Commun::Carte::Etat::Case::Bombe::BEtatBombe
// -----------------------------------------------------------

typedef struct BEtatBombe
{
	// Identifiant bombe
	NU32 m_identifiantBombe;

	// Identifiant joueur ayant pose la bombe
	NU32 m_identifiantJoueur;

	// Temps de la pose
	NU32 m_tempsPose;

	// Duree de vie
	NU32 m_dureeVie;

	// Puissance
	NU32 m_puissance;

	// Animation
	NBOOL m_estAnimationExterne;
	NEtatAnimation *m_etatAnimation;

	// Type animation (BContenuAnimationBombe)
	NU32 m_typeAnimation;
} BEtatBombe;

/* Construire */
__ALLOC BEtatBombe *Projet_Commun_Carte_Etat_Case_Bombe_BEtatBombe_Construire( NU32 identifiantBombe,
	NU32 joueur,
	NU32 tempsPose,
	NU32 dureeVie,
	NU32 puissance,
	NU32 typeAnimation, /* BContenuAnimationBombe */
	const void* );
__ALLOC BEtatBombe *Projet_Commun_Carte_Etat_Case_Bombe_BEtatBombe_Construire2( NU32 identifiantBombe,
	NU32 joueur,
	NU32 tempsPose,
	NU32 dureeVie,
	NU32 puissance,
	NU32 typeAnimation, /* BContenuAnimationBombe */
	const void*,
	const NEtatAnimation* );

/* Detruire */
void Projet_Commun_Carte_Etat_Case_Bombe_BEtatBombe_Detruire( BEtatBombe** );

/* Update */
void Projet_Commun_Carte_Etat_Case_Bombe_BEtatBombe_Update( BEtatBombe* );

/* Obtenir identifiant joueur */
NU32 Projet_Commun_Carte_Etat_Case_Bombe_BEtatBombe_ObtenirIdentifiantJoueur( const BEtatBombe* );

/* Obtenir temps de pose */
NU32 Projet_Commun_Carte_Etat_Case_Bombe_BEtatBombe_ObtenirTempsPose( const BEtatBombe* );

/* Obtenir duree de vie */
NU32 Projet_Commun_Carte_Etat_Case_Bombe_BEtatBombe_ObtenirDureeVie( const BEtatBombe* );

/* Obtenir la puissance */
NU32 Projet_Commun_Carte_Etat_Case_Bombe_BEtatBombe_ObtenirPuissance( const BEtatBombe* );

/* Obtenir identifiant bombe */
NU32 Projet_Commun_Carte_Etat_Case_Bombe_BEtatBombe_ObtenirIdentifiant( const BEtatBombe* );

/* Obtenir type animation (BContenuAnimationBombe) */
NU32 Projet_Commun_Carte_Etat_Case_Bombe_BEtatBombe_ObtenirTypeAnimation( const BEtatBombe* );

/* Obtenir frame animation */
NU32 Projet_Commun_Carte_Etat_Case_Bombe_BEtatBombe_ObtenirFrame( const BEtatBombe* );

#endif // !PROJET_COMMUN_CARTE_ETAT_CASE_BOMBE_BETATBOMBE_PROTECT

