#ifndef PROJET_COMMUN_CARTE_ENSEMBLE_BENSEMBLECARTE_PROTECT
#define PROJET_COMMUN_CARTE_ENSEMBLE_BENSEMBLECARTE_PROTECT

// ------------------------------------------------------
// struct Projet::Commun::Carte::Ensemble::BEnsembleCarte
// ------------------------------------------------------

typedef struct BEnsembleCarte
{
	// Nombre de cartes
	NU32 m_nombreCarte;

	// Carte
	BCarte **m_carte;

	// Checksum
	NU32 m_checksum;
} BEnsembleCarte;

/* Construire l'ensemble des cartes */
__ALLOC BEnsembleCarte *Projet_Commun_Carte_Ensemble_BEnsembleCarte_Construire( const char *dossierCarte );

/* Detruire */
void Projet_Commun_Carte_Ensemble_BEnsembleCarte_Detruire( BEnsembleCarte** );

/* Obtenir carte */
const BCarte *Projet_Commun_Carte_Ensemble_BEnsembleCarte_Obtenir( const BEnsembleCarte*,
	NU32 index );
const BCarte *Projet_Commun_Carte_Ensemble_BEnsembleCarte_Obtenir2( const BEnsembleCarte*,
	NU32 identifiant );

/* Obtenir nombre carte */
NU32 Projet_Commun_Carte_Ensemble_BEnsembleCarte_ObtenirNombre( const BEnsembleCarte* );

/* Obtenir checksum */
NU32 Projet_Commun_Carte_Ensemble_BEnsembleCarte_ObtenirChecksum( const BEnsembleCarte* );

#endif // !PROJET_COMMUN_CARTE_ENSEMBLE_BENSEMBLECARTE_PROTECT

