#ifndef PROJET_COMMUN_CARTE_TILESET_CHARGEMENT_PROTECT
#define PROJET_COMMUN_CARTE_TILESET_CHARGEMENT_PROTECT

// ----------------------------------------------------
// namespace Projet::Commun::Carte::Tileset::Chargement
// ----------------------------------------------------

// Repertoire contenant les tilests
static const char BREPERTOIRE_TILESET[ 32 ] = "Assets/Tileset";

// Base du nom des fichiers de definition de tileset [Permet un chargement fixe, quelque soit
// l'ordre d'ajout. Implique une rigueur dans l'ajout des tilesets (Tileset1.btl...TilesetN.btl)]
static const char BBASE_NOM_TILESET[ 32 ] = "Tileset";

/* Charger les tilesets */
__ALLOC BTileset **Projet_Commun_Carte_Tileset_Chargement_Charger( __OUTPUT NU32 *nombreTileset,
	const NFenetre* );

#endif // !PROJET_COMMUN_CARTE_TILESET_CHARGEMENT_PROTECT

