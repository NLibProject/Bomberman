#ifndef PROJET_COMMUN_CARTE_TILESET_BTILESET_PROTECT
#define PROJET_COMMUN_CARTE_TILESET_BTILESET_PROTECT

// -----------------------------------------------
// struct Projet::Commun::Carte::Tileset::BTileset
// -----------------------------------------------

typedef struct BTileset
{
	// Tileset
	NTileset *m_tileset;

	// Animation
	NAnimation *m_animation;

	// Est anime?
	NBOOL m_estAnime;
} BTileset;

/* Construire */
__ALLOC BTileset *Projet_Commun_Carte_Tileset_BTileset_Construire( const char*,
	const NFenetre* );

/* Detruire */
void Projet_Commun_Carte_Tileset_BTileset_Detruire( BTileset** );

/* Update */
void Projet_Commun_Carte_Tileset_BTileset_Update( BTileset* );

/* Est anime? */
NBOOL Projet_Commun_Carte_Tileset_BTileset_EstAnime( const BTileset* );

/* Obtenir nombre cases */
NUPoint Projet_Commun_Carte_Tileset_BTileset_ObtenirNombreCase( const BTileset* );

/* Afficher */
void Projet_Commun_Carte_Tileset_BTileset_Afficher( const BTileset*,
	NUPoint caseAAfficher,
	NSPoint position,
	NU32 zoom );

#endif // !PROJET_COMMUN_CARTE_TILESET_BTILESET_PROTECT

