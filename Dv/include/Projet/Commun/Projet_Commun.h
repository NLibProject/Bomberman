#ifndef PROJET_COMMUN_PROTECT
#define PROJET_COMMUN_PROTECT

/*
	Header contenant les definitions communes
	a tous les projets de la solution.

	@author SOARES Lucas
*/

// ------------------------
// namespace Projet::Commun
// ------------------------

// Nombre minimum de joueurs
#define BNOMBRE_MINIMUM_JOUEUR					1

// Nombre maximum de joueurs
#define BNOMBRE_MAXIMUM_JOUEUR					4

// Verifier le nombre de joueurs
#if BNOMBRE_MINIMUM_JOUEUR > BNOMBRE_MAXIMUM_JOUEUR || BNOMBRE_MINIMUM_JOUEUR <= 0
#error Projet::Commun::Erreur nombre joueurs.
#endif

// namespace Projet::Commun::Erreur
#include "Erreur/Projet_Commun_Erreur.h"

// namespace Projet::Commun::Ressource::Police
#include "Ressource/Police/Projet_Commun_Ressource_Police.h"

// namespace Projet::Commun::Reseau
#include "Reseau/Projet_Commun_Reseau.h"

// namespace Projet::Commun::Carte
#include "Carte/Projet_Commun_Carte.h"

// namespace Projet::Commun::Fichier
#include "Fichier/Projet_Commun_Fichier.h"

// namespace Projet::Commun::Personnage
#include "Personnage/Projet_Commun_Personnage.h"

// namespace Projet::Commun::Ressource
#include "Ressource/Projet_Commun_Ressource.h"

// namespace Projet::Commun::Monde
#include "Monde/Projet_Commun_Monde.h"

#endif // !PROJET_COMMUN_PROTECT

