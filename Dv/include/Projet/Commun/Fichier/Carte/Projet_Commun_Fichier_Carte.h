#ifndef PROJET_COMMUN_FICHIER_CARTE_PROTECT
#define PROJET_COMMUN_FICHIER_CARTE_PROTECT

// ----------------------------------------
// namespace Projet::Commun::Fichier::Carte
// ----------------------------------------

// Extension fichier
static const char BEXTENSION_FICHIER_CARTE[ 32 ] = "bcr";

// Base nom carte
static const char BBASE_NOM_FICHIER_CARTE[ 32 ] = "Carte";

// Taille header
#define BTAILLE_HEADER_FICHIER_CARTE		16

// Header
static const char BHEADER_FICHIER_CARTE[ BTAILLE_HEADER_FICHIER_CARTE + 1 ] = "NHDRBMBCARTESCRN";

// Repertoire carte
static const char BREPERTOIRE_ENSEMBLE_CARTE[ 32 ] = "Assets/Carte";

#endif // !PROJET_COMMUN_FICHIER_CARTE_PROTECT

