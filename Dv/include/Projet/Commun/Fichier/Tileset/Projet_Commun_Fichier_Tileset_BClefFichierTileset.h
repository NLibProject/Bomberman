#ifndef PROJET_COMMUN_FICHIER_TILESET_BCLEFFICHIERTILESET_PROTECT
#define PROJET_COMMUN_FICHIER_TILESET_BCLEFFICHIERTILESET_PROTECT

// ----------------------------------------------------------
// enum Projet::Commun::Fichier::Tileset::BClefFichierTileset
// ----------------------------------------------------------

typedef enum BClefFichierTileset
{
	// Base
	BCLEF_FICHIER_TILESET_EST_ANIME,
	BCLEF_FICHIER_TILESET_NOM,

	// Animation
	BCLEF_FICHIER_TILESET_TAILLE_ORDRE_ANIMATION,
	BCLEF_FICHIER_TILESET_ORDRE_ANIMATION,
	BCLEF_FICHIER_TILESET_TEMPS_CHANGEMENT_ANIMATION,

	BCLEFS_FICHIER_TILESET
} BClefFichierTileset;

/* Composer l'ensemble de clefs */
__ALLOC char **Projet_Commun_Fichier_Tileset_BClefFichierTileset_ComposerEnsembleClef( void );

#ifdef PROJET_COMMUN_FICHIER_TILESET_BCLEFFICHIERTILESET_INTERNE
static const char BCLEFS_FICHIER_TILESET_TEXTE[ BCLEFS_FICHIER_TILESET ][ 32 ] =
{
	// Base
	"Anime: ",
	"Nom: ",

	// Si anime
	"Taille ordre: ",
	"Ordre: ",
	"Temps changement: "
};
#endif // PROJET_COMMUN_FICHIER_TILESET_BCLEFFICHIERTILESET_INTERNE

#endif // !PROJET_COMMUN_FICHIER_TILESET_BCLEFFICHIERTILESET_PROTECT

