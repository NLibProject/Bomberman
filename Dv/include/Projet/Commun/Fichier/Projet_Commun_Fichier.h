#ifndef PROJET_COMMUN_FICHIER_PROTECT
#define PROJET_COMMUN_FICHIER_PROTECT

/*
	Regroupement des definitions de clefs/methodes de chargement
	des fichiers propres au projet

	@author SOARES Lucas
*/

// ---------------------------------
// namespace Projet::Commun::Fichier
// ---------------------------------

// Extension fichiers image
#define BEXTENSION_FICHIER_IMAGE			"png"

// namespace Projet::Commun::Fichier::Personnage
#include "Personnage/Projet_Commun_Fichier_Personnage.h"

// namespace Projet::Commun::Fichier::Tileset
#include "Tileset/Projet_Commun_Fichier_Tileset.h"

// namespace Projet::Commun::Fichier::Carte
#include "Carte/Projet_Commun_Fichier_Carte.h"

#endif // !PROJET_COMMUN_FICHIER_PROTECT

