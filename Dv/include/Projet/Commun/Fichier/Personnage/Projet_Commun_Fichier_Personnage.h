#ifndef PROJET_COMMUN_FICHIER_PERSONNAGE_PROTECT
#define PROJET_COMMUN_FICHIER_PERSONNAGE_PROTECT

/*
	Ensemble des clefs pour chaque fichier de definitions

	@author SOARES Lucas
*/
// ---------------------------------------------
// namespace Projet::Commun::Fichier::Personnage
// ---------------------------------------------

// Extension fichiers personnage
static const char BEXTENSION_FICHIER_PERSONNAGE[ 32 ]			= "bpe";

// Extension fichiers charset
static const char BEXTENSION_FICHIER_CHARSET[ 32 ]				= "bch";

// enum Projet::Commun::Fichier::Personnage::BClefFichierPersonnage
#include "Projet_Commun_Fichier_Personnage_BClefFichierPersonnage.h"

// enum Projet::Commun::Fichier::Personnage::BClefFichierCharset
#include "Projet_Commun_Fichier_Personnage_BClefFichierCharset.h"


#endif // !PROJET_COMMUN_FICHIER_PERSONNAGE_PROTECT

