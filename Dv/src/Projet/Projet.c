#include "../../include/Projet/Projet.h"

/*
	@author SOARES Lucas
*/

// ----------------
// namespace Projet
// ----------------

int main( int argc,
	char *argv[ ] )
{
	// Sortie
	__OUTPUT NS32 sortie;

	// Referencer
	NREFERENCER( argc );
	NREFERENCER( argv );

#if defined( PROJET_TEST )
	sortie = Projet_Test_Main( );
#elif defined( PROJET_CLIENT ) // PROJET_TEST
	sortie = Projet_Client_Main( );
#elif defined( PROJET_SERVEUR ) // !PROJET_TEST && PROJET_CLIENT
	sortie = Projet_Serveur_Main( );
#elif defined( PROJET_EDITEUR_MAP ) // !PROJET_TEST && !PROJET_CLIENT && PROJET_SERVEUR
	sortie = Projet_EditeurMap_Main( );
#else // !PROJET_TEST && !PROJET_CLIENT && !PROJET_SERVEUR && PROJET_EDITEUR_MAP
#error Erreur projet
#endif // !PROJET_TEST && !PROJET_CLIENT && !PROJET_SERVEUR && !PROJET_EDITEUR_MAP

	// Delai pour affichage des derniers messages
	NLib_Temps_Attendre( 100 );

	// OK?
	return sortie;
}

