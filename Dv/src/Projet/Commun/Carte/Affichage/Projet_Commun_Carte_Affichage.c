#include "../../../../../include/Projet/Projet.h"

// ------------------------------------------
// namespace Projet::Commun::Carte::Affichage
// ------------------------------------------

/* Obtenir la taille a afficher */
NUPoint Projet_Commun_Carte_Affichage_ObtenirTailleAAfficher( const BCarte *carte,
	NU32 zoom )
{
	// Sortie
	__OUTPUT NUPoint sortie;

	// Calculer
	NDEFINIR_POSITION( sortie,
		Projet_Commun_Carte_BCarte_ObtenirTaille( carte )->x * BTAILLE_CASE_TILESET.x * zoom,
		Projet_Commun_Carte_BCarte_ObtenirTaille( carte )->y * BTAILLE_CASE_TILESET.y * zoom );

	// OK
	return sortie;
}

/* Afficher bordure */
void Projet_Commun_Carte_Affichage_AfficherBordure( const BCarte *carte,
	const BTileset **tileset,
	NSPoint positionInitiale,
	NU32 zoom,
	const NFenetre *fenetre )
{
	// Positions
	NSPoint positionInitialeRemplissage,
		position;

	// Nombre cases
	NUPoint nombreCase;

	// Id tileset
	NU32 idTileset;

	// Position tileset
	NUPoint positionTileset;

	// Taille a afficher
	NUPoint tailleAAfficher;

	// Iterateurs
	NU32 x = 0,
		y;

	// Calculer le nombre de cases
	NDEFINIR_POSITION( nombreCase,
		NLib_Module_SDL_NFenetre_ObtenirResolution( fenetre )->x / ( BTAILLE_CASE_TILESET.x * zoom ),
		NLib_Module_SDL_NFenetre_ObtenirResolution( fenetre )->y / ( BTAILLE_CASE_TILESET.y * zoom ) );

	// Calculer position initiale
	NDEFINIR_POSITION( positionInitialeRemplissage,
		( positionInitiale.x % ( BTAILLE_CASE_TILESET.x * zoom ) ) - ( BTAILLE_CASE_TILESET.x * zoom ),
		( positionInitiale.y % ( BTAILLE_CASE_TILESET.y * zoom ) ) - ( BTAILLE_CASE_TILESET.y * zoom ) );

	// Obtenir id tileset
	if( ( idTileset = Projet_Commun_Carte_BCarte_ObtenirTilesetBlocBordure( carte ) ) == BBLOC_CARTE_AUCUN )
		return;

	// Obtenir la taille a afficher
	tailleAAfficher = Projet_Commun_Carte_Affichage_ObtenirTailleAAfficher( carte,
		zoom );

	// Obtenir position
	positionTileset = Projet_Commun_Carte_BCarte_ObtenirBlocBordure( carte );

	// Remplir
	for( ; x < nombreCase.x + BAFFICHAGE_NOMBRE_CASE_SUPPLEMENTAIRE_BLOC_BORDURE; x++ )
		for( y = 0; y < nombreCase.y + BAFFICHAGE_NOMBRE_CASE_SUPPLEMENTAIRE_BLOC_BORDURE; y++ )
		{
			// Calculer la position
			NDEFINIR_POSITION( position,
				positionInitialeRemplissage.x + ( x * BTAILLE_CASE_TILESET.x ) * zoom,
				positionInitialeRemplissage.y + ( y * BTAILLE_CASE_TILESET.y ) * zoom );

			// Limiter l'affichage au contour de la carte
			if( position.x < positionInitiale.x
				|| position.y < positionInitiale.y
				|| position.x >= positionInitiale.x + (NS32)tailleAAfficher.x
				|| position.y >= positionInitiale.y + (NS32)tailleAAfficher.y )
				// Afficher
				Projet_Commun_Carte_Tileset_BTileset_Afficher( tileset[ idTileset ],
					positionTileset,
					position,
					zoom );
		}
}

/* Afficher */
void Projet_Commun_Carte_Affichage_Afficher( const BCarte *carte,
	NU32 couche,
	const BTileset **tileset,
	NSPoint positionInitiale,
	NU32 zoom )
{
	// Iterateur
	NU32 x, y;

	// Position
	NSPoint position;

	// Cases
	const BBloc **cases;

	// Obtenir les cases
	cases = Projet_Commun_Carte_BCarte_ObtenirCases( carte );

	// Afficher
	for( x = 0; x < Projet_Commun_Carte_BCarte_ObtenirTaille( carte )->x; x++ )
		for( y = 0; y < Projet_Commun_Carte_BCarte_ObtenirTaille( carte )->y; y++ )
			// Verifier si il faut afficher
			if( cases[ x ][ y ].m_tileset[ couche ] != BBLOC_CARTE_AUCUN )
			{
				// Calculer la position
				NDEFINIR_POSITION( position,
					positionInitiale.x + ( x * BTAILLE_CASE_TILESET.x ) * zoom,
					positionInitiale.y + ( y * BTAILLE_CASE_TILESET.y ) * zoom );

				// Afficher
				Projet_Commun_Carte_Tileset_BTileset_Afficher( tileset[ cases[ x ][ y ].m_tileset[ couche ] ],
					cases[ x ][ y ].m_positionTileset[ couche ],
					position,
					zoom );
			}
}

/* Afficher remplissage */
void Projet_Commun_Carte_Affichage_AfficherRemplissageComplet( const BCarte *carte,
	const BTileset **tileset,
	NSPoint positionInitiale,
	NU32 zoom )
{
	// Iterateurs
	NU32 x,
		y;

	// Position
	NSPoint position;

	// Cases
	const BBloc **cases;

	// Id tileset
	NU32 idTileset;

	// Position tileset
	NUPoint positionTileset;

	// Obtenir id tileset
	if( ( idTileset = Projet_Commun_Carte_BCarte_ObtenirTilesetBlocRemplissage( carte ) ) == BBLOC_CARTE_AUCUN )
		return;

	// Obtenir position
	positionTileset = Projet_Commun_Carte_BCarte_ObtenirBlocRemplissage( carte );

	// Obtenir les cases
	cases = Projet_Commun_Carte_BCarte_ObtenirCases( carte );

	// Afficher
	for( x = 0; x < Projet_Commun_Carte_BCarte_ObtenirTaille( carte )->x; x++ )
		for( y = 0; y < Projet_Commun_Carte_BCarte_ObtenirTaille( carte )->y; y++ )
			if( cases[ x ][ y ].m_type == BTYPE_BLOC_REMPLISSABLE )
			{
				// Calculer la position
				NDEFINIR_POSITION( position,
					positionInitiale.x + ( x * BTAILLE_CASE_TILESET.x ) * zoom,
					positionInitiale.y + ( y * BTAILLE_CASE_TILESET.y ) * zoom );

				// Afficher
				Projet_Commun_Carte_Tileset_BTileset_Afficher( tileset[ idTileset ],
					positionTileset,
					position,
					zoom );
			}
}

void Projet_Commun_Carte_Affichage_AfficherRemplissageSchema( const BCarte *carte,
	const BTileset **tileset,
	NSPoint positionInitiale,
	NU32 zoom,
	const void *etat )
{
	// Iterateurs
	NU32 x,
		y;

	// Cases
	const BCaseEtatCarte **casesEtat;

	// Position
	NSPoint position;

	// Cases
	const BBloc **cases;

	// Id tileset
	NU32 idTileset;

	// Position tileset
	NUPoint positionTileset;

	// Obtenir id tileset
	if( ( idTileset = Projet_Commun_Carte_BCarte_ObtenirTilesetBlocRemplissage( carte ) ) == BBLOC_CARTE_AUCUN
		|| !( casesEtat = Projet_Commun_Carte_Etat_BEtatCarte_ObtenirCase( etat ) ) )
		return;

	// Obtenir position
	positionTileset = Projet_Commun_Carte_BCarte_ObtenirBlocRemplissage( carte );

	// Obtenir les cases
	cases = Projet_Commun_Carte_BCarte_ObtenirCases( carte );

	// Afficher
	for( x = 0; x < Projet_Commun_Carte_BCarte_ObtenirTaille( carte )->x; x++ )
		for( y = 0; y < Projet_Commun_Carte_BCarte_ObtenirTaille( carte )->y; y++ )
			if( cases[ x ][ y ].m_type == BTYPE_BLOC_REMPLISSABLE
				&& casesEtat[ x ][ y ].m_estRempli )
			{
				// Calculer la position
				NDEFINIR_POSITION( position,
					positionInitiale.x + ( x * BTAILLE_CASE_TILESET.x ) * zoom,
					positionInitiale.y + ( y * BTAILLE_CASE_TILESET.y ) * zoom );

				// Afficher
				Projet_Commun_Carte_Tileset_BTileset_Afficher( tileset[ idTileset ],
					positionTileset,
					position,
					zoom );
			}
}

/* Afficher bombes */
void Projet_Commun_Carte_Affichage_AfficherBombe( const BCarte *carte,
	const void *ensembleAnimation,
	NSPoint positionInitiale,
	NU32 zoom,
	const void *etatCarte )
{
	// Iterateurs
	NU32 x,
		y;

	// Cases
	const BCaseEtatCarte **casesEtat;

	// Position
	NSPoint position;

	// Obtenir cases etat
	if( !( casesEtat = Projet_Commun_Carte_Etat_BEtatCarte_ObtenirCase( etatCarte ) ) )
		return;

	// Proteger
	Projet_Commun_Carte_Etat_BEtatCarte_Proteger( (BEtatCarte*)etatCarte );

	// Afficher
	for( x = 0; x < Projet_Commun_Carte_BCarte_ObtenirTaille( carte )->x; x++ )
		for( y = 0; y < Projet_Commun_Carte_BCarte_ObtenirTaille( carte )->y; y++ )
			if( casesEtat[ x ][ y ].m_etatBombe != NULL )
			{
				// Calculer la position
				NDEFINIR_POSITION( position,
					positionInitiale.x + ( x * BTAILLE_CASE_TILESET.x ) * zoom,
					positionInitiale.y + ( y * BTAILLE_CASE_TILESET.y ) * zoom );

				// Afficher
				Projet_Commun_Ressource_Animation_BEnsembleAnimation_Afficher2( ensembleAnimation,
					BLISTE_ANIMATION_BOMBE,
					Projet_Commun_Carte_Etat_Case_Bombe_BEtatBombe_ObtenirTypeAnimation( casesEtat[ x ][ y ].m_etatBombe ),
					position,
					zoom,
					Projet_Commun_Carte_Etat_Case_Bombe_BEtatBombe_ObtenirFrame( casesEtat[ x ][ y ].m_etatBombe ) );
			}

	// Ne plus proteger
	Projet_Commun_Carte_Etat_BEtatCarte_NePlusProteger( (BEtatCarte*)etatCarte );
}

/* Afficher explosions */
void Projet_Commun_Carte_Affichage_AfficherExplosion( const BCarte *carte,
	NSPoint positionInitiale,
	NU32 zoom,
	const void *etatCarte )
{
	// Iterateurs
	NU32 x,
		y;

	// Cases
	const BCaseEtatCarte **casesEtat;

	// Position
	NSPoint position;

	// Obtenir cases etat
	if( !( casesEtat = Projet_Commun_Carte_Etat_BEtatCarte_ObtenirCase( etatCarte ) ) )
		return;

	// Afficher
	for( x = 0; x < Projet_Commun_Carte_BCarte_ObtenirTaille( carte )->x; x++ )
		for( y = 0; y < Projet_Commun_Carte_BCarte_ObtenirTaille( carte )->y; y++ )
			if( Projet_Commun_Carte_Etat_Case_AnimationExplosion_BEtatAnimationExplosion_EstEnCours( casesEtat[ x ][ y ].m_etatExplosion ) )
			{
				// Calculer la position
				NDEFINIR_POSITION( position,
					positionInitiale.x + ( x * BTAILLE_CASE_TILESET.x ) * zoom,
					positionInitiale.y + ( y * BTAILLE_CASE_TILESET.y ) * zoom );

				// Definir la position
				NLib_Module_SDL_Surface_NAnimation_DefinirPosition( (NAnimation*)Projet_Commun_Carte_Etat_Case_AnimationExplosion_BEtatAnimationExplosion_ObtenirAnimation( casesEtat[ x ][ y ].m_etatExplosion ),
					position );

				// Afficher
				NLib_Module_SDL_Surface_NAnimation_Afficher3( Projet_Commun_Carte_Etat_Case_AnimationExplosion_BEtatAnimationExplosion_ObtenirAnimation( casesEtat[ x ][ y ].m_etatExplosion ),
					Projet_Commun_Carte_Etat_Case_AnimationExplosion_BEtatAnimationExplosion_ObtenirTypeAnimation( casesEtat[ x ][ y ].m_etatExplosion ),
					zoom,
					Projet_Commun_Carte_Etat_Case_AnimationExplosion_BEtatAnimationExplosion_ObtenirFrameAnimation( casesEtat[ x ][ y ].m_etatExplosion ) );
			}
}

/* Afficher bonus */
void Projet_Commun_Carte_Affichage_AfficherBonus( const BCarte *carte,
	const void *ensembleBonus,
	NSPoint positionInitiale,
	NU32 zoom,
	const void *etatCarte )
{
	// Iterateurs
	NU32 x,
		y;

	// Cases
	const BCaseEtatCarte **casesEtat;

	// Surface
	NSurface *surface;

	// Position
	NSPoint position;

	// Obtenir cases etat
	if( !( casesEtat = Projet_Commun_Carte_Etat_BEtatCarte_ObtenirCase( etatCarte ) ) )
		return;

	// Proteger
	Projet_Commun_Carte_Etat_BEtatCarte_Proteger( (BEtatCarte*)etatCarte );

	// Afficher
	for( x = 0; x < Projet_Commun_Carte_BCarte_ObtenirTaille( carte )->x; x++ )
		for( y = 0; y < Projet_Commun_Carte_BCarte_ObtenirTaille( carte )->y; y++ )
			if( casesEtat[ x ][ y ].m_etatBonus != NULL )
			{
				// Calculer la position
				NDEFINIR_POSITION( position,
					positionInitiale.x + ( x * BTAILLE_CASE_TILESET.x ) * zoom,
					positionInitiale.y + ( y * BTAILLE_CASE_TILESET.y ) * zoom );

				// Obtenir surface
				if( !( surface = (NSurface*)Projet_Commun_Ressource_Bonus_BRessourceBonus_Obtenir( ensembleBonus,
					Projet_Commun_Carte_Etat_Case_Bonus_BBonusCase_ObtenirType( casesEtat[ x ][ y ].m_etatBonus ) ) ) )
					continue;

				// Definir zoom
				NLib_Module_SDL_Surface_NSurface_DefinirZoom( surface,
					zoom );

				// Definir alpha
				NLib_Module_SDL_Surface_NSurface_DefinirModificationAlpha( surface,
					(NU8)Projet_Commun_Carte_Etat_Case_Bonus_BBonusCase_ObtenirModificateurAlpha( casesEtat[ x ][ y ].m_etatBonus ) );

				// Definir position
				NLib_Module_SDL_Surface_NSurface_DefinirPosition2( surface,
					position );

				// Afficher
				NLib_Module_SDL_Surface_NSurface_Afficher( surface );
			}

	// Ne plus proteger
	Projet_Commun_Carte_Etat_BEtatCarte_NePlusProteger( (BEtatCarte*)etatCarte );
}

