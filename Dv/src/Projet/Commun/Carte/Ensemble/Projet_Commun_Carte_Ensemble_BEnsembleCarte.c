#include "../../../../../include/Projet/Projet.h"

// ------------------------------------------------------
// struct Projet::Commun::Carte::Ensemble::BEnsembleCarte
// ------------------------------------------------------

/* Calculer le checksum (privee) */
NU32 Projet_Commun_Carte_Ensemble_BEnsembleCarte_CalculerChecksumInterne( const BEnsembleCarte *this )
{
	// Sortie
	__OUTPUT NU32 resultat = 0;

	// Iterateur
	NU32 i;

	// Ajouter nombre cartes
	resultat += this->m_nombreCarte * 3;

	// Ajouter checksum carte
	for( i = 0; i < this->m_nombreCarte; i++ )
		resultat += Projet_Commun_Carte_BCarte_ObtenirChecksum( this->m_carte[ i ] );

	// OK
	return resultat;
}

/* Construire l'ensemble des cartes */
__ALLOC BEnsembleCarte *Projet_Commun_Carte_Ensemble_BEnsembleCarte_Construire( const char *dossierCarte )
{
	// Sortie
	__OUTPUT BEnsembleCarte *out;

	// Repertoire
	NRepertoire *repertoire;

	// Filtre
	char filtre[ 256 ];

	// Buffer
	char buffer[ MAX_PATH ];

	// Iterateurs
	NU32 i,
		j;

	// Construire
	if( !( repertoire = NLib_Module_Repertoire_NRepertoire_Construire( ) ) )
	{
		// Notifier
		NOTIFIER_ERREUR( NERREUR_DIRECTORY );

		// Quitter
		return NULL;
	}

	// Changer de repertoire
	if( !NLib_Module_Repertoire_Changer( dossierCarte ) )
	{
		// Notifier
		NOTIFIER_ERREUR( NERREUR_DIRECTORY );

		// Detruire
		NLib_Module_Repertoire_NRepertoire_Detruire( &repertoire );

		// Quitter
		return NULL;
	}

	// Creer le filtre
#ifdef IS_WINDOWS
	sprintf( filtre,
		"*.%s",
		BEXTENSION_FICHIER_CARTE );
#else // IS_WINDOWS
	snprintf( filtre,
		256,
		".*" );
#endif // !IS_WINDOW

	// Lister
	if( !NLib_Module_Repertoire_NRepertoire_Lister( repertoire,
		filtre,
		NATTRIBUT_REPERTOIRE_NORMAL ) )
	{
		// Notifier
		NOTIFIER_ERREUR( NERREUR_DIRECTORY );

		// Detruire
		NLib_Module_Repertoire_NRepertoire_Detruire( &repertoire );

		// Restaurer
		NLib_Module_Repertoire_RestaurerInitial( );

		// Quitter
		return NULL;
	}

	// Restaurer
	NLib_Module_Repertoire_RestaurerInitial( );

	// Allouer la memoire
	if( !( out = calloc( 1,
		sizeof( BEnsembleCarte ) ) ) )
	{
		// Notifier
		NOTIFIER_ERREUR( NERREUR_ALLOCATION_FAILED );

		// Liberer
		NLib_Module_Repertoire_NRepertoire_Detruire( &repertoire );

		// Quitter
		return NULL;
	}

	// Recuperer le nombre de cartes
	if( !( out->m_nombreCarte = NLib_Module_Repertoire_NRepertoire_ObtenirNombreFichiers( repertoire ) ) )
	{
		// Notifier
		NOTIFIER_ERREUR( NERREUR_FILE_NOT_FOUND );

		// Detruire
		NLib_Module_Repertoire_NRepertoire_Detruire( &repertoire );

		// Liberer
		NFREE( out );

		// Quitter
		return NULL;
	}

	// Detruire repertoire
	NLib_Module_Repertoire_NRepertoire_Detruire( &repertoire );

	// Allouer conteneur carte
	if( !( out->m_carte = calloc( out->m_nombreCarte,
		sizeof( BCarte* ) ) ) )
	{
		// Notifier
		NOTIFIER_ERREUR( NERREUR_ALLOCATION_FAILED );

		// Liberer
		NFREE( out );

		// Quitter
		return NULL;
	}

	// Charger les cartes
	for( i = 0; i < out->m_nombreCarte; i++ )
	{
		// Creer le lien
		sprintf( buffer,
			"%s/%s%d.%s",
			dossierCarte,
			BBASE_NOM_FICHIER_CARTE,
			i + 1,
			BEXTENSION_FICHIER_CARTE );

		// Charger
		if( !( out->m_carte[ i ] = Projet_Commun_Carte_BCarte_Construire2( buffer ) ) )
		{
			// Notifier
			NOTIFIER_ERREUR( NERREUR_CONSTRUCTOR_FAILED );

			// Liberer
			for( j = 0; j < i; j++ )
				Projet_Commun_Carte_BCarte_Detruire( &out->m_carte[ j ] );
			NFREE( out->m_carte );
			NFREE( out );

			// Quitter
			return NULL;
		}
	}

	// Calculer le checksum
	out->m_checksum = Projet_Commun_Carte_Ensemble_BEnsembleCarte_CalculerChecksumInterne( out );

	// OK
	return out;
}

/* Detruire */
void Projet_Commun_Carte_Ensemble_BEnsembleCarte_Detruire( BEnsembleCarte **this )
{
	// Iterateur
	NU32 i = 0;

	// Detruire cartes
	for( i = 0; i < (*this)->m_nombreCarte; i++ )
		Projet_Commun_Carte_BCarte_Detruire( &(*this)->m_carte[ i ] );
	NFREE( (*this)->m_carte );

	// Liberer
	NFREE( (*this) );
}

/* Obtenir carte */
const BCarte *Projet_Commun_Carte_Ensemble_BEnsembleCarte_Obtenir( const BEnsembleCarte *this,
	NU32 index )
{
	// Verifier
	if( index >= this->m_nombreCarte )
		return NULL;

	// OK
	return this->m_carte[ index ];
}

const BCarte *Projet_Commun_Carte_Ensemble_BEnsembleCarte_Obtenir2( const BEnsembleCarte *this,
	NU32 identifiant )
{
	// Iterateur
	NU32 i = 0;

	// Chercher
	for( ; i< this->m_nombreCarte; i++ )
		if( Projet_Commun_Carte_BCarte_ObtenirHandle( this->m_carte[ i ] ) == identifiant )
			return this->m_carte[ i ];

	// Introuvable
	return NULL;
}

/* Obtenir nombre carte */
NU32 Projet_Commun_Carte_Ensemble_BEnsembleCarte_ObtenirNombre( const BEnsembleCarte *this )
{
	return this->m_nombreCarte;
}

/* Obtenir checksum */
NU32 Projet_Commun_Carte_Ensemble_BEnsembleCarte_ObtenirChecksum( const BEnsembleCarte *this )
{
	return this->m_checksum;
}

