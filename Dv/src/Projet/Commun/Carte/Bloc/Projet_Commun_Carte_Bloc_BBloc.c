#include "../../../../../include/Projet/Projet.h"

/*
	@author SOARES Lucas
*/

// -----------------------------------------
// struct Projet::Commun::Carte::Bloc::BBloc
// -----------------------------------------

/* Sauvegarder */
NBOOL Projet_Commun_Carte_Bloc_BBloc_Sauvegarder( BBloc *this,
	NFichierBinaire *fichier )
{
	// Iterateur
	NU32 i;

	// Ecrire
		// Type de bloc
			if( !NLib_Fichier_NFichierBinaire_Ecrire( fichier,
				(char*)&this->m_type,
				sizeof( BTypeBloc ) ) )
			{
				// Notifier
				NOTIFIER_ERREUR( NERREUR_FILE_CANT_WRITE );

				// Quitter
				return NFALSE;
			}

	// Ecrire couches
	for( i = 0; i < BCOUCHES_CARTE; i++ )
	{
		// Ecrire
			// Tileset
				if( !NLib_Fichier_NFichierBinaire_Ecrire( fichier,
					(char*)&this->m_tileset[ i ],
					sizeof( NU32 ) ) )
				{
					// Notifier
					NOTIFIER_ERREUR( NERREUR_FILE_CANT_WRITE );

					// Quitter
					return NFALSE;
				}
			// Details tileset
				if( this->m_tileset[ i ] != BBLOC_CARTE_AUCUN )
					// Position
					if( !NLib_Fichier_NFichierBinaire_Ecrire( fichier,
							(char*)&this->m_positionTileset[ i ].x,
							sizeof( NU32 ) )
						|| !NLib_Fichier_NFichierBinaire_Ecrire( fichier,
							(char*)&this->m_positionTileset[ i ].y,
							sizeof( NU32 ) ) )
					{
						// Notifier
						NOTIFIER_ERREUR( NERREUR_FILE_CANT_WRITE );

						// Quitter
						return NFALSE;
					}
	}

	// OK
	return NTRUE;
}

/* Charger */
NBOOL Projet_Commun_Carte_Bloc_BBloc_Charger( __OUTPUT BBloc *this,
	NFichierBinaire *fichier )
{
	// Iterateur
	NU32 i;

	// Lire
		// Type de bloc
			if( !NLib_Fichier_NFichierBinaire_Lire2( fichier,
				(char*)&this->m_type,
				sizeof( BTypeBloc ) ) )
			{
				// Notifier
				NOTIFIER_ERREUR( NERREUR_FILE_CANT_READ );

				// Quitter
				return NFALSE;
			}

	// Ecrire couches
	for( i = 0; i < BCOUCHES_CARTE; i++ )
	{
		// Ecrire
			// Tileset
				if( !NLib_Fichier_NFichierBinaire_Lire2( fichier,
					(char*)&this->m_tileset[ i ],
					sizeof( NU32 ) ) )
				{
					// Notifier
					NOTIFIER_ERREUR( NERREUR_FILE_CANT_READ );

					// Quitter
					return NFALSE;
				}
			// Details tileset
				if( this->m_tileset[ i ] != BBLOC_CARTE_AUCUN )
					// Position
					if( !NLib_Fichier_NFichierBinaire_Lire2( fichier,
							(char*)&this->m_positionTileset[ i ].x,
							sizeof( NU32 ) )
						|| !NLib_Fichier_NFichierBinaire_Lire2( fichier,
							(char*)&this->m_positionTileset[ i ].y,
							sizeof( NU32 ) ) )
					{
						// Notifier
						NOTIFIER_ERREUR( NERREUR_FILE_CANT_READ );

						// Quitter
						return NFALSE;
					}
	}

	// OK
	return NTRUE;
}

