#include "../../../../../../include/Projet/Projet.h"

// ----------------------------------------------------
// namespace Projet::Commun::Carte::Tileset::Chargement
// ----------------------------------------------------

/* Charger les tilesets */
__ALLOC BTileset **Projet_Commun_Carte_Tileset_Chargement_Charger( __OUTPUT NU32 *nombreTileset,
	const NFenetre *fenetre )
{
	// Sortie
	__OUTPUT BTileset **out;

	// Repertoire
	NRepertoire *repertoire;

	// Filtre
	char filtre[ 32 ];

	// Iterateur
	NU32 i, j;

	// Lien
	char lien[ MAX_PATH ];

	// Creer le repertoire
	if( !( repertoire = NLib_Module_Repertoire_NRepertoire_Construire( )) )
	{
		// Notifier
		NOTIFIER_ERREUR( NERREUR_CONSTRUCTOR_FAILED );

		// Quitter
		return NULL;
	}

	// Se placer dans le repertoire des tilesets
	if( !NLib_Module_Repertoire_Changer( BREPERTOIRE_TILESET ) )
	{
		// Notifier
		NOTIFIER_ERREUR( NERREUR_DIRECTORY );

		// Detruire repertoire
		NLib_Module_Repertoire_NRepertoire_Detruire( &repertoire );

		// Quitter
		return NULL;
	}

	// Creer le filtre
#ifdef IS_WINDOWS
	sprintf( filtre,
		"*.%s",
		BEXTENSION_FICHIER_TILESET );
#else // IS_WINDOWS
	snprintf( filtre,
		32,
		".*\\.%s",
		BEXTENSION_FICHIER_TILESET );
#endif // !IS_WINDOWS

	// Lister
	if( !NLib_Module_Repertoire_NRepertoire_Lister( repertoire,
		filtre,
		NATTRIBUT_REPERTOIRE_NORMAL ) )
	{
		// Notifier
		NOTIFIER_ERREUR( NERREUR_DIRECTORY );

		// Restaurer le repertoire
		NLib_Module_Repertoire_RestaurerInitial( );

		// Detruire repertoire
		NLib_Module_Repertoire_NRepertoire_Detruire( &repertoire );

		// Quitter
		return NULL;
	}

	// Restaurer le repertoire initial
	NLib_Module_Repertoire_RestaurerInitial( );

	// Enregistrer le nombre de tilesets
	*nombreTileset = NLib_Module_Repertoire_NRepertoire_ObtenirNombreFichiers( repertoire );

	// Allouer la memoire
	if( !( out = calloc( *nombreTileset,
		sizeof( BTileset* ) ) ) )
	{
		// Notifier
		NOTIFIER_ERREUR( NERREUR_ALLOCATION_FAILED );

		// Detruire repertoire
		NLib_Module_Repertoire_NRepertoire_Detruire( &repertoire );

		// Quitter
		return NULL;
	}

	// Construire les tilesets
	for( i = 0; i < *nombreTileset; i++ )
	{
		// Creer le lien
		sprintf( lien,
			"%s/%s%d.%s",
			BREPERTOIRE_TILESET,
			BBASE_NOM_TILESET,
			i + 1,
			BEXTENSION_FICHIER_TILESET );

		// Construire tileset
		if( !( out[ i ] = Projet_Commun_Carte_Tileset_BTileset_Construire( lien,
			fenetre ) ) )
		{
			// Notifier
			NOTIFIER_ERREUR( NERREUR_CONSTRUCTOR_FAILED );

			// Detruire la sortie
			for( j = 0; j < i; j++ )
				Projet_Commun_Carte_Tileset_BTileset_Detruire( &out[ j ] );
			NFREE( out );

			// Detruire le repertoire
			NLib_Module_Repertoire_NRepertoire_Detruire( &repertoire );

			// Quitter
			return NULL;
		}
	}

	// Detruire le repertoire
	NLib_Module_Repertoire_NRepertoire_Detruire( &repertoire );

	// OK
	return out;
}

