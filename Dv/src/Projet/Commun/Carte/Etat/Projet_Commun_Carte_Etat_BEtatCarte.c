#include "../../../../../include/Projet/Projet.h"

// ----------------------------------------------
// struct Projet::Commun::Carte::Etat::BEtatCarte
// ----------------------------------------------

/* Construire */
__ALLOC BEtatCarte *Projet_Commun_Carte_Etat_BEtatCarte_Construire( const BCarte *carte,
	const struct BRessource *ressource )
{
	// Iterateurs
	NU32 i,
		j;

	// Iterateur nettoyage
	NU32 k,
		l;

	// Sortie
	__OUTPUT BEtatCarte *out;

	// Animation bombe
	const NAnimation *animationBombe;

	// Allouer la memoire
	if( !( out = calloc( 1,
		sizeof( BEtatCarte ) ) ) )
	{
		// Notifier
		NOTIFIER_ERREUR( NERREUR_ALLOCATION_FAILED );

		// Quitter
		return NULL;
	}

	// Enregistrer
	out->m_carte = carte;

	// Construire le mutex
	if( !( out->m_mutex = NLib_Mutex_NMutex_Construire( ) ) )
	{
		// Notifier
		NOTIFIER_ERREUR( NERREUR_MUTEX );

		// Liberer
		NFREE( out );

		// Quitter
		return NULL;
	}

	// Construire les cases
		// 1D
			if( !( out->m_case = calloc( Projet_Commun_Carte_BCarte_ObtenirTaille( carte )->x,
				sizeof( BCaseEtatCarte* ) ) ) )
			{
				// Notifier
				NOTIFIER_ERREUR( NERREUR_ALLOCATION_FAILED );

				// Liberer
				NLib_Mutex_NMutex_Detruire( &out->m_mutex );
				NFREE( out );

				// Quitter
				return NULL;
			}
		// 2D
			for( i = 0; i < Projet_Commun_Carte_BCarte_ObtenirTaille( carte )->x; i++ )
				if( !( out->m_case[ i ] = calloc( Projet_Commun_Carte_BCarte_ObtenirTaille( carte )->y,
					sizeof( BCaseEtatCarte ) ) ) )
				{
					// Notifier
					NOTIFIER_ERREUR( NERREUR_ALLOCATION_FAILED );

					// Liberer
					NLib_Mutex_NMutex_Detruire( &out->m_mutex );
					for( j = 0; j < i; j++ )
						NFREE( out->m_case[ j ] );
					NFREE( out->m_case );
					NFREE( out );

					// Quitter
					return NULL;
				}

	// Obtenir l'animation bombe
	if( !( animationBombe = Projet_Commun_Ressource_Animation_BEnsembleAnimation_Obtenir( Projet_Commun_Ressource_BRessource_ObtenirEnsembleAnimation( ressource ),
		BLISTE_ANIMATION_BOMBE ) ) )
	{
		// Notifier
		NOTIFIER_ERREUR( NERREUR_ALLOCATION_FAILED );

		// Liberer
		NLib_Mutex_NMutex_Detruire( &out->m_mutex );
		for( i = 0; i < Projet_Commun_Carte_BCarte_ObtenirTaille( out->m_carte )->x; i++ )
			NFREE( out->m_case[ i ] );
		NFREE( out->m_case );
		NFREE( out );

		// Quitter
		return NULL;
	}

	// Construire etat animation
	for( i = 0; i < Projet_Commun_Carte_BCarte_ObtenirTaille( carte )->x; i++ )
		for( j = 0; j < Projet_Commun_Carte_BCarte_ObtenirTaille( carte )->y; j++ )
			if( !( out->m_case[ i ][ j ].m_etatExplosion = Projet_Commun_Carte_Etat_Case_AnimationExplosion_BEtatAnimationExplosion_Construire( animationBombe ) ) )
			{
				// Notifier
				NOTIFIER_ERREUR( NERREUR_ALLOCATION_FAILED );

				// Liberer
				for( k = 0; k < i; k++ )
					for( l = 0; l < ( ( ( k == i - 1 ) ) ? j : Projet_Commun_Carte_BCarte_ObtenirTaille( out->m_carte )->y ); l++ )
						Projet_Commun_Carte_Etat_Case_AnimationExplosion_BEtatAnimationExplosion_Detruire( &out->m_case[ k ][ l ].m_etatExplosion );
				NLib_Mutex_NMutex_Detruire( &out->m_mutex );
				for( i = 0; i < Projet_Commun_Carte_BCarte_ObtenirTaille( out->m_carte )->x; i++ )
					NFREE( out->m_case[ i ] );
				NFREE( out->m_case );
				NFREE( out );

				// Quitter
				return NULL;
			}

	// Zero
	out->m_dernierIdentifiant = 0;

	// OK
	return out;
}

/* Detruire */
void Projet_Commun_Carte_Etat_BEtatCarte_Detruire( BEtatCarte **this )
{
	// Iterateurs
	NU32 i = 0,
		j;

	// Detruire mutex
	NLib_Mutex_NMutex_Detruire( &(*this)->m_mutex );

	// Detruire cases
	for( ; i < Projet_Commun_Carte_BCarte_ObtenirTaille( (*this)->m_carte )->x; i++ )
	{
		// Bombe/Animation/Bonus
		for( j = 0; j < Projet_Commun_Carte_BCarte_ObtenirTaille( (*this)->m_carte )->y; j++ )
		{
			// Bombe
			if( (*this)->m_case[ i ][ j ].m_etatBombe != NULL )
				Projet_Commun_Carte_Etat_Case_Bombe_BEtatBombe_Detruire( &(*this)->m_case[ i ][ j ].m_etatBombe );

			// Bonus
			if( (*this)->m_case[ i ][ j ].m_etatBonus != NULL )
				Projet_Commun_Carte_Etat_Case_Bonus_BBonusCase_Detruire( &(*this)->m_case[ i ][ j ].m_etatBonus );

			// Animations
			Projet_Commun_Carte_Etat_Case_AnimationExplosion_BEtatAnimationExplosion_Detruire( &(*this)->m_case[ i ][ j ].m_etatExplosion );
		}

		// Liberer
		NFREE( (*this)->m_case[ i ] );
	}

	// Liberer
	NFREE( *this );
}

/* Obtenir carte */
const BCarte *Projet_Commun_Carte_Etat_BEtatCarte_ObtenirCarte( const BEtatCarte *this )
{
	return this->m_carte;
}

/* Obtenir taille */
const NUPoint *Projet_Commun_Carte_Etat_BEtatCarte_ObtenirTaille( const BEtatCarte *this )
{
	return Projet_Commun_Carte_BCarte_ObtenirTaille( this->m_carte );
}

/* Obtenir cases */
const BCaseEtatCarte **Projet_Commun_Carte_Etat_BEtatCarte_ObtenirCase( const BEtatCarte *this )
{
	return (const BCaseEtatCarte**)this->m_case;
}

/* Generer bloc */
NBOOL Projet_Commun_Carte_Etat_BEtatCarte_GenererBloc( BEtatCarte *this )
{
	// Iterateur
	NU32 i;

	// Position
	NUPoint position;

	// Nombre de blocs a remplir
	NU32 nombreBlocARemplir;

	// Cases
	const BBloc **cases;

	// Obtenir cases
	if( !( cases = Projet_Commun_Carte_BCarte_ObtenirCases( this->m_carte ) ) )
	{
		// Notifier
		NOTIFIER_ERREUR( NERREUR_OBJECT_STATE );

		// Quitter
		return NFALSE;
	}

	// Compter le nombre de bloc remplissable
	nombreBlocARemplir = Projet_Commun_Carte_BCarte_CompterBlocRemplissable( this->m_carte ) / BFACTEUR_REDUCTION_BLOC_REMPLISSABLE_CARTE;

	// Remplir
	for( i = 0; i < nombreBlocARemplir; i++ )
	{
		// Chercher une position inoccupee et remplissable
		do
		{
			// Obtenir une position aleatoire
			NDEFINIR_POSITION( position,
				NLib_Temps_ObtenirNombreAleatoire( )%Projet_Commun_Carte_BCarte_ObtenirTaille( this->m_carte )->x,
				NLib_Temps_ObtenirNombreAleatoire( )%Projet_Commun_Carte_BCarte_ObtenirTaille( this->m_carte )->y );
		} while( this->m_case[ position.x ][ position.y ].m_estRempli == NTRUE
			|| cases[ position.x ][ position.y ].m_type != BTYPE_BLOC_REMPLISSABLE );

		// Remplir la case
		this->m_case[ position.x ][ position.y ].m_estRempli = NTRUE;
	}

	// OK
	return NTRUE;
}

NBOOL Projet_Commun_Carte_Etat_BEtatCarte_GenererBloc2( BEtatCarte *this,
	const NBOOL **estCaseRemplie )
{
	// Iterateurs
	NU32 x = 0,
		y;

	// Copier
	for( ; x < Projet_Commun_Carte_BCarte_ObtenirTaille( this->m_carte )->x; x++ )
		for( y = 0; y < Projet_Commun_Carte_BCarte_ObtenirTaille( this->m_carte )->y; y++ )
			this->m_case[ x ][ y ].m_estRempli = estCaseRemplie[ x ][ y ];

	// OK
	return NTRUE;
}

/* Poser bombe */
NU32 Projet_Commun_Carte_Etat_BEtatCarte_PoserBombeInterne( BEtatCarte *this,
	NSPoint position,
	NU32 identifiantJoueur,
	NU32 dureeVie,
	NU32 tempsPose,
	NU32 puissance,
	const void *ensembleAnimation,
	NU32 identifiantBombe )
{
	// Sortie
	__OUTPUT NU32 sortie;

	// Verifier
	if( this->m_case[ position.x ][ position.y ].m_etatBombe != NULL )
	{
		// Notifier
		NOTIFIER_AVERTISSEMENT( NERREUR_OBJECT_STATE );

		// Unlock
		NLib_Mutex_NMutex_Unlock( this->m_mutex );

		// Quitter
		return NERREUR;
	}

	// Enregistrer
	sortie = ( identifiantBombe != NERREUR ) ? identifiantBombe : this->m_dernierIdentifiant;

	// Placer la bombe
	if( !( this->m_case[ position.x ][ position.y ].m_etatBombe = Projet_Commun_Carte_Etat_Case_Bombe_BEtatBombe_Construire( ( identifiantBombe != NERREUR ) ? identifiantBombe : ( this->m_dernierIdentifiant++ ),
		identifiantJoueur,
		tempsPose,
		dureeVie,
		puissance,
		BCONTENU_ANIMATION_BOMBE_BOMBE,
		ensembleAnimation ) ) )
	{
		// Notifier
		NOTIFIER_ERREUR( NERREUR_CONSTRUCTOR_FAILED );

		// Unlock
		NLib_Mutex_NMutex_Unlock( this->m_mutex );

		// Quitter
		return NERREUR;
	}

	// OK
	return sortie;
}

NU32 Projet_Commun_Carte_Etat_BEtatCarte_PoserBombe( BEtatCarte *this,
	NSPoint position,
	NU32 identifiantJoueur,
	NU32 dureeVie,
	NU32 tempsPose,
	NU32 puissance,
	const void *ensembleAnimation )
{
	// Sortie
	__OUTPUT NU32 sortie;

	// Lock
	if( !NLib_Mutex_NMutex_Lock( this->m_mutex ) )
	{
		// Notifier
		NOTIFIER_ERREUR( NERREUR_LOCK_MUTEX );

		// Quitter
		return NERREUR;
	}

	// Obtenir sortie
	sortie = Projet_Commun_Carte_Etat_BEtatCarte_PoserBombeInterne( this,
		position,
		identifiantJoueur,
		dureeVie,
		tempsPose,
		puissance,
		ensembleAnimation,
		NERREUR );

	// Unlock
	NLib_Mutex_NMutex_Unlock( this->m_mutex );

	// OK
	return sortie;
}

NU32 Projet_Commun_Carte_Etat_BEtatCarte_PoserBombe2( BEtatCarte *this,
	NSPoint position,
	NU32 identifiantJoueur,
	NU32 dureeVie,
	NU32 tempsPose,
	NU32 puissance,
	const void *ensembleAnimation,
	NU32 identifiantBombe )
{
	// Sortie
	__OUTPUT NU32 sortie;

	// Lock
	if( !NLib_Mutex_NMutex_Lock( this->m_mutex ) )
	{
		// Notifier
		NOTIFIER_ERREUR( NERREUR_LOCK_MUTEX );

		// Quitter
		return NERREUR;
	}

	// Obtenir sortie
	sortie = Projet_Commun_Carte_Etat_BEtatCarte_PoserBombeInterne( this,
		position,
		identifiantJoueur,
		dureeVie,
		tempsPose,
		puissance,
		ensembleAnimation,
		identifiantBombe );

	// Unlock
	NLib_Mutex_NMutex_Unlock( this->m_mutex );

	// OK
	return sortie;
}

/* Poser bonus */
NU32 Projet_Commun_Carte_Etat_BEtatCarte_PoserBonusInterne( BEtatCarte *this,
	NSPoint position,
	BListeBonus type,
	NU32 duree,
	NU32 tempsPose,
	NU32 identifiant )
{
	// Sortie
	__OUTPUT NU32 sortie;

	// Verifier position
	if( !Projet_Commun_Carte_BCarte_EstPositionCorrecte( this->m_carte,
		position ) )
	{
		// Notifier
		NOTIFIER_ERREUR( NERREUR_PARAMETER_ERROR );

		// Quitter
		return NERREUR;
	}

	// Verifier case deja occupee
	if( this->m_case[ position.x ][ position.y ].m_etatBonus != NULL )
	{
		// Notifier
		NOTIFIER_AVERTISSEMENT( NERREUR_OBJECT_STATE );

		// Quitter
		return NERREUR;
	}

	// Enregistrer
	sortie = ( identifiant != NERREUR ) ? identifiant : this->m_dernierIdentifiant;

	// Placer la bombe
	if( !( this->m_case[ position.x ][ position.y ].m_etatBonus = Projet_Commun_Carte_Etat_Case_Bonus_BBonusCase_Construire( type,
		tempsPose,
		duree,
		( identifiant != NERREUR ) ? identifiant : ( this->m_dernierIdentifiant++ ) ) ) )
	{
		// Notifier
		NOTIFIER_ERREUR( NERREUR_CONSTRUCTOR_FAILED );

		// Quitter
		return NERREUR;
	}

	// OK
	return sortie;
}

NU32 Projet_Commun_Carte_Etat_BEtatCarte_PoserBonus( BEtatCarte *this,
	NSPoint position,
	BListeBonus type,
	NU32 duree,
	NU32 tempsPose )
{
	// Sortie
	__OUTPUT NU32 sortie;

	// Lock mutex
	NLib_Mutex_NMutex_Lock( this->m_mutex );

	// Ajouter
	sortie = Projet_Commun_Carte_Etat_BEtatCarte_PoserBonusInterne( this,
		position,
		type,
		duree,
		tempsPose,
		NERREUR );

	// Unlock mutex
	NLib_Mutex_NMutex_Unlock( this->m_mutex );

	// OK?
	return sortie;
}

NU32 Projet_Commun_Carte_Etat_BEtatCarte_PoserBonus2( BEtatCarte *this,
	NSPoint position,
	BListeBonus type,
	NU32 duree,
	NU32 tempsPose,
	NU32 identifiant )
{
	// Sortie
	__OUTPUT NU32 sortie;

	// Lock mutex
	NLib_Mutex_NMutex_Lock( this->m_mutex );

	// Ajouter
	sortie = Projet_Commun_Carte_Etat_BEtatCarte_PoserBonusInterne( this,
		position,
		type,
		duree,
		tempsPose,
		identifiant );

	// Unlock mutex
	NLib_Mutex_NMutex_Unlock( this->m_mutex );

	// OK?
	return sortie;
}

/* Supprimer bombe (doit etre protege) */
NBOOL Projet_Commun_Carte_Etat_BEtatCarte_SupprimerBombe( BEtatCarte *this,
	NU32 identifiantBombe )
{
	// Iterateurs
	NU32 i = 0,
		j;

	// Parcourir les cases
	for( ; i < Projet_Commun_Carte_BCarte_ObtenirTaille( this->m_carte )->x; i++ )
		for( j = 0; j < Projet_Commun_Carte_BCarte_ObtenirTaille( this->m_carte )->y; j++ )
			// Si on trouve la bombe
			if( this->m_case[ i ][ j ].m_etatBombe != NULL )
				// Si il s'agit du bon identifiant
				if( Projet_Commun_Carte_Etat_Case_Bombe_BEtatBombe_ObtenirIdentifiant( this->m_case[ i ][ j ].m_etatBombe ) == identifiantBombe )
					// Detruire
					Projet_Commun_Carte_Etat_Case_Bombe_BEtatBombe_Detruire( &this->m_case[ i ][ j ].m_etatBombe );

	// OK
	return NTRUE;
}

/* Supprimer bonus */
NBOOL Projet_Commun_Carte_Etat_BEtatCarte_DisparaitreBonus( BEtatCarte *this,
	NU32 identifiantBonus )
{
	// Iterateurs
	NU32 x = 0,
		y;

	// Parcourir carte
	for( ; x < Projet_Commun_Carte_BCarte_ObtenirTaille( this->m_carte )->x; x++ )
		for( y = 0; y < Projet_Commun_Carte_BCarte_ObtenirTaille( this->m_carte )->y; y++ )
			if( this->m_case[ x ][ y ].m_etatBonus != NULL
				&& Projet_Commun_Carte_Etat_Case_Bonus_BBonusCase_ObtenirIdentifiant( this->m_case[ x ][ y ].m_etatBonus ) == identifiantBonus )
				// Detruire
				Projet_Commun_Carte_Etat_Case_Bonus_BBonusCase_Detruire( &this->m_case[ x ][ y ].m_etatBonus );

	// OK
	return NTRUE;
}

/* Obtenir position bombe */
NBOOL Projet_Commun_Carte_Etat_BEtatCarte_ObtenirPositionBombe( const BEtatCarte *this,
	NU32 identifiantBombe,
	__OUTPUT NSPoint *position )
{
	// Iterateurs
	NU32 i = 0,
		j;

	// Proteger
	NLib_Mutex_NMutex_Lock( this->m_mutex );

	// Parcourir les cases
	for( ; i < Projet_Commun_Carte_BCarte_ObtenirTaille( this->m_carte )->x; i++ )
		for( j = 0; j < Projet_Commun_Carte_BCarte_ObtenirTaille( this->m_carte )->y; j++ )
			// Si on trouve la bombe
			if( this->m_case[ i ][ j ].m_etatBombe != NULL )
				// Si il s'agit du bon identifiant
				if( Projet_Commun_Carte_Etat_Case_Bombe_BEtatBombe_ObtenirIdentifiant( this->m_case[ i ][ j ].m_etatBombe ) == identifiantBombe )
				{
					// Enregistrer
					NDEFINIR_POSITION( (*position),
						i,
						j );

					// Ne plus proteger
					NLib_Mutex_NMutex_Unlock( this->m_mutex );

					// OK
					return NTRUE;
				}

	// Ne plus proteger
	NLib_Mutex_NMutex_Unlock( this->m_mutex );

	// Introuvable
	return NFALSE;
}

/* Update */
void Projet_Commun_Carte_Etat_BEtatCarte_Update( BEtatCarte *this )
{
	// Iterateurs
	NU32 x = 0,
		y;

	// Lock
	NLib_Mutex_NMutex_Lock( this->m_mutex );

	// Update bombes/Animations
	for( ; x < Projet_Commun_Carte_BCarte_ObtenirTaille( this->m_carte )->x; x++ )
		for( y = 0; y < Projet_Commun_Carte_BCarte_ObtenirTaille( this->m_carte )->y; y++ )
		{
			// Bombe
			if( this->m_case[ x ][ y ].m_etatBombe != NULL )
				Projet_Commun_Carte_Etat_Case_Bombe_BEtatBombe_Update( this->m_case[ x ][ y ].m_etatBombe );

			// Bonus
			if( this->m_case[ x ][ y ].m_etatBonus != NULL )
				Projet_Commun_Carte_Etat_Case_Bonus_BBonusCase_Update( this->m_case[ x ][ y ].m_etatBonus );

			// Animation
			Projet_Commun_Carte_Etat_Case_AnimationExplosion_BEtatAnimationExplosion_Update( this->m_case[ x ][ y ].m_etatExplosion );
		}

	// Unlock
	NLib_Mutex_NMutex_Unlock( this->m_mutex );
}

/* Proteger etat */
NBOOL Projet_Commun_Carte_Etat_BEtatCarte_Proteger( BEtatCarte *this )
{
	return NLib_Mutex_NMutex_Lock( this->m_mutex );
}

/* Ne plus proteger */
void Projet_Commun_Carte_Etat_BEtatCarte_NePlusProteger( BEtatCarte *this )
{
	NLib_Mutex_NMutex_Unlock( this->m_mutex );
}

