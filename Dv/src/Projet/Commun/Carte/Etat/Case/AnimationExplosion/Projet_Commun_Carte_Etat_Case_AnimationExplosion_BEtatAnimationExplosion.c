#include "../../../../../../../include/Projet/Projet.h"

// -------------------------------------------------------------------------------------
// struct Projet::Commun::Carte::Etat::Case::AnimationExplosion::BEtatAnimationExplosion
// -------------------------------------------------------------------------------------

/* Construire */
__ALLOC BEtatAnimationExplosion *Projet_Commun_Carte_Etat_Case_AnimationExplosion_BEtatAnimationExplosion_Construire( const NAnimation *animationBombe )
{
	// Sortie
	__OUTPUT BEtatAnimationExplosion *out;

	// Allouer la memoire
	if( !( out = calloc( 1,
		sizeof( BEtatAnimationExplosion ) ) ) )
	{
		// Notifier
		NOTIFIER_ERREUR( NERREUR_ALLOCATION_FAILED );

		// Quitter
		return NULL;
	}

	// Construire etat animation
	if( !( out->m_etat = NLib_Temps_Animation_NEtatAnimation_Construire( NLib_Temps_Animation_NEtatAnimation_ObtenirDelaiFrame( NLib_Module_SDL_Surface_NAnimation_ObtenirEtat( animationBombe ) ),
		NLib_Temps_Animation_NEtatAnimation_ObtenirNombreFrame( NLib_Module_SDL_Surface_NAnimation_ObtenirEtat( animationBombe ) ),
		NLib_Temps_Animation_NEtatAnimation_ObtenirNombreEtapeAnimation( NLib_Module_SDL_Surface_NAnimation_ObtenirEtat( animationBombe ) ),
		NLib_Temps_Animation_NEtatAnimation_ObtenirOrdreAnimation( NLib_Module_SDL_Surface_NAnimation_ObtenirEtat( animationBombe ) ) ) ) )
	{
		// Notifier
		NOTIFIER_ERREUR( NERREUR_CONSTRUCTOR_FAILED );

		// Quitter
		return NULL;
	}

	// Enregistrer
	out->m_animation = animationBombe;

	// OK
	return out;
}

/* Detruire */
void Projet_Commun_Carte_Etat_Case_AnimationExplosion_BEtatAnimationExplosion_Detruire( BEtatAnimationExplosion **this )
{
	// Detruire etat animation
	NLib_Temps_Animation_NEtatAnimation_Detruire( &(*this)->m_etat );
	
	// Liberer
	NFREE( *this );
}

/* Update */
void Projet_Commun_Carte_Etat_Case_AnimationExplosion_BEtatAnimationExplosion_Update( BEtatAnimationExplosion *this )
{
	// Si pas en cours
	if( !this->m_estEnCours )
		// Quitter
		return;

	// Verifier si on est a la fin de l'animation
	if( NLib_Temps_Animation_NEtatAnimation_EstFinAnimation( this->m_etat ) )
	{
		// Plus en cours
		this->m_estEnCours = NFALSE;
		
		// Quitter
		return;
	}

	// Update animation
	NLib_Temps_Animation_NEtatAnimation_Update( this->m_etat );
}

/* Activer l'animation */
void Projet_Commun_Carte_Etat_Case_AnimationExplosion_BEtatAnimationExplosion_ActiverAnimation( BEtatAnimationExplosion *this,
	NU32 typeAnimation,
	NU32 identifiantJoueur )
{
	// Remettre l'etat a zero
	NLib_Temps_Animation_NEtatAnimation_RemettreAZero( this->m_etat );

	// Enregistrer
	this->m_typeAnimation = typeAnimation;
	this->m_identifiantJoueur = identifiantJoueur;

	// Est maintenant en cours
	this->m_estEnCours = NTRUE;
}

/* Obtenir animation */
const NAnimation *Projet_Commun_Carte_Etat_Case_AnimationExplosion_BEtatAnimationExplosion_ObtenirAnimation( const BEtatAnimationExplosion *this )
{
	return this->m_animation;
}

/* Obtenir type animation */
NU32 Projet_Commun_Carte_Etat_Case_AnimationExplosion_BEtatAnimationExplosion_ObtenirTypeAnimation( const BEtatAnimationExplosion *this )
{
	return this->m_typeAnimation;
}

/* Obtenir etat animation */
NU32 Projet_Commun_Carte_Etat_Case_AnimationExplosion_BEtatAnimationExplosion_ObtenirFrameAnimation( const BEtatAnimationExplosion *this )
{
	return NLib_Temps_Animation_NEtatAnimation_ObtenirFrame( this->m_etat );
}

/* Obtenir identifiant joueur */
NU32 Projet_Commun_Carte_Etat_Case_AnimationExplosion_BEtatAnimationExplosion_ObtenirIdentifiantJoueur( const BEtatAnimationExplosion *this )
{
	return this->m_identifiantJoueur;
}

/* Est en cours? */
NBOOL Projet_Commun_Carte_Etat_Case_AnimationExplosion_BEtatAnimationExplosion_EstEnCours( const BEtatAnimationExplosion *this )
{
	return this->m_estEnCours;
}

