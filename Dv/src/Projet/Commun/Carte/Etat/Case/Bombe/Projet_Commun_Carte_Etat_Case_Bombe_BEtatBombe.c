#include "../../../../../../../include/Projet/Projet.h"

// -----------------------------------------------------------
// struct Projet::Commun::Carte::Etat::Case::Bombe::BEtatBombe
// -----------------------------------------------------------

/* Construire */
__ALLOC BEtatBombe *Projet_Commun_Carte_Etat_Case_Bombe_BEtatBombe_Construire( NU32 identifiantBombe,
	NU32 joueur,
	NU32 tempsPose,
	NU32 dureeVie,
	NU32 puissance,
	NU32 typeAnimation,
	const void *ensembleAnimation )
{
	return Projet_Commun_Carte_Etat_Case_Bombe_BEtatBombe_Construire2( identifiantBombe,
		joueur,
		tempsPose,
		dureeVie,
		puissance,
		typeAnimation,
		ensembleAnimation,
		NULL );
}

__ALLOC BEtatBombe *Projet_Commun_Carte_Etat_Case_Bombe_BEtatBombe_Construire2( NU32 identifiantBombe,
	NU32 joueur,
	NU32 tempsPose,
	NU32 dureeVie,
	NU32 puissance,
	NU32 typeAnimation,
	const void *ensembleAnimation,
	const NEtatAnimation *etatAnimationExterne )
{
	// Sortie
	__OUTPUT BEtatBombe *out;

	// Allouer
	if( !( out = calloc( 1,
		sizeof( BEtatBombe ) ) ) )
	{
		// Notifier
		NOTIFIER_ERREUR( NERREUR_ALLOCATION_FAILED );

		// Quitter
		return NULL;
	}

	// Etat animation externe
	if( etatAnimationExterne != NULL )
	{
		// Enregistrer
		out->m_etatAnimation = (NEtatAnimation*)etatAnimationExterne;

		// Est externe
		out->m_estAnimationExterne = NTRUE;
	}
	// Etat animation interne
	else
		// Construire etat
		if( !( out->m_etatAnimation = NLib_Temps_Animation_NEtatAnimation_Construire( NLib_Temps_Animation_NEtatAnimation_ObtenirDelaiFrame( NLib_Module_SDL_Surface_NAnimation_ObtenirEtat( Projet_Commun_Ressource_Animation_BEnsembleAnimation_Obtenir( ensembleAnimation,
				BLISTE_ANIMATION_BOMBE ) ) ),
			NLib_Temps_Animation_NEtatAnimation_ObtenirNombreFrame( NLib_Module_SDL_Surface_NAnimation_ObtenirEtat( Projet_Commun_Ressource_Animation_BEnsembleAnimation_Obtenir( ensembleAnimation,
				BLISTE_ANIMATION_BOMBE ) ) ),
			NLib_Temps_Animation_NEtatAnimation_ObtenirNombreEtapeAnimation( NLib_Module_SDL_Surface_NAnimation_ObtenirEtat( Projet_Commun_Ressource_Animation_BEnsembleAnimation_Obtenir( ensembleAnimation,
				BLISTE_ANIMATION_BOMBE ) ) ),
			NLib_Temps_Animation_NEtatAnimation_ObtenirOrdreAnimation( NLib_Module_SDL_Surface_NAnimation_ObtenirEtat( Projet_Commun_Ressource_Animation_BEnsembleAnimation_Obtenir( ensembleAnimation,
				BLISTE_ANIMATION_BOMBE ) ) ) ) ) )
		{
			// Notifier
			NOTIFIER_ERREUR( NERREUR_CONSTRUCTOR_FAILED );

			// Liberer
			NFREE( out );

			// Quitter
			return NULL;
		}

	// Enregistrer
	out->m_identifiantJoueur = joueur;
	out->m_dureeVie = dureeVie;
	out->m_tempsPose = tempsPose;
	out->m_puissance = puissance;
	out->m_identifiantBombe = identifiantBombe;
	out->m_typeAnimation = typeAnimation;

	// OK
	return out;
}

/* Detruire */
void Projet_Commun_Carte_Etat_Case_Bombe_BEtatBombe_Detruire( BEtatBombe **this )
{
	// Si l'etat animation n'est pas externe
	if( !(*this)->m_estAnimationExterne )
		NLib_Temps_Animation_NEtatAnimation_Detruire( &(*this)->m_etatAnimation );

	// Liberer
	NFREE( *this );
}

/* Update */
void Projet_Commun_Carte_Etat_Case_Bombe_BEtatBombe_Update( BEtatBombe *this )
{
	// Verifier animation externe
	if( !this->m_estAnimationExterne )
		// Update animation
		NLib_Temps_Animation_NEtatAnimation_Update( this->m_etatAnimation );
}

/* Obtenir identifiant joueur */
NU32 Projet_Commun_Carte_Etat_Case_Bombe_BEtatBombe_ObtenirIdentifiantJoueur( const BEtatBombe *this )
{
	return this->m_identifiantJoueur;
}

/* Obtenir temps de pose */
NU32 Projet_Commun_Carte_Etat_Case_Bombe_BEtatBombe_ObtenirTempsPose( const BEtatBombe *this )
{
	return this->m_tempsPose;
}

/* Obtenir duree de vie */
NU32 Projet_Commun_Carte_Etat_Case_Bombe_BEtatBombe_ObtenirDureeVie( const BEtatBombe *this )
{
	return this->m_dureeVie;
}

/* Obtenir la puissance */
NU32 Projet_Commun_Carte_Etat_Case_Bombe_BEtatBombe_ObtenirPuissance( const BEtatBombe *this )
{
	return this->m_puissance;
}

/* Obtenir identifiant bombe */
NU32 Projet_Commun_Carte_Etat_Case_Bombe_BEtatBombe_ObtenirIdentifiant( const BEtatBombe *this )
{
	return this->m_identifiantBombe;
}

/* Obtenir type */
NU32 Projet_Commun_Carte_Etat_Case_Bombe_BEtatBombe_ObtenirTypeAnimation( const BEtatBombe *this )
{
	return this->m_typeAnimation;
}

/* Obtenir frame animation */
NU32 Projet_Commun_Carte_Etat_Case_Bombe_BEtatBombe_ObtenirFrame( const BEtatBombe *this )
{
	return NLib_Temps_Animation_NEtatAnimation_ObtenirFrame( this->m_etatAnimation );
}

