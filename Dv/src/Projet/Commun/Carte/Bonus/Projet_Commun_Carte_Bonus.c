#include "../../../../../include/Projet/Projet.h"

// --------------------------------------
// namespace Projet::Commun::Carte::Bonus
// --------------------------------------

/* Gerer la prise de bonus */
void Projet_Commun_Carte_Bonus_GererPriseBonus( BEtatClient *joueur,
	BListeBonus bonus )
{
	// Puissance
	NU32 puissance;

	// Obtenir puissance
	puissance = Projet_Commun_Reseau_Client_BEtatClient_ObtenirPuissance( joueur );

	// Traiter le bonus
	switch( bonus )
	{
		case BLISTE_BONUS_AUGMENTE_PUISSANCE:
			Projet_Commun_Reseau_Client_BEtatClient_DefinirPuissance( joueur,
				( puissance + 1 ) >= BPUISSANCE_MAXIMALE_JOUEUR ?
					BPUISSANCE_MAXIMALE_JOUEUR
					: puissance + 1 );
			break;
		case BLISTE_BONUS_DIMINUE_PUISSANCE:
			Projet_Commun_Reseau_Client_BEtatClient_DefinirPuissance( joueur,
				(NS32)( puissance - 1 ) >= BPUISSANCE_MINIMALE_JOUEUR ?
					puissance - 1
					: BPUISSANCE_MINIMALE_JOUEUR );
			break;
		case BLISTE_BONUS_PUISSANCE_MAXIMALE:
			Projet_Commun_Reseau_Client_BEtatClient_DefinirPuissance( joueur,
				BPUISSANCE_MAXIMALE_JOUEUR );
			break;
		case BLISTE_BONUS_AUGMENTE_NOMBRE_BOMBE:
			Projet_Commun_Reseau_Client_BEtatClient_IncrementerNombreBombeMaximum( joueur );
			break;

		default:
			break;
	}
}

