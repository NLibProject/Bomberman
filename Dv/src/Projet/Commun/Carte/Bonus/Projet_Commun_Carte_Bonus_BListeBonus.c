#define PROJET_COMMUN_CARTE_BONUS_BLISTEBONUS_INTERNE
#include "../../../../../include/Projet/Projet.h"

// ----------------------------------------------
// enum Projet::Commun::Carte::Bonus::BListeBonus
// ----------------------------------------------

/* Obtenir ensemble probabilite bonus */
const NU32 *Projet_Commun_carte_Bonus_BListeBonus_ObtenirEnsembleProbabilite( void )
{
	return BPROBABILITE_SPAWN_BONUS;
}

