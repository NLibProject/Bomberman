#include "../../../../include/Projet/Projet.h"

/*
	@author SOARES Lucas
*/

// ------------------------------------
// struct Projet::Commun::Carte::BCarte
// ------------------------------------

/* Calculer checksum (privee) */
NU32 Projet_Commun_Carte_BCarte_CalculerChecksumInterne( const BCarte *this )
{
	// Sortie
	__OUTPUT NU32 resultat = 0;

	// Iterateurs
	NU32 i, j, k;

	// Ajouter
	for( j = 0; j < this->m_taille.x; j++ )
		for( k = 0; k < this->m_taille.y; k++ )
			for( i = 0; i < BCOUCHES_CARTE; i++ )
				if( this->m_case[ j ][ k ].m_tileset[ i ] != BBLOC_CARTE_AUCUN )
					resultat += ( this->m_case[ j ][ k ].m_tileset[ i ] + ( this->m_case[ j ][ k ].m_positionTileset[ i ].x + this->m_case[ j ][ k ].m_positionTileset[ i ].y ) + this->m_case[ j ][ k ].m_type ) * 5;

	// Ajouter details
	resultat += this->m_taille.x * this->m_taille.y;
	resultat += this->m_handle * 2;
	resultat += this->m_blocRemplissage.x * this->m_blocRemplissage.y;
	resultat += this->m_tilesetBlocRemplissage * 9;

	// Ajouter position depart
	for( i = 0; i < BNOMBRE_MAXIMUM_JOUEUR; i++ )
		resultat += this->m_positionDepart[ i ].x * this->m_positionDepart[ i ].y;

	// Multiplier par l'identifiant
	resultat *= ( this->m_handle + 2 );

	// OK
	return resultat;
}

/* Construire */
__ALLOC BCarte *Projet_Commun_Carte_BCarte_Construire( NU32 handle,
	const char *nom,
	NUPoint taille )
{
	// Sortie
	__OUTPUT BCarte *out;

	// Iterateurs
	NU32 i, j, k;

	// Verifier
	if( !taille.x
		|| !taille.y
		|| taille.x >= BTAILLE_MAXIMUM_CARTE.x
		|| taille.y >= BTAILLE_MAXIMUM_CARTE.y )
	{
		// Notifier
		NOTIFIER_ERREUR( NERREUR_PARAMETER_ERROR );

		// Quitter
		return NULL;
	}

	// Allouer la memoire
	if( !( out = calloc( 1,
		sizeof( BCarte ) ) ) )
	{
		// Notifier
		NOTIFIER_ERREUR( NERREUR_ALLOCATION_FAILED );

		// Quitter
		return NULL;
	}

	// Copier le nom
		// Allouer la memoire
			if( !( out->m_nom = calloc( strlen( nom ) + 1,
				sizeof( char ) ) ) )
			{
				// Notifier
				NOTIFIER_ERREUR( NERREUR_ALLOCATION_FAILED );

				// Liberer
				NFREE( out );

				// Quitter
				return NULL;
			}
		// Copier
			memcpy( out->m_nom,
				nom,
				strlen( nom ) );

	// Allouer les cases
		// 1D
			if( !( out->m_case = calloc( taille.x,
				sizeof( BBloc* ) ) ) )
			{
				// Notifier
				NOTIFIER_ERREUR( NERREUR_ALLOCATION_FAILED );

				// Liberer le nom
				NFREE( out->m_nom );

				// Liberer
				NFREE( out );

				// Quitter
				return NULL;
			}
		// 2D
			for( i = 0; i < taille.x; i++ )
				if( !( out->m_case[ i ] = calloc( taille.y,
					sizeof( BBloc ) ) ) )
				{
					// Notifier
					NOTIFIER_ERREUR( NERREUR_ALLOCATION_FAILED );

					// Liberer bloc
					NFREE( out->m_case );

					// Liberer le nom
					NFREE( out->m_nom );

					// Liberer
					NFREE( out );

					// Quitter
					return NULL;
				}

	// Enregistrer
	out->m_taille = taille;
	out->m_handle = handle;

	// Zero
	for( i = 0; i < taille.x; i++ )
		for( j = 0; j < taille.y; j++ )
			for( k = 0; k < BCOUCHES_CARTE; k++ )
			{
				// Tileset
				out->m_case[ i ][ j ].m_tileset[ k ] = BBLOC_CARTE_AUCUN;

				// Bloc
				NDEFINIR_POSITION( out->m_case[ i ][ j ].m_positionTileset[ k ],
					BBLOC_CARTE_AUCUN,
					BBLOC_CARTE_AUCUN );
			}
	out->m_tilesetBlocRemplissage = BBLOC_CARTE_AUCUN;
	out->m_tilesetBlocBordure = BBLOC_CARTE_AUCUN;
	NDEFINIR_POSITION( out->m_blocRemplissage,
		BBLOC_CARTE_AUCUN,
		BBLOC_CARTE_AUCUN );
	NDEFINIR_POSITION( out->m_blocBordure,
		BBLOC_CARTE_AUCUN,
		BBLOC_CARTE_AUCUN );

	// OK
	return out;
}

__ALLOC BCarte *Projet_Commun_Carte_BCarte_Construire2( const char *lien )
{
	// Sortie
	__OUTPUT BCarte *out;

	// Fichier
	NFichierBinaire *fichier;

	// Iterateurs
	NU32 x, y;

	// Header
	char header[ BTAILLE_HEADER_FICHIER_CARTE + 1 ] = { 0, };

	// Taille nom
	NU32 tailleNom;

	// Allouer la memoire
	if( !( out = calloc( 1,
		sizeof( BCarte ) ) ) )
	{
		// Notifier
		NOTIFIER_ERREUR( NERREUR_ALLOCATION_FAILED );

		// Quitter
		return NULL;
	}

	// Ouvrir
	if( !( fichier = NLib_Fichier_NFichierBinaire_ConstruireLecture( lien ) ) )
	{
		// Notifier
		NOTIFIER_ERREUR( NERREUR_CONSTRUCTOR_FAILED );

		// Liberer
		NFREE( out );

		// Quitter
		return NULL;
	}

	// Vider
	memset( header,
		0,
		BTAILLE_HEADER_FICHIER_CARTE );

	// Lire
		// Header
			if( !NLib_Fichier_NFichierBinaire_Lire2( fichier,
				header,
				BTAILLE_HEADER_FICHIER_CARTE )
				|| strcmp( BHEADER_FICHIER_CARTE,
					header )
		// Handle
				|| !NLib_Fichier_NFichierBinaire_Lire2( fichier,
					(char*)&out->m_handle,
					sizeof( NU32 ) )
		// Taille nom
				|| !NLib_Fichier_NFichierBinaire_Lire2( fichier,
					(char*)&tailleNom,
					sizeof( NU32 ) )
				|| !( out->m_nom = calloc( tailleNom + 1,
					sizeof( char ) ) )
		// Nom
				|| !NLib_Fichier_NFichierBinaire_Lire2( fichier,
					out->m_nom,
					tailleNom )
		// Taille
			// x
				|| !NLib_Fichier_NFichierBinaire_Lire2( fichier,
					(char*)&out->m_taille.x,
					sizeof( NU32 ) )
			// y
				|| !NLib_Fichier_NFichierBinaire_Lire2( fichier,
					(char*)&out->m_taille.y,
					sizeof( NU32 ) )
		// Musique
				|| !NLib_Fichier_NFichierBinaire_Lire2( fichier,
					(char*)&out->m_musique,
					sizeof( BListeMusique ) )
		// Bloc de remplissage
			// Tileset
				|| !NLib_Fichier_NFichierBinaire_Lire2( fichier,
					(char*)&out->m_tilesetBlocRemplissage,
					sizeof( NU32 ) ) )
			{
				// Notifier
				NOTIFIER_ERREUR( NERREUR_FILE_CANT_READ );

				// Liberer
				NFREE( out->m_nom );
				NFREE( out );

				// Detruire le fichier
				NLib_Fichier_NFichierBinaire_Detruire( &fichier );

				// Quitter
				return NULL;
			}
		// Position tileset bloc remplissage
			if( out->m_tilesetBlocRemplissage != BBLOC_CARTE_AUCUN )
			{
				// x
					if( !NLib_Fichier_NFichierBinaire_Lire2( fichier,
						(char*)&out->m_blocRemplissage.x,
						sizeof( NU32 ) )
				// y
					|| !NLib_Fichier_NFichierBinaire_Lire2( fichier,
						(char*)&out->m_blocRemplissage.y,
						sizeof( NU32 ) ) )
				{
					// Notifier
					NOTIFIER_ERREUR( NERREUR_FILE_CANT_READ );

					// Liberer
					NFREE( out->m_nom );
					NFREE( out );

					// Detruire le fichier
					NLib_Fichier_NFichierBinaire_Detruire( &fichier );

					// Quitter
					return NULL;
				}
			}
		// Bloc de bordure
			// Tileset
				if( !NLib_Fichier_NFichierBinaire_Lire2( fichier,
					(char*)&out->m_tilesetBlocBordure,
					sizeof( NU32 ) ) )
				{
					// Notifier
					NOTIFIER_ERREUR( NERREUR_FILE_CANT_READ );

					// Liberer
					NFREE( out->m_nom );
					NFREE( out );

					// Detruire le fichier
					NLib_Fichier_NFichierBinaire_Detruire( &fichier );

					// Quitter
					return NULL;
				}
			// Position tileset bloc bordure
				if( out->m_tilesetBlocBordure != BBLOC_CARTE_AUCUN )
				{
					// x
					if( !NLib_Fichier_NFichierBinaire_Lire2( fichier,
							(char*)&out->m_blocBordure.x,
							sizeof( NU32 ) )
					// y
						|| !NLib_Fichier_NFichierBinaire_Lire2( fichier,
							(char*)&out->m_blocBordure.y,
							sizeof( NU32 ) ) )
					{
						// Notifier
						NOTIFIER_ERREUR( NERREUR_FILE_CANT_READ );

						// Liberer
						NFREE( out->m_nom );
						NFREE( out );

						// Detruire le fichier
						NLib_Fichier_NFichierBinaire_Detruire( &fichier );

						// Quitter
						return NULL;
					}
				}
		// Position de depart
			for( x = 0; x < BNOMBRE_MAXIMUM_JOUEUR; x++ )
				if( !NLib_Fichier_NFichierBinaire_Lire2( fichier,
					(char*)&out->m_positionDepart[ x ].x,
					sizeof( NU32 ) )
					|| !NLib_Fichier_NFichierBinaire_Lire2( fichier,
						(char*)&out->m_positionDepart[ x ].y,
						sizeof( NU32 ) ) )
				{
					// Notifier
					NOTIFIER_ERREUR( NERREUR_FILE_CANT_READ );

					// Liberer
					NFREE( out->m_nom );
					NFREE( out );

					// Detruire le fichier
					NLib_Fichier_NFichierBinaire_Detruire( &fichier );

					// Quitter
					return NULL;
				}

	// Allouer les cases
		// 1D
			if( !( out->m_case = calloc( out->m_taille.x,
				sizeof( BBloc* ) ) ) )
			{
				// Notifier
				NOTIFIER_ERREUR( NERREUR_ALLOCATION_FAILED );

				// Liberer
				NFREE( out->m_nom );
				NFREE( out );

				// Detruire le fichier
				NLib_Fichier_NFichierBinaire_Detruire( &fichier );

				// Quitter
				return NULL;
			}
		// 2D
			for( x = 0; x < out->m_taille.x; x++ )
				if( !( out->m_case[ x ] = calloc( out->m_taille.y,
					sizeof( BBloc ) ) ) )
				{
					// Notifier
					NOTIFIER_ERREUR( NERREUR_ALLOCATION_FAILED );

					// Liberer
					for( y = 0; y < x; y++ )
						NFREE( out->m_case[ y ] );
					NFREE( out->m_case );
					NFREE( out->m_nom );
					NFREE( out );

					// Quitter
					return NULL;
				}

	// Enregistrer les cases
	for( x = 0; x < out->m_taille.x; x++ )
		for( y = 0; y < out->m_taille.y; y++ )
			if( !Projet_Commun_Carte_Bloc_BBloc_Charger( &out->m_case[ x ][ y ],
				fichier ) )
			{
				// Notifier
				NOTIFIER_ERREUR( NERREUR_FILE_CANT_READ );

				// Liberer
				for( x = 0; x < out->m_taille.x; x++ )
					NFREE( out->m_case[ x ] );
				NFREE( out->m_case );
				NFREE( out->m_nom );
				NFREE( out );

				// Detruire le fichier
				NLib_Fichier_NFichierBinaire_Detruire( &fichier );

				// Quitter
				return NULL;
			}

	// Fermer le fichier
	NLib_Fichier_NFichierBinaire_Detruire( &fichier );

	// Calculer checksum
	out->m_checksum = Projet_Commun_Carte_BCarte_CalculerChecksumInterne( out );

	// OK
	return out;
}

/* Detruire */
void Projet_Commun_Carte_BCarte_Detruire( BCarte **this )
{
	// Iterateur
	NU32 i;

	// Detruire les blocs
	for( i = 0; i < (*this)->m_taille.x; i++ )
		NFREE( (*this)->m_case[ i ] );
	NFREE( (*this)->m_case );

	// Detruire le nom
	NFREE( (*this)->m_nom );

	// Liberer
	NFREE( *this );
}

/* Sauvegarder */
NBOOL Projet_Commun_Carte_BCarte_Sauvegarder( const BCarte *this,
	const char *lien )
{
	// Fichier
	NFichierBinaire *fichier;

	// Taille nom
	NU32 tailleNom;

	// Iterateurs
	NU32 x, y;

	// Ouvrir
	if( !( fichier = NLib_Fichier_NFichierBinaire_ConstruireEcriture( lien,
		NTRUE ) ) )
	{
		// Notifier
		NOTIFIER_ERREUR( NERREUR_FILE_CANNOT_BE_OPENED );

		// Quitter
		return NFALSE;
	}

	// Recuperer la taille du nom
	tailleNom = strlen( this->m_nom );

	// Ecrire
		// Header
			if( !NLib_Fichier_NFichierBinaire_Ecrire( fichier,
				BHEADER_FICHIER_CARTE,
				BTAILLE_HEADER_FICHIER_CARTE )
		// Handle
				|| !NLib_Fichier_NFichierBinaire_Ecrire( fichier,
					(char*)&this->m_handle,
					sizeof( NU32 ) )
		// Taille nom
				|| !NLib_Fichier_NFichierBinaire_Ecrire( fichier,
					(char*)&tailleNom,
					sizeof( NU32 ) )
		// Nom
				|| !NLib_Fichier_NFichierBinaire_Ecrire( fichier,
					this->m_nom,
					tailleNom )
		// Taille
			// x
				|| !NLib_Fichier_NFichierBinaire_Ecrire( fichier,
					(char*)&this->m_taille.x,
					sizeof( NU32 ) )
			// y
				|| !NLib_Fichier_NFichierBinaire_Ecrire( fichier,
					(char*)&this->m_taille.y,
					sizeof( NU32 ) )
		// Musique
				|| !NLib_Fichier_NFichierBinaire_Ecrire( fichier,
					(char*)&this->m_musique,
					sizeof( BListeMusique ) )
		// Bloc de remplissage
			// Tileset
				|| !NLib_Fichier_NFichierBinaire_Ecrire( fichier,
					(char*)&this->m_tilesetBlocRemplissage,
					sizeof( NU32 ) ) )
			{
				// Notifier
				NOTIFIER_ERREUR( NERREUR_FILE_CANT_WRITE );

				// Detruire le fichier
				NLib_Fichier_NFichierBinaire_Detruire( &fichier );

				// Quitter
				return NFALSE;
			}
		// Position bloc de remplissage
			if( this->m_tilesetBlocRemplissage != BBLOC_CARTE_AUCUN )
			{
				// x
				if( !NLib_Fichier_NFichierBinaire_Ecrire( fichier,
					(char*)&this->m_blocRemplissage.x,
					sizeof( NU32 ) )
				// y
					|| !NLib_Fichier_NFichierBinaire_Ecrire( fichier,
						(char*)&this->m_blocRemplissage.y,
						sizeof( NU32 ) ) )
				{
					// Notifier
					NOTIFIER_ERREUR( NERREUR_FILE_CANT_WRITE );

					// Detruire le fichier
					NLib_Fichier_NFichierBinaire_Detruire( &fichier );

					// Quitter
					return NFALSE;
				}
			}
		// Bloc de bordure
			// Tileset
				if( !NLib_Fichier_NFichierBinaire_Ecrire( fichier,
					(char*)&this->m_tilesetBlocBordure,
					sizeof( NU32 ) ) )
				{
					// Notifier
					NOTIFIER_ERREUR( NERREUR_FILE_CANT_WRITE );

					// Detruire le fichier
					NLib_Fichier_NFichierBinaire_Detruire( &fichier );

					// Quitter
					return NFALSE;
				}
			// Position bloc de bordure
				if( this->m_tilesetBlocBordure != BBLOC_CARTE_AUCUN )
				{
					// x
					if( !NLib_Fichier_NFichierBinaire_Ecrire( fichier,
						(char*)&this->m_blocBordure.x,
						sizeof( NU32 ) )
					// y
						|| !NLib_Fichier_NFichierBinaire_Ecrire( fichier,
							(char*)&this->m_blocBordure.y,
							sizeof( NU32 ) ) )
					{
						// Notifier
						NOTIFIER_ERREUR( NERREUR_FILE_CANT_WRITE );

						// Detruire le fichier
						NLib_Fichier_NFichierBinaire_Detruire( &fichier );

						// Quitter
						return NFALSE;
					}
				}
		// Position de depart
			for( x = 0; x < BNOMBRE_MAXIMUM_JOUEUR; x++ )
				if( !NLib_Fichier_NFichierBinaire_Ecrire( fichier,
					(char*)&this->m_positionDepart[ x ].x,
					sizeof( NU32 ) )
					|| !NLib_Fichier_NFichierBinaire_Ecrire( fichier,
						(char*)&this->m_positionDepart[ x ].y,
						sizeof( NU32 ) ) )
				{
					// Notifier
					NOTIFIER_ERREUR( NERREUR_FILE_CANT_WRITE );

					// Detruire le fichier
					NLib_Fichier_NFichierBinaire_Detruire( &fichier );

					// Quitter
					return NFALSE;
				}

	// Enregistrer les cases
	for( x = 0; x < this->m_taille.x; x++ )
		for( y = 0; y < this->m_taille.y; y++ )
			if( !Projet_Commun_Carte_Bloc_BBloc_Sauvegarder( &this->m_case[ x ][ y ],
				fichier ) )
			{
				// Notifier
				NOTIFIER_ERREUR( NERREUR_FILE_CANT_WRITE );

				// Detruire le fichier
				NLib_Fichier_NFichierBinaire_Detruire( &fichier );

				// Quitter
				return NFALSE;
			}

	// Fermer
	NLib_Fichier_NFichierBinaire_Detruire( &fichier );

	// OK
	return NTRUE;
}

/* Obtenir la taille */
const NUPoint *Projet_Commun_Carte_BCarte_ObtenirTaille( const BCarte *this )
{
	return &this->m_taille;
}

/* Obtenir le nom */
const char *Projet_Commun_Carte_BCarte_ObtenirNom( const BCarte *this )
{
	return this->m_nom;
}

/* Obtenir les cases */
const BBloc **Projet_Commun_Carte_BCarte_ObtenirCases( const BCarte *this )
{
	return (const BBloc**)this->m_case;
}

/* Obtenir position de depart */
const NSPoint *Projet_Commun_Carte_BCarte_ObtenirPositionDepart( const BCarte *this,
	NU32 joueur )
{
	return &this->m_positionDepart[ joueur ];
}

/* Obtenir l'handle */
NU32 Projet_Commun_Carte_BCarte_ObtenirHandle( const BCarte *this )
{
	return this->m_handle;
}

/* Obtenir le checksum */
NU32 Projet_Commun_Carte_BCarte_ObtenirChecksum( const BCarte *this )
{
	return this->m_checksum;
}

/* Obtenir le tileset de bloc de remplissage */
NU32 Projet_Commun_Carte_BCarte_ObtenirTilesetBlocRemplissage( const BCarte *this )
{
	return this->m_tilesetBlocRemplissage;
}

/* Obtenir le bloc de remplissage */
NUPoint Projet_Commun_Carte_BCarte_ObtenirBlocRemplissage( const BCarte *this )
{
	return this->m_blocRemplissage;
}

/* Obtenir le tileset de bloc de bordure */
NU32 Projet_Commun_Carte_BCarte_ObtenirTilesetBlocBordure( const BCarte *this )
{
	return this->m_tilesetBlocBordure;
}

/* Obtenir le bloc de bordure */
NUPoint Projet_Commun_Carte_BCarte_ObtenirBlocBordure( const BCarte *this )
{
	return this->m_blocBordure;
}

/* Obtenir la musique */
NU32 Projet_Commun_Carte_BCarte_ObtenirMusique( const BCarte *this )
{
	return this->m_musique;
}

/* Est position correcte? */
NBOOL Projet_Commun_Carte_BCarte_EstPositionCorrecte( const BCarte *this,
	NSPoint position )
{
	return  ( position.x >= 0
		&& position.y >= 0
		&& position.x < (NS32)this->m_taille.x
		&& position.y < (NS32)this->m_taille.y );
}

/* Compter le nombre de bloc remplissable */
NU32 Projet_Commun_Carte_BCarte_CompterBlocRemplissable( const BCarte *this )
{
	// Iterateurs
	NU32 x,
		y;

	// Sortie
	__OUTPUT NU32 sortie = 0;

	// Compter
	for( x = 0; x < this->m_taille.x; x++ )
		for( y = 0; y < this->m_taille.y; y++ )
			sortie += ( this->m_case[ x ][ y ].m_type == BTYPE_BLOC_REMPLISSABLE ) ? 1 : 0;

	// OK
	return sortie;
}

/* Definir le nom */
NBOOL Projet_Commun_Carte_BCarte_DefinirNom( BCarte *this,
	const char *nom )
{
	// Nom
	char *nomTemp;

	// Allouer la memoire
	if( !( nomTemp = calloc( strlen( nom ) + 1,
		sizeof( char ) ) ) )
	{
		// Notifier
		NOTIFIER_ERREUR( NERREUR_ALLOCATION_FAILED );

		// Quitter
		return NFALSE;
	}

	// Liberer
	NFREE( this->m_nom );

	// Copier
	memcpy( nomTemp,
		nom,
		strlen( nom ) );

	// Enregistrer
	this->m_nom = nomTemp;

	// OK
	return NTRUE;
}

NBOOL Projet_Commun_Carte_BCarte_DefinirTaille( BCarte *this,
	NUPoint taille )
{
	// Cases
	BBloc **blocs;

	// Iterateurs
	NU32 i,
		j,
		k;

	// Limite
	NUPoint limite;

	// Verifier
	if( !taille.x
		|| !taille.y
		|| taille.x >= BTAILLE_MAXIMUM_CARTE.x
		|| taille.y >= BTAILLE_MAXIMUM_CARTE.y )
	{
		// Notifier
		NOTIFIER_ERREUR( NERREUR_PARAMETER_ERROR );

		// Quitter
		return NFALSE;
	}

	// Allouer la memoire
		// 1D
			if( !( blocs = calloc( taille.x,
				sizeof( BBloc* ) ) ) )
			{
				// Notifier
				NOTIFIER_ERREUR( NERREUR_ALLOCATION_FAILED );

				// Quitter
				return NFALSE;
			}
		// 2D
			for( i = 0; i < taille.x; i++ )
				if( !( blocs[ i ] = calloc( taille.y,
					sizeof( BBloc ) ) ) )
				{
					// Notifier
					NOTIFIER_ERREUR( NERREUR_ALLOCATION_FAILED );

					// Liberer
					for( j = 0; j < i; j++ )
						NFREE( blocs[ j ] );
					NFREE( blocs );

					// Quitter
					return NFALSE;
				}

	// Definir la limite
	NDEFINIR_POSITION( limite,
		( taille.x >= this->m_taille.x ?
			this->m_taille.x
			: taille.x ),
		( taille.y >= this->m_taille.y ?
			this->m_taille.y
			: taille.y ) );

	// Copier
	for( i = 0; i < taille.x; i++ )
		for( j = 0; j < taille.y; j++ )
			if( i < limite.x
				&& j < limite.y )
				memcpy( &blocs[ i ][ j ],
					&this->m_case[ i ][ j ],
					sizeof( BBloc ) );
			else
				for( k = 0; k < BCOUCHES_CARTE; k++ )
				{
					// Position tileset
					NDEFINIR_POSITION( blocs[ i ][ j ].m_positionTileset[ k ],
						BBLOC_CARTE_AUCUN,
						BBLOC_CARTE_AUCUN );

					// Tileset
					blocs[ i ][ j ].m_tileset[ k ] = BBLOC_CARTE_AUCUN;
				}

	// Liberer
	for( i = 0; i < this->m_taille.x; i++ )
		NFREE( this->m_case[ i ] );
	NFREE( this->m_case );

	// Copier
	this->m_case = blocs;
	this->m_taille = taille;

	// OK
	return NTRUE;
}

/* Definir position de depart */
void Projet_Commun_Carte_BCarte_DefinirPositionDepart( BCarte *this,
	NU32 idJoueur,
	NSPoint position )
{
	// Verifier
	if( idJoueur >= BNOMBRE_MAXIMUM_JOUEUR )
	{
		// Notifier
		NOTIFIER_ERREUR( NERREUR_PARAMETER_ERROR );

		// Quitter
		return;
	}

	// Enregistrer
	this->m_positionDepart[ idJoueur ] = position;
}

/* Definir le bloc de remplissage */
void Projet_Commun_Carte_BCarte_DefinirBlocRemplissage( BCarte *this,
	NU32 tileset,
	NUPoint positionTileset )
{
	this->m_blocRemplissage = positionTileset;
	this->m_tilesetBlocRemplissage = tileset;
}

/* Definir le bloc de bordure */
void Projet_Commun_Carte_BCarte_DefinirBlocBordure( BCarte *this,
	NU32 tileset,
	NUPoint positionTileset )
{
	this->m_blocBordure = positionTileset;
	this->m_tilesetBlocBordure = tileset;
}

/* Definir la musique */
void Projet_Commun_Carte_BCarte_DefinirMusique( BCarte *this,
	NU32 musique )
{
	// Enregistrer
	this->m_musique = musique;
}

