#include "../../../../../../include/Projet/Projet.h"

// ----------------------------------------------------------
// struct Projet::Commun::Reseau::Client::Cache::BCacheClient
// ----------------------------------------------------------

/* Construire */
__ALLOC BCacheClient *Projet_Commun_Reseau_Client_Cache_BCacheClient_Construire( const struct BRessource *ressource )
{
	// Sortie
	__OUTPUT BCacheClient *out;

	// Construire
	if( !( out = calloc( 1,
		sizeof( BCacheClient ) ) ) )
	{
		// Notifier
		NOTIFIER_ERREUR( NERREUR_ALLOCATION_FAILED );

		// Quitter
		return NULL;
	}

	// Construire mutex
	if( !( out->m_mutex = NLib_Mutex_NMutex_Construire( ) ) )
	{
		// Notifier
		NOTIFIER_ERREUR( NERREUR_MUTEX );

		// Liberer
		NFREE( out );

		// Quitter
		return NULL;
	}

	// Enregistrer
	out->m_ressource = ressource;

	// Le cache est ouvert
	out->m_estCacheFerme = NFALSE;

	// Le cache n'est pas encore modifie
	out->m_identifiantModificationCache = 0;
	out->m_identifiantClientCourant = NERREUR;

	// OK
	return out;
}

/* Detruire */
void Projet_Commun_Reseau_Client_Cache_BCacheClient_Detruire( BCacheClient **this )
{
	// Iterateur
	NU32 i = 0;

	// Lock mutex
	if( !NLib_Mutex_NMutex_Lock( (*this)->m_mutex ) )
	{
		// Notifier
		NOTIFIER_ERREUR( NERREUR_MUTEX );

		// Quitter
		return;
	}

	// Liberer clients
	for( ; i < (*this)->m_nombreClient; i++ )
		Projet_Commun_Reseau_Client_BEtatClient_Detruire( &(*this)->m_etat[ i ] );

	// Zero
	(*this)->m_nombreClient = 0;
	(*this)->m_estCacheFerme = NTRUE;

	// Unlock mutex
	NLib_Mutex_NMutex_Unlock( (*this)->m_mutex );

	// Detruire le mutex
	NLib_Mutex_NMutex_Detruire( &(*this)->m_mutex );

	// Liberer
	NFREE( (*this) );
}

/* Ajouter un client (privee) */
const BEtatClient *Projet_Commun_Reseau_Client_Cache_BCacheClient_AjouterClientInterne( BCacheClient *this,
	__WILLBEOWNED BEtatClient *etat )
{
	// Verifier parametre
	if( !etat )
	{
		// Notifier
		NOTIFIER_ERREUR( NERREUR_PARAMETER_ERROR );

		// Quitter
		return NULL;
	}

	// Verifier etat
	if( this->m_nombreClient >= BNOMBRE_MAXIMUM_JOUEUR
		|| this->m_estCacheFerme )
	{
		// Notifier
		NOTIFIER_ERREUR( NERREUR_LOGIN_FAILED );

		// Liberer
		Projet_Commun_Reseau_Client_BEtatClient_Detruire( &etat );

		// Quitter
		return NFALSE;
	}

	// Ajouter
	this->m_etat[ this->m_nombreClient ] = etat;

	// Incrementer nombre
	this->m_nombreClient++;

	// Le cache est modifie
	this->m_identifiantModificationCache++;

	// OK
	return etat;
}

/* Ajouter un client */
const BEtatClient *Projet_Commun_Reseau_Client_Cache_BCacheClient_AjouterClientCourant( BCacheClient *this,
	NU32 identifiant,
	void *donneeSupplementaire )
{
	// Enregistrer l'identifiant
	this->m_identifiantClientCourant = identifiant;

	// Ajouter
	return Projet_Commun_Reseau_Client_Cache_BCacheClient_AjouterClientInterne( this,
		Projet_Commun_Reseau_Client_BEtatClient_Construire( identifiant,
			NTRUE,
			donneeSupplementaire,
			this->m_ressource ) );
}

const BEtatClient *Projet_Commun_Reseau_Client_Cache_BCacheClient_AjouterClientExterne( BCacheClient *this,
	NU32 identifiant,
	void *donneeSupplementaire )
{
	return Projet_Commun_Reseau_Client_Cache_BCacheClient_AjouterClientInterne( this,
		Projet_Commun_Reseau_Client_BEtatClient_Construire( identifiant,
			NFALSE,
			donneeSupplementaire,
			this->m_ressource ) );
}

/* Fermer le cache (empecher connexion) */
void Projet_Commun_Reseau_Client_Cache_BCacheClient_Fermer( BCacheClient *this )
{
	this->m_estCacheFerme = NTRUE;
}

/* Ouvrir le cache (autoriser connexion) */
void Projet_Commun_Reseau_Client_Cache_BCacheClient_Ouvrir( BCacheClient *this )
{
	this->m_estCacheFerme = NFALSE;
}

/* Supprimer un client (doit etre protege) */
NBOOL Projet_Commun_Reseau_Client_Cache_BCacheClient_SupprimerClientNoLock( BCacheClient *this,
	NU32 identifiant )
{
	// Index
	NU32 index;

	// Iterateur
	NU32 i;

	// Obtenir l'index
	if( ( index = Projet_Commun_Reseau_Client_Cache_BCacheClient_ObtenirIndexClient( this,
		identifiant ) ) == NERREUR )
	{
		// Notifier
		NOTIFIER_ERREUR( NERREUR_PARAMETER_ERROR );

		// Quitter
		return NFALSE;
	}

	// Liberer
	Projet_Commun_Reseau_Client_BEtatClient_Detruire( &this->m_etat[ index ] );

	// Decaler
	for( i = index; i < BNOMBRE_MAXIMUM_JOUEUR - 1; i++ )
		this->m_etat[ i ] = this->m_etat[ i + 1 ];

	// Decrementer
	this->m_nombreClient--;

	// Le cache est modifie
	this->m_identifiantModificationCache++;

	// OK
	return NTRUE;
}

NBOOL Projet_Commun_Reseau_Client_Cache_BCacheClient_SupprimerClient( BCacheClient *this,
	NU32 identifiant )
{
	// Code retour
	NBOOL codeRetour;

	// Lock mutex
	NLib_Mutex_NMutex_Lock( this->m_mutex );

	// Supprimer client
	codeRetour = Projet_Commun_Reseau_Client_Cache_BCacheClient_SupprimerClientNoLock( this,
		identifiant );

	// Unlock mutex
	NLib_Mutex_NMutex_Unlock( this->m_mutex );

	// OK?
	return codeRetour;
}

/* Vider (protection necessaire) */
NBOOL Projet_Commun_Reseau_Client_Cache_BCacheClient_ViderNoLock( BCacheClient *this )
{
	// Iterateur
	NU32 i;

	// Liberer
	for( i = 0; i < this->m_nombreClient; i++ )
		Projet_Commun_Reseau_Client_BEtatClient_Detruire( &this->m_etat[ i ] );

	// Zero
	this->m_nombreClient = 0;
	this->m_identifiantClientCourant = NERREUR;

	// Modification suivante
	this->m_identifiantModificationCache++;

	// OK
	return NTRUE;
}

/* Vider (aucune protection necessaire) */
NBOOL Projet_Commun_Reseau_Client_Cache_BCacheClient_Vider( BCacheClient *this )
{
	// Retour
	__OUTPUT NBOOL codeRetour;

	// Lock mutex
	NLib_Mutex_NMutex_Lock( this->m_mutex );

	// Executer
	codeRetour = Projet_Commun_Reseau_Client_Cache_BCacheClient_ViderNoLock( this );

	// Unlock mutex
	NLib_Mutex_NMutex_Unlock( this->m_mutex );

	// OK
	return codeRetour;
}

/* Envoyer un packet */
NBOOL Projet_Commun_Reseau_Client_Cache_BCacheClient_EnvoyerPacketDepuisServeurTousNoLock( BCacheClient *this,
	__WILLBEOWNED NPacket *packet )
{
	// Iterateur
	NU32 i;

	// Envoyer le packet
	for( i = 0; i < this->m_nombreClient; i++ )
		if( !NLib_Module_Reseau_Serveur_NClientServeur_AjouterPacketCopie( Projet_Commun_Reseau_Client_BEtatClient_ObtenirDonneeSupplementaire( this->m_etat[ i ] ),
			packet ) )
		{
			// Notifier
			NOTIFIER_ERREUR( NERREUR_CONSTRUCTOR_FAILED );

			// Detruire le packet
			NLib_Module_Reseau_Packet_NPacket_Detruire( &packet );

			// Quitter
			return NFALSE;
		}

	// Detruire le packet
	NLib_Module_Reseau_Packet_NPacket_Detruire( &packet );

	// OK
	return NTRUE;
}

NBOOL Projet_Commun_Reseau_Client_Cache_BCacheClient_EnvoyerPacketDepuisServeurTous( BCacheClient *this,
	__WILLBEOWNED NPacket *packet )
{
	// Retour
	__OUTPUT NBOOL codeRetour;

	// Lock mutex
	NLib_Mutex_NMutex_Lock( this->m_mutex );

	codeRetour = Projet_Commun_Reseau_Client_Cache_BCacheClient_EnvoyerPacketDepuisServeurTousNoLock( this,
		packet );

	// Unlock mutex
	NLib_Mutex_NMutex_Unlock( this->m_mutex );

	// OK?
	return codeRetour;
}

/* Obtenir nombre de clients morts (protection necessaire) */
NU32 Projet_Commun_Reseau_Client_Cache_BCacheClient_ObtenirNombreClientMort( const BCacheClient *this )
{
	// Iterateur
	NU32 i = 0;

	// Compteur mort
	__OUTPUT NU32 nombreMort = 0;

	// Parcourir
	for( ; i < this->m_nombreClient; i++ )
		// Verifier si le joueur est en vie
		nombreMort += !Projet_Commun_Reseau_Client_BEtatClient_EstEnVie( this->m_etat[ i ] );

	// OK
	return nombreMort;
}

/* Update cache (desynchronise) */
NBOOL Projet_Commun_Reseau_Client_Cache_BCacheClient_UpdateNoLock( BCacheClient *this )
{
	// Iterateur
	NU32 i = 0;

	// Update
	for( ; i < this->m_nombreClient; i++ )
		Projet_Commun_Reseau_Client_BEtatClient_Update( this->m_etat[ i ] );

	// OK
	return NTRUE;
}

NBOOL Projet_Commun_Reseau_Client_Cache_BCacheClient_Update( BCacheClient *this )
{
	// Sortie
	__OUTPUT NBOOL resultat;

	// Lock mutex
	NLib_Mutex_NMutex_Lock( this->m_mutex );

	// Update
	resultat = Projet_Commun_Reseau_Client_Cache_BCacheClient_UpdateNoLock( this );

	// Unlock mutex
	NLib_Mutex_NMutex_Unlock( this->m_mutex );

	// OK
	return resultat;
}

/* Enregistrer demande personnalisation client (desynchronise) */
NBOOL Projet_Commun_Reseau_Client_Cache_BCacheClient_EnregistrerDemandePersonnalisation( BCacheClient *this,
	NU32 identifiant,
	NU32 charset,
	NU32 couleurCharset,
	const char *nom,
	const struct BRessource *ressource )
{
	// Iterateurs
	NU32 i,
		j = 0;

	// Est joueur identique trouve?
	NBOOL estIdentique = NFALSE;

	// Index
	NU32 index;

	// Buffer
	char *buffer = NULL;

	// Lock mutex
	NLib_Mutex_NMutex_Lock( this->m_mutex );

	do
	{
		// N'est plus identique
		estIdentique = NFALSE;

		// Chercher si le charset et la couleur sont deja pris
		for( i = 0; i < this->m_nombreClient; i++ )
			if( Projet_Commun_Reseau_Client_BEtatClient_ObtenirCharset( this->m_etat[ i ] ) == charset
				&& Projet_Commun_Reseau_Client_BEtatClient_ObtenirCouleurCharset( this->m_etat[ i ] ) == couleurCharset
				&& identifiant != Projet_Commun_Reseau_Client_BEtatClient_ObtenirIdentifiant( this->m_etat[ i ] ) )
				estIdentique = NTRUE;

		// Si deja pris, on change
		if( estIdentique )
		{
			// Couleur charset suivante
			if( couleurCharset < Projet_Commun_Personnage_BPersonnage_ObtenirNombreCouleur( Projet_Commun_Ressource_BRessource_ObtenirPersonnage( ressource,
				charset ) ) - 1 )
				couleurCharset++;
			else
			{
				// Couleur charset a zero
				couleurCharset = 0;

				// Charset suivant
				if( charset < Projet_Commun_Ressource_BRessource_ObtenirNombrePersonnage( ressource ) - 1 )
					charset++;
				else
					charset = 0;
			}
		}
	} while( estIdentique );

	do
	{
		// N'est plus identique
		estIdentique = NFALSE;

		// Liberer
		NFREE( buffer );

		// Allouer la memoire
		if( !( buffer = calloc( strlen( nom ) + ( j == 0 ? 0 : NLib_Math_ObtenirNombreDigit( j ) ) + 1,
			sizeof( char ) ) ) )
		{
			// Notifier
			NOTIFIER_ERREUR( NERREUR_ALLOCATION_FAILED );

			// Unlock mutex
			NLib_Mutex_NMutex_Unlock( this->m_mutex );

			// Quitter
			return NFALSE;
		}

		// Ecrire le nom
		if( j != 0 )
			sprintf( buffer,
				"%s%d",
				nom,
				j );
		else
			strcpy( buffer,
				nom );

		// Chercher si le nom est deja pris
		for( i = 0; i < this->m_nombreClient; i++ )
			if( identifiant != Projet_Commun_Reseau_Client_BEtatClient_ObtenirIdentifiant( this->m_etat[ i ] )
				&& Projet_Commun_Reseau_Client_BEtatClient_ObtenirNom( this->m_etat[ i ] ) != NULL
				&& NLib_Chaine_Comparer( Projet_Commun_Reseau_Client_BEtatClient_ObtenirNom( this->m_etat[ i ] ),
					buffer,
					NFALSE,
					0 ) )
			{
				// Identique
				estIdentique = NTRUE;

				// Nom suivant
				j++;

				// Sortir
				break;
			}
	} while( estIdentique );

	// Obtenir l'index du client concerne
	if( ( index = Projet_Commun_Reseau_Client_Cache_BCacheClient_ObtenirIndexClient( this,
		identifiant ) ) == NERREUR )
	{
		// Notifier
		NOTIFIER_ERREUR( NERREUR_OBJECT_STATE );

		// Unlock mutex
		NLib_Mutex_NMutex_Unlock( this->m_mutex );

		// Quitter
		return NFALSE;
	}

	// Enregistrer modification
	Projet_Commun_Reseau_Client_BEtatClient_DefinirCharset( this->m_etat[ index ],
		charset );
	Projet_Commun_Reseau_Client_BEtatClient_DefinirCouleurCharset( this->m_etat[ index ],
		couleurCharset );
	Projet_Commun_Reseau_Client_BEtatClient_DefinirNom( this->m_etat[ index ],
		buffer );

	// Liberer
	NFREE( buffer );

	// Notifier modification
	this->m_identifiantModificationCache++;

	// Unlock mutex
	NLib_Mutex_NMutex_Unlock( this->m_mutex );

	// Quitter
	return NTRUE;
}

NBOOL Projet_Commun_Reseau_Client_Cache_BCacheClient_EnregistrerDemandePersonnalisation2( BCacheClient *this,
	NU32 identifiant,
	NU32 charset,
	NU32 couleurCharset,
	const char *nom )
{
	// Index
	NU32 index;

	// Joueur
	BEtatClient *joueur;

	// Lock mutex
	NLib_Mutex_NMutex_Lock( this->m_mutex );

	// Obtenir l'index du client concerne
	if( ( index = Projet_Commun_Reseau_Client_Cache_BCacheClient_ObtenirIndexClient( this,
		identifiant ) ) == NERREUR )
	{
		// Construire etat
		if( !( joueur = Projet_Commun_Reseau_Client_BEtatClient_Construire( identifiant,
			NFALSE,
			NULL,
			this->m_ressource ) ) )
		{
			// Notifier
			NOTIFIER_ERREUR( NERREUR_CONSTRUCTOR_FAILED );

			// Unlock mutex
			NLib_Mutex_NMutex_Unlock( this->m_mutex );

			// Quitter
			return NFALSE;
		}

		// Ajouter le client en question
		if( !Projet_Commun_Reseau_Client_Cache_BCacheClient_AjouterClientInterne( this,
			joueur ) )
		{
			// Notifier
			NOTIFIER_ERREUR( NERREUR_OBJECT_STATE );

			// Liberer
			Projet_Commun_Reseau_Client_BEtatClient_Detruire( &joueur );

			// Unlock mutex
			NLib_Mutex_NMutex_Unlock( this->m_mutex );

			// Quitter
			return NFALSE;
		}

		// Recuperer index
		if( ( index = Projet_Commun_Reseau_Client_Cache_BCacheClient_ObtenirIndexClient( this,
			identifiant ) ) == NERREUR )
		{
			// Notifier
			NOTIFIER_ERREUR( NERREUR_OBJECT_STATE );

			// Unlock mutex
			NLib_Mutex_NMutex_Unlock( this->m_mutex );

			// Quitter
			return NFALSE;
		}
	}

	// Enregistrer modification
	Projet_Commun_Reseau_Client_BEtatClient_DefinirCharset( this->m_etat[ index ],
		charset );
	Projet_Commun_Reseau_Client_BEtatClient_DefinirCouleurCharset( this->m_etat[ index ],
		couleurCharset );
	Projet_Commun_Reseau_Client_BEtatClient_DefinirNom( this->m_etat[ index ],
		nom );

	// Modification effectuee
	this->m_identifiantModificationCache++;

	// Unlock mutex
	NLib_Mutex_NMutex_Unlock( this->m_mutex );

	// OK
	return NTRUE;
}

/* Enregistrer changement etat pret (desynchronise) */
NBOOL Projet_Commun_Reseau_Client_Cache_BCacheClient_EnregistrerChangementEtatPret2( BCacheClient *this,
	NU32 identifiant,
	BEtatPret etat )
{
	// Index
	NU32 index;

	// Lock mutex
	NLib_Mutex_NMutex_Lock( this->m_mutex );

	// Recuperer le client
	if( ( index = Projet_Commun_Reseau_Client_Cache_BCacheClient_ObtenirIndexClient( this,
		identifiant ) ) == NERREUR )
	{
		// Notifier
		NOTIFIER_ERREUR( NERREUR_OBJECT_STATE );

		// Unlock mutex
		NLib_Mutex_NMutex_Unlock( this->m_mutex );

		// Quitter
		return NFALSE;
	}

	// Changer etat
	Projet_Commun_Reseau_Client_BEtatClient_DefinirEstPret( this->m_etat[ index ],
		etat );

	// Modification effectuee
	this->m_identifiantModificationCache++;

	// Unlock mutex
	NLib_Mutex_NMutex_Unlock( this->m_mutex );

	// OK
	return NTRUE;
}

NBOOL Projet_Commun_Reseau_Client_Cache_BCacheClient_EnregistrerChangementEtatPret( BCacheClient *this,
	NU32 identifiant )
{
	// Index
	NU32 index;

	// Lock mutex
	NLib_Mutex_NMutex_Lock( this->m_mutex );

	// Recuperer le client
	if( ( index = Projet_Commun_Reseau_Client_Cache_BCacheClient_ObtenirIndexClient( this,
		identifiant ) ) == NERREUR )
	{
		// Notifier
		NOTIFIER_ERREUR( NERREUR_OBJECT_STATE );

		// Unlock mutex
		NLib_Mutex_NMutex_Unlock( this->m_mutex );

		// Quitter
		return NFALSE;
	}

	// Changer etat
	Projet_Commun_Reseau_Client_BEtatClient_DefinirEstPret( this->m_etat[ index ],
		!Projet_Commun_Reseau_Client_BEtatClient_EstPret( this->m_etat[ index ] ) );

	// Modification effectuee
	this->m_identifiantModificationCache++;

	// Unlock mutex
	NLib_Mutex_NMutex_Unlock( this->m_mutex );

	// OK
	return NTRUE;
}

/* Enregistrer changement direction (aucune protection necessaire) */
NBOOL Projet_Commun_Reseau_Client_Cache_BCacheClient_EnregistrerChangementDirection( BCacheClient *this,
	NU32 identifiant,
	NDirection direction )
{
	// Joueur
	const BEtatClient *joueur;

	// Lock mutex
	NLib_Mutex_NMutex_Lock( this->m_mutex );

	// Obtenir joueur
	if( !( joueur = Projet_Commun_Reseau_Client_Cache_BCacheClient_ObtenirClient( this,
		identifiant ) ) )
	{
		// Notifier
		NOTIFIER_ERREUR( NERREUR_STREAM_ERROR );

		// Unlock mutex
		NLib_Mutex_NMutex_Unlock( this->m_mutex );

		// Quitter
		return NFALSE;
	}

	// Enregistrer direction
	Projet_Commun_Reseau_Client_BEtatClient_DefinirDirection( (BEtatClient*)joueur,
		direction );

	// Unlock mutex
	NLib_Mutex_NMutex_Unlock( this->m_mutex );

	// OK
	return NTRUE;
}

/* Enregistrer changement position (protection necessaire) */
NBOOL Projet_Commun_Reseau_Client_Cache_BCacheClient_EnregistrerChangementPosition( BCacheClient *this,
	NU32 identifiant,
	NSPoint position,
	NDirection direction )
{
	// Joueur
	const BEtatClient *joueur;

	// Obtenir joueur
	if( !( joueur = Projet_Commun_Reseau_Client_Cache_BCacheClient_ObtenirClient( this,
		identifiant ) ) )
	{
		// Notifier
		NOTIFIER_ERREUR( NERREUR_STREAM_ERROR );

		// Quitter
		return NFALSE;
	}

	// Enregistrer
	Projet_Commun_Reseau_Client_BEtatClient_DefinirPosition( (BEtatClient*)joueur,
		position );
	Projet_Commun_Reseau_Client_BEtatClient_DefinirDirection( (BEtatClient*)joueur,
		direction );

	// OK
	return NTRUE;
}

/* Le client est verifie (desynchronise) */
NBOOL Projet_Commun_Reseau_Client_Cache_BCacheClient_DefinirClientVerifie( BCacheClient *this,
	NU32 identifiant )
{
	// Index
	NU32 index;

	// Lock mutex
	NLib_Mutex_NMutex_Lock( this->m_mutex );

	// Recuperer l'index
	if( ( index = Projet_Commun_Reseau_Client_Cache_BCacheClient_ObtenirIndexClient( this,
		identifiant ) ) == NERREUR )
	{
		// Notifier
		NOTIFIER_ERREUR( NERREUR_OBJECT_STATE );

		// Unlock mutex
		NLib_Mutex_NMutex_Unlock( this->m_mutex );

		// Quitter
		return NFALSE;
	}

	// Le client est verifie
	Projet_Commun_Reseau_Client_BEtatClient_DefinirEstVerifie( this->m_etat[ index ],
		NTRUE );

	// Il faut mettre a jour le cache
	this->m_identifiantModificationCache++;

	// Unlock mutex
	NLib_Mutex_NMutex_Unlock( this->m_mutex );

	// OK
	return NTRUE;
}

/* Enregistrer confirmation lancement client (aucune protection necessaire) */
NBOOL Projet_Commun_Reseau_Client_Cache_BCacheClient_EnregistrerConfirmationLancement( BCacheClient *this,
	NU32 identifiant )
{
	// Joueur
	BEtatClient *joueur;

	// Lock mutex
	NLib_Mutex_NMutex_Lock( this->m_mutex );

	// Obtenir joueur
	if( !( joueur = (BEtatClient*)Projet_Commun_Reseau_Client_Cache_BCacheClient_ObtenirClient( this,
		identifiant ) ) )
	{
		// Notifier
		NOTIFIER_ERREUR( NERREUR_PARAMETER_ERROR );

		// Unlock mutex
		NLib_Mutex_NMutex_Unlock( this->m_mutex );

		// Quitter
		return NFALSE;
	}

	// Definir
	Projet_Commun_Reseau_Client_BEtatClient_ConfirmerLancement( joueur );

	// Unlock mutex
	NLib_Mutex_NMutex_Unlock( this->m_mutex );

	// OK
	return NTRUE;
}

/* Proteger cache (thread safe) */
NBOOL Projet_Commun_Reseau_Client_Cache_BCacheClient_ProtegerCache( BCacheClient *this )
{
	// Lock mutex
	NLib_Mutex_NMutex_Lock( this->m_mutex );

	// OK
	return NTRUE;
}

/* Ne plus proteger cache (plus thread safe) */
NBOOL Projet_Commun_Reseau_Client_Cache_BCacheClient_NePlusProtegerCache( BCacheClient *this )
{
	// Unlock mutex
	NLib_Mutex_NMutex_Unlock( this->m_mutex );

	// OK
	return NTRUE;
}


/* Obtenir identifiant client courant (aucune protection necessaire) */
NU32 Projet_Commun_Reseau_Client_Cache_BCacheClient_ObtenirIdentifiantClientCourant( const BCacheClient *this )
{
	return this->m_identifiantClientCourant;
}

/* Obtenir un client (protection necessaire) */
const BEtatClient *Projet_Commun_Reseau_Client_Cache_BCacheClient_ObtenirClient( const BCacheClient *this,
	NU32 identifiant )
{
	// Executer
	return Projet_Commun_Reseau_Client_Cache_BCacheClient_ObtenirClient2( this,
		Projet_Commun_Reseau_Client_Cache_BCacheClient_ObtenirIndexClient( this,
			identifiant ) );
}

const BEtatClient *Projet_Commun_Reseau_Client_Cache_BCacheClient_ObtenirClient2( const BCacheClient *this,
	NU32 index )
{
	// Verifier
	if( index == NERREUR
		|| index >= BNOMBRE_MAXIMUM_JOUEUR )
	{
		// Notifier
		NOTIFIER_AVERTISSEMENT( NERREUR_PARAMETER_ERROR );

		// Quitter
		return NULL;
	}

	// OK
	return this->m_etat[ index ];
}

const BEtatClient *Projet_Commun_Reseau_Client_Cache_BCacheClient_ObtenirClientCourant( const BCacheClient *this )
{
	return Projet_Commun_Reseau_Client_Cache_BCacheClient_ObtenirClient( this,
		this->m_identifiantClientCourant );
}

/* Obtenir un index client (protection necessaire) */
NU32 Projet_Commun_Reseau_Client_Cache_BCacheClient_ObtenirIndexClientCourant( const BCacheClient *this )
{
	return Projet_Commun_Reseau_Client_Cache_BCacheClient_ObtenirIndexClient( this,
		this->m_identifiantClientCourant );
}

NU32 Projet_Commun_Reseau_Client_Cache_BCacheClient_ObtenirIndexClient( const BCacheClient *this,
	NU32 identifiant )
{
	// Iterateur
	__OUTPUT NU32 i = 0;

	// Chercher
	for( ; i < this->m_nombreClient; i++ )
		if( Projet_Commun_Reseau_Client_BEtatClient_ObtenirIdentifiant( this->m_etat[ i ] ) == identifiant )
			return i;

	// Introuvable
	return NERREUR;
}

/* Obtenir nombre clients (protection necessaire) */
NU32 Projet_Commun_Reseau_Client_Cache_BCacheClient_ObtenirNombreClient( const BCacheClient *this )
{
	return this->m_nombreClient;
}

/* Obtenir identifiant derniere modification (aucune protection necessaire) */
NU32 Projet_Commun_Reseau_Client_Cache_BCacheClient_ObtenirIdentifiantDerniereModification( const BCacheClient *this )
{
	return this->m_identifiantModificationCache;
}

/* Est clients prets? (protection necessaire) */
NBOOL Projet_Commun_Reseau_Client_Cache_BCacheClient_EstClientsPrets( const BCacheClient *this )
{
	// Iterateur
	NU32 i;

	// Lock mutex
	NLib_Mutex_NMutex_Lock( this->m_mutex );

	// Verifier
	for( i = 0; i < this->m_nombreClient; i++ )
		if( !Projet_Commun_Reseau_Client_BEtatClient_EstPret( this->m_etat[ i ] ) )
		{
			// Unlock mutex
			NLib_Mutex_NMutex_Unlock( this->m_mutex );

			// Quitter
			return NFALSE;
		}

	// Unlock mutex
	NLib_Mutex_NMutex_Unlock( this->m_mutex );

	// OK
	return NTRUE;
}

