#define PROJET_COMMUN_RESEAU_CLIENT_BETATPRET_INTERNE
#include "../../../../../include/Projet/Projet.h"

// ----------------------------------------------
// enum Projet::Commun::Reseau::Client::BEtatPret
// ----------------------------------------------

/* Obtenir nom etat */
const char *Projet_Commun_Reseau_Client_BEtatPret_ObtenirNomEtat( BEtatPret etat )
{
	return BEtatPretTexte[ etat ];
}

