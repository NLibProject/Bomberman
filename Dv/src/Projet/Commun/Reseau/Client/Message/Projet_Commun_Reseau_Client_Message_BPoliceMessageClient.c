#define PROJET_COMMUN_RESEAU_CLIENT_MESSAGE_BPOLICEMESSAGECLIENT_INTERNE
#include "../../../../../../include/Projet/Projet.h"

// ------------------------------------------------------------------
// enum Projet::Commun::Reseau::Client::Message::BPoliceMessageClient
// ------------------------------------------------------------------

/* Obtenir police(BListePolice) */
BListePolice Projet_Commun_Reseau_Client_Message_BPoliceMessageClient_ObtenirPolice( BPoliceMessageClient police )
{
	return BPoliceMessageClientPolice[ police ];
}

/* Obtenir taille */
NU32 Projet_Commun_Reseau_Client_Message_BPoliceMessageClient_ObtenirTaille( BPoliceMessageClient police )
{
	return BPoliceMessageClientTaille[ police ];
}

