#include "../../../../../include/Projet/Projet.h"

// -------------------------------------------------------------
// struct Projet::Commun::Reseau::Client::BEtatDeplacementClient
// -------------------------------------------------------------

/* Construire */
__ALLOC BEtatDeplacementClient *Projet_Commun_Reseau_Client_BEtatDeplacementClient_Construire( void )
{
	// Sortie
	__OUTPUT BEtatDeplacementClient *out;

	// Allouer la memoire
	if( !( out = calloc( 1,
		sizeof( BEtatDeplacementClient ) ) ) )
	{
		// Notifier
		NOTIFIER_ERREUR( NERREUR_ALLOCATION_FAILED );

		// Quitter
		return NULL;
	}

	// OK
	return out;
}

__ALLOC BEtatDeplacementClient *Projet_Commun_Reseau_Client_BEtatDeplacementClient_Construire2( const BEtatDeplacementClient *src )
{
	// Sortie
	__OUTPUT BEtatDeplacementClient *out;

	// Allouer la memoire
	if( !( out = calloc( 1,
		sizeof( BEtatDeplacementClient ) ) ) )
	{
		// Notifier
		NOTIFIER_ERREUR( NERREUR_ALLOCATION_FAILED );

		// Quitter
		return NULL;
	}

	// Copier
	memcpy( out,
		src,
		sizeof( BEtatDeplacementClient ) );

	// OK
	return out;
}

/* Detruire */
void Projet_Commun_Reseau_Client_BEtatDeplacementClient_Detruire( BEtatDeplacementClient **this )
{
	NFREE( *this );
}

/* Demarrer deplacement */
void Projet_Commun_Reseau_Client_BEtatDeplacementClient_Deplacer( BEtatDeplacementClient *this,
	NSPoint positionInitiale,
	NDirection direction )
{
	// Activer deplacement
	this->m_estDeplacementActif = NTRUE;

	// Enregistrer
	this->m_direction = direction;
	this->m_positionInitiale = positionInitiale;
	this->m_tempsDernierChangementScrolling = NLib_Temps_ObtenirTick( );

	// Zero
	NDEFINIR_POSITION( this->m_etatScrolling,
		0,
		0 );
}

/* Obtenir etat scrolling */
const NSPoint *Projet_Commun_Reseau_Client_BEtatDeplacementClient_ObtenirEtatScrolling( const BEtatDeplacementClient *this )
{
	return &this->m_etatScrolling;
}

/* Est deplacement actif? */
NBOOL Projet_Commun_Reseau_Client_BEtatDeplacementClient_EstDeplacementActif( const BEtatDeplacementClient *this )
{
	return this->m_estDeplacementActif;
}

/* Obtenir position initiale */
NSPoint Projet_Commun_Reseau_Client_BEtatDeplacementClient_ObtenirPositionInitiale( const BEtatDeplacementClient *this )
{
	return this->m_positionInitiale;
}

/* Obtenir direction deplacement */
NDirection Projet_Commun_Reseau_Client_BEtatDeplacementClient_ObtenirDirectionDeplacement( const BEtatDeplacementClient *this )
{
	return this->m_direction;
}

/* Update */
void Projet_Commun_Reseau_Client_BEtatDeplacementClient_Update( BEtatDeplacementClient *this )
{
	// Valeur
	NS32 *valeur = NULL;

	// Verifier etat
	if( !this->m_estDeplacementActif
		|| NLib_Temps_ObtenirTick( ) - this->m_tempsDernierChangementScrolling < BTEMPS_ENTRE_UPDATE_DEPLACEMENT_CLIENT )
		return;

	// Analyser direction
	switch( this->m_direction )
	{
		case NHAUT:
			(*( valeur = &this->m_etatScrolling.y ))--;
			break;
		case NBAS:
			(*( valeur = &this->m_etatScrolling.y ))++;
			break;
		case NGAUCHE:
			(*( valeur = &this->m_etatScrolling.x ))--;
			break;
		case NDROITE:
			(*( valeur = &this->m_etatScrolling.x ))++;
			break;

		default:
			this->m_estDeplacementActif = NFALSE;
			return;
	}

	// Verifier fin deplacement
	if( NLib_Math_Abs( *valeur ) >= (NS32)BTAILLE_CASE_TILESET.x )
		this->m_estDeplacementActif = NFALSE;
}

