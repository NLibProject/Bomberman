#include "../../../../../include/Projet/Projet.h"

// --------------------------------------------------
// struct Projet::Commun::Reseau::Client::BEtatClient
// --------------------------------------------------

/* Construire */
__ALLOC BEtatClient *Projet_Commun_Reseau_Client_BEtatClient_Construire( NU32 identifiant,
	NBOOL estClientCourant,
	void *donneeSupplementaire,
	const void *ressource )
{
	// Sortie
	__OUTPUT BEtatClient *out;

	// Personnage
	const BPersonnage *personnage;

	// Charset
	const BCharset *charset;

	// Animation
	const NAnimation *animation;

	// Etat animation
	const NEtatAnimation *etatAnimation;

	// Obtenir ressources
	if( !( personnage = Projet_Commun_Ressource_BRessource_ObtenirPersonnage( ressource,
			0 ) )
		|| !( charset = Projet_Commun_Personnage_BPersonnage_ObtenirCharset( personnage ) )
		|| !( animation = Projet_Commun_Personnage_Charset_BCharset_ObtenirAnimationMort( charset ) )
		|| !( etatAnimation = NLib_Module_SDL_Surface_NAnimation_ObtenirEtat( animation ) ) )
	{
		// Notifier
		NOTIFIER_ERREUR( NERREUR_PARAMETER_ERROR );

		// Quitter
		return NULL;
	}

	// Allouer la memoire
	if( !( out = calloc( 1,
		sizeof( BEtatClient ) ) ) )
	{
		// Notifier
		NOTIFIER_ERREUR( NERREUR_ALLOCATION_FAILED );

		// Quitter
		return NULL;
	}

	// Construire etat deplacement
	if( !( out->m_etatDeplacement = Projet_Commun_Reseau_Client_BEtatDeplacementClient_Construire( ) ) )
	{
		// Notifier
		NOTIFIER_ERREUR( NERREUR_CONSTRUCTOR_FAILED );

		// Liberer
		NFREE( out );

		// Quitter
		return NULL;
	}

	// Construire animation mort
	if( !( out->m_etatAnimationMort = NLib_Temps_Animation_NEtatAnimation_Construire2( etatAnimation ) ) )
	{
		// Notifier
		NOTIFIER_ERREUR( NERREUR_CONSTRUCTOR_FAILED );

		// Detruire
		Projet_Commun_Reseau_Client_BEtatDeplacementClient_Detruire( &out->m_etatDeplacement );

		// Liberer
		NFREE( out );

		// Quitter
		return NULL;
	}

	// Zero
	out->m_estPret = BETAT_PRET_PAS_PRET;
	out->m_nombreBombePosee = 0;
	out->m_puissance = BPUISSANCE_INITIALE_JOUEUR;
	out->m_nombreBombeMaximum = BNOMBRE_BOMBE_INITIAL_JOUEUR;
	out->m_estEnVie = NTRUE;
	out->m_estAnimationMortEnCours = NFALSE;

	// Enregistrer
	out->m_identifiant = identifiant;
	out->m_estClientCourant = estClientCourant;
	out->m_tickConnexion = NLib_Temps_ObtenirTick( );
	out->m_donneeSupplementaire = donneeSupplementaire;

	// OK
	return out;
}

__ALLOC BEtatClient *Projet_Commun_Reseau_Client_BEtatClient_Construire2( const BEtatClient *src )
{
	// Sortie
	__OUTPUT BEtatClient *out;

	// Allouer la memoire
	if( !( out = calloc( 1,
		sizeof( BEtatClient ) ) ) )
	{
		// Notifier
		NOTIFIER_ERREUR( NERREUR_ALLOCATION_FAILED );

		// Quitter
		return NULL;
	}

	// Copier
	memcpy( out,
		src,
		sizeof( BEtatClient ) );

	// Allouer nom
	if( src->m_nom != NULL )
		if( !( out->m_nom = calloc( strlen( src->m_nom ) + 1,
			sizeof( char ) ) ) )
		{
			// Notifier
			NOTIFIER_ERREUR( NERREUR_ALLOCATION_FAILED );

			// Liberer
			NFREE( out );

			// Quitter
			return NULL;
		}

	// Dupliquer etat deplacement
	if( !( out->m_etatDeplacement = Projet_Commun_Reseau_Client_BEtatDeplacementClient_Construire2( src->m_etatDeplacement ) ) )
	{
		// Notifier
		NOTIFIER_ERREUR( NERREUR_CONSTRUCTOR_FAILED );

		// Liberer
		NFREE( out->m_nom );
		NFREE( out );

		// Quitter
		return NULL;
	}

	// Dupliquer animation mort
	if( !( out->m_etatAnimationMort = NLib_Temps_Animation_NEtatAnimation_Construire2( src->m_etatAnimationMort ) ) )
	{
		// Notifier
		NOTIFIER_ERREUR( NERREUR_CONSTRUCTOR_FAILED );

		// Liberer
		Projet_Commun_Reseau_Client_BEtatDeplacementClient_Detruire( &out->m_etatDeplacement );
		NFREE( out->m_nom );
		NFREE( out );

		// Quitter
		return NULL;
	}

	// Dupliquer nom
	if( src->m_nom != NULL)
		memcpy( out->m_nom,
			src->m_nom,
			strlen( src->m_nom ) );

	// OK
	return out;
}

/* Detruire */
void Projet_Commun_Reseau_Client_BEtatClient_Detruire( BEtatClient **this )
{
	// Detruire etat animation mort
	NLib_Temps_Animation_NEtatAnimation_Detruire( &(*this)->m_etatAnimationMort );

	// Detruire etat deplacement
	Projet_Commun_Reseau_Client_BEtatDeplacementClient_Detruire( &(*this)->m_etatDeplacement );

	// Liberer nom
	NFREE( (*this)->m_nom );

	// Liberer
	NFREE( *this );
}

/* Obtenir nom */
const char *Projet_Commun_Reseau_Client_BEtatClient_ObtenirNom( const BEtatClient *this )
{
	return this->m_nom;
}

/* Obtenir identifiant */
NU32 Projet_Commun_Reseau_Client_BEtatClient_ObtenirIdentifiant( const BEtatClient *this )
{
	return this->m_identifiant;
}

/* Obtenir charset */
NU32 Projet_Commun_Reseau_Client_BEtatClient_ObtenirCharset( const BEtatClient *this )
{
	return this->m_charset;
}

/* Obtenir couleur charset */
NU32 Projet_Commun_Reseau_Client_BEtatClient_ObtenirCouleurCharset( const BEtatClient *this )
{
	return this->m_couleurCharset;
}

/* Obtenir position */
const NSPoint *Projet_Commun_Reseau_Client_BEtatClient_ObtenirPosition( const BEtatClient *this )
{
	return &this->m_position;
}

/* Est confirme le lancement? */
NBOOL Projet_Commun_Reseau_Client_BEtatClient_EstConfirmeLancement( const BEtatClient *this )
{
	return this->m_estConfirmeLancement;
}

/* Obtenir donnee supplementaire */
void *Projet_Commun_Reseau_Client_BEtatClient_ObtenirDonneeSupplementaire( const BEtatClient *this )
{
	return this->m_donneeSupplementaire;
}

/* Obtenir etat deplacement */
const BEtatDeplacementClient *Projet_Commun_Reseau_Client_BEtatClient_ObtenirEtatDeplacement( const BEtatClient *this )
{
	return this->m_etatDeplacement;
}

/* Obtenir etat animation */
const NEtatAnimation *Projet_Commun_Reseau_Client_BEtatClient_ObtenirEtatAnimationMort( const BEtatClient *this )
{
	return this->m_etatAnimationMort;
}

/* Obtenir nombre bombes posees */
NU32 Projet_Commun_Reseau_Client_BEtatClient_ObtenirNombreBombePosee( const BEtatClient *this )
{
	return this->m_nombreBombePosee;
}

/* Obtenir nombre maximum bombes */
NU32 Projet_Commun_Reseau_Client_BEtatClient_ObtenirNombreMaximumBombe( const BEtatClient *this )
{
	return this->m_nombreBombeMaximum;
}

/* Est client verifie? */
NBOOL Projet_Commun_Reseau_Client_BEtatClient_EstVerifie( const BEtatClient *this )
{
	return this->m_estChecksumVerifie;
}

/* On doit afficher ce client? */
NBOOL Projet_Commun_Reseau_Client_BEtatClient_EstDoitAfficher( const BEtatClient *this )
{
	return this->m_estEnVie
		|| !NLib_Temps_Animation_NEtatAnimation_EstFinAnimation( this->m_etatAnimationMort );
}

/* Verifier etat client */
NBOOL Projet_Commun_Reseau_Client_BEtatClient_EstClientEtatCorrect( const BEtatClient *this )
{
	// Verifier etat
	return ( this->m_estChecksumVerifie
		|| NLib_Temps_ObtenirTick( ) - this->m_tickConnexion < BDELAI_KICK_CLIENT_NON_VERIFIE );
}

/* Obtenir tick connexion */
NU32 Projet_Commun_Reseau_Client_BEtatClient_ObtenirTickConnexion( const BEtatClient *this )
{
	return this->m_tickConnexion;
}

/* Est pret? */
BEtatPret Projet_Commun_Reseau_Client_BEtatClient_EstPret( const BEtatClient *this )
{
	return this->m_estPret;
}

/* Confirmer le lancement */
void Projet_Commun_Reseau_Client_BEtatClient_ConfirmerLancement( BEtatClient *this )
{
	this->m_estConfirmeLancement = NTRUE;
}

/* Definir nom */
NBOOL Projet_Commun_Reseau_Client_BEtatClient_DefinirNom( BEtatClient *this,
	const char *nom )
{
	// Nouveau nom
	char *nouveauNom;

	// Allouer
	if( !( nouveauNom = calloc( strlen( nom ) + 1,
		sizeof( char ) ) ) )
	{
		// Notifier
		NOTIFIER_ERREUR( NERREUR_ALLOCATION_FAILED );

		// Quitter
		return NFALSE;
	}

	// Copier
	memcpy( nouveauNom,
		nom,
		strlen( nom ) );

	// Liberer
	NFREE( this->m_nom );

	// Enregistrer
	this->m_nom = nouveauNom;

	// OK
	return NTRUE;
}

/* Definir charset */
void Projet_Commun_Reseau_Client_BEtatClient_DefinirCharset( BEtatClient *this,
	NU32 charset )
{
	this->m_charset = charset;
}

/* Definir couleur charset */
void Projet_Commun_Reseau_Client_BEtatClient_DefinirCouleurCharset( BEtatClient *this,
	NU32 couleur )
{
	this->m_couleurCharset = couleur;
}

/* Definir donnee supplementaire */
void Projet_Commun_Reseau_Client_BEtatClient_DefinirDonneeSupplementaire( BEtatClient *this,
	void *data )
{
	// Enregistrer
	this->m_donneeSupplementaire = data;
}

/* Obtenir puissance */
NU32 Projet_Commun_Reseau_Client_BEtatClient_ObtenirPuissance( const BEtatClient *this )
{
	return this->m_puissance;
}

/* Definir etat verification */
void Projet_Commun_Reseau_Client_BEtatClient_DefinirEstVerifie( BEtatClient *this,
	NBOOL estVerifie )
{
	this->m_estChecksumVerifie = estVerifie;
}

/* Definir est pret */
void Projet_Commun_Reseau_Client_BEtatClient_DefinirEstPret( BEtatClient *this,
	BEtatPret etat )
{
	this->m_estPret = etat;
}

/* Definir position */
void Projet_Commun_Reseau_Client_BEtatClient_DefinirPosition( BEtatClient *this,
	NSPoint position )
{
	this->m_position = position;
}

/* Definir direction */
void Projet_Commun_Reseau_Client_BEtatClient_DefinirDirection( BEtatClient *this,
	NDirection direction )
{
	this->m_direction = direction;
}

/* Definir puissance */
void Projet_Commun_Reseau_Client_BEtatClient_DefinirPuissance( BEtatClient *this,
	NU32 puissance )
{
	this->m_puissance = puissance;
}

/* Definir est en vie */
void Projet_Commun_Reseau_Client_BEtatClient_DefinirEstEnVie( BEtatClient *this,
	NBOOL estEnVie )
{
	// Est en vie?
	this->m_estEnVie = estEnVie;
}

/* Est en vie? */
NBOOL Projet_Commun_Reseau_Client_BEtatClient_EstEnVie( const BEtatClient *this )
{
	return this->m_estEnVie;
}

/* Obtenir direction */
NDirection Projet_Commun_Reseau_Client_BEtatClient_ObtenirDirection( const BEtatClient *this )
{
	return this->m_direction;
}

/* Incrementer nombre bombes maximum */
void Projet_Commun_Reseau_Client_BEtatClient_IncrementerNombreBombeMaximum( BEtatClient *this )
{
	if( this->m_nombreBombeMaximum < BNOMBRE_BOMBE_MAXIMUM_JOUEUR )
		this->m_nombreBombeMaximum++;
}

/* Incrementer/decrementer nombre bombes posees */
void Projet_Commun_Reseau_Client_BEtatClient_IncrementerNombreBombePosee( BEtatClient *this )
{
	this->m_nombreBombePosee++;
}

void Projet_Commun_Reseau_Client_BEtatClient_DecrementerNombreBombePosee( BEtatClient *this )
{
	// Verifier que le nombre de bombes soit correct
	if( this->m_nombreBombePosee > 0 )
		// Decrementer
		this->m_nombreBombePosee--;
	else
		// Notifier
		NOTIFIER_ERREUR( NERREUR_OBJECT_STATE );
}

/* Lancer l'animation de mort */
void Projet_Commun_Reseau_Client_BEtatClient_LancerAnimationMort( BEtatClient *this )
{
	// Activer l'animation
	this->m_estAnimationMortEnCours = NTRUE;

	// Mettre l'animation a zero
	NLib_Temps_Animation_NEtatAnimation_RemettreAZero( this->m_etatAnimationMort );
}

/* Update */
void Projet_Commun_Reseau_Client_BEtatClient_Update( BEtatClient *this )
{
	// Mettre a jour deplacement
	Projet_Commun_Reseau_Client_BEtatDeplacementClient_Update( this->m_etatDeplacement );

	// Mettre a jour l'animation de mort si necessaire
	if( this->m_estAnimationMortEnCours
		&& !NLib_Temps_Animation_NEtatAnimation_EstFinAnimation( this->m_etatAnimationMort ) )
		// Mettre a jour
		NLib_Temps_Animation_NEtatAnimation_Update( this->m_etatAnimationMort );
}

