#include "../../../../../include/Projet/Projet.h"

/*
	@author SOARES Lucas
*/

// ----------------------------------------
// namespace Projet::Commun::Reseau::Packet
// ----------------------------------------

/* Creer packet */
__ALLOC NPacket *Projet_Commun_Reseau_Packet_Creer( BTypePacket type,
	__WILLBEOWNED void *data )
{
	// Sortie
	__OUTPUT NPacket *out;

	// Fonction creation
	NBOOL ( __CALLBACK *fct )( NPacket*,
		const void* ) = NULL;

	// Construire la base
	if( !( out = Projet_Commun_Reseau_Packet_Forge_CreerPacket( type ) ) )
	{
		// Notifier
		NOTIFIER_ERREUR( NERREUR_CONSTRUCTOR_FAILED );

		// Liberer donnees
		Projet_Commun_Reseau_Packet_Donnee_Liberer2( data,
			type );

		// Quitter
		return NULL;
	}

	// Creer
	switch( type )
	{
		// Client vers serveur
		case BTYPE_PACKET_CLIENT_SERVEUR_TRANSMET_INFORMATIONS_JOUEUR:
			fct = (NBOOL ( ___cdecl* )( NPacket*, const void* ))Projet_Commun_Reseau_Packet_Forge_ForgerPacketClientServeurTransmetInformationsJoueur;
			break;
		case BTYPE_PACKET_CLIENT_SERVEUR_TRANSMET_CHECKSUM:
			fct = (NBOOL ( ___cdecl* )( NPacket*, const void* ))Projet_Commun_Reseau_Packet_Forge_ForgerPacketClientServeurTransmetChecksum;
			break;
		case BTYPE_PACKET_CLIENT_SERVEUR_PING:
			fct = (NBOOL ( ___cdecl* )( NPacket*, const void* ))Projet_Commun_Reseau_Packet_Forge_ForgerPacketClientServeurPing;
			break;
		case BTYPE_PACKET_CLIENT_SERVEUR_PONG:
			fct = (NBOOL ( ___cdecl* )( NPacket*, const void* ))Projet_Commun_Reseau_Packet_Forge_ForgerPacketClientServeurPong;
			break;
		case BTYPE_PACKET_CLIENT_SERVEUR_CHANGE_ETAT_PRET:
			fct = (NBOOL ( ___cdecl* )( NPacket*, const void* ))Projet_Commun_Reseau_Packet_Forge_ForgerPacketClientServeurChangeEtatPret;
			break;
		case BTYPE_PACKET_CLIENT_SERVEUR_CONFIRME_LANCEMENT:
			fct = (NBOOL ( ___cdecl* )( NPacket*, const void* ))Projet_Commun_Reseau_Packet_Forge_ForgerPacketClientServeurConfirmeLancement;
			break;
		case BTYPE_PACKET_CLIENT_SERVEUR_CHANGE_DIRECTION:
			fct = (NBOOL ( ___cdecl* )( NPacket*, const void* ))Projet_Commun_Reseau_Packet_Forge_ForgerPacketClientServeurChangeDirection;
			break;
		case BTYPE_PACKET_CLIENT_SERVEUR_CHANGE_POSITION:
			fct = (NBOOL ( ___cdecl* )( NPacket*, const void* ))Projet_Commun_Reseau_Packet_Forge_ForgerPacketClientServeurChangePosition;
			break;
		case BTYPE_PACKET_CLIENT_SERVEUR_POSE_BOMBE:
			fct = (NBOOL ( ___cdecl* )( NPacket*, const void* ))Projet_Commun_Reseau_Packet_Forge_ForgerPacketClientServeurPoseBombe;
			break;

		// Serveur vers client
		case BTYPE_PACKET_SERVEUR_CLIENT_CONNEXION_TRANSMET_IDENTIFIANT:
			fct = (NBOOL ( ___cdecl* )( NPacket*, const void* ))Projet_Commun_Reseau_Packet_Forge_ForgerPacketServeurClientConnexionTransmetIdentifiant;
			break;
		case BTYPE_PACKET_SERVEUR_CLIENT_REPONSE_INFORMATIONS_JOUEUR:
			fct = (NBOOL ( ___cdecl* )( NPacket*, const void* ))Projet_Commun_Reseau_Packet_Forge_ForgerPacketServeurClientReponseInformationsJoueur;
			break;
		case BTYPE_PACKET_SERVEUR_CLIENT_DECONNEXION_JOUEUR:
			fct = (NBOOL ( ___cdecl* )( NPacket*, const void* ))Projet_Commun_Reseau_Packet_Forge_ForgerPacketServeurClientDeconnexionJoueur;
			break;
		case BTYPE_PACKET_SERVEUR_CLIENT_CHECKSUM_INCORRECT:
			fct = (NBOOL ( ___cdecl* )( NPacket*, const void* ))Projet_Commun_Reseau_Packet_Forge_ForgerPacketServeurClientChecksumIncorrect;
			break;
		case BTYPE_PACKET_SERVEUR_CLIENT_PING:
			fct = (NBOOL ( ___cdecl* )( NPacket*, const void* ))Projet_Commun_Reseau_Packet_Forge_ForgerPacketServeurClientPing;
			break;
		case BTYPE_PACKET_SERVEUR_CLIENT_PONG:
			fct = (NBOOL ( ___cdecl* )( NPacket*, const void* ))Projet_Commun_Reseau_Packet_Forge_ForgerPacketServeurClientPong;
			break;
		case BTYPE_PACKET_SERVEUR_CLIENT_DIFFUSE_ETAT_PRET:
			fct = (NBOOL ( ___cdecl* )( NPacket*, const void* ))Projet_Commun_Reseau_Packet_Forge_ForgerPacketServeurClientDiffuseEtatPret;
			break;
		case BTYPE_PACKET_SERVEUR_CLIENT_DIFFUSE_CHANGEMENT_CARTE:
			fct = (NBOOL ( ___cdecl* )( NPacket*, const void* ))Projet_Commun_Reseau_Packet_Forge_ForgerPacketServeurClientDiffuseChangementCarte;
			break;
		case BTYPE_PACKET_SERVEUR_CLIENT_DIFFUSE_LANCEMENT:
			fct = (NBOOL ( ___cdecl* )( NPacket*, const void* ))Projet_Commun_Reseau_Packet_Forge_ForgerPacketServeurClientDiffuseLancement;
			break;
		case BTYPE_PACKET_SERVEUR_CLIENT_DIFFUSE_MESSAGE_AFFICHER:
			fct = (NBOOL ( ___cdecl* )( NPacket*, const void* ))Projet_Commun_Reseau_Packet_Forge_ForgerPacketServeurClientDiffuseMessageAfficher;
			break;
		case BTYPE_PACKET_SERVEUR_CLIENT_DIFFUSE_DEBUT_PARTIE:
			fct = (NBOOL ( ___cdecl* )( NPacket*, const void* ))Projet_Commun_Reseau_Packet_Forge_ForgerPacketServeurClientDiffuseDebutPartie;
			break;
		case BTYPE_PACKET_SERVEUR_CLIENT_DIFFUSE_CHANGEMENT_DIRECTION:
			fct = (NBOOL ( ___cdecl* )( NPacket*, const void* ))Projet_Commun_Reseau_Packet_Forge_ForgerPacketServeurClientDiffuseChangementDirection;
			break;
		case BTYPE_PACKET_SERVEUR_CLIENT_DIFFUSE_CHANGEMENT_POSITION:
			fct = (NBOOL ( ___cdecl* )( NPacket*, const void* ))Projet_Commun_Reseau_Packet_Forge_ForgerPacketServeurClientDiffuseChangementPosition;
			break;
		case BTYPE_PACKET_SERVEUR_CLIENT_DIFFUSE_POSE_BOMBE:
			fct = (NBOOL ( ___cdecl* )( NPacket*, const void* ))Projet_Commun_Reseau_Packet_Forge_ForgerPacketServeurClientDiffusePoseBombe;
			break;
		case BTYPE_PACKET_SERVEUR_CLIENT_REFUSE_POSE_BOMBE:
			fct = (NBOOL ( ___cdecl* )( NPacket*, const void* ))Projet_Commun_Reseau_Packet_Forge_ForgerPacketServeurClientRefusePoseBombe;
			break;
		case BTYPE_PACKET_SERVEUR_CLIENT_BOMBE_EXPLOSE:
			fct = (NBOOL ( ___cdecl* )( NPacket*, const void* ))Projet_Commun_Reseau_Packet_Forge_ForgerPacketServeurClientBombeExplose;
			break;
		case BTYPE_PACKET_SERVEUR_CLIENT_BLOC_REMPLI_DETRUIT:
			fct = (NBOOL ( ___cdecl* )( NPacket*, const void* ))Projet_Commun_Reseau_Packet_Forge_ForgerPacketServeurClientBlocRempliDetruit;
			break;
		case BTYPE_PACKET_SERVEUR_CLIENT_DIFFUSE_APPARITION_BONUS:
			fct = (NBOOL ( ___cdecl* )( NPacket*, const void* ))Projet_Commun_Reseau_Packet_Forge_ForgerPacketServeurClientDiffuseApparitionBonus;
			break;
		case BTYPE_PACKET_SERVEUR_CLIENT_DIFFUSE_DISPARITION_BONUS:
			fct = (NBOOL ( ___cdecl* )( NPacket*, const void* ))Projet_Commun_Reseau_Packet_Forge_ForgerPacketServeurClientDiffuseDisparitionBonus;
			break;
		case BTYPE_PACKET_SERVEUR_CLIENT_DISTRIBUE_BONUS:
			fct = (NBOOL ( ___cdecl* )( NPacket*, const void* ))Projet_Commun_Reseau_Packet_Forge_ForgerPacketServeurClientDistribueBonus;
			break;
		case BTYPE_PACKET_SERVEUR_CLIENT_DIFFUSE_MORT_JOUEUR:
			fct = (NBOOL ( ___cdecl* )( NPacket*, const void* ))Projet_Commun_Reseau_Packet_Forge_ForgerPacketServeurClientDiffuseMortJoueur;
			break;
		case BTYPE_PACKET_SERVEUR_CLIENT_ANNONCE_FIN_PARTIE:
			fct = (NBOOL ( ___cdecl* )( NPacket*, const void* ))Projet_Commun_Reseau_Packet_Forge_ForgerPacketServeurClientAnnonceFinPartie;
			break;

		default:
			// Notifier
			NOTIFIER_ERREUR( NERREUR_UNKNOWN_PACKET );

			// Liberer donnees
			Projet_Commun_Reseau_Packet_Donnee_Liberer2( data,
				type );

			// Quitter
			return NULL;
	}

	// Executer
	if( !fct( out,
		data ) )
	{
		// Notifier
		NOTIFIER_ERREUR( NERREUR_CONSTRUCTOR_FAILED );

		// Liberer donnees
		Projet_Commun_Reseau_Packet_Donnee_Liberer2( data,
			type );

		// Liberer
		NLib_Module_Reseau_Packet_NPacket_Detruire( &out );

		// Quitter
		return NULL;
	}

	// Liberer donnees
	Projet_Commun_Reseau_Packet_Donnee_Liberer2( data,
		type );

	// OK
	return out;
}

__ALLOC void *Projet_Commun_Reseau_Lire( const NPacket *packet,
	__OUTPUT BTypePacket *typePacket )
{
	// Curseur dans les donnees du packet
	NU32 curseur = 0;

	// Donnees du packet
	const char *donnee;

	// Donnee lue (sous forme de structure propre a chaque packet)
	void *donneeLue;

	// Fonction a appeler pour recuperer les donnees suivant le type de packet
	void *( __CALLBACK *fct )( const char*, NU32*, NU32 );

	// Recuperer les donnees
	donnee = NLib_Module_Reseau_Packet_NPacket_ObtenirData( packet );

	// Traiter le packet
	switch( ( *typePacket = Projet_Commun_Reseau_Packet_Lecteur_LireType( donnee,
		&curseur,
		NLib_Module_Reseau_Packet_NPacket_ObtenirTaille( packet ) ) ) )
	{
		// Client vers serveur
		case BTYPE_PACKET_CLIENT_SERVEUR_TRANSMET_INFORMATIONS_JOUEUR:
			fct = (void *( ___cdecl* )( const char*, NU32*, NU32 ))Projet_Commun_Reseau_Packet_Lecteur_LirePacketClientServeurTransmetInformationsJoueur;
			break;
		case BTYPE_PACKET_CLIENT_SERVEUR_TRANSMET_CHECKSUM:
			fct = (void *( ___cdecl* )( const char*, NU32*, NU32 ))Projet_Commun_Reseau_Packet_Lecteur_LirePacketClientServeurTransmetChecksum;
			break;
		case BTYPE_PACKET_CLIENT_SERVEUR_PING:
			fct = (void *( ___cdecl* )( const char*, NU32*, NU32 ))Projet_Commun_Reseau_Packet_Lecteur_LirePacketClientServeurPing;
			break;
		case BTYPE_PACKET_CLIENT_SERVEUR_PONG:
			fct = (void *( ___cdecl* )( const char*, NU32*, NU32 ))Projet_Commun_Reseau_Packet_Lecteur_LirePacketClientServeurPong;
			break;
		case BTYPE_PACKET_CLIENT_SERVEUR_CHANGE_ETAT_PRET:
			fct = (void *( ___cdecl* )( const char*, NU32*, NU32 ))Projet_Commun_Reseau_Packet_Lecteur_LirePacketClientServeurChangeEtatPret;
			break;
		case BTYPE_PACKET_CLIENT_SERVEUR_CONFIRME_LANCEMENT:
			fct = (void *( ___cdecl* )( const char*, NU32*, NU32 ))Projet_Commun_Reseau_Packet_Lecteur_LirePacketClientServeurConfirmeLancement;
			break;
		case BTYPE_PACKET_CLIENT_SERVEUR_CHANGE_DIRECTION:
			fct = (void *( ___cdecl* )( const char*, NU32*, NU32 ))Projet_Commun_Reseau_Packet_Lecteur_LirePacketClientServeurChangeDirection;
			break;
		case BTYPE_PACKET_CLIENT_SERVEUR_CHANGE_POSITION:
			fct = (void *( ___cdecl* )( const char*, NU32*, NU32 ))Projet_Commun_Reseau_Packet_Lecteur_LirePacketClientServeurChangePosition;
			break;
		case BTYPE_PACKET_CLIENT_SERVEUR_POSE_BOMBE:
			fct = (void *( ___cdecl* )( const char*, NU32*, NU32 ))Projet_Commun_Reseau_Packet_Lecteur_LirePacketClientServeurPoseBombe;
			break;

		// Serveur vers client
		case BTYPE_PACKET_SERVEUR_CLIENT_CONNEXION_TRANSMET_IDENTIFIANT:
			fct = (void *( ___cdecl* )( const char*, NU32*, NU32 ))Projet_Commun_Reseau_Packet_Lecteur_LirePacketServeurClientConnexionTransmetIdentifiant;
			break;
		case BTYPE_PACKET_SERVEUR_CLIENT_REPONSE_INFORMATIONS_JOUEUR:
			fct = (void *( ___cdecl* )( const char*, NU32*, NU32 ))Projet_Commun_Reseau_Packet_Lecteur_LirePacketServeurClientReponseInformationsJoueur;
			break;
		case BTYPE_PACKET_SERVEUR_CLIENT_DECONNEXION_JOUEUR:
			fct = (void *( ___cdecl* )( const char*, NU32*, NU32 ))Projet_Commun_Reseau_Packet_Lecteur_LirePacketServeurClientDeconnexionJoueur;
			break;
		case BTYPE_PACKET_SERVEUR_CLIENT_CHECKSUM_INCORRECT:
			fct = (void *( ___cdecl* )( const char*, NU32*, NU32 ))Projet_Commun_Reseau_Packet_Lecteur_LirePacketServeurClientChecksumIncorrect;
			break;
		case BTYPE_PACKET_SERVEUR_CLIENT_PING:
			fct = (void *( ___cdecl* )( const char*, NU32*, NU32 ))Projet_Commun_Reseau_Packet_Lecteur_LirePacketServeurClientPing;
			break;
		case BTYPE_PACKET_SERVEUR_CLIENT_PONG:
			fct = (void *( ___cdecl* )( const char*, NU32*, NU32 ))Projet_Commun_Reseau_Packet_Lecteur_LirePacketServeurClientPong;
			break;
		case BTYPE_PACKET_SERVEUR_CLIENT_DIFFUSE_ETAT_PRET:
			fct = (void *( ___cdecl* )( const char*, NU32*, NU32 ))Projet_Commun_Reseau_Packet_Lecteur_LirePacketServeurClientDiffuseEtatPret;
			break;
		case BTYPE_PACKET_SERVEUR_CLIENT_DIFFUSE_CHANGEMENT_CARTE:
			fct = (void *( ___cdecl* )( const char*, NU32*, NU32 ))Projet_Commun_Reseau_Packet_Lecteur_LirePacketServeurClientDiffuseChangementCarte;
			break;
		case BTYPE_PACKET_SERVEUR_CLIENT_DIFFUSE_LANCEMENT:
			fct = (void *( ___cdecl* )( const char*, NU32*, NU32 ))Projet_Commun_Reseau_Packet_Lecteur_LirePacketServeurClientDiffuseLancement;
			break;
		case BTYPE_PACKET_SERVEUR_CLIENT_DIFFUSE_MESSAGE_AFFICHER:
			fct = (void *( ___cdecl* )( const char*, NU32*, NU32 ))Projet_Commun_Reseau_Packet_Lecteur_LirePacketServeurClientDiffuseMessageAfficher;
			break;
		case BTYPE_PACKET_SERVEUR_CLIENT_DIFFUSE_DEBUT_PARTIE:
			fct = (void *( ___cdecl* )( const char*, NU32*, NU32 ))Projet_Commun_Reseau_Packet_Lecteur_LirePacketServeurClientDiffuseDebutPartie;
			break;
		case BTYPE_PACKET_SERVEUR_CLIENT_DIFFUSE_CHANGEMENT_DIRECTION:
			fct = (void *( ___cdecl* )( const char*, NU32*, NU32 ))Projet_Commun_Reseau_Packet_Lecteur_LirePacketServeurClientDiffuseChangementDirection;
			break;
		case BTYPE_PACKET_SERVEUR_CLIENT_DIFFUSE_CHANGEMENT_POSITION:
			fct = (void *( ___cdecl* )( const char*, NU32*, NU32 ))Projet_Commun_Reseau_Packet_Lecteur_LirePacketServeurClientDiffuseChangementPosition;
			break;
		case BTYPE_PACKET_SERVEUR_CLIENT_DIFFUSE_POSE_BOMBE:
			fct = (void *( ___cdecl* )( const char*, NU32*, NU32 ))Projet_Commun_Reseau_Packet_Lecteur_LirePacketServeurClientDiffusePoseBombe;
			break;
		case BTYPE_PACKET_SERVEUR_CLIENT_REFUSE_POSE_BOMBE:
			fct = (void *( ___cdecl* )( const char*, NU32*, NU32 ))Projet_Commun_Reseau_Packet_Lecteur_LirePacketServeurClientRefusePoseBombe;
			break;
		case BTYPE_PACKET_SERVEUR_CLIENT_BOMBE_EXPLOSE:
			fct = (void *( ___cdecl* )( const char*, NU32*, NU32 ))Projet_Commun_Reseau_Packet_Lecteur_LirePacketServeurClientBombeExplose;
			break;
		case BTYPE_PACKET_SERVEUR_CLIENT_BLOC_REMPLI_DETRUIT:
			fct = (void *( ___cdecl* )( const char*, NU32*, NU32 ))Projet_Commun_Reseau_Packet_Lecteur_LirePacketServeurClientBlocRempliDetruit;
			break;
		case BTYPE_PACKET_SERVEUR_CLIENT_DIFFUSE_APPARITION_BONUS:
			fct = (void *( ___cdecl* )( const char*, NU32*, NU32 ))Projet_Commun_Reseau_Packet_Lecteur_LirePacketServeurClientDiffuseApparitionBonus;
			break;
		case BTYPE_PACKET_SERVEUR_CLIENT_DIFFUSE_DISPARITION_BONUS:
			fct = (void *( ___cdecl* )( const char*, NU32*, NU32 ))Projet_Commun_Reseau_Packet_Lecteur_LirePacketServeurClientDiffuseDisparitionBonus;
			break;
		case BTYPE_PACKET_SERVEUR_CLIENT_DISTRIBUE_BONUS:
			fct = (void *( ___cdecl* )( const char*, NU32*, NU32 ))Projet_Commun_Reseau_Packet_Lecteur_LirePacketServeurClientDistribueBonus;
			break;
		case BTYPE_PACKET_SERVEUR_CLIENT_DIFFUSE_MORT_JOUEUR:
			fct = (void *( ___cdecl* )( const char*, NU32*, NU32 ))Projet_Commun_Reseau_Packet_Lecteur_LirePacketServeurClientDiffuseMortJoueur;
			break;
		case BTYPE_PACKET_SERVEUR_CLIENT_ANNONCE_FIN_PARTIE:
			fct = (void *( ___cdecl* )( const char*, NU32*, NU32 ))Projet_Commun_Reseau_Packet_Lecteur_LirePacketServeurClientAnnonceFinPartie;
			break;

		default:
			// Notifier
			NOTIFIER_ERREUR( NERREUR_UNKNOWN_PACKET );

			// Quitter
			return NULL;
	}

	// Executer la lecture
	if( !( donneeLue = fct( donnee,
		&curseur,
		NLib_Module_Reseau_Packet_NPacket_ObtenirTaille( packet ) ) ) )
	{
		// Notifier
		NOTIFIER_ERREUR( NERREUR_CONSTRUCTOR_FAILED );

		// Quitter
		return NULL;
	}

	// OK
	return donneeLue;
}

