#include "../../../../../../include/Projet/Projet.h"

// -----------------------------------------------
// namespace Projet::Commun::Reseau::Packet::Forge
// -----------------------------------------------

#define AJOUTER( d, t ) \
	NLib_Module_Reseau_Packet_NPacket_AjouterData( out, \
		(char*)d, \
		t )

__ALLOC NPacket *Projet_Commun_Reseau_Packet_Forge_CreerPacket( BTypePacket type )
{
	// Sortie
	__OUTPUT NPacket *out;

	// Construire
	if( !( out = NLib_Module_Reseau_Packet_NPacket_Construire( ) ) )
	{
		// Notifier
		NOTIFIER_ERREUR( NERREUR_CONSTRUCTOR_FAILED );

		// Quitter
		return NULL;
	}

	// Ajouter type
	if( !AJOUTER( &type,
		sizeof( BTypePacket ) ) )
	{
		// Notifier
		NOTIFIER_ERREUR( NERREUR_MEMORY );

		// Liberer
		NLib_Module_Reseau_Packet_NPacket_Detruire( &out );

		// Quitter
		return NULL;
	}

	// OK
	return out;
}

#define ERREUR( ) \
	{ \
		/* Notifier */ \
		NOTIFIER_ERREUR( NERREUR_CONSTRUCTOR_FAILED ); \
 \
		/* Quitter */ \
		return NFALSE; \
	}

#undef AJOUTER
#define AJOUTER( d, t ) \
	NLib_Module_Reseau_Packet_NPacket_AjouterData( packet, \
		(char*)d, \
		t )

/* Forge */
// --------------------
// Client vers serveur:
// --------------------
// BTYPE_PACKET_CLIENT_SERVEUR_TRANSMET_INFORMATIONS_JOUEUR
NBOOL Projet_Commun_Reseau_Packet_Forge_ForgerPacketClientServeurTransmetInformationsJoueur( NPacket *packet,
	const struct BPacketClientServeurTransmetInformationsJoueur *data )
{
	// Taille nom
	NU32 tailleNom;

	// Calculer taille nom
	tailleNom = strlen( data->m_nom );

	// Ajouter
		// Taille nom
			if( !AJOUTER( &tailleNom,
				sizeof( NU32 ) )
		// Nom
				|| !AJOUTER( data->m_nom,
					tailleNom )
		// ID charset
				|| !AJOUTER( &data->m_charset,
					sizeof( NU32 ) )
		// ID couleur charset
				|| !AJOUTER( &data->m_couleurCharset,
					sizeof( NU32 ) ) )
			ERREUR( );
		
	// OK
	return NTRUE;
}

// BTYPE_PACKET_CLIENT_SERVEUR_TRANSMET_CHECKSUM
NBOOL Projet_Commun_Reseau_Packet_Forge_ForgerPacketClientServeurTransmetChecksum( NPacket *packet,
	const struct BPacketClientServeurTransmetChecksum *data )
{
	// Ajouter checksum
	if( !AJOUTER( &data->m_checksum,
		sizeof( NU32 ) ) )
		ERREUR( );

	// OK
	return NTRUE;
}

// BTYPE_PACKET_CLIENT_SERVEUR_PING
NBOOL Projet_Commun_Reseau_Packet_Forge_ForgerPacketClientServeurPing( NPacket *packet,
	const struct BPacketClientServeurPing *data )
{
	// Ajouter
		// Identifiant
			if( !AJOUTER( &data->m_identifiant,
				sizeof( NU32 ) ) )
				ERREUR( );

	// OK
	return NTRUE;
}

// BTYPE_PACKET_CLIENT_SERVEUR_PONG
NBOOL Projet_Commun_Reseau_Packet_Forge_ForgerPacketClientServeurPong( NPacket *packet,
	const struct BPacketClientServeurPong *data )
{
	// Ajouter
		// Identifiant
			if( !AJOUTER( &data->m_identifiant,
				sizeof( NU32 ) ) )
				ERREUR( );

	// OK
	return NTRUE;
}

// BTYPE_PACKET_CLIENT_SERVEUR_CHANGE_ETAT_PRET
NBOOL Projet_Commun_Reseau_Packet_Forge_ForgerPacketClientServeurChangeEtatPret( NPacket *packet,
	const struct BPacketClientServeurChangeEtatPret *data )
{
	// Ajouter identifiant
	if( !AJOUTER( &data->m_identifiant,
		sizeof( NU32 ) ) )
		ERREUR( );

	// OK
	return NTRUE;
}

// BTYPE_PACKET_CLIENT_SERVEUR_CONFIRME_LANCEMENT
NBOOL Projet_Commun_Reseau_Packet_Forge_ForgerPacketClientServeurConfirmeLancement( NPacket *packet,
	const struct BPacketClientServeurConfirmeLancement *data )
{
	// Ajouter identifiant
	if( !AJOUTER( &data->m_identifiant,
		sizeof( NU32 ) ) )
		ERREUR( );

	// OK
	return NTRUE;
}

// BTYPE_PACKET_CLIENT_SERVEUR_CHANGE_DIRECTION
NBOOL Projet_Commun_Reseau_Packet_Forge_ForgerPacketClientServeurChangeDirection( NPacket *packet,
	const struct BPacketClientServeurChangeDirection *data )
{
	// Ajouter direction
	if( !AJOUTER( &data->m_direction,
		sizeof( NDirection ) ) )
		ERREUR( );

	// OK
	return NTRUE;
}

// BTYPE_PACKET_CLIENT_SERVEUR_CHANGE_POSITION
NBOOL Projet_Commun_Reseau_Packet_Forge_ForgerPacketClientServeurChangePosition( NPacket *packet,
	const struct BPacketClientServeurChangePosition *data )
{
	// Ajouter direction
	if( !AJOUTER( &data->m_direction,
		sizeof( NDirection ) ) )
		ERREUR( );

	// OK
	return NTRUE;
}

// BTYPE_PACKET_CLIENT_SERVEUR_POSE_BOMBE
NBOOL Projet_Commun_Reseau_Packet_Forge_ForgerPacketClientServeurPoseBombe( NPacket *packet,
	const struct BPacketClientServeurPoseBombe *data )
{
	// Ajouter identifiant
	if( !AJOUTER( &data->m_identifiant,
		sizeof( NU32 ) ) )
		ERREUR( );

	// OK
	return NTRUE;
}

// -------------------
// Serveur vers client
// -------------------
// BTYPE_PACKET_SERVEUR_CLIENT_CONNEXION_TRANSMET_IDENTIFIANT
NBOOL Projet_Commun_Reseau_Packet_Forge_ForgerPacketServeurClientConnexionTransmetIdentifiant( NPacket *packet,
	const struct BPacketServeurClientConnexionTransmetIdentifiant *data )
{
	// Ajouter
		// Identifiant
			if( !AJOUTER( &data->m_identifiant,
				sizeof( NU32 ) ) )
				ERREUR( );

	// OK
	return NTRUE;
}

// BTYPE_PACKET_SERVEUR_CLIENT_REPONSE_INFORMATIONS_JOUEUR
NBOOL Projet_Commun_Reseau_Packet_Forge_ForgerPacketServeurClientReponseInformationsJoueur( NPacket *packet,
	const struct BPacketServeurClientReponseInformationsJoueur *data )
{
	// Taille nom
	NU32 tailleNom;

	// Calculer taille nom
	tailleNom = strlen( data->m_nom );

	// Ajouter
		// Taille nom
			if( !AJOUTER( &tailleNom,
				sizeof( NU32 ) )
		// Nom
				|| !AJOUTER( data->m_nom,
					tailleNom )
		// ID charset
				|| !AJOUTER( &data->m_charset,
					sizeof( NU32 ) )
		// ID couleur charset
				|| !AJOUTER( &data->m_couleurCharset,
					sizeof( NU32 ) )
		// Identifiant
				|| !AJOUTER( &data->m_identifiant,
					sizeof( NU32 ) ) )
				ERREUR( );
		
	// OK
	return NTRUE;
}

// BTYPE_PACKET_SERVEUR_CLIENT_DECONNEXION_JOUEUR
NBOOL Projet_Commun_Reseau_Packet_Forge_ForgerPacketServeurClientDeconnexionJoueur( NPacket *packet,
	const struct BPacketServeurClientDeconnexionJoueur *data )
{
	// Ajouter
		// Identifiant
			if( !AJOUTER( &data->m_identifiant,
				sizeof( NU32 ) ) )
				ERREUR( );

	// OK
	return NTRUE;
}

// BTYPE_PACKET_SERVEUR_CLIENT_CHECKSUM_INCORRECT
NBOOL Projet_Commun_Reseau_Packet_Forge_ForgerPacketServeurClientChecksumIncorrect( NPacket *packet,
	const struct BPacketServeurClientChecksumIncorrect *data )
{
	// Ajouter identifiant
	if( !AJOUTER( &data->m_identifiant,
		sizeof( NU32 ) ) )
		ERREUR( );

	// OK
	return NTRUE;
}

// BTYPE_PACKET_SERVEUR_CLIENT_PING
NBOOL Projet_Commun_Reseau_Packet_Forge_ForgerPacketServeurClientPing( NPacket *packet,
	const struct BPacketServeurClientPing *data )
{
	// Ajouter identifiant
	if( !AJOUTER( &data->m_identifiant,
		sizeof( NU32 ) ) )
		ERREUR( );

	// OK
	return NTRUE;
}

// BTYPE_PACKET_SERVEUR_CLIENT_PONG
NBOOL Projet_Commun_Reseau_Packet_Forge_ForgerPacketServeurClientPong( NPacket *packet,
	const struct BPacketServeurClientPong *data )
{
	// Ajouter identifiant
	if( !AJOUTER( &data->m_identifiant,
		sizeof( NU32 ) ) )
		ERREUR( );

	// OK
	return NTRUE;
}

// BTYPE_PACKET_SERVEUR_CLIENT_DIFFUSE_ETAT_PRET
NBOOL Projet_Commun_Reseau_Packet_Forge_ForgerPacketServeurClientDiffuseEtatPret( NPacket *packet,
	const struct BPacketServeurClientDiffuseEtatPret *data )
{
	// Ajouter
		// Identifiant
			if( !AJOUTER( &data->m_identifiant,
				sizeof( NU32 ) )
		// Etat
				|| !AJOUTER( &data->m_etat,
					sizeof( BEtatPret ) ) )
				ERREUR( );

	// OK
	return NTRUE;
}

// BTYPE_PACKET_SERVEUR_CLIENT_DIFFUSE_CHANGEMENT_CARTE
NBOOL Projet_Commun_Reseau_Packet_Forge_ForgerPacketServeurClientDiffuseChangementCarte( NPacket *packet,
	const struct BPacketServeurClientDiffuseChangementCarte *data )
{
	// Ajouter
		// Identifiant
			if( !AJOUTER( &data->m_identifiant,
				sizeof( NU32 ) ) )
				ERREUR( );

	// OK
	return NTRUE;
}

// BTYPE_PACKET_SERVEUR_CLIENT_DIFFUSE_LANCEMENT
NBOOL Projet_Commun_Reseau_Packet_Forge_ForgerPacketServeurClientDiffuseLancement( NPacket *packet,
	const struct BPacketServeurClientDiffuseLancement *data )
{
	// Iterateur
	NU32 i;

	// Ajouter joueur
		// Nombre
			if( !AJOUTER( &data->m_nombreJoueur,
				sizeof( NU32 ) ) )
				ERREUR( );
		// Identifiants/Position
			for( i = 0; i < data->m_nombreJoueur; i++ )
				// Identifiant
				if( !AJOUTER( &data->m_identifiantJoueur[ i ],
					sizeof( NU32 ) )
				// Position
					// x
						|| !AJOUTER( &data->m_positionJoueur[ i ].x,
							sizeof( NS32 ) )
					// y
						|| !AJOUTER( &data->m_positionJoueur[ i ].y,
							sizeof( NS32 ) ) )
					ERREUR( );

	// Ajouter carte
		// Identifiant
			if( !AJOUTER( &data->m_identifiantCarte,
				sizeof( NU32 ) )
		// Taille carte
			// x
				|| !AJOUTER( &data->m_tailleCarte.x,
					sizeof( NU32 ) )
			// y
				|| !AJOUTER( &data->m_tailleCarte.y,
					sizeof( NU32 ) ) )
				ERREUR( );
		// Est rempli?
			for( i = 0; i < data->m_tailleCarte.x; i++ )
				if( !AJOUTER( data->m_estCaseRemplie[ i ],
						sizeof( NBOOL ) * data->m_tailleCarte.y ) )
						ERREUR( );

	// OK
	return NTRUE;
}

// BTYPE_PACKET_SERVEUR_CLIENT_DIFFUSE_MESSAGE_AFFICHER
NBOOL Projet_Commun_Reseau_Packet_Forge_ForgerPacketServeurClientDiffuseMessageAfficher( NPacket *packet,
	const struct BPacketServeurClientDiffuseMessageAfficher *data )
{
	// Ajouter
		// Taille message
			if( !AJOUTER( &data->m_tailleMessage,
				sizeof( NU32 ) )
		// Message
				|| !AJOUTER( data->m_message,
					sizeof( char ) * data->m_tailleMessage )
		// Duree
				|| !AJOUTER( &data->m_dureeAffichage,
					sizeof( NU32 ) )
		// Police
				|| !AJOUTER( &data->m_police,
					sizeof( BPoliceMessageClient ) )
		// Couleur
				|| !AJOUTER( &data->m_couleur.r,
					sizeof( NU8 ) )
				|| !AJOUTER( &data->m_couleur.g,
					sizeof( NU8 ) )
				|| !AJOUTER( &data->m_couleur.b,
					sizeof( NU8 ) )
		// Position
				|| !AJOUTER( &data->m_position,
					sizeof( BPositionAffichageMessageClient ) )
		// Est doit afficher cadre?
				|| !AJOUTER( &data->m_estDoitAfficherCadre,
					sizeof( NBOOL ) )
		// Couleur cadre
				|| !AJOUTER( &data->m_couleurCadre.r,
					sizeof( NU8 ) )
				|| !AJOUTER( &data->m_couleurCadre.g,
					sizeof( NU8 ) )
				|| !AJOUTER( &data->m_couleurCadre.b,
					sizeof( NU8 ) )
				|| !AJOUTER( &data->m_couleurCadre.a,
					sizeof( NU8 ) ) )
			ERREUR( );

	// OK
	return NTRUE;
}

// BTYPE_PACKET_SERVEUR_CLIENT_DIFFUSE_DEBUT_PARTIE
NBOOL Projet_Commun_Reseau_Packet_Forge_ForgerPacketServeurClientDiffuseDebutPartie( NPacket *packet,
	const struct BPacketServeurClientDiffuseDebutPartie *data )
{
	// Ajouter identifiant
	if( !AJOUTER( &data->m_identifiant,
		sizeof( NU32 ) ) )
		ERREUR( );

	// OK
	return NTRUE;
}

// BTYPE_PACKET_SERVEUR_CLIENT_DIFFUSE_CHANGEMENT_DIRECTION
NBOOL Projet_Commun_Reseau_Packet_Forge_ForgerPacketServeurClientDiffuseChangementDirection( NPacket *packet,
	const struct BPacketServeurClientDiffuseChangementDirection *data )
{
	// Ajouter identifiant
	if( !AJOUTER( &data->m_identifiant,
		sizeof( NU32 ) )
		// Direction
		|| !AJOUTER( &data->m_direction,
			sizeof( NDirection ) ) )
		ERREUR( );

	// OK
	return NTRUE;
}

// BTYPE_PACKET_SERVEUR_CLIENT_DIFFUSE_CHANGEMENT_POSITION
NBOOL Projet_Commun_Reseau_Packet_Forge_ForgerPacketServeurClientDiffuseChangementPosition( NPacket *packet,
	const struct BPacketServeurClientDiffuseChangementPosition *data )
{
	// Ajouter identifiant
	if( !AJOUTER( &data->m_identifiant,
		sizeof( NU32 ) )
		// Direction
		|| !AJOUTER( &data->m_direction,
			sizeof( NDirection ) )
		// Position
		|| !AJOUTER( &data->m_nouvellePosition.x,
			sizeof( NS32 ) )
		|| !AJOUTER( &data->m_nouvellePosition.y,
			sizeof( NS32 ) ) )
		ERREUR( );

	// OK
	return NTRUE;
}

// BTYPE_PACKET_SERVEUR_CLIENT_DIFFUSE_POSE_BOMBE
NBOOL Projet_Commun_Reseau_Packet_Forge_ForgerPacketServeurClientDiffusePoseBombe( NPacket *packet,
	const struct BPacketServeurClientDiffusePoseBombe *data )
{
	// Ajouter identifiant joueur
	if( !AJOUTER( &data->m_identifiantJoueur,
		sizeof( NU32 ) )
		// Identifiant bobme
		|| !AJOUTER( &data->m_identifiantBombe,
			sizeof( NU32 ) )
		// Position
		|| !AJOUTER( &data->m_position.x,
			sizeof( NS32 ) )
		|| !AJOUTER( &data->m_position.y,
			sizeof( NS32 ) ) )
		ERREUR( );

	// OK
	return NTRUE;
}

// BTYPE_PACKET_SERVEUR_CLIENT_REFUSE_POSE_BOMBE
NBOOL Projet_Commun_Reseau_Packet_Forge_ForgerPacketServeurClientRefusePoseBombe( NPacket *packet,
	const struct BPacketServeurClientRefusePoseBombe *data )
{
	// Ajouter identifiant joueur
	if( !AJOUTER( &data->m_identifiant,
		sizeof( NU32 ) ) )
		ERREUR( );

	// OK
	return NTRUE;
}

// BTYPE_PACKET_SERVEUR_CLIENT_BOMBE_EXPLOSE
NBOOL Projet_Commun_Reseau_Packet_Forge_ForgerPacketServeurClientBombeExplose( NPacket *packet,
	const struct BPacketServeurClientBombeExplose *data )
{
	// Ajouter identifiant bombe
	if( !AJOUTER( &data->m_identifiant,
		sizeof( NU32 ) )
		// Puissance
		|| !AJOUTER( &data->m_puissance,
			sizeof( NU32 ) )
		// Identifiant joueur
		|| !AJOUTER( &data->m_identifiantJoueur,
			sizeof( NU32 ) ) )
		ERREUR( );

	// OK
	return NTRUE;
}

// BTYPE_PACKET_SERVEUR_CLIENT_BLOC_REMPLI_DETRUIT
NBOOL Projet_Commun_Reseau_Packet_Forge_ForgerPacketServeurClientBlocRempliDetruit( NPacket *packet,
	const struct BPacketServeurClientBlocRempliDetruit *data )
{
	// Ajouter position
	if( !AJOUTER( &data->m_position.x,
		sizeof( NS32 ) )
		|| !AJOUTER( &data->m_position.y,
			sizeof( NS32 ) ) )
		ERREUR( );

	// OK
	return NTRUE;
}

NBOOL Projet_Commun_Reseau_Packet_Forge_ForgerPacketServeurClientDiffuseApparitionBonus( NPacket *packet,
	const struct BPacketServeurClientDiffuseApparitionBonus *data )
{
	// Ajouter position
	if( !AJOUTER( &data->m_position.x,
		sizeof( NS32 ) )
		|| !AJOUTER( &data->m_position.y,
			sizeof( NS32 ) )
		// Type de bonus
		|| !AJOUTER( &data->m_type,
			sizeof( BListeBonus ) )
		// Duree
		|| !AJOUTER( &data->m_duree,
			sizeof( NU32 ) )
		// Identifiant bonus
		|| !AJOUTER( &data->m_identifiant,
			sizeof( NU32 ) ) )
		ERREUR( );

	// OK
	return NTRUE;
}

// BTYPE_PACKET_SERVEUR_CLIENT_DIFFUSE_DISPARITION_BONUS
NBOOL Projet_Commun_Reseau_Packet_Forge_ForgerPacketServeurClientDiffuseDisparitionBonus( NPacket *packet,
	const struct BPacketServeurClientDiffuseDisparitionBonus *data )
{
	// Ajouter identifiant
	if( !AJOUTER( &data->m_identifiant,
		sizeof( NU32 ) ) )
		ERREUR( );

	// OK
	return NTRUE;
}

// BTYPE_PACKET_SERVEUR_CLIENT_DISTRIBUE_BONUS
NBOOL Projet_Commun_Reseau_Packet_Forge_ForgerPacketServeurClientDistribueBonus( NPacket *packet,
	const struct BPacketServeurClientDistribueBonus *data )
{
	// Ajouter type bonus
	if( !AJOUTER( &data->m_typeBonus,
		sizeof( BListeBonus ) )
		// Identifiant joueur
		|| !AJOUTER( &data->m_identifiant,
			sizeof( NU32 ) ) )
		ERREUR( );

	// OK
	return NTRUE;
}

// BTYPE_PACKET_SERVEUR_CLIENT_DIFFUSE_MORT_JOUEUR
NBOOL Projet_Commun_Reseau_Packet_Forge_ForgerPacketServeurClientDiffuseMortJoueur( NPacket *packet,
	const struct BPacketServeurClientDiffuseMortJoueur *data )
{
	// Ajouter identifiant joueur
	if( !AJOUTER( &data->m_identifiantJoueur,
		sizeof( NU32 ) ) )
		ERREUR( );

	// OK
	return NTRUE;
}

// BTYPE_PACKET_SERVEUR_CLIENT_ANNONCE_FIN_PARTIE
NBOOL Projet_Commun_Reseau_Packet_Forge_ForgerPacketServeurClientAnnonceFinPartie( NPacket *packet,
	const struct BPacketServeurClientAnnonceFinPartie *data )
{
	// Ajouter type fin de partie
	if( !AJOUTER( &data->m_type,
		sizeof( BTypeFinPartie ) )
		// Ajouter identifiant joueur
		|| !AJOUTER( &data->m_identifiant,
			sizeof( NU32 ) ) )
		ERREUR( );

	// OK
	return NTRUE;
}

