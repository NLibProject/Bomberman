#include "../../../../include/Projet/Projet.h"

/*
	@author SOARES Lucas
*/

// --------------------------------
// namespace Projet::Commun::Erreur
// --------------------------------

/* Callback notification erreurs */
__CALLBACK void Projet_Commun_Erreur_CallbackNotificationErreur( const NErreur *erreur )
{
	switch( NLib_Erreur_NErreur_ObtenirCode( erreur ) )
	{
		case NERREUR_USER:
			puts( NLib_Erreur_NErreur_ObtenirMessage( erreur ) );
			break;

		case NERREUR_SDL_TTF:
			// Osef des erreurs de creation de texte
			break;

		default:
			if( NLib_Erreur_NErreur_ObtenirNiveau( erreur ) > NNIVEAU_ERREUR_AVERTISSEMENT )
				NLib_Erreur_Notification_FluxStandard( erreur );
			break;
	}
}

