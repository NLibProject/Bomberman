#include "../../../../../include/Projet/Projet.h"

// --------------------------------------------------------
// struct Projet::Commun::Ressource::Bonus::BRessourceBonus
// --------------------------------------------------------

/* Construire */
__ALLOC BRessourceBonus *Projet_Commun_Ressource_Bonus_BRessourceBonus_Construire( const char *lien,
	const NFenetre *fenetre )
{
	// Sortie
	__OUTPUT BRessourceBonus *out;

	// Allouer la memoire
	if( !( out = calloc( 1,
		sizeof( BRessourceBonus ) ) ) )
	{
		// Notifier
		NOTIFIER_ERREUR( NERREUR_ALLOCATION_FAILED );

		// Quitter
		return NULL;
	}

	// Charger tileset
	if( !( out->m_bonus = NLib_Module_SDL_Surface_NTileset_Construire( lien,
		BTAILLE_CASE_TILESET,
		fenetre ) ) )
	{
		// Notifier
		NOTIFIER_ERREUR( NERREUR_CONSTRUCTOR_FAILED );

		// Liberer
		NFREE( out );

		// Quitter
		return NULL;
	}

	// OK
	return out;
}

/* Detruire */
void Projet_Commun_Ressource_Bonus_BRessourceBonus_Detruire( BRessourceBonus **this )
{
	// Detruire tileset
	NLib_Module_SDL_Surface_NTileset_Detruire( &(*this)->m_bonus );

	// Liberer
	NFREE( *this );
}

/* Obtenir ic�ne */
const NSurface *Projet_Commun_Ressource_Bonus_BRessourceBonus_Obtenir( const BRessourceBonus *this,
	BListeBonus bonus )
{
	// Position
	NUPoint position;

	// Definir
	NDEFINIR_POSITION( position,
		bonus,
		0 );

	// Obtenir
	return NLib_Module_SDL_Surface_NTileset_ObtenirCase( this->m_bonus,
		position );
}

