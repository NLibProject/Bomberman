#define PROJET_COMMUN_RESSOURCE_AUDIO_EFFET_BLISTEEFFETSONORE_INTERNE
#include "../../../../../../include/Projet/Projet.h"

// ---------------------------------------------------------------
// enum Projet::Commun::Ressource::Audio::Effet::BListeEffetSonore
// ---------------------------------------------------------------

/* Obtenir lien effet sonore */
const char *Projet_Commun_Ressource_Audio_Effet_BListeEffetSonore_ObtenirLien( BListeEffetSonore effet )
{
	return BListeEffetSonoreTexte[ effet ];
}

