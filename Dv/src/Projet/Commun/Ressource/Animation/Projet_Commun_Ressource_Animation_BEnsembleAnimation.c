#include "../../../../../include/Projet/Projet.h"

// ---------------------------------------------------------------
// struct Projet::Commun::Ressource::Animation::BEnsembleAnimation
// ---------------------------------------------------------------

/* Construire */
__ALLOC BEnsembleAnimation *Projet_Commun_Ressource_Animation_BEnsembleAnimation_Construire( const NFenetre *fenetre )
{
	// Sortie
	__OUTPUT BEnsembleAnimation *out;

	// Iterateurs
	NU32 i,
		j;

	// Allouer la memoire
	if( !( out = calloc( 1,
		sizeof( BEnsembleAnimation ) ) ) )
	{
		// Notifier
		NOTIFIER_ERREUR( NERREUR_ALLOCATION_FAILED );

		// Quitter
		return NULL;
	}

	// Construire les animations
	for( i = 0; i < BLISTE_ANIMATIONS; i++ )
		if( !( out->m_animation[ i ] = NLib_Module_SDL_Surface_NAnimation_Construire( Projet_Commun_Ressource_Animation_BListeAnimation_ObtenirLien( i ),
			*Projet_Commun_Ressource_Animation_BListeAnimation_ObtenirTailleFrame( i ),
			Projet_Commun_Ressource_Animation_BListeAnimation_ObtenirDelaiFrame( i ),
			fenetre ) ) )
		{
			// Notifier
			NOTIFIER_ERREUR( NERREUR_CONSTRUCTOR_FAILED );

			// Liberer
			for( j = 0; j < i; j++ )
				NLib_Module_SDL_Surface_NAnimation_Detruire( &out->m_animation[ j ] );
			NFREE( out );

			// Quitter
			return NULL;
		}

	// OK
	return out;
}

/* Detruire */
void Projet_Commun_Ressource_Animation_BEnsembleAnimation_Detruire( BEnsembleAnimation **this )
{
	// Iterateur
	NU32 i = 0;

	// Detruire les animations
	for( ; i < BLISTE_ANIMATIONS; i++ )
		NLib_Module_SDL_Surface_NAnimation_Detruire( &(*this)->m_animation[ i ] );

	// Liberer
	NFREE( *this );
}

/* Obtenir */
const NAnimation *Projet_Commun_Ressource_Animation_BEnsembleAnimation_Obtenir( const BEnsembleAnimation *this,
	BListeAnimation animation )
{
	return this->m_animation[ animation ];
}

/* Update */
void Projet_Commun_Ressource_Animation_BEnsembleAnimation_Update( BEnsembleAnimation *this )
{
	// Iterateur
	NU32 i = 0;

	// Update
	for( ; i < BLISTE_ANIMATIONS; i++ )
		NLib_Module_SDL_Surface_NAnimation_Update( this->m_animation[ i ] );
}

/* Afficher */
NBOOL Projet_Commun_Ressource_Animation_BEnsembleAnimation_Afficher( const BEnsembleAnimation *this,
	BListeAnimation animation,
	NU32 contenuAnimation,
	NSPoint position,
	NU32 zoom )
{
	// Afficher
	return Projet_Commun_Ressource_Animation_BEnsembleAnimation_Afficher2( this,
		animation,
		contenuAnimation,
		position,
		zoom,
		NLib_Module_SDL_Surface_NAnimation_ObtenirFrame( this->m_animation[ animation ] ) );
}

NBOOL Projet_Commun_Ressource_Animation_BEnsembleAnimation_Afficher2( const BEnsembleAnimation *this,
	BListeAnimation animation,
	NU32 contenuAnimation,
	NSPoint position,
	NU32 zoom,
	NU32 frame )
{
	// Definir la position
	NLib_Module_SDL_Surface_NAnimation_DefinirPosition( this->m_animation[ animation ],
		position );

	// Afficher
	return NLib_Module_SDL_Surface_NAnimation_Afficher3( this->m_animation[ animation ],
		contenuAnimation,
		zoom,
		frame );
}
