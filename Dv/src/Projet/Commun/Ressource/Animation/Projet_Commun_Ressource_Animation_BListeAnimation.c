#define PROJET_COMMUN_RESSOURCE_ANIMATION_BLISTEANIMATION_INTERNE
#include "../../../../../include/Projet/Projet.h"

// ----------------------------------------------------------
// enum Projet::Commun::Ressource::Animation::BListeAnimation
// ----------------------------------------------------------

/* Obtenir lien vers animation */
const char *Projet_Commun_Ressource_Animation_BListeAnimation_ObtenirLien( BListeAnimation animation )
{
	return BListeAnimationTexte[ animation ];
}

/* Obtenir taille frame animation */
const NUPoint *Projet_Commun_Ressource_Animation_BListeAnimation_ObtenirTailleFrame( BListeAnimation animation )
{
	return &BListeAnimationTaille[ animation ];
}

/* Obtenir delai entre frame animation */
NU32 Projet_Commun_Ressource_Animation_BListeAnimation_ObtenirDelaiFrame( BListeAnimation animation )
{
	return BListeAnimationDelaiFrame[ animation ];
}

