#define PROJET_COMMUN_FICHIER_PERSONNAGE_BCLEFFICHIERCHARSET_INTERNE
#include "../../../../../include/Projet/Projet.h"

// -------------------------------------------------------------
// enum Projet::Commun::Fichier::Personnage::BClefFichierCharset
// -------------------------------------------------------------

/* Obtenir clef */
const char *Projet_Commun_Fichier_Personnage_BClefFichierCharset_ObtenirClef( BClefFichierCharset clef )
{
	return BCLEF_FICHIER_CHARSET_TEXTE[ clef ];
}

/* Obtenir ensemble des clefs */
__ALLOC char **Projet_Commun_Fichier_Personnage_BClefFichierCharset_ObtenirEnsembleClef( void )
{
	// Sortie
	__OUTPUT char **out;

	// Iterateur
	NU32 i, j;

	// Allouer la memoire
	if( !( out = calloc( BCLEFS_FICHIER_CHARSET,
		sizeof( char* ) ) ) )
	{
		// Notifier
		NOTIFIER_ERREUR( NERREUR_ALLOCATION_FAILED );

		// Quitter
		return NULL;
	}

	// Copier
	for( i = 0; i < BCLEFS_FICHIER_CHARSET; i++ )
	{
		// Allouer la clef
		if( !( out[ i ] = calloc( strlen( BCLEF_FICHIER_CHARSET_TEXTE[ i ] ) + 1,
			sizeof( char ) ) ) )
		{
			// Notifier
			NOTIFIER_ERREUR( NERREUR_ALLOCATION_FAILED );

			// Liberer
			for( j = 0; j < i; j++ )
				NFREE( out[ j ] );
			NFREE( out );

			// Quitter
			return NULL;
		}

		// Copier
		memcpy( out[ i ],
			BCLEF_FICHIER_CHARSET_TEXTE[ i ],
			strlen( BCLEF_FICHIER_CHARSET_TEXTE[ i ] ) );
	}

	// OK
	return out;
}

