#define PROJET_COMMUN_FICHIER_PERSONNAGE_BCLEFFICHIERPERSONNAGE_INTERNE
#include "../../../../../include/Projet/Projet.h"

// ----------------------------------------------------------------
// enum Projet::Commun::Fichier::Personnage::BClefFichierPersonnage
// ----------------------------------------------------------------

/* Obtenir clef */
const char *Projet_Commun_Fichier_Personnage_BClefFichierPersonnage_ObtenirClef( BClefFichierPersonnage clef )
{
	return BCLEF_FICHIER_PERSONNAGE_TEXTE[ clef ];
}

/* Compose un tableau de l'ensemble des clefs */
__ALLOC char **Projet_Commun_Fichier_Personnage_BClefFichierPersonnage_ObtenirEnsembleClef( void )
{
	// Sortie
	__OUTPUT char **out;

	// Iterateur
	NU32 i, j;

	// Allouer la memoire
	if( !( out = calloc( BCLEFS_FICHIER_PERSONNAGE,
		sizeof( char* ) ) ) )
	{
		// Notifier
		NOTIFIER_ERREUR( NERREUR_ALLOCATION_FAILED );

		// Quitter
		return NULL;
	}

	// Copier
	for( i = 0; i < BCLEFS_FICHIER_PERSONNAGE; i++ )
	{
		// Allouer la clef
		if( !( out[ i ] = calloc( strlen( BCLEF_FICHIER_PERSONNAGE_TEXTE[ i ] ) + 1,
			sizeof( char ) ) ) )
		{
			// Notifier
			NOTIFIER_ERREUR( NERREUR_ALLOCATION_FAILED );

			// Liberer
			for( j = 0; j < i; j++ )
				NFREE( out[ j ] );
			NFREE( out );

			// Quitter
			return NULL;
		}

		// Copier
		memcpy( out[ i ],
			BCLEF_FICHIER_PERSONNAGE_TEXTE[ i ],
			strlen( BCLEF_FICHIER_PERSONNAGE_TEXTE[ i ] ) );
	}

	// OK
	return out;
}

