#define PROJET_COMMUN_FICHIER_TILESET_BCLEFFICHIERTILESET_INTERNE
#include "../../../../../include/Projet/Projet.h"

// ----------------------------------------------------------
// enum Projet::Commun::Fichier::Tileset::BClefFichierTileset
// ----------------------------------------------------------

/* Composer l'ensemble de clefs */
__ALLOC char **Projet_Commun_Fichier_Tileset_BClefFichierTileset_ComposerEnsembleClef( void )
{
	// Sortie
	__OUTPUT char **sortie;

	// Iterateur
	NU32 i, j;

	// Allouer la memoire
	if( !( sortie = calloc( BCLEFS_FICHIER_TILESET,
		sizeof( char* ) ) ) )
	{
		// Notifier
		NOTIFIER_ERREUR( NERREUR_ALLOCATION_FAILED );

		// Quitter
		return NULL;
	}

	// Ajouter les clefs
	for( i = 0; i < BCLEFS_FICHIER_TILESET; i++ )
	{
		// Allouer
		if( !( sortie[ i ] = calloc( strlen( BCLEFS_FICHIER_TILESET_TEXTE[ i ] ) + 1,
			sizeof( char ) ) ) )
		{
			// Notifier
			NOTIFIER_ERREUR( NERREUR_ALLOCATION_FAILED );

			// Liberer
			for( j = 0; j < i; j++ )
				NFREE( sortie[ j ] );
			NFREE( sortie );

			// Quitter
			return NULL;
		}

		// Copier
		memcpy( sortie[ i ],
			BCLEFS_FICHIER_TILESET_TEXTE[ i ],
			strlen( BCLEFS_FICHIER_TILESET_TEXTE[ i ] ) );
	}

	// OK
	return sortie;
}

