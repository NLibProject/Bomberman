#include "../../../../../include/Projet/Projet.h"

// -----------------------------------------------
// namespace Projet::Commun::Personnage::Affichage
// -----------------------------------------------

struct BMappingOrdrePersonnage
{
	// Hauteur ordonnee
	NS32 m_hauteur;

	// Joueur correspondant
	const BEtatClient *m_joueur;
};

/* Ordonner mapping (privee) */
NU32 Projet_Commun_Personnage_Affichage_ObtenirMinimumMappingInterne( struct BMappingOrdrePersonnage *ordre,
	NU32 debut,
	NU32 fin )
{
	// Iterateur
	NU32 i = debut;

	// Sortie
	__OUTPUT NU32 sortie = debut;

	// Chercher le minimum
	for( ; i < fin; i++ )
		if( ordre[ i ].m_hauteur < ordre[ sortie ].m_hauteur )
			sortie = i;

	// OK
	return sortie;
}

void Projet_Commun_Personnage_Affichage_OrdonnerMappingInterne( struct BMappingOrdrePersonnage *ordre,
	NU32 nombrePersonnage )
{
	// Iterateur
	NU32 i;

	// Minimum
	NU32 minimum;

	// Verifier
	if( !nombrePersonnage )
		return;

	// Ordonner
	for( i = 0; i < nombrePersonnage; i++ )
	{
		// Recuperer le minimum
		if( ( minimum = Projet_Commun_Personnage_Affichage_ObtenirMinimumMappingInterne( ordre,
				i,
				nombrePersonnage ) ) == i )
			continue;

		// Inverser la valeur courante et le minimum
		NLib_Memoire_Swap( &ordre[ i ],
			&ordre[ minimum ],
			sizeof( struct BMappingOrdrePersonnage ) );
	}
}

/* Afficher personnages */
NBOOL Projet_Commun_Personnage_Affichage_Afficher( BCacheClient *cache,
	NBOOL estAttenteReponseDeplacementClientCourant,
	BPersonnage **personnage,
	NSPoint baseAffichage,
	NU32 zoom )
{
	// Mapping
	struct BMappingOrdrePersonnage mapping[ BNOMBRE_MAXIMUM_JOUEUR ];

	// Iterateurs
	NS32 i;
	
	// Nombre de clients a afficher
	NS32 nombreClientAAfficher = 0;

	// Joueur
	const BEtatClient *joueur;

	// Position
	NSPoint position;

	// Direction
	NDirection direction;

	// Proteger le cache
	Projet_Commun_Reseau_Client_Cache_BCacheClient_ProtegerCache( cache );

	// Verifier le nombre de clients
	if( !Projet_Commun_Reseau_Client_Cache_BCacheClient_ObtenirNombreClient( cache ) )
	{
		// Unlock cache
		Projet_Commun_Reseau_Client_Cache_BCacheClient_NePlusProtegerCache( cache );

		// Quitter
		return NFALSE;
	}

	// Ajouter les clients (en partant de la fin pour afficher le joueur courant au dessus de tous les autres)
	for( i = Projet_Commun_Reseau_Client_Cache_BCacheClient_ObtenirNombreClient( cache ) - 1; i >= 0; i-- )
	{
		// Ajouter joueur
		if( !( joueur = Projet_Commun_Reseau_Client_Cache_BCacheClient_ObtenirClient2( cache,
			i ) ) )
			continue;

		// On doit afficher ce joueur?
		if( Projet_Commun_Reseau_Client_BEtatClient_EstDoitAfficher( joueur ) )
		{
			// Enregister
			mapping[ nombreClientAAfficher ].m_joueur = joueur;
			mapping[ nombreClientAAfficher ].m_hauteur = Projet_Commun_Reseau_Client_BEtatClient_ObtenirPosition( mapping[ nombreClientAAfficher ].m_joueur )->y;

			// Incrementer le nombre de joueurs
			nombreClientAAfficher++;
		}
	}

	// Ordonner ordre mapping
	Projet_Commun_Personnage_Affichage_OrdonnerMappingInterne( mapping,
		nombreClientAAfficher );

	// Afficher
	for( i = 0; i < nombreClientAAfficher; i++ )
	{
		// En deplacement?
		if( ( Projet_Commun_Reseau_Client_BEtatDeplacementClient_EstDeplacementActif( Projet_Commun_Reseau_Client_BEtatClient_ObtenirEtatDeplacement( mapping[ i ].m_joueur ) )
			|| ( Projet_Commun_Reseau_Client_BEtatClient_ObtenirIdentifiant( mapping[ i ].m_joueur ) == Projet_Commun_Reseau_Client_Cache_BCacheClient_ObtenirIdentifiantClientCourant( cache )
				&& estAttenteReponseDeplacementClientCourant ) )
			&& Projet_Commun_Reseau_Client_BEtatClient_EstEnVie( mapping[ i ].m_joueur ) )
		{
			// Calculer position
			NDEFINIR_POSITION( position,
				baseAffichage.x + ( zoom * BTAILLE_CASE_TILESET.x * Projet_Commun_Reseau_Client_BEtatDeplacementClient_ObtenirPositionInitiale( Projet_Commun_Reseau_Client_BEtatClient_ObtenirEtatDeplacement( mapping[ i ].m_joueur ) ).x ) + ( ( ( zoom * BTAILLE_CASE_TILESET.x ) / 2 ) - ( ( zoom * BTAILLE_CHARSET.x ) / 2 ) ) + BCORRECTION_POSITION_JOUEUR.x * zoom,
				baseAffichage.y + ( zoom * BTAILLE_CASE_TILESET.y * ( Projet_Commun_Reseau_Client_BEtatDeplacementClient_ObtenirPositionInitiale( Projet_Commun_Reseau_Client_BEtatClient_ObtenirEtatDeplacement( mapping[ i ].m_joueur ) ).y - 1 ) ) + BCORRECTION_POSITION_JOUEUR.y * zoom );

			// Ajouter scrolling
			position.x += ( Projet_Commun_Reseau_Client_BEtatDeplacementClient_ObtenirEtatScrolling( Projet_Commun_Reseau_Client_BEtatClient_ObtenirEtatDeplacement( mapping[ i ].m_joueur ) )->x * zoom );
			position.y += ( Projet_Commun_Reseau_Client_BEtatDeplacementClient_ObtenirEtatScrolling( Projet_Commun_Reseau_Client_BEtatClient_ObtenirEtatDeplacement( mapping[ i ].m_joueur ) )->y * zoom );

			// Definir direction
			direction = Projet_Commun_Reseau_Client_BEtatDeplacementClient_ObtenirDirectionDeplacement( Projet_Commun_Reseau_Client_BEtatClient_ObtenirEtatDeplacement( mapping[ i ].m_joueur ) );
		}
		// Au repos
		else
		{
			// Calculer position
			NDEFINIR_POSITION( position,
				baseAffichage.x + ( zoom * BTAILLE_CASE_TILESET.x * Projet_Commun_Reseau_Client_BEtatClient_ObtenirPosition( mapping[ i ].m_joueur )->x ) + ( ( ( zoom * BTAILLE_CASE_TILESET.x ) / 2 ) - ( ( zoom * BTAILLE_CHARSET.x ) / 2 ) ) + BCORRECTION_POSITION_JOUEUR.x * zoom,
				baseAffichage.y + ( zoom * BTAILLE_CASE_TILESET.y * ( Projet_Commun_Reseau_Client_BEtatClient_ObtenirPosition( mapping[ i ].m_joueur )->y - 1 ) ) + BCORRECTION_POSITION_JOUEUR.y * zoom );

			// Definir direction
			direction = Projet_Commun_Reseau_Client_BEtatClient_ObtenirDirection( mapping[ i ].m_joueur );
		}

		// Definir position
		Projet_Commun_Personnage_BPersonnage_DefinirPosition( personnage[ Projet_Commun_Reseau_Client_BEtatClient_ObtenirCharset( mapping[ i ].m_joueur ) ],
			position );

		// Afficher charset
		if( Projet_Commun_Reseau_Client_BEtatClient_EstEnVie( mapping[ i ].m_joueur ) )
			// Vivant
			Projet_Commun_Personnage_BPersonnage_AfficherCharset( personnage[ Projet_Commun_Reseau_Client_BEtatClient_ObtenirCharset( mapping[ i ].m_joueur ) ],
				direction,
				Projet_Commun_Reseau_Client_BEtatClient_ObtenirCouleurCharset( mapping[ i ].m_joueur ),
				zoom );
		else
			// Mort
			Projet_Commun_Personnage_BPersonnage_AfficherCharsetMort( personnage[ Projet_Commun_Reseau_Client_BEtatClient_ObtenirCharset( mapping[ i ].m_joueur ) ],
				zoom,
				Projet_Commun_Reseau_Client_BEtatClient_ObtenirEtatAnimationMort( mapping[ i ].m_joueur ) );
	}

	// Ne plus proteger
	Projet_Commun_Reseau_Client_Cache_BCacheClient_NePlusProtegerCache( cache );

	// OK
	return NTRUE;
}

