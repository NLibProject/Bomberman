#include "../../../../include/Projet/Projet.h"

/*
	@author SOARES Lucas
*/

// ----------------------------------------------
// struct Projet::Commun::Personnage::BPersonnage
// ----------------------------------------------

/* Construire */
__ALLOC BPersonnage *Projet_Commun_Personnage_BPersonnage_Construire( const char *nomFichier,
	const NFenetre *fenetre )
{
	// Sortie
	__OUTPUT BPersonnage *out;

	// Fichier
	NFichierClef *fichier;

	// Iterateur
	NU32 i;

	// Clefs
	char **clefsPersonnage;

	// Lien
	char lien[ MAX_PATH ] = { 0, };

	// Construire le lien
	sprintf( lien,
		"%s/%s",
		BREPERTOIRE_PERSONNAGE,
		nomFichier );

	// Obtenir les clefs
	if( !( clefsPersonnage = Projet_Commun_Fichier_Personnage_BClefFichierPersonnage_ObtenirEnsembleClef( ) ) )
	{
		// Notifier
		NOTIFIER_ERREUR( NERREUR_CONSTRUCTOR_FAILED );

		// Quitter
		return NULL;
	}

	// Construire le fichier
	if( !( fichier = NLib_Fichier_Clef_NFichierClef_Construire( lien,
		(const char **)clefsPersonnage,
		BCLEFS_FICHIER_PERSONNAGE ) ) )
	{
		// Notifier
		NOTIFIER_ERREUR( NERREUR_CONSTRUCTOR_FAILED );

		// Liberer les clefs
		for( i = 0; i < BCLEFS_FICHIER_PERSONNAGE; i++ )
			NFREE( clefsPersonnage[ i ] );
		NFREE( clefsPersonnage );

		// Quitter
		return NULL;
	}

	// Liberer les clefs
	for( i = 0; i < BCLEFS_FICHIER_PERSONNAGE; i++ )
		NFREE( clefsPersonnage[ i ] );
	NFREE( clefsPersonnage );

	// Allouer la memoire
	if( !( out = calloc( 1,
		sizeof( BPersonnage ) ) ) )
	{
		// Notifier
		NOTIFIER_ERREUR( NERREUR_ALLOCATION_FAILED );

		// Fermer le fichier
		NLib_Fichier_Clef_NFichierClef_Detruire( &fichier );

		// Quitter
		return NULL;
	}

	// Enregistrer
	out->m_vitesse = NLib_Fichier_Clef_NFichierClef_ObtenirValeur2( fichier,
		BCLEF_FICHIER_PERSONNAGE_VITESSE );
	out->m_tauxDrop = NLib_Fichier_Clef_NFichierClef_ObtenirValeur2( fichier,
		BCLEF_FICHIER_PERSONNAGE_TAUX_DROP );
	if( !( out->m_nom = NLib_Fichier_Clef_NFichierClef_ObtenirCopieValeur( fichier,
		BCLEF_FICHIER_PERSONNAGE_NOM ) ) )
	{
		// Notifier
		NOTIFIER_ERREUR( NERREUR_CONSTRUCTOR_FAILED );

		// Liberer
		NFREE( out );

		// Fermer le fichier
		NLib_Fichier_Clef_NFichierClef_Detruire( &fichier );

		// Quitter
		return NULL;
	}

	// Charger le charset
	if( !( out->m_charset = Projet_Commun_Personnage_Charset_BCharset_Construire( NLib_Fichier_Clef_NFichierClef_ObtenirValeur( fichier,
			BCLEF_FICHIER_PERSONNAGE_CHARSET ),
		fenetre ) ) )
	{
		// Notifier
		NOTIFIER_ERREUR( NERREUR_CONSTRUCTOR_FAILED );

		// Fermer le fichier
		NLib_Fichier_Clef_NFichierClef_Detruire( &fichier );

		// Liberer
		NFREE( out );

		// Quitter
		return NULL;
	}

	// Fermer le fichier
	NLib_Fichier_Clef_NFichierClef_Detruire( &fichier );

	// OK
	return out;
}

/* Detruire */
void Projet_Commun_Personnage_BPersonnage_Detruire( BPersonnage **this )
{
	// Detruire charset
	Projet_Commun_Personnage_Charset_BCharset_Detruire( &(*this)->m_charset );

	// Detruire nom
	NFREE( (*this)->m_nom );

	// Liberer
	NFREE( *this );
}

/* Obtenir le nom */
const char *Projet_Commun_Personnage_BPersonnage_ObtenirNom( const BPersonnage *this )
{
	return this->m_nom;
}

/* Obtenir la vitesse */
NU32 Projet_Commun_Personnage_BPersonnage_ObtenirVitesse( const BPersonnage *this )
{
	return this->m_vitesse;
}

/* Obtenir le taux de drop */
NU32 Projet_Commun_Personnage_BPersonnage_ObtenirTauxDrop( const BPersonnage *this )
{
	return this->m_tauxDrop;
}

/* Obtenir la taille de l'animation deplacement */
NUPoint Projet_Commun_Personnage_BPersonnage_ObtenirTailleFrameDeplacement( const BPersonnage *this )
{
	return Projet_Commun_Personnage_Charset_BCharset_ObtenirTailleFrameDeplacement( this->m_charset );
}

/* Obtenir la taille de l'animation mort */
NUPoint Projet_Commun_Personnage_BPersonnage_ObtenirTailleFrameMort( const BPersonnage *this )
{
	return Projet_Commun_Personnage_Charset_BCharset_ObtenirTailleFrameMort( this->m_charset );
}

/* Obtenir le nombre de couleurs */
NU32 Projet_Commun_Personnage_BPersonnage_ObtenirNombreCouleur( const BPersonnage *this )
{
	return Projet_Commun_Personnage_Charset_BCharset_ObtenirNombreCouleur( this->m_charset );
}

/* Obtenir le charset */
const BCharset *Projet_Commun_Personnage_BPersonnage_ObtenirCharset( const BPersonnage *this )
{
	return this->m_charset;
}

/* Definir position affichage */
void Projet_Commun_Personnage_BPersonnage_DefinirPosition( BPersonnage *this,
	NSPoint position )
{
	Projet_Commun_Personnage_Charset_BCharset_DefinirPosition( this->m_charset,
		position );
}

/* Afficher le charset */
NBOOL Projet_Commun_Personnage_BPersonnage_AfficherCharset( const BPersonnage *this,
	NDirection direction,
	NU32 couleur,
	NU32 zoom )
{
	return Projet_Commun_Personnage_Charset_BCharset_AfficherFrame( this->m_charset,
		direction,
		couleur,
		zoom );
}

NBOOL Projet_Commun_Personnage_BPersonnage_AfficherCharsetMort( const BPersonnage *this,
	NU32 zoom,
	const NEtatAnimation *etatAnimation )
{
	return Projet_Commun_Personnage_Charset_BCharset_AfficherFrameMort( this->m_charset,
		zoom,
		etatAnimation );
}

/* Update */
void Projet_Commun_Personnage_BPersonnage_Update( BPersonnage *this )
{
	// Mettre a jour le charset
	Projet_Commun_Personnage_Charset_BCharset_Update( this->m_charset );
}

