#include "../../../../../include/Projet/Projet.h"

/*
	@author SOARES Lucas
*/

// ----------------------------------------------------
// struct Projet::Commun::Personnage::Charset::BCharset
// ----------------------------------------------------

/* Decouper les couleurs */
__ALLOC char **Projet_Commun_Personnage_Charset_BCharset_DecouperCouleursInterne( const char *couleurs,
	NU32 nombreCouleur )
{
	// Sortie
	__OUTPUT char **out;

	// Curseur
	NU32 curseur = 0;

	// Iterateur
	NU32 i, j;

	// Verifier syntaxe
	if( NLib_Chaine_ObtenirProchainCaractere( couleurs,
		&curseur,
		NFALSE ) != '{' )
	{
		// Notifier
		NOTIFIER_ERREUR( NERREUR_SYNTAX );

		// Quitter
		return NULL;
	}

	// Allouer la memoire
	if( !( out = calloc( nombreCouleur,
		sizeof( char* ) ) ) )
	{
		// Notifier
		NOTIFIER_ERREUR( NERREUR_ALLOCATION_FAILED );

		// Quitter
		return NULL;
	}

	// Copier les couleurs
	for( i = 0; i < nombreCouleur; i++ )
		if( !( out[ i ] = NLib_Chaine_LireEntre2( couleurs,
			'"',
			&curseur,
			NFALSE ) ) )
		{
			// Notifier
			NOTIFIER_ERREUR( NERREUR_CONSTRUCTOR_FAILED );

			// Liberer
			for( j = 0; j < i; j++ )
				NFREE( out[ j ] );
			NFREE( out );

			// Quitter
			return NULL;
		}

	// OK
	return out;
}

/* Construire */
__ALLOC BCharset *Projet_Commun_Personnage_Charset_BCharset_Construire( const char *nomCharset,
	const NFenetre *fenetre )
{
	// Sortie
	__OUTPUT BCharset *out;

	// Lien
	char lien[ MAX_PATH ];

	// Clefs
	char **clefs;

	// Iterateur
	NU32 i, j;

	// Fichier
	NFichierClef *fichier;

	// Construire le lien
	sprintf( lien,
		"%s/%s.%s",
		BREPERTOIRE_CHARSET,
		nomCharset,
		BEXTENSION_FICHIER_CHARSET );

	// Obtenir ensemble clefs
	if( !( clefs = Projet_Commun_Fichier_Personnage_BClefFichierCharset_ObtenirEnsembleClef( ) ) )
	{
		// Notifier
		NOTIFIER_ERREUR( NERREUR_CONSTRUCTOR_FAILED );

		// Quitter
		return NULL;
	}

	// Construire le fichier
	if( !( fichier = NLib_Fichier_Clef_NFichierClef_Construire( lien,
		(const char**)clefs,
		BCLEFS_FICHIER_CHARSET ) ) )
	{
		// Notifier
		NOTIFIER_ERREUR( NERREUR_CONSTRUCTOR_FAILED );

		// Liberer clefs
		for( i = 0; i < BCLEFS_FICHIER_CHARSET; i++ )
			NFREE( clefs[ i ] );
		NFREE( clefs );

		// Quitter
		return NULL;
	}

	// Liberer clefs
	for( i = 0; i < BCLEFS_FICHIER_CHARSET; i++ )
		NFREE( clefs[ i ] );
	NFREE( clefs );

	// Allouer la memoire
	if( !( out = calloc( 1,
		sizeof( BCharset ) ) ) )
	{
		// Notifier
		NOTIFIER_ERREUR( NERREUR_ALLOCATION_FAILED );

		// Fermer le fichier
		NLib_Fichier_Clef_NFichierClef_Detruire( &fichier );

		// Quitter
		return NULL;
	}

	// Enregistrer
	out->m_nombreCouleur = NLib_Fichier_Clef_NFichierClef_ObtenirValeur2( fichier,
		BCLEF_FICHIER_CHARSET_NOMBRE_COULEURS );

	// Obtenir les couleurs
	if( !( out->m_nomCouleur = Projet_Commun_Personnage_Charset_BCharset_DecouperCouleursInterne( NLib_Fichier_Clef_NFichierClef_ObtenirValeur( fichier,
		BCLEF_FICHIER_CHARSET_COULEURS ),
		out->m_nombreCouleur ) ) )
	{
		// Notifier
		NOTIFIER_ERREUR( NERREUR_CONSTRUCTOR_FAILED );

		// Liberer
		NFREE( out );

		// Fermer le fichier
		NLib_Fichier_Clef_NFichierClef_Detruire( &fichier );

		// Quitter
		return NULL;
	}

	// Fermer le fichier
	NLib_Fichier_Clef_NFichierClef_Detruire( &fichier );

	// Allouer les animations
	if( !( out->m_animationDeplacement = calloc( out->m_nombreCouleur,
		sizeof( NAnimation* ) ) ) )
	{
		// Notifier
		NOTIFIER_ERREUR( NERREUR_ALLOCATION_FAILED );

		// Liberer
		for( i = 0; i < out->m_nombreCouleur; i++ )
			NFREE( out->m_nomCouleur[ i ] );
		NFREE( out->m_nomCouleur );
		NFREE( out );

		// Quitter
		return NULL;
	}

	// Charger les animations
	for( i = 0; i < out->m_nombreCouleur; i++ )
	{
		// Creer le lien
		sprintf( lien,
			"%s/%s%s.%s",
			BREPERTOIRE_CHARSET,
			nomCharset,
			out->m_nomCouleur[ i ],
			BEXTENSION_FICHIER_IMAGE );

		// Charger l'animation
		if( !( out->m_animationDeplacement[ i ] = NLib_Module_SDL_Surface_NAnimation_Construire( lien,
			BTAILLE_CHARSET,
			BDELAI_ENTRE_FRAME_ANIMATION_DEPLACEMENT,
			fenetre ) ) )
		{
			// Notifier
			NOTIFIER_ERREUR( NERREUR_CONSTRUCTOR_FAILED );

			// Liberer
			for( j = 0; j < i; j++ )
				NLib_Module_SDL_Surface_NAnimation_Detruire( &out->m_animationDeplacement[ j ] );
			NFREE( out->m_animationDeplacement );
			for( i = 0; i < out->m_nombreCouleur; i++ )
				NFREE( out->m_nomCouleur[ i ] );
			NFREE( out->m_nomCouleur );
			NFREE( out );

			// Quitrer
			return NULL;
		}
	}

	// Creer le lien
	sprintf( lien,
		"%s/%s%s.%s",
		BREPERTOIRE_CHARSET,
		BBASE_NOM_CHARSET_MORT,
		nomCharset,
		BEXTENSION_FICHIER_IMAGE );

	// Charger l'animation de mort
	if( !( out->m_animationMort = NLib_Module_SDL_Surface_NAnimation_Construire( lien,
		BTAILLE_CHARSET,
		BDELAI_ENTRE_FRAME_ANIMATION_MORT,
		fenetre ) ) )
	{
		// Notifier
		NOTIFIER_ERREUR( NERREUR_CONSTRUCTOR_FAILED );

		// Liberer
		for( i = 0; i < out->m_nombreCouleur; i++ )
			NLib_Module_SDL_Surface_NAnimation_Detruire( &out->m_animationDeplacement[ i ] );
		NFREE( out->m_animationDeplacement );
		for( i = 0; i < out->m_nombreCouleur; i++ )
			NFREE( out->m_nomCouleur[ i ] );
		NFREE( out->m_nomCouleur );
		NFREE( out );

		// Quitrer
		return NULL;
	}

	// OK
	return out;
}

/* Detruire */
void Projet_Commun_Personnage_Charset_BCharset_Detruire( BCharset **this )
{
	// Iterateur
	NU32 i = 0;

	// Detruire les animations
		// Deplacement
			for( ; i < (*this)->m_nombreCouleur; i++ )
				NLib_Module_SDL_Surface_NAnimation_Detruire( &(*this)->m_animationDeplacement[ i ] );
			NFREE( (*this)->m_animationDeplacement );
		// Mort
			NLib_Module_SDL_Surface_NAnimation_Detruire( &(*this)->m_animationMort );

	// Detruire les noms de couleur
	for( i = 0; i < (*this)->m_nombreCouleur; i++ )
		NFREE( (*this)->m_nomCouleur[ i ] );
	NFREE( (*this)->m_nomCouleur );

	// Liberer
	NFREE( (*this) );
}

/* Definir position */
void Projet_Commun_Personnage_Charset_BCharset_DefinirPosition( BCharset *this,
	NSPoint position )
{
	// Iterateur
	NU32 i;

	// Definir
	NLib_Module_SDL_Surface_NAnimation_DefinirPosition( this->m_animationMort,
		position );
	for( i = 0; i < this->m_nombreCouleur; i++ )
		NLib_Module_SDL_Surface_NAnimation_DefinirPosition( this->m_animationDeplacement[ i ],
			position );
}

/* Afficher frame */
NBOOL Projet_Commun_Personnage_Charset_BCharset_AfficherFrame( BCharset *this,
	NDirection direction,
	NU32 couleur,
	NU32 zoom )
{
	// Afficher
	return NLib_Module_SDL_Surface_NAnimation_Afficher2( this->m_animationDeplacement[ couleur ],
		direction,
		zoom );
}

NBOOL Projet_Commun_Personnage_Charset_BCharset_AfficherFrameMort( BCharset *this,
	NU32 zoom,
	const NEtatAnimation *etatAnimation )
{
	// Afficher
	return NLib_Module_SDL_Surface_NAnimation_Afficher3( this->m_animationMort,
		0,
		zoom,
		NLib_Temps_Animation_NEtatAnimation_ObtenirFrame( etatAnimation ) );
}

/* Obtenir taille frame */
NUPoint Projet_Commun_Personnage_Charset_BCharset_ObtenirTailleFrameDeplacement( const BCharset *this )
{
	return NLib_Module_SDL_Surface_NAnimation_ObtenirTaille( this->m_animationDeplacement[ 0 ] );
}

NUPoint Projet_Commun_Personnage_Charset_BCharset_ObtenirTailleFrameMort( const BCharset *this )
{
	return NLib_Module_SDL_Surface_NAnimation_ObtenirTaille( this->m_animationMort );
}

/* Obtenir le nombre de couleurs */
NU32 Projet_Commun_Personnage_Charset_BCharset_ObtenirNombreCouleur( const BCharset *this )
{
	return this->m_nombreCouleur;
}

/* Obtenir animation mort */
const NAnimation *Projet_Commun_Personnage_Charset_BCharset_ObtenirAnimationMort( const BCharset *this )
{
	return this->m_animationMort;
}

/* Update */
void Projet_Commun_Personnage_Charset_BCharset_Update( BCharset *this )
{
	// Iterateur
	NU32 i = 0;

	// Mettre a jour les animations
		// Deplacement
			for( ; i < this->m_nombreCouleur; i++ )
				NLib_Module_SDL_Surface_NAnimation_Update( this->m_animationDeplacement[ i ] );
		// Mort
			NLib_Module_SDL_Surface_NAnimation_Update( this->m_animationMort );
}

