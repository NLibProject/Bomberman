#include "../../../../../include/Projet/Projet.h"

/*
	@author SOARES Lucas
*/

// ------------------------------------------------
// namespace Projet::Commun::Personnage::Chargement
// ------------------------------------------------

/* Charger les personnages */
BPersonnage **Projet_Commun_Personnage_Chargement_Charger( __OUTPUT NU32 *nbPersonnages,
	const NFenetre *fenetre )
{
	// Sortie
	__OUTPUT BPersonnage **out;

	// Buffer
	char buffer[ 256 ] = { 0, };

	// Iterateurs
	NU32 i, j;

	// Repertoire
	NRepertoire *repertoire;

	// Aller dans le repertoire des personnages
	if( !NLib_Module_Repertoire_Changer( BREPERTOIRE_PERSONNAGE ) )
	{
		// Notifier
		NOTIFIER_ERREUR( NERREUR_FILE_NOT_FOUND );

		// Quitter
		return NULL;
	}

	// Construire le repertoire
	if( !( repertoire = NLib_Module_Repertoire_NRepertoire_Construire( ) ) )
	{
		// Notifier
		NOTIFIER_ERREUR( NERREUR_DIRECTORY );

		// Restaurer
		NLib_Module_Repertoire_RestaurerInitial( );

		// Quitter
		return NULL;
	}

	// Creer le filtre
#ifdef IS_WINDOWS
	sprintf( buffer,
		"*.%s",
		BEXTENSION_FICHIER_PERSONNAGE );
#else // IS_WINDOWS
	snprintf( buffer,
		256,
		".*" );
#endif // !IS_WINDOWS

	// Lister
	if( !NLib_Module_Repertoire_NRepertoire_Lister( repertoire,
		buffer,
		NATTRIBUT_REPERTOIRE_NORMAL ) )
	{
		// Notifier
		NOTIFIER_ERREUR( NERREUR_DIRECTORY );

		// Restaurer
		NLib_Module_Repertoire_RestaurerInitial( );

		// Detruire
		NLib_Module_Repertoire_NRepertoire_Detruire( &repertoire );

		// Quitter
		return NULL;
	}

	// Restaurer le repertoire
	NLib_Module_Repertoire_RestaurerInitial( );

	// Allouer la memoire
	if( !( out = calloc( NLib_Module_Repertoire_NRepertoire_ObtenirNombreFichiers( repertoire ),
		sizeof( BPersonnage* ) ) ) )
	{
		// Notifier
		NOTIFIER_ERREUR( NERREUR_ALLOCATION_FAILED );

		// Restaurer
		NLib_Module_Repertoire_RestaurerInitial( );

		// Detruire
		NLib_Module_Repertoire_NRepertoire_Detruire( &repertoire );

		// Quitter
		return NULL;
	}

	// Enregistrer le nombre de personnages
	*nbPersonnages = NLib_Module_Repertoire_NRepertoire_ObtenirNombreFichiers( repertoire );

	// Charger
	for( i = 0; i < *nbPersonnages; i++ )
		if( !( out[ i ] = Projet_Commun_Personnage_BPersonnage_Construire(
								NLib_Module_Repertoire_Element_NElementRepertoire_ObtenirNom(
									NLib_Module_Repertoire_NRepertoire_ObtenirFichier( repertoire,
				i ) ),
			fenetre ) ) )
		{
			// Notifier
			NOTIFIER_ERREUR( NERREUR_CONSTRUCTOR_FAILED );

			// Liberer
				// 2D
					for( j = 0; (NS32)j < (NS32)( i - 1 ); j++ )
						Projet_Commun_Personnage_BPersonnage_Detruire( &out[ i ] );
				// 1D
					NFREE( out );

			// Restaurer
			NLib_Module_Repertoire_RestaurerInitial( );

			// Liberer
			NLib_Module_Repertoire_NRepertoire_Detruire( &repertoire );

			// Quitter
			return NULL;
		}

	// Liberer
	NLib_Module_Repertoire_NRepertoire_Detruire( &repertoire );

	// OK
	return out;
}

