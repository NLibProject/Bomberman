#include "../../../../include/Projet/Projet.h"

// ------------------------------------
// struct Projet::Commun::Monde::BMonde
// ------------------------------------

/* Construire */
__ALLOC BMonde *Projet_Commun_Monde_BMonde_Construire( BCacheClient *clients,
	const BConfigurationMonde *configuration,
	const BEnsembleCarte *ensembleCarte,
	const BRessource *ressource )
{
	// Sortie
	__OUTPUT BMonde *out;

	// Carte
	const BCarte *carte;

	// Obtenir la carte
	if( !( carte = Projet_Commun_Carte_Ensemble_BEnsembleCarte_Obtenir( ensembleCarte,
		Projet_Commun_Monde_BConfigurationMonde_ObtenirIdentifiantCarte( configuration ) ) ) )
	{
		// Notifier
		NOTIFIER_ERREUR( NERREUR_PARAMETER_ERROR );

		// Quitter
		return NULL;
	}

	// Allouer la memoire
	if( !( out = calloc( 1,
		sizeof( BMonde ) ) ) )
	{
		// Notifier
		NOTIFIER_ERREUR( NERREUR_ALLOCATION_FAILED );

		// Quitter
		return NULL;
	}

	// Construire etat carte
	if( !( out->m_etatCarte = Projet_Commun_Carte_Etat_BEtatCarte_Construire( carte,
		ressource ) ) )
	{
		// Notifier
		NOTIFIER_ERREUR( NERREUR_CONSTRUCTOR_FAILED );

		// Liberer
		NFREE( out );

		// Quitter
		return NULL;
	}

	// Enregistrer
	out->m_cacheClient = clients;
	out->m_configuration = configuration;
	out->m_carte = carte;

	// OK
	return out;
}

/* Detruire */
void Projet_Commun_Monde_BMonde_Detruire( BMonde **this )
{
	// Detruire etat carte
	Projet_Commun_Carte_Etat_BEtatCarte_Detruire( &(*this)->m_etatCarte );

	// Liberer
	NFREE( *this );
}

/* Obtenir carte */
const BCarte *Projet_Commun_Monde_BMonde_ObtenirCarte( const BMonde *this )
{
	return this->m_carte;
}

/* Obtenir etat carte */
const BEtatCarte *Projet_Commun_Monde_BMonde_ObtenirEtatCarte( const BMonde *this )
{
	return this->m_etatCarte;
}

/* Obtenir cases etat carte */
const BCaseEtatCarte **Projet_Commun_Monde_BMonde_ObtenirCaseEtatCarte( const BMonde *this )
{
	return Projet_Commun_Carte_Etat_BEtatCarte_ObtenirCase( this->m_etatCarte );
}

/* Generer blocs remplis */
NBOOL Projet_Commun_Monde_BMonde_GenererBloc( BMonde *this )
{
	return Projet_Commun_Carte_Etat_BEtatCarte_GenererBloc( this->m_etatCarte );
}

NBOOL Projet_Commun_Monde_BMonde_GenererBloc2( BMonde *this,
	const NBOOL **estCaseRemplie )
{
	return Projet_Commun_Carte_Etat_BEtatCarte_GenererBloc2( this->m_etatCarte,
		estCaseRemplie );
}

/* Update */
void Projet_Commun_Monde_BMonde_Update( BMonde *this )
{
	// Etat carte
	Projet_Commun_Carte_Etat_BEtatCarte_Update( this->m_etatCarte );
}

/* Exploser bombe */
NBOOL Projet_Commun_Monde_BMonde_ExploserBombe( BMonde *this,
	NU32 identifiantBombe,
	NU32 puissance,
	NU32 identifiantJoueur )
{
	// Position bombe
	NSPoint positionBombe;

	// Etat joueur
	BEtatClient *joueur;

	// Cases
	BCaseEtatCarte **caseEtat;
	BBloc **caseCarte;

	// Iterateur
	NS32 i;

	// Obtenir la position de la bombe
	if( !Projet_Commun_Carte_Etat_BEtatCarte_ObtenirPositionBombe( Projet_Commun_Monde_BMonde_ObtenirEtatCarte( this ),
		identifiantBombe,
		&positionBombe )
		// Supprimer la bombe
		|| !Projet_Commun_Carte_Etat_BEtatCarte_SupprimerBombe( (BEtatCarte*)Projet_Commun_Monde_BMonde_ObtenirEtatCarte( this ),
			identifiantBombe ) )
	{
		// Notifier
		NOTIFIER_ERREUR( NERREUR_OBJECT_STATE );

		// Quitter
		return NFALSE;
	}

	// Obtenir cases
	if( !( caseEtat = (BCaseEtatCarte**)Projet_Commun_Carte_Etat_BEtatCarte_ObtenirCase( Projet_Commun_Monde_BMonde_ObtenirEtatCarte( this ) ) )
		|| !( caseCarte = (BBloc**)Projet_Commun_Carte_BCarte_ObtenirCases( this->m_carte ) ) )
	{
		// Notifier
		NOTIFIER_ERREUR( NERREUR_OBJECT_STATE );

		// Quitter
		return NFALSE;
	}

	// Diffuser trainee
		// Centre
			Projet_Commun_Carte_Etat_Case_AnimationExplosion_BEtatAnimationExplosion_ActiverAnimation( caseEtat[ positionBombe.x ][ positionBombe.y ].m_etatExplosion,
				BCONTENU_ANIMATION_BOMBE_EXPLOSION_TRAINEE_CENTRE1,
				identifiantJoueur );
		// Haut
			for( i = 1; i < (NS32)puissance + 1; i++ )
				if( positionBombe.y - i >= 0
					&& caseCarte[ positionBombe.x ][ positionBombe.y - i ].m_type != BTYPE_BLOC_SOLIDE )
				{
					// Verticale
					Projet_Commun_Carte_Etat_Case_AnimationExplosion_BEtatAnimationExplosion_ActiverAnimation( caseEtat[ positionBombe.x ][ positionBombe.y - i ].m_etatExplosion,
						( i == (NS32)puissance
							|| positionBombe.y - i - 1 < 0
							|| caseCarte[ positionBombe.x ][ positionBombe.y - i - 1 ].m_type == BTYPE_BLOC_SOLIDE ) ?
								BCONTENU_ANIMATION_BOMBE_EXPLOSION_TRAINEE_HAUT_BOUT
								: BCONTENU_ANIMATION_BOMBE_EXPLOSION_TRAINEE_VERTICALE,
							identifiantJoueur );

					// Est bloc rempli rencontre?
					if( caseEtat[ positionBombe.x ][ positionBombe.y - i ].m_estRempli )
					{
						// Activer
						Projet_Commun_Carte_Etat_Case_AnimationExplosion_BEtatAnimationExplosion_ActiverAnimation( caseEtat[ positionBombe.x ][ positionBombe.y - i ].m_etatExplosion,
							BCONTENU_ANIMATION_BOMBE_EXPLOSION_TRAINEE_HAUT_BOUT,
							identifiantJoueur );

						// Sortir
						break;
					}
				}
				else
					break;
		// Bas
			for( i = 1; i < (NS32)puissance + 1; i++ )
				if( positionBombe.y + i < (NS32)Projet_Commun_Carte_BCarte_ObtenirTaille( this->m_carte )->y
					&& caseCarte[ positionBombe.x ][ positionBombe.y + i ].m_type != BTYPE_BLOC_SOLIDE )
				{
					// Verticale
					Projet_Commun_Carte_Etat_Case_AnimationExplosion_BEtatAnimationExplosion_ActiverAnimation( caseEtat[ positionBombe.x ][ positionBombe.y + i ].m_etatExplosion,
						( i == (NS32)puissance
							|| positionBombe.y + i + 1 >= (NS32)Projet_Commun_Carte_BCarte_ObtenirTaille( this->m_carte )->y
							|| caseCarte[ positionBombe.x ][ positionBombe.y + i + 1 ].m_type == BTYPE_BLOC_SOLIDE ) ?
								BCONTENU_ANIMATION_BOMBE_EXPLOSION_TRAINEE_BAS_BOUT
								: BCONTENU_ANIMATION_BOMBE_EXPLOSION_TRAINEE_VERTICALE,
							identifiantJoueur );

					// Est bloc rempli rencontre?
					if( caseEtat[ positionBombe.x ][ positionBombe.y + i ].m_estRempli )
					{
						// Activer
						Projet_Commun_Carte_Etat_Case_AnimationExplosion_BEtatAnimationExplosion_ActiverAnimation( caseEtat[ positionBombe.x ][ positionBombe.y + i ].m_etatExplosion,
							BCONTENU_ANIMATION_BOMBE_EXPLOSION_TRAINEE_BAS_BOUT,
							identifiantJoueur );

						// Sortir
						break;
					}
				}
				else
					break;
		// Gauche
			for( i = 1; i < (NS32)puissance + 1; i++ )
				if( positionBombe.x - i >= 0
					&& caseCarte[ positionBombe.x - i ][ positionBombe.y ].m_type != BTYPE_BLOC_SOLIDE )
				{
					// Horizontale
					Projet_Commun_Carte_Etat_Case_AnimationExplosion_BEtatAnimationExplosion_ActiverAnimation( caseEtat[ positionBombe.x - i ][ positionBombe.y ].m_etatExplosion,
						( i == (NS32)puissance
							|| positionBombe.x - i - 1 < 0
							|| caseCarte[ positionBombe.x - i - 1 ][ positionBombe.y ].m_type == BTYPE_BLOC_SOLIDE ) ?
								BCONTENU_ANIMATION_BOMBE_EXPLOSION_TRAINEE_GAUCHE_BOUT
								: BCONTENU_ANIMATION_BOMBE_EXPLOSION_TRAINEE_HORIZONTALE,
							identifiantJoueur );

					// Est bloc rempli rencontre?
					if( caseEtat[ positionBombe.x - i ][ positionBombe.y ].m_estRempli )
					{
						// Activer
						Projet_Commun_Carte_Etat_Case_AnimationExplosion_BEtatAnimationExplosion_ActiverAnimation( caseEtat[ positionBombe.x - i  ][ positionBombe.y].m_etatExplosion,
							BCONTENU_ANIMATION_BOMBE_EXPLOSION_TRAINEE_GAUCHE_BOUT,
							identifiantJoueur );

						// Sortir
						break;
					}
				}
				else
					break;
		// Droite
			for( i = 1; i < (NS32)puissance + 1; i++ )
				if( positionBombe.x + i < (NS32)Projet_Commun_Carte_BCarte_ObtenirTaille( this->m_carte )->x
					&& caseCarte[ positionBombe.x + i ][ positionBombe.y ].m_type != BTYPE_BLOC_SOLIDE )
				{
					// Verticale
					Projet_Commun_Carte_Etat_Case_AnimationExplosion_BEtatAnimationExplosion_ActiverAnimation( caseEtat[ positionBombe.x + i ][ positionBombe.y ].m_etatExplosion,
						( i == (NS32)puissance
							|| positionBombe.x + i + 1 >= (NS32)Projet_Commun_Carte_BCarte_ObtenirTaille( this->m_carte )->x
							|| caseCarte[ positionBombe.x + i + 1 ][ positionBombe.y ].m_type == BTYPE_BLOC_SOLIDE ) ?
								BCONTENU_ANIMATION_BOMBE_EXPLOSION_TRAINEE_DROITE_BOUT
								: BCONTENU_ANIMATION_BOMBE_EXPLOSION_TRAINEE_HORIZONTALE,
							identifiantJoueur );

					// Est bloc rempli rencontre?
					if( caseEtat[ positionBombe.x + i ][ positionBombe.y ].m_estRempli )
					{
						// Activer
						Projet_Commun_Carte_Etat_Case_AnimationExplosion_BEtatAnimationExplosion_ActiverAnimation( caseEtat[ positionBombe.x + i ][ positionBombe.y ].m_etatExplosion,
							BCONTENU_ANIMATION_BOMBE_EXPLOSION_TRAINEE_DROITE_BOUT,
							identifiantJoueur );

						// Sortir
						break;
					}
				}
				else
					break;

	// Lock le cache
	Projet_Commun_Reseau_Client_Cache_BCacheClient_ProtegerCache( this->m_cacheClient );

	// Obtenir joueur
	if( !( joueur = (BEtatClient*)Projet_Commun_Reseau_Client_Cache_BCacheClient_ObtenirClient( this->m_cacheClient,
		identifiantJoueur ) ) )
	{
		// Notifier
		NOTIFIER_ERREUR( NERREUR_OBJECT_STATE );

		// Unlock le cache
		Projet_Commun_Reseau_Client_Cache_BCacheClient_NePlusProtegerCache( this->m_cacheClient );

		// Quitter
		return NFALSE;
	}

	// Decrementer le nombre de bombes posees
	Projet_Commun_Reseau_Client_BEtatClient_DecrementerNombreBombePosee( joueur );

	// Unlock le cache
	Projet_Commun_Reseau_Client_Cache_BCacheClient_NePlusProtegerCache( this->m_cacheClient );

	// OK
	return NTRUE;
}

/* Supprimer bloc rempli */
NBOOL Projet_Commun_Monde_BMonde_SupprimerBlocRempli( BMonde *this,
	NSPoint position )
{
	// Cases
	const BCaseEtatCarte **caseEtat;

	// Verifier parametres
	if( (NU32)position.x >= Projet_Commun_Carte_BCarte_ObtenirTaille( this->m_carte )->x
		|| (NU32)position.y >= Projet_Commun_Carte_BCarte_ObtenirTaille( this->m_carte )->y )
	{
		// Notifier
		NOTIFIER_ERREUR( NERREUR_STREAM_ERROR );

		// Quitter
		return NFALSE;
	}

	// Obtenir case
	if( !( caseEtat = Projet_Commun_Carte_Etat_BEtatCarte_ObtenirCase( this->m_etatCarte ) ) )
	{
		// Notifier
		NOTIFIER_ERREUR( NERREUR_OBJECT_STATE );

		// Quitter
		return NFALSE;
	}

	// Vider le bloc
	((BCaseEtatCarte**)caseEtat)[ position.x ][ position.y ].m_estRempli = NFALSE;

	// OK
	return NTRUE;
}

