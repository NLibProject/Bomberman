#include "../../../../include/Projet/Projet.h"

// -------------------------------------------------
// struct Projet::Commun::Monde::BConfigurationMonde
// -------------------------------------------------

/* Construire */
__ALLOC BConfigurationMonde *Projet_Commun_Monde_BConfigurationMonde_Construire( void )
{
	// Sortie
	__OUTPUT BConfigurationMonde *out;
	
	// Allouer
	if( !( out = calloc( 1,
		sizeof( BConfigurationMonde ) ) ) )
	{
		// Notifier
		NOTIFIER_ERREUR( NERREUR_ALLOCATION_FAILED );

		// Quitter
		return NULL;
	}

	// Zero
	out->m_identifiantCarteSelectionnee = 0;

	// OK
	return out;
}

/* Detruire */
void Projet_Commun_Monde_BConfigurationMonde_Detruire( BConfigurationMonde **this )
{
	NFREE( *this );
}

/* Obtenir identifiant carte */
NU32 Projet_Commun_Monde_BConfigurationMonde_ObtenirIdentifiantCarte( const BConfigurationMonde *this )
{
	return this->m_identifiantCarteSelectionnee;
}

/* Definir l'identifiant carte */
void Projet_Commun_Monde_BConfigurationMonde_DefinirIdentifiantCarte( BConfigurationMonde *this,
	NU32 id )
{
	this->m_identifiantCarteSelectionnee = id;
}

