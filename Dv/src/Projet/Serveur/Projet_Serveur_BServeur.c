#include "../../../include/Projet/Projet.h"

// --------------------------------
// struct Projet::Serveur::BServeur
// --------------------------------

/* Gerer le ping (privee) */
void Projet_Serveur_BServeur_GererPingInterne( BServeur *this )
{
	// Iterateur
	NU32 i = 0;

	// Joueur
	const BEtatClient *joueur;

	// Ping
	const NPing *ping;

	// Traiter clients
	for( ; i < Projet_Commun_Reseau_Client_Cache_BCacheClient_ObtenirNombreClient( this->m_cacheClient ); i++ )
	{
		// Obtenir le client
		if( !( joueur = Projet_Commun_Reseau_Client_Cache_BCacheClient_ObtenirClient2( this->m_cacheClient,
			i ) ) )
			continue;

		// Obtenir le ping
		ping = NLib_Module_Reseau_Serveur_NClientServeur_ObtenirPing( (NClientServeur*)Projet_Commun_Reseau_Client_BEtatClient_ObtenirDonneeSupplementaire( joueur ) );

		// Verifier si le client est TO
		if( NLib_Module_Reseau_NPing_EstTimeout( ping ) )
		{
			// Tuer
			NLib_Module_Reseau_Serveur_NClientServeur_Tuer( (NClientServeur*)Projet_Commun_Reseau_Client_BEtatClient_ObtenirDonneeSupplementaire( joueur ) );

			// Continuer
			continue;
		}

		// Faire une requete?
		if( NLib_Module_Reseau_NPing_EstDoitEffectuerRequete( ping ) )
		{
			// Envoyer
			if( !Projet_Serveur_TraitementPacket_EnvoyerRequetePing( this,
				(NClientServeur*)Projet_Commun_Reseau_Client_BEtatClient_ObtenirDonneeSupplementaire( joueur ) ) )
			{
				// Tuer
				NLib_Module_Reseau_Serveur_NClientServeur_Tuer( (NClientServeur*)Projet_Commun_Reseau_Client_BEtatClient_ObtenirDonneeSupplementaire( joueur ) );

				// Continuer
				continue;
			}

			// Notifier requete
			NLib_Module_Reseau_NPing_EffectuerRequete( (NPing*)ping );
		}
	}
}

/* Thread gestion (privee) */
NBOOL Projet_Serveur_BServeur_ThreadGestion( BServeur *this )
{
	// Iterateur
	NU32 i;

	// Joueur
	const BEtatClient *joueur;

	// Tick
	NU32 tick;

	// Buffer
	char buffer[ 2048 ];

	// Tout le monde a confirme le lancement
	NBOOL estToutLeMondeConfirmeLancement;

	do
	{
		// Proteger cache
		Projet_Commun_Reseau_Client_Cache_BCacheClient_ProtegerCache( this->m_cacheClient );

		// Update cache
		Projet_Commun_Reseau_Client_Cache_BCacheClient_UpdateNoLock( this->m_cacheClient );

		// Gerer les clients suivant leurs etats
		switch( this->m_etat )
		{
			case BETAT_SERVEUR_SALLE_ATTENTE:
				for( i = 0; i < Projet_Commun_Reseau_Client_Cache_BCacheClient_ObtenirNombreClient( this->m_cacheClient ); i++ )
				{
					// Obtenir client
					if( !( joueur = Projet_Commun_Reseau_Client_Cache_BCacheClient_ObtenirClient2( this->m_cacheClient,
						i ) ) )
						continue;

					// Si il y a un probleme avec l'etat du client
					if( !Projet_Commun_Reseau_Client_BEtatClient_EstClientEtatCorrect( joueur ) )
					{
						// Notifier
							// Composer message
								sprintf( buffer,
									"[SERVEUR] Le client %d n'a pas envoye le CRC de ses ressources.",
									Projet_Commun_Reseau_Client_BEtatClient_ObtenirIdentifiant( joueur ) );
							// Notifier
								NOTIFIER_AVERTISSEMENT_UTILISATEUR( NERREUR_USER,
									buffer,
									0 );

						// Envoyer packet
						Projet_Serveur_TraitementPacket_NotifierJoueurKickCRC( this,
							(NClientServeur*)Projet_Commun_Reseau_Client_BEtatClient_ObtenirDonneeSupplementaire( joueur ) );

						// Enregistrer ticks
						tick = NLib_Temps_ObtenirTick( );

						// Attendre que le packet soit parti
						while( NLib_Temps_ObtenirTick( ) - tick < BTIMEOUT_ENVOI_PACKET_KICK_SERVEUR
							&& NLib_Module_Reseau_Serveur_NClientServeur_EstPacketsDansCache( (NClientServeur*)Projet_Commun_Reseau_Client_BEtatClient_ObtenirDonneeSupplementaire( joueur ) ) )
							NLib_Temps_Attendre( 1 );

						// Kicker
						NLib_Module_Reseau_Serveur_NClientServeur_Tuer( (NClientServeur*)Projet_Commun_Reseau_Client_BEtatClient_ObtenirDonneeSupplementaire( joueur ) );
					}
				}
				break;

			case BETAT_SERVEUR_ATTENTE_CONFIRMATION_LANCEMENT:
				// Zero
				estToutLeMondeConfirmeLancement = NTRUE;

				// Verifier
				for( i = 0; i < Projet_Commun_Reseau_Client_Cache_BCacheClient_ObtenirNombreClient( this->m_cacheClient ); i++ )
				{
					// Obtenir le client
					if( !( joueur = Projet_Commun_Reseau_Client_Cache_BCacheClient_ObtenirClient2( this->m_cacheClient,
						i ) ) )
						continue;

					// Confirme?
					if( !Projet_Commun_Reseau_Client_BEtatClient_EstConfirmeLancement( joueur ) )
					{
						// Tout le monde n'a pas confirme
						estToutLeMondeConfirmeLancement = NFALSE;

						// Sortir
						break;
					}
				}

				// Traiter resultat
				if( estToutLeMondeConfirmeLancement )
				{
					// Lancer
					Projet_Serveur_Monde_BMondeServeur_LancerPartie( this->m_mondeServeur );

					// Passer a l'etape suivante
					this->m_etat = BETAT_SERVEUR_EN_JEU;
				}
				break;

			case BETAT_SERVEUR_EN_JEU:
				// Update monde
				Projet_Serveur_Monde_BMondeServeur_Update( this->m_mondeServeur );
				break;

			default:
				break;
		}

		// Gerer ping clients
		Projet_Serveur_BServeur_GererPingInterne( this );

		// Enlever protection cache
		Projet_Commun_Reseau_Client_Cache_BCacheClient_NePlusProtegerCache( this->m_cacheClient );

		// Delai
		NLib_Temps_Attendre( 1 );
	} while( this->m_estEnCours );

	// OK
	return NTRUE;
}

/* Detruire construction (privee) */
void Projet_Serveur_BServeur_DetruireConstructionInterne( BServeur *this,
	BEtapeConstructionServeur etape )
{
	switch( etape )
	{
		default:
		case BETAPE_CONSTRUCTION_SERVEUR_SERVEUR:
			if( this->m_serveur != NULL )
			{
				// Interdire connexion
				NLib_Module_Reseau_Serveur_NServeur_InterdireConnexion( this->m_serveur );

				// Detruire
				NLib_Module_Reseau_Serveur_NServeur_Detruire( &this->m_serveur );
			}

		case BETAPE_CONSTRUCTION_SERVEUR_THREAD_GESTION:
			// On attend la fin du thread
			NLib_Thread_NThread_Detruire( &this->m_threadGestion );

			// Detruire le monde
			if( this->m_monde != NULL )
				Projet_Commun_Monde_BMonde_Detruire( &this->m_monde );

			// Detruire le monde serveur
			if( this->m_mondeServeur != NULL )
				Projet_Serveur_Monde_BMondeServeur_Detruire( &this->m_mondeServeur );

		case BETAPE_CONSTRUCTION_SERVEUR_RESSOURCE:
			Projet_Commun_Ressource_BRessource_Detruire( &this->m_ressource );

		case BETAPE_CONSTRUCTION_SERVEUR_ENSEMBLE_CARTE:
			Projet_Commun_Carte_Ensemble_BEnsembleCarte_Detruire( &this->m_carte );

		case BETAPE_CONSTRUCTION_SERVEUR_FENETRE:
			NLib_Module_SDL_NFenetre_Detruire( &this->m_fenetre );

		case BETAPE_CONSTRUCTION_SERVEUR_CACHE_CLIENT:
			Projet_Commun_Reseau_Client_Cache_BCacheClient_Detruire( &this->m_cacheClient );

		case BETAPE_CONSTRUCTION_SERVEUR_THIS:
			NFREE( this );
			break;
	}
}

/* Construire */
__ALLOC BServeur *Projet_Serveur_BServeur_Construire( NU32 port,
	const BConfigurationMonde *configurationMonde )
{
	// Sortie
	__OUTPUT BServeur *out;

	// Construire
	if( !( out = calloc( 1,
		sizeof( BServeur ) ) ) )
	{
		// Notifier
		NOTIFIER_ERREUR( NERREUR_ALLOCATION_FAILED );

		// Quitter
		return NULL;
	}

	// Creer fenetre invisible
	if( !( out->m_fenetre = NLib_Module_SDL_NFenetre_Construire2( "Serveur",
		(NUPoint){ 640, 480 },
		NFALSE,
		NFALSE ) ) )
	{
		// Notifier
		NOTIFIER_ERREUR( NERREUR_CONSTRUCTOR_FAILED );

		// Detruire
		Projet_Serveur_BServeur_DetruireConstructionInterne( out,
			BETAPE_CONSTRUCTION_SERVEUR_THIS );

		// Quitter
		return NULL;
	}

	// Charger ensemble carte
	if( !( out->m_carte = Projet_Commun_Carte_Ensemble_BEnsembleCarte_Construire( BREPERTOIRE_ENSEMBLE_CARTE ) ) )
	{
		// Notifier
		NOTIFIER_ERREUR( NERREUR_CONSTRUCTOR_FAILED );

		// Detruire
		Projet_Serveur_BServeur_DetruireConstructionInterne( out,
			BETAPE_CONSTRUCTION_SERVEUR_FENETRE );

		// Quitter
		return NULL;
	}

	// Charger ressources
	if( !( out->m_ressource = Projet_Commun_Ressource_BRessource_Construire( out->m_fenetre ) ) )
	{
		// Notifier
		NOTIFIER_ERREUR( NERREUR_CONSTRUCTOR_FAILED );

		// Detruire
		Projet_Serveur_BServeur_DetruireConstructionInterne( out,
			BETAPE_CONSTRUCTION_SERVEUR_ENSEMBLE_CARTE );

		// Quitter
		return NULL;
	}

	// Creer le cache client
	if( !( out->m_cacheClient = Projet_Commun_Reseau_Client_Cache_BCacheClient_Construire( out->m_ressource ) ) )
	{
		// Notifier
		NOTIFIER_ERREUR( NERREUR_CONSTRUCTOR_FAILED );

		// Detruire
		Projet_Serveur_BServeur_DetruireConstructionInterne( out,
			BETAPE_CONSTRUCTION_SERVEUR_RESSOURCE );

		// Quitter
		return NULL;
	}

	// Zero
	out->m_etat = BETAT_SERVEUR_PAUSE;
	out->m_estEnCours = NTRUE;

	// Creer le thread de gestion
	if( !( out->m_threadGestion = NLib_Thread_NThread_Construire( (NBOOL ( ___cdecl* )( void* ))Projet_Serveur_BServeur_ThreadGestion,
		out,
		&out->m_estEnCours ) ) )
	{
		// Notifier
		NOTIFIER_ERREUR( NERREUR_CONSTRUCTOR_FAILED );

		// Detruire
		Projet_Serveur_BServeur_DetruireConstructionInterne( out,
			BETAPE_CONSTRUCTION_SERVEUR_CACHE_CLIENT );

		// Quitter
		return NULL;
	}

	// Enregistrer
	out->m_port = port;
	out->m_configurationMonde = configurationMonde;

	// OK
	return out;
}

/* Detruire */
void Projet_Serveur_BServeur_Detruire( BServeur **this )
{
	// Detruire
	Projet_Serveur_BServeur_DetruireConstructionInterne( *this,
		BETAPES_CONSTRUCTION_SERVEUR );

	// Dissocier adresse
	NDISSOCIER_ADRESSE( *this );
}

/* Obtenir ressources */
const BRessource *Projet_Serveur_BServeur_ObtenirRessource( const BServeur *this )
{
	return this->m_ressource;
}

/* Obtenir cache clients */
const BCacheClient *Projet_Serveur_BServeur_ObtenirCacheClient( const BServeur *this )
{
	return this->m_cacheClient;
}

/* Obtenir checksum ressources */
NU32 Projet_Serveur_BServeur_ObtenirChecksumRessource( const BServeur *this )
{
	return Projet_Commun_Ressource_BRessource_ObtenirChecksum( this->m_ressource )
		+ Projet_Commun_Carte_Ensemble_BEnsembleCarte_ObtenirChecksum( this->m_carte );
}

/* Obtenir configuration monde */
const BConfigurationMonde *Projet_Serveur_BServeur_ObtenirConfigurationMonde( const BServeur *this )
{
	return this->m_configurationMonde;
}

/* Obtenir monde */
const BMonde *Projet_Serveur_BServeur_ObtenirMonde( const BServeur *this )
{
	return this->m_monde;
}

/* Est en cours? */
NBOOL Projet_Serveur_BServeur_EstEnCours( const BServeur *this )
{
	return this->m_estEnCours;
}

/* Lancer le serveur */
NBOOL Projet_Serveur_BServeur_Lancer( BServeur *this )
{
	// On est en mode salle d'attente
	this->m_etat = BETAT_SERVEUR_SALLE_ATTENTE;

	// Construire le serveur
	if( !( this->m_serveur = NLib_Module_Reseau_Serveur_NServeur_Construire( this->m_port,
		Projet_Serveur_Reseau_CallbackReceptionPacket,
		Projet_Serveur_Reseau_CallbackConnexionClient,
		Projet_Serveur_Reseau_CallbackDeconnexionClient,
		this,
		0,
		0,
		NTYPE_SERVEUR_TCP,
		NMETHODE_TRANSMISSION_PACKETIO_NPROJECT ) ) )
	{
		// Notifier
		NOTIFIER_ERREUR( NERREUR_CONSTRUCTOR_FAILED );

		// Quitter
		return NFALSE;
	}

	// Limiter le nombre de joueurs
	NLib_Module_Reseau_Serveur_NServeur_DefinirNombreMaximumClients( this->m_serveur,
		BNOMBRE_MAXIMUM_JOUEUR );

	// Ouvrir serveur
	NLib_Module_Reseau_Serveur_NServeur_AutoriserConnexion( this->m_serveur );

	// OK
	return NTRUE;
}

/* Ajouter un client */
NBOOL Projet_Serveur_BServeur_AjouterClient( BServeur *this,
	NClientServeur *client )
{
	// Etat client
	const BEtatClient *etat;

	// Proteger le cache
	Projet_Commun_Reseau_Client_Cache_BCacheClient_ProtegerCache( this->m_cacheClient );

	// Ajouter le client au cache
	if( !( etat = Projet_Commun_Reseau_Client_Cache_BCacheClient_AjouterClientExterne( this->m_cacheClient,
		NLib_Module_Reseau_Serveur_NClientServeur_ObtenirIdentifiant( client ),
		client ) ) )
	{
		// Notifier
		NOTIFIER_ERREUR( NERREUR_LOGIN_FAILED );

		// On ne protege plus
		Projet_Commun_Reseau_Client_Cache_BCacheClient_NePlusProtegerCache( this->m_cacheClient );

		// Quitter
		return NFALSE;
	}

	// Notifier identifiant
	if( !Projet_Serveur_TraitementPacket_NotifierIdentifiantJoueur( this,
		client ) )
	{
		// Notifier
		NOTIFIER_ERREUR( NERREUR_STREAM_ERROR );

		// On ne protege plus
		Projet_Commun_Reseau_Client_Cache_BCacheClient_NePlusProtegerCache( this->m_cacheClient );

		// Quitter
		return NFALSE;
	}

	// Informer au sujet des joueurs deja connectes
	if( !Projet_Serveur_TraitementPacket_NotifierJoueurConnecte( (BServeur*)NLib_Module_Reseau_Serveur_NClientServeur_ObtenirDonneeExterne( client ),
		(NClientServeur*)client ) )
	{
		// Notifier
		NOTIFIER_ERREUR( NERREUR_STREAM_ERROR );

		// On ne protege plus
		Projet_Commun_Reseau_Client_Cache_BCacheClient_NePlusProtegerCache( this->m_cacheClient );

		// Quitter
		return NFALSE;
	}

	// Informer au sujet de l'etat de la configuration
	if( !Projet_Serveur_TraitementPacket_NotifierEtatServeur( (BServeur*)NLib_Module_Reseau_Serveur_NClientServeur_ObtenirDonneeExterne( client ),
		(NClientServeur*)client ) )
	{
		// Notifier
		NOTIFIER_ERREUR( NERREUR_STREAM_ERROR );

		// On ne protege plus
		Projet_Commun_Reseau_Client_Cache_BCacheClient_NePlusProtegerCache( this->m_cacheClient );

		// Quitter
		return NFALSE;
	}

	// On ne protege plus
	Projet_Commun_Reseau_Client_Cache_BCacheClient_NePlusProtegerCache( this->m_cacheClient );

	// OK
	return NTRUE;
}

/* Supprimer un client */
NBOOL Projet_Serveur_BServeur_SupprimerClient( BServeur *this,
	NClientServeur *client )
{
	// Proteger le cache
	Projet_Commun_Reseau_Client_Cache_BCacheClient_ProtegerCache( this->m_cacheClient );

	// Supprimer client du cache
	if( !Projet_Commun_Reseau_Client_Cache_BCacheClient_SupprimerClientNoLock( this->m_cacheClient,
		NLib_Module_Reseau_Serveur_NClientServeur_ObtenirIdentifiant( client ) ) )
	{
		// Notifier
		NOTIFIER_ERREUR( NERREUR_LOGOUT_FAILED );

		// Arreter le serveur
		this->m_estEnCours = NFALSE;

		// Ne plus proteger le cache
		Projet_Commun_Reseau_Client_Cache_BCacheClient_NePlusProtegerCache( this->m_cacheClient );

		// Quitter
		return NFALSE;
	}

	// Notifier
	if( !Projet_Serveur_TraitementPacket_NotifierDeconnexionJoueur( (BServeur*)NLib_Module_Reseau_Serveur_NClientServeur_ObtenirDonneeExterne( client ),
		client ) )
	{
		// Notifier
		NOTIFIER_ERREUR( NERREUR_STREAM_ERROR );

		// Ne plus proteger le cache
		Projet_Commun_Reseau_Client_Cache_BCacheClient_NePlusProtegerCache( this->m_cacheClient );

		// Quitter
		return NFALSE;
	}

	// Ne plus proteger le cache
	Projet_Commun_Reseau_Client_Cache_BCacheClient_NePlusProtegerCache( this->m_cacheClient );

	// OK
	return NTRUE;
}

/* Envoyer packet (si cache protege) */
NBOOL Projet_Serveur_BServeur_EnvoyerPacketTousNoLock( BServeur *this,
	__WILLBEOWNED NPacket *packet )
{
	return Projet_Commun_Reseau_Client_Cache_BCacheClient_EnvoyerPacketDepuisServeurTousNoLock( this->m_cacheClient,
		packet );
}

/* Envoyer packet (si cache non protege) */
NBOOL Projet_Serveur_BServeur_EnvoyerPacketTous( BServeur *this,
	__WILLBEOWNED NPacket *packet )
{
	return Projet_Commun_Reseau_Client_Cache_BCacheClient_EnvoyerPacketDepuisServeurTous( this->m_cacheClient,
		packet );
}

/* Traiter */
NBOOL Projet_Serveur_BServeur_TraiterPacket( BServeur *this,
	const NClientServeur *client,
	BTypePacket type,
	const void *data )
{
	switch( type )
	{
		case BTYPE_PACKET_CLIENT_SERVEUR_TRANSMET_INFORMATIONS_JOUEUR:
			return Projet_Serveur_TraitementPacket_TraiterTransmetInformationsJoueur( this,
				client,
				data );

		case BTYPE_PACKET_CLIENT_SERVEUR_TRANSMET_CHECKSUM:
			return Projet_Serveur_TraitementPacket_TraiterTransmetChecksum( this,
				client,
				data );

		case BTYPE_PACKET_CLIENT_SERVEUR_PING:
			return Projet_Serveur_TraitementPacket_TraiterPing( this,
				client,
				data );
		case BTYPE_PACKET_CLIENT_SERVEUR_PONG:
			return Projet_Serveur_TraitementPacket_TraiterPong( this,
				client,
				data );

		case BTYPE_PACKET_CLIENT_SERVEUR_CHANGE_ETAT_PRET:
			return Projet_Serveur_TraitementPacket_TraiterChangeEtatPret( this,
				client,
				data );

		case BTYPE_PACKET_CLIENT_SERVEUR_CONFIRME_LANCEMENT:
			return Projet_Serveur_TraitementPacket_TraiterConfirmeLancement( this,
				client,
				data );

		case BTYPE_PACKET_CLIENT_SERVEUR_CHANGE_DIRECTION:
			return Projet_Serveur_TraitementPacket_TraiterChangeDirection( this,
				client,
				data );
		case BTYPE_PACKET_CLIENT_SERVEUR_CHANGE_POSITION:
			return Projet_Serveur_TraitementPacket_TraiterChangePosition( this,
				client,
				data );

		case BTYPE_PACKET_CLIENT_SERVEUR_POSE_BOMBE:
			return Projet_Serveur_TraitementPacket_TraiterPoseBombe( this,
				client,
				data );

		default:
			// Notifier
			NOTIFIER_ERREUR( NERREUR_UNKNOWN_PACKET );

			// Quitter
			return NFALSE;
	}
}

/* Passer en attente de confirmation de lancement des clients */
NBOOL Projet_Serveur_BServeur_ValiderLancementClient( BServeur *this )
{
	// Iterateur
	NU32 i = 0;

	// Joueur
	const BEtatClient *joueur;

	// Tick
	NU32 tickDebutKick;

	// Construire le monde
	if( !( this->m_monde = Projet_Commun_Monde_BMonde_Construire( this->m_cacheClient,
		this->m_configurationMonde,
		this->m_carte,
		this->m_ressource ) ) )
	{
		// Notifier
		NOTIFIER_ERREUR( NERREUR_CONSTRUCTOR_FAILED );

		// Quitter
		return NFALSE;
	}

	// Construire le monde serveur
	if( !( this->m_mondeServeur = Projet_Serveur_Monde_BMondeServeur_Construire( this ) ) )
	{
		// Notifier
		NOTIFIER_ERREUR( NERREUR_CONSTRUCTOR_FAILED );

		// Detruire monde
		Projet_Commun_Monde_BMonde_Detruire( &this->m_monde );

		// Quitter
		return NFALSE;
	}

	// Creer les blocs
	Projet_Commun_Monde_BMonde_GenererBloc( this->m_monde );

	// Fermer le serveur
	NLib_Module_Reseau_Serveur_NServeur_InterdireConnexion( this->m_serveur );

	// Proteger le cache
	Projet_Commun_Reseau_Client_Cache_BCacheClient_ProtegerCache( this->m_cacheClient );

	// Fermer le cache
	Projet_Commun_Reseau_Client_Cache_BCacheClient_Fermer( this->m_cacheClient );

	// Kicker les joueurs pas pret
	for( ; i < Projet_Commun_Reseau_Client_Cache_BCacheClient_ObtenirNombreClient( this->m_cacheClient ); i++ )
		// Verifier
		if( !( joueur = Projet_Commun_Reseau_Client_Cache_BCacheClient_ObtenirClient2( this->m_cacheClient,
				i ) )
			|| !Projet_Commun_Reseau_Client_BEtatClient_EstPret( joueur )
			|| !Projet_Commun_Reseau_Client_BEtatClient_EstClientEtatCorrect( joueur ) )
			// Tuer le client
			NLib_Module_Reseau_Serveur_NClientServeur_Tuer( (NClientServeur*)Projet_Commun_Reseau_Client_BEtatClient_ObtenirDonneeSupplementaire( joueur ) );

	// Ne plus proteger le cache
	Projet_Commun_Reseau_Client_Cache_BCacheClient_NePlusProtegerCache( this->m_cacheClient );

	// Relever tick debut kick
	tickDebutKick = NLib_Temps_ObtenirTick( );

	// Kicker
	while( !Projet_Commun_Reseau_Client_Cache_BCacheClient_EstClientsPrets( this->m_cacheClient )
		&& NLib_Temps_ObtenirTick( ) - tickDebutKick < BTEMPS_AVANT_TIMEOUT_SERVEUR_KICK_JOUEUR_NON_PRET_DEBUT_PRET )
		NLib_Temps_Attendre( 1 );

	// Verifier que (tous) le(s) client(s) non prets aient bien ete kicke(s)
	if( !Projet_Commun_Reseau_Client_Cache_BCacheClient_EstClientsPrets( this->m_cacheClient ) )
	{
		// Notifier
		NOTIFIER_ERREUR_UTILISATEUR( NERREUR_USER,
			"[SERVEUR] Tous les clients ne sont pas prets.",
			0 );

		// Detruire le monde
		Projet_Commun_Monde_BMonde_Detruire( &this->m_monde );
		Projet_Serveur_Monde_BMondeServeur_Detruire( &this->m_mondeServeur );

		// Reouvrir le cache
		Projet_Commun_Reseau_Client_Cache_BCacheClient_Ouvrir( this->m_cacheClient );

		// Reouvrir le serveur
		NLib_Module_Reseau_Serveur_NServeur_AutoriserConnexion( this->m_serveur );

		// Quitter
		return NFALSE;
	}

	// Proteger le cache
	Projet_Commun_Reseau_Client_Cache_BCacheClient_ProtegerCache( this->m_cacheClient );

	// Verifier le nombre de joueurs soit toujours correct
	if( Projet_Commun_Reseau_Client_Cache_BCacheClient_ObtenirNombreClient( this->m_cacheClient ) < BNOMBRE_MINIMUM_JOUEUR )
	{
		// Notifier
		NOTIFIER_ERREUR_UTILISATEUR( NERREUR_USER,
			"[SERVEUR] Apres kick des clients non prets, nombre de client(s) trop faible.",
			0 );

		// Detruire le monde
		Projet_Commun_Monde_BMonde_Detruire( &this->m_monde );
		Projet_Serveur_Monde_BMondeServeur_Detruire( &this->m_mondeServeur );

		// Reouvrir le cache
		Projet_Commun_Reseau_Client_Cache_BCacheClient_Ouvrir( this->m_cacheClient );

		// Ne plus proteger le cache
		Projet_Commun_Reseau_Client_Cache_BCacheClient_NePlusProtegerCache( this->m_cacheClient );

		// Reouvrir le serveur
		NLib_Module_Reseau_Serveur_NServeur_AutoriserConnexion( this->m_serveur );

		// Quitter
		return NFALSE;
	}

	// Placer les joueurs
	for( i = 0; i < Projet_Commun_Reseau_Client_Cache_BCacheClient_ObtenirNombreClient( this->m_cacheClient ); i++ )
	{
		// Obtenir joueur
		if( !( joueur = Projet_Commun_Reseau_Client_Cache_BCacheClient_ObtenirClient2( this->m_cacheClient,
			i ) ) )
		{
			// Notifier
			NOTIFIER_ERREUR( NERREUR_OBJECT_STATE );

			// Detruire le monde
			Projet_Commun_Monde_BMonde_Detruire( &this->m_monde );
			Projet_Serveur_Monde_BMondeServeur_Detruire( &this->m_mondeServeur );

			// Reouvrir le cache
			Projet_Commun_Reseau_Client_Cache_BCacheClient_Ouvrir( this->m_cacheClient );

			// Ne plus proteger le cache
			Projet_Commun_Reseau_Client_Cache_BCacheClient_NePlusProtegerCache( this->m_cacheClient );

			// Reouvrir le serveur
			NLib_Module_Reseau_Serveur_NServeur_AutoriserConnexion( this->m_serveur );

			// Quitter
			return NFALSE;
		}

		// Placer
		Projet_Commun_Reseau_Client_BEtatClient_DefinirPosition( (BEtatClient*)joueur,
			*Projet_Commun_Carte_BCarte_ObtenirPositionDepart( Projet_Commun_Monde_BMonde_ObtenirCarte( this->m_monde ),
				i ) );
		Projet_Commun_Reseau_Client_BEtatClient_DefinirDirection( (BEtatClient*)joueur,
			NBAS );
	}

	// Notifier lancement partie
	if( !Projet_Serveur_TraitementPacket_NotifierLancementPartie( this ) )
	{
		// Notifier
		NOTIFIER_ERREUR( NERREUR_STREAM_ERROR );

		// Detruire le monde
		Projet_Commun_Monde_BMonde_Detruire( &this->m_monde );
		Projet_Serveur_Monde_BMondeServeur_Detruire( &this->m_mondeServeur );

		// Reouvrir le cache
		Projet_Commun_Reseau_Client_Cache_BCacheClient_Ouvrir( this->m_cacheClient );

		// Ne plus proteger le cache
		Projet_Commun_Reseau_Client_Cache_BCacheClient_NePlusProtegerCache( this->m_cacheClient );

		// Reouvrir le serveur
		NLib_Module_Reseau_Serveur_NServeur_AutoriserConnexion( this->m_serveur );

		// Quitter
		return NFALSE;
	}

	// Changer etat
	this->m_etat = BETAT_SERVEUR_ATTENTE_CONFIRMATION_LANCEMENT;

	// Ne plus proteger le cache
	Projet_Commun_Reseau_Client_Cache_BCacheClient_NePlusProtegerCache( this->m_cacheClient );

	// OK
	return NTRUE;
}

/* Est partie terminee? */
NBOOL Projet_Serveur_BServeur_EstPartieTerminee( const BServeur *this )
{
	return !Projet_Serveur_Monde_BMondeServeur_EstPartieEnCours( this->m_mondeServeur );
}

