#include "../../../include/Projet/Projet.h"

/*
	@author SOARES Lucas
*/

// -------------------------
// namespace Projet::Serveur
// -------------------------

/* Main */
NS32 Projet_Serveur_Main( void )
{
	// Serveur
	BServeur *serveur;

	// Configuration monde
	BConfigurationMonde *configuration;

	// Initialiser NLib
	if( !NLib_Initialiser( Projet_Commun_Erreur_CallbackNotificationErreur ) )
	{
		// Notifier
		NOTIFIER_ERREUR( NERREUR_INIT_FAILED );

		// Quitter
		return EXIT_FAILURE;
	}

	// Creer la configuration
	if( !( configuration = Projet_Commun_Monde_BConfigurationMonde_Construire( ) ) )
	{
		// Notifier
		NOTIFIER_ERREUR( NERREUR_CONSTRUCTOR_FAILED );

		// Quitter NLib
		NLib_Detruire( );

		// Quitter
		return EXIT_FAILURE;
	}

	// Creer le serveur
	if( !( serveur = Projet_Serveur_BServeur_Construire( 16500,
		configuration ) ) )
	{
		// Notifier
		NOTIFIER_ERREUR( NERREUR_CONSTRUCTOR_FAILED );

		// Detruire configuration
		Projet_Commun_Monde_BConfigurationMonde_Detruire( &configuration );

		// Fermer NLib
		NLib_Detruire( );

		// Quitter
		return EXIT_FAILURE;
	}
	
	// Lancer le serveur
	if( !Projet_Serveur_BServeur_Lancer( serveur ) )
	{
		// Notifier
		NOTIFIER_ERREUR( NERREUR_STREAM_ERROR );

		// Detruire
		Projet_Serveur_BServeur_Detruire( &serveur );

		// Fermer NLib
		NLib_Detruire( );

		// Quitter
		return EXIT_FAILURE;
	}

	// Attendre la fin d'execution
	while( Projet_Serveur_BServeur_EstEnCours( serveur ) )
		NLib_Temps_Attendre( 1 );

	// Detruire le serveur
	Projet_Serveur_BServeur_Detruire( &serveur );

	// Detruire la configuration
	Projet_Commun_Monde_BConfigurationMonde_Detruire( &configuration );

	// Fermer NLib
	NLib_Detruire( );

	// OK
	return EXIT_SUCCESS;
}

