#include "../../../../include/Projet/Projet.h"

// --------------------------------------------
// struct Projet::Serveur::Monde::BMondeServeur
// --------------------------------------------

/* Construire */
__ALLOC BMondeServeur *Projet_Serveur_Monde_BMondeServeur_Construire( struct BServeur *serveur )
{
	// Sortie
	__OUTPUT BMondeServeur *out;

	// Allouer la memoire
	if( !( out = calloc( 1,
		sizeof( BMondeServeur ) ) ) )
	{
		// Notifier
		NOTIFIER_ERREUR( NERREUR_ALLOCATION_FAILED );

		// Quitter
		return NULL;
	}

	// Construire experience
	if( !( out->m_experienceApparitionBonus = NLib_Math_Probabilite_NExperience_Construire( Projet_Commun_carte_Bonus_BListeBonus_ObtenirEnsembleProbabilite( ),
		BLISTE_BONUS ) ) )
	{
		// Notifier
		NOTIFIER_ERREUR( NERREUR_CONSTRUCTOR_FAILED );

		// Liberer
		NFREE( out );

		// Quitter
		return NULL;
	}

	// Enregistrer
	if( !( out->m_serveur = serveur )
		|| !( out->m_carte = Projet_Commun_Carte_Ensemble_BEnsembleCarte_Obtenir( serveur->m_carte,
			Projet_Commun_Monde_BConfigurationMonde_ObtenirIdentifiantCarte( serveur->m_configurationMonde ) ) )
		|| !( out->m_monde = serveur->m_monde )
		|| !( out->m_etatCarte = Projet_Commun_Monde_BMonde_ObtenirEtatCarte( out->m_monde ) )
		|| !( out->m_cacheClient = Projet_Serveur_BServeur_ObtenirCacheClient( out->m_serveur ) ) )
	{
		// Notifier
		NOTIFIER_ERREUR( NERREUR_PARAMETER_ERROR );

		// Detruire
		NLib_Math_Probabilite_NExperience_Detruire( &out->m_experienceApparitionBonus );

		// Liberer
		NFREE( out );

		// Quitter
		return NULL;
	}

	// Zero
	out->m_valeurCompteRebours = BTEMPS_AVANT_LANCEMENT_PARTIE_MONDE_SERVEUR + 1;
	out->m_estPartieEnCours = NFALSE;

	// OK
	return out;
}

/* Detruire */
void Projet_Serveur_Monde_BMondeServeur_Detruire( BMondeServeur **this )
{
	// Detruire experience
	NLib_Math_Probabilite_NExperience_Detruire( &(*this)->m_experienceApparitionBonus );

	// Liberer
	NFREE( *this );
}

/* Est partie en cours? */
NBOOL Projet_Serveur_Monde_BMondeServeur_EstPartieEnCours( const BMondeServeur *this )
{
	return this->m_estPartieEnCours;
}

/* Lancer partie */
void Projet_Serveur_Monde_BMondeServeur_LancerPartie( BMondeServeur *this )
{
	// Enregistrer lancement
	this->m_estPartieEnCours = NTRUE;
}

/* Obtenir carte */
const BCarte *Projet_Serveur_Monde_BMondeServeur_ObtenirCarte( BMondeServeur *this )
{
	return this->m_carte;
}

/* Obtenir etat carte */
const BEtatCarte *Projet_Serveur_Monde_BMondeServeur_ObtenirEtatCarte( BMondeServeur *this )
{
	return this->m_etatCarte;
}

/* Poser une bombe */
NU32 Projet_Serveur_Monde_BMondeServeur_PoserBombe( BMondeServeur *this,
	NSPoint position,
	NU32 identifiantJoueur )
{
	// Cases
	const BCaseEtatCarte **cases;

	// Identifiant bombe
	NU32 identifiantBombe;

	// Joueur
	const BEtatClient *joueur;

	// Puissance
	NU32 puissance;

	// Obtenir cases
	if( !( cases = Projet_Commun_Carte_Etat_BEtatCarte_ObtenirCase( this->m_etatCarte ) ) )
	{
		// Notifier
		NOTIFIER_ERREUR( NERREUR_OBJECT_STATE );

		// Quitter
		return NERREUR;
	}

	// Obtenir joueur
	if( !( joueur = Projet_Commun_Reseau_Client_Cache_BCacheClient_ObtenirClient( (BCacheClient*)this->m_cacheClient,
		identifiantJoueur ) ) )
	{
		// Notifier
		NOTIFIER_ERREUR( NERREUR_STREAM_ERROR );

		// Quitter
		return NERREUR;
	}

	// Obtenir puissance joueur
	puissance = Projet_Commun_Reseau_Client_BEtatClient_ObtenirPuissance( joueur );

	// Verifier parametres
	if( !Projet_Commun_Carte_BCarte_EstPositionCorrecte( this->m_carte,
		position ) )
	{
		// Notifier
		NOTIFIER_ERREUR( NERREUR_STREAM_ERROR );

		// Quitter
		return NERREUR;
	}

	// Poser
	if( ( identifiantBombe = Projet_Commun_Carte_Etat_BEtatCarte_PoserBombe( (BEtatCarte*)this->m_etatCarte,
		position,
		identifiantJoueur,
#if BOPTION_TEMPS_AVANT_EXPLOSION_BOMBE_ALEATOIRE == 1
		BTEMPS_AVANT_EXPLOSION_BOMBE_DEFAUT + NLib_Temps_ObtenirNombreAleatoire( )%BTEMPS_ADDITIONNEL_MAXIMUM_EXPLOSION_BOMBE,
#else // BOPTION_TEMPS_AVANT_EXPLOSION_BOMBE_ALEATOIRE == 1
		BTEMPS_AVANT_EXPLOSION_BOMBE_DEFAUT,
#endif // BOPTION_TEMPS_AVANT_EXPLOSION_BOMBE_ALEATOIRE != 1
		NLib_Temps_ObtenirTick( ),
		puissance,
		Projet_Commun_Ressource_BRessource_ObtenirEnsembleAnimation( Projet_Serveur_BServeur_ObtenirRessource( this->m_serveur ) ) ) ) == NERREUR )
	{
		// Notifier
		NOTIFIER_AVERTISSEMENT( NERREUR_OBJECT_STATE );

		// Quitter
		return NERREUR;
	}

	// OK
	return identifiantBombe;
}

/* Detruire bloc rempli effectif (privee) */
NBOOL Projet_Serveur_Monde_BMondeServeur_DetruireBlocRempliExplosionBombeEffectifInterne( BMondeServeur *this,
	BCaseEtatCarte **casesEtat,
	NS32 x,
	NS32 y )
{
	// Bonus
	BListeBonus bonus;

	// Position
	NSPoint position;

	// Identifiant bonus
	NU32 identifiantBonus;

	// Duree bonus
	NU32 dureeBonus;

	// Detruire le bloc
	casesEtat[ x ][ y ].m_estRempli = NFALSE;

	// Enregistrer position
	NDEFINIR_POSITION( position,
		x,
		y );

	// Notifier la destruction
	if( !Projet_Serveur_TraitementPacket_NotifierDestructionBlocRempli( this->m_serveur,
		x,
		y ) )
	{
		// Notifier
		NOTIFIER_ERREUR( NERREUR_OBJECT_STATE );

		// Quitter
		return NFALSE;
	}

	// Obtenir la duree du bonus
	dureeBonus = 10000;

	// Faire une experience
	switch( ( bonus = NLib_Math_Probabilite_NExperience_Lancer( this->m_experienceApparitionBonus ) ) )
	{
		// Pas d'objet qui spawn
		case (BListeBonus)NERREUR:
			break;

		default:
			// Ajouter bonus
			if( ( identifiantBonus = Projet_Commun_Carte_Etat_BEtatCarte_PoserBonus( (BEtatCarte*)this->m_etatCarte,
				position,
				bonus,
				dureeBonus,
				NLib_Temps_ObtenirTick( ) ) ) == NERREUR )
			{
				// Notifier
				NOTIFIER_AVERTISSEMENT( NERREUR_OBJECT_STATE );

				// Quitter
				return NFALSE;
			}

			// Notifier l'apparition du bonus
			if( !Projet_Serveur_TraitementPacket_NotifierApparitionBonus( this->m_serveur,
				position,
				bonus,
				dureeBonus,
				identifiantBonus ) )
			{
				// Notifier
				NOTIFIER_ERREUR( NERREUR_STREAM_ERROR );

				// Quitter
				return NFALSE;
			}
			break;
	}

	// OK
	return NTRUE;
}

/* Detruire bloc rempli (privee) */
NBOOL Projet_Serveur_Monde_BMondeServeur_DetruireBlocRempliExplosionBombeInterne( BMondeServeur *this,
	NU32 puissance,
	NS32 x,
	NS32 y )
{
	// Iterateur
	NS32 i;

	// Cases
	BCaseEtatCarte **caseEtat;
	BBloc **caseCarte;

	// Obtenir cases
	if( !( caseEtat = (BCaseEtatCarte**)Projet_Commun_Carte_Etat_BEtatCarte_ObtenirCase( this->m_etatCarte ) )
		|| !( caseCarte = (BBloc**)Projet_Commun_Carte_BCarte_ObtenirCases( this->m_carte ) ) )
	{
		// Notifier
		NOTIFIER_ERREUR( NERREUR_OBJECT_STATE );

		// Quitter
		return NFALSE;
	}

	// Haut
	for( i = 1; i < (NS32)puissance + 1; i++ )
		if( y - i >= 0
			&& caseCarte[ x ][ y - i ].m_type != BTYPE_BLOC_SOLIDE )
		{
			// Est bloc rempli rencontre?
			if( caseEtat[ x ][ y - i ].m_estRempli )
			{
				// Detruire le bloc
				Projet_Serveur_Monde_BMondeServeur_DetruireBlocRempliExplosionBombeEffectifInterne( this,
					caseEtat,
					x,
					y - i );

				// Sortir
				break;
			}
		}
		else
			break;

	// Bas
	for( i = 1; i < (NS32)puissance + 1; i++ )
		if( y + i < (NS32)Projet_Commun_Carte_BCarte_ObtenirTaille( this->m_carte )->y
			&& caseCarte[ x ][ y + i ].m_type != BTYPE_BLOC_SOLIDE )
		{
			// Est bloc rempli rencontre?
			if( caseEtat[ x ][ y + i ].m_estRempli )
			{
				// Detruire le bloc
				Projet_Serveur_Monde_BMondeServeur_DetruireBlocRempliExplosionBombeEffectifInterne( this,
					caseEtat,
					x,
					y + i );

				// Sortir
				break;
			}
		}
		else
			break;

	// Gauche
	for( i = 1; i < (NS32)puissance + 1; i++ )
		if( x - i >= 0
			&& caseCarte[ x - i ][ y ].m_type != BTYPE_BLOC_SOLIDE )
		{
			// Est bloc rempli rencontre?
			if( caseEtat[ x - i ][ y ].m_estRempli )
			{
				// Detruire le bloc
				Projet_Serveur_Monde_BMondeServeur_DetruireBlocRempliExplosionBombeEffectifInterne( this,
					caseEtat,
					x - i,
					y );

				// Sortir
				break;
			}
		}
		else
			break;

	// Droite
	for( i = 1; i < (NS32)puissance + 1; i++ )
		if( x + i < (NS32)Projet_Commun_Carte_BCarte_ObtenirTaille( this->m_carte )->x
			&& caseCarte[ x + i ][ y ].m_type != BTYPE_BLOC_SOLIDE )
		{
			// Est bloc rempli rencontre?
			if( caseEtat[ x + i ][ y ].m_estRempli )
			{
				// Detruire le bloc
				Projet_Serveur_Monde_BMondeServeur_DetruireBlocRempliExplosionBombeEffectifInterne( this,
					caseEtat,
					x + i,
					y );

				// Sortir
				break;
			}
		}
		else
			break;

	// OK
	return NTRUE;
}

/* Gerer bombes (privee) */
NBOOL Projet_Serveur_Monde_BMondeServeur_GererBombeInterne( BMondeServeur *this )
{
	// Cases etat
	const BCaseEtatCarte **caseEtat;

	// Iterateurs
	NU32 x,
		y;

	// Puissance
	NU32 puissance;

	// Obtenir cases
	if( !( caseEtat = Projet_Commun_Carte_Etat_BEtatCarte_ObtenirCase( this->m_etatCarte ) ) )
	{
		// Notifier
		NOTIFIER_ERREUR( NERREUR_OBJECT_STATE );

		// Quitter
		return NFALSE;
	}

	// Chercher les bombes a faire exploser
	for( x = 0; x < Projet_Commun_Carte_BCarte_ObtenirTaille( this->m_carte )->x; x++ )
		for( y = 0; y < Projet_Commun_Carte_BCarte_ObtenirTaille( this->m_carte )->y; y++ )
			// Il y a une bombe ici?
			if( caseEtat[ x ][ y ].m_etatBombe != NULL )
				// Elle doit exploser?
				if( NLib_Temps_ObtenirTick( ) - Projet_Commun_Carte_Etat_Case_Bombe_BEtatBombe_ObtenirTempsPose( caseEtat[ x ][ y ].m_etatBombe ) >= Projet_Commun_Carte_Etat_Case_Bombe_BEtatBombe_ObtenirDureeVie( caseEtat[ x ][ y ].m_etatBombe )
					|| Projet_Commun_Carte_Etat_Case_AnimationExplosion_BEtatAnimationExplosion_EstEnCours( caseEtat[ x ][ y ].m_etatExplosion ) )
				{
					// Notifier le client de l'explosion
					Projet_Serveur_TraitementPacket_NotifierExplosionBombe( this->m_serveur,
						Projet_Commun_Carte_Etat_Case_Bombe_BEtatBombe_ObtenirIdentifiant( caseEtat[ x ][ y ].m_etatBombe ),
						Projet_Commun_Carte_Etat_Case_Bombe_BEtatBombe_ObtenirPuissance( caseEtat[ x ][ y ].m_etatBombe ),
						Projet_Commun_Carte_Etat_Case_Bombe_BEtatBombe_ObtenirIdentifiantJoueur( caseEtat[ x ][ y ].m_etatBombe ) );

					// Obtenir la puissance
					puissance = Projet_Commun_Carte_Etat_Case_Bombe_BEtatBombe_ObtenirPuissance( caseEtat[ x ][ y ].m_etatBombe );

					// Detruire la bombe
					Projet_Commun_Monde_BMonde_ExploserBombe( (BMonde*)this->m_monde,
						Projet_Commun_Carte_Etat_Case_Bombe_BEtatBombe_ObtenirIdentifiant( caseEtat[ x ][ y ].m_etatBombe ),
						Projet_Commun_Carte_Etat_Case_Bombe_BEtatBombe_ObtenirPuissance( caseEtat[ x ][ y ].m_etatBombe ),
						Projet_Commun_Carte_Etat_Case_Bombe_BEtatBombe_ObtenirIdentifiantJoueur( caseEtat[ x ][ y ].m_etatBombe ) );

					// Detruire les blocs remplis dans le rayon de l'explosion
					Projet_Serveur_Monde_BMondeServeur_DetruireBlocRempliExplosionBombeInterne( this,
						puissance,
						(NS32)x,
						(NS32)y );
				}

	// OK
	return NTRUE;
}

/* Faire disparaitre bonus (privee) */
NBOOL Projet_Serveur_Monde_BMondeServeur_FaireDisparaitreBonusInterne( BMondeServeur *this,
	const BCaseEtatCarte **caseEtat,
	NS32 x,
	NS32 y )
{
	// Code retour
	NBOOL codeRetour = NTRUE;

	// Faire disparaitre le bonus
	if( !Projet_Serveur_TraitementPacket_NotifierDisparitionBonus( this->m_serveur,
		Projet_Commun_Carte_Etat_Case_Bonus_BBonusCase_ObtenirIdentifiant( caseEtat[ x ][ y ].m_etatBonus ) ) )
		codeRetour = NFALSE;

	// Supprimer le bonus
	if( !Projet_Commun_Carte_Etat_BEtatCarte_DisparaitreBonus( (BEtatCarte*)this->m_etatCarte,
		Projet_Commun_Carte_Etat_Case_Bonus_BBonusCase_ObtenirIdentifiant( caseEtat[ x ][ y ].m_etatBonus ) ) )
		codeRetour = NFALSE;

	// OK?
	return codeRetour;
}

/* Gerer bonus (privee) */
NBOOL Projet_Serveur_Monde_BMondeServeur_GererBonusInterne( BMondeServeur *this )
{
	// Iterateurs
	NU32 x,
		y;

	// Cases d'etat carte
	BCaseEtatCarte **caseEtat;

	// Obtenir cases
	if( !( caseEtat = (BCaseEtatCarte**)Projet_Commun_Carte_Etat_BEtatCarte_ObtenirCase( this->m_etatCarte ) ) )
	{
		// Notifier
		NOTIFIER_ERREUR( NERREUR_OBJECT_STATE );

		// Quitter
		return NFALSE;
	}

	for( x = 0; x < Projet_Commun_Carte_BCarte_ObtenirTaille( this->m_carte )->x; x++ )
		for( y = 0; y < Projet_Commun_Carte_BCarte_ObtenirTaille( this->m_carte )->y; y++ )
			// Il y a un bonus sur cette case?
			if( caseEtat[ x ][ y ].m_etatBonus != NULL )
			{
				// Verifier si le bonus doit disparaitre
				if( NLib_Temps_ObtenirTick( ) - Projet_Commun_Carte_Etat_Case_Bonus_BBonusCase_ObtenirTempsApparition( caseEtat[ x ][ y ].m_etatBonus ) >= Projet_Commun_Carte_Etat_Case_Bonus_BBonusCase_ObtenirDureeBonus( caseEtat[ x ][ y ].m_etatBonus ) )
					Projet_Serveur_Monde_BMondeServeur_FaireDisparaitreBonusInterne( this,
						(const BCaseEtatCarte**)caseEtat,
						x,
						y );
			}

	// OK
	return NTRUE;
}

/* Traiter la prise de bonus eventuelle */
NBOOL Projet_Serveur_Monde_BMondeServeur_TraiterPriseBonus( BMondeServeur *this,
	const NClientServeur *client,
	const BEtatClient *joueur )
{
	// Position
	NSPoint position;

	// Cases etat
	const BCaseEtatCarte **caseEtat;

	// Obtenir position
	position = *Projet_Commun_Reseau_Client_BEtatClient_ObtenirPosition( joueur );

	// Obtenir cases etat
	if( !( caseEtat = Projet_Commun_Carte_Etat_BEtatCarte_ObtenirCase( this->m_etatCarte ) ) )
	{
		// Notifier
		NOTIFIER_ERREUR( NERREUR_OBJECT_STATE );

		// Quitter
		return NFALSE;
	}

	// Verifier la position
	if( !Projet_Commun_Carte_BCarte_EstPositionCorrecte( this->m_carte,
		position ) )
	{
		// Notifier
		NOTIFIER_ERREUR( NERREUR_OBJECT_STATE );

		// Quitter
		return NFALSE;
	}

	// Verifier s'il y a un bonus sur la case
	if( caseEtat[ position.x ][ position.y ].m_etatBonus != NULL )
	{
		// Gerer la prise de bonus
		Projet_Commun_Carte_Bonus_GererPriseBonus( (BEtatClient*)joueur,
			Projet_Commun_Carte_Etat_Case_Bonus_BBonusCase_ObtenirType( caseEtat[ position.x ][ position.y ].m_etatBonus ) );

		// Notifier la prise de bonus au client
		if( !Projet_Serveur_TraitementPacket_NotifierPriseBonus( client,
			Projet_Commun_Carte_Etat_Case_Bonus_BBonusCase_ObtenirType( caseEtat[ position.x ][ position.y ].m_etatBonus ) ) )
		{
			// Notifier
			NOTIFIER_ERREUR( NERREUR_STREAM_ERROR );

			// Quitter
			return NFALSE;
		}

		// Supprimer le bonus
		Projet_Serveur_Monde_BMondeServeur_FaireDisparaitreBonusInterne( this,
			caseEtat,
			position.x,
			position.y );
	}



	// OK
	return NTRUE;
}

/* Gerer les joueurs (privee) */
NBOOL Projet_Serveur_Monde_BMondeServeur_GererJoueurInterne( BMondeServeur *this )
{
	// Joueurs
	BEtatClient *joueur;
	const BEtatClient *joueur2;

	// Iterateur
	NU32 i = 0;

	// Position
	NSPoint position;

	// Cases etat
	const BCaseEtatCarte **caseEtat;

	// Buffer
	char buffer[ 2048 ];

	// Obtenir cache
	if( !( caseEtat = Projet_Commun_Carte_Etat_BEtatCarte_ObtenirCase( this->m_etatCarte ) ) )
	{
		// Notifier
		NOTIFIER_ERREUR( NERREUR_OBJECT_STATE );

		// Quitter
		return NFALSE;
	}

	// Parcourir clients
	for( ; i < Projet_Commun_Reseau_Client_Cache_BCacheClient_ObtenirNombreClient( this->m_cacheClient ); i++ )
	{
		// Obtenir joueur
		if( !( joueur = (BEtatClient*)Projet_Commun_Reseau_Client_Cache_BCacheClient_ObtenirClient2( this->m_cacheClient,
			i ) ) )
		{
			// Notifier
			NOTIFIER_ERREUR( NERREUR_OBJECT_STATE );

			// Quitter
			return NFALSE;
		}

		// Verifier si le joueur est toujours vivant
		if( !Projet_Commun_Reseau_Client_BEtatClient_EstEnVie( joueur ) )
			continue;

		// Obtenir position
		position = *Projet_Commun_Reseau_Client_BEtatClient_ObtenirPosition( joueur );

		// Verifier la position
		if( !Projet_Commun_Carte_BCarte_EstPositionCorrecte( this->m_carte,
			position ) )
		{
			// Notifier
			NOTIFIER_ERREUR( NERREUR_OBJECT_STATE );

			// Kicker
			NLib_Module_Reseau_Serveur_NClientServeur_Tuer( Projet_Commun_Reseau_Client_BEtatClient_ObtenirDonneeSupplementaire( joueur ) );

			// Quitter
			return NFALSE;
		}

		// Le joueur doit-il mourir?
		if( Projet_Commun_Carte_Etat_Case_AnimationExplosion_BEtatAnimationExplosion_EstEnCours( caseEtat[ position.x ][ position.y ].m_etatExplosion ) )
		{
			// Enregistrer l'information
			Projet_Commun_Reseau_Client_BEtatClient_DefinirEstEnVie( joueur,
				NFALSE );

			// Notifier la mort
			Projet_Serveur_TraitementPacket_NotifierMortJoueur( this->m_serveur,
				Projet_Commun_Reseau_Client_BEtatClient_ObtenirIdentifiant( joueur ) );

			// Notifier message mort
				// Obtenir deuxieme joueur
					if( !( joueur2 = Projet_Commun_Reseau_Client_Cache_BCacheClient_ObtenirClient( this->m_cacheClient,
						Projet_Commun_Carte_Etat_Case_AnimationExplosion_BEtatAnimationExplosion_ObtenirIdentifiantJoueur( caseEtat[ position.x ][ position.y ].m_etatExplosion ) ) ) )
						continue;
				// Composer
					if( Projet_Commun_Reseau_Client_BEtatClient_ObtenirIdentifiant( joueur ) == Projet_Commun_Reseau_Client_BEtatClient_ObtenirIdentifiant( joueur2 ) )
						sprintf( buffer,
							BMESSAGE_MORT_JOUEUR_SUICIDE_SERVEUR,
							Projet_Commun_Reseau_Client_BEtatClient_ObtenirNom( joueur ) );
					else
						sprintf( buffer,
							BMESSAGE_MORT_JOUEUR_SERVEUR,
							Projet_Commun_Reseau_Client_BEtatClient_ObtenirNom( joueur ),
							Projet_Commun_Reseau_Client_BEtatClient_ObtenirNom( joueur2 ) );
				// Enregistrer le temps d'envoi
					this->m_tempsDernierChangement = NLib_Temps_ObtenirTick( );
				// Notifier
					Projet_Serveur_TraitementPacket_NotifierMessageClientTous( this->m_serveur,
						buffer,
						BPOLICE_MESSAGE_CLIENT_MORT_JOUEUR,
						BCOULEUR_MESSAGE_MORT_JOUEUR_SERVEUR,
						BDUREE_MESSAGE_MORT_JOUEUR_SERVEUR,
						BPOSITION_AFFICHAGE_MESSAGE_CLIENT_HAUT,
						NTRUE,
						BCOULEUR_CADRE_MESSAGE_MORT_JOUEUR_SERVEUR );
		}
	}

	// OK
	return NTRUE;
}

/* Gerer le compte a rebours (privee) */
void Projet_Serveur_Monde_BMondeServeur_GererCompteRebours( BMondeServeur *this )
{
	// Buffer
	char buffer[ 32 ];

	// Couleur
	NCouleur couleur;

	// Si une seconde est passee
	if( NLib_Temps_ObtenirTick( ) - this->m_tempsDernierChangement >= 1000
		|| !this->m_tempsDernierChangement )
	{
		// Si on est a la fin du compte a rebours
		if( this->m_valeurCompteRebours <= 1 )
		{
			// Definir couleur aleatoire
			NDEFINIR_COULEUR( couleur,
				0xFF,
				0xFF,
				0xFF );

			// Enregistrer le temps d'envoi
			this->m_tempsDernierChangement = NLib_Temps_ObtenirTick( );

			// Envoyer message au client
			Projet_Serveur_TraitementPacket_NotifierMessageClientTous( this->m_serveur,
				BMESSAGE_DEBUT_PARTIE_MONDE_SERVEUR,
				BPOLICE_MESSAGE_CLIENT_DEBUT_PARTIE,
				couleur,
				1000,
				BPOSITION_AFFICHAGE_MESSAGE_CLIENT_DEFAUT,
				NFALSE,
				(NCouleur){ 0x00, 0x00, 0x00, 0x00 } );

			// Notifier le debut de la partie
			Projet_Serveur_TraitementPacket_NotifierDebutPartie( this->m_serveur );

			// Compte a rebours effectue
			this->m_estCompteReboursEffectue = NTRUE;
		}
		else
		{
			// Decrementer compte a rebours
			this->m_valeurCompteRebours--;

			// Notifier message client
			sprintf( buffer,
				"%d",
				this->m_valeurCompteRebours );

			// Definir couleur aleatoire
			NDEFINIR_COULEUR( couleur,
				NLib_Temps_ObtenirNombreAleatoire( )%0xFF,
				NLib_Temps_ObtenirNombreAleatoire( )%0xFF,
				NLib_Temps_ObtenirNombreAleatoire( )%0xFF );

			// Enregistrer le temps d'envoi
			this->m_tempsDernierChangement = NLib_Temps_ObtenirTick( );

			// Envoyer message aux clients
			Projet_Serveur_TraitementPacket_NotifierMessageClientTous( this->m_serveur,
				buffer,
				BPOLICE_MESSAGE_CLIENT_COMPTE_REBOURS,
				couleur,
				1000,
				BPOSITION_AFFICHAGE_MESSAGE_CLIENT_DEFAUT,
				NFALSE,
				(NCouleur){ 0x00, 0x00, 0x00, 0x00 } );

			// Enregistrer nouveau temps
			this->m_tempsDernierChangement = NLib_Temps_ObtenirTick( );
		}
	}
}

/* Est partie terminee? (privee) */
NBOOL Projet_Serveur_Monde_BMondeServeur_EstPartieTermineeInterne( const BMondeServeur *this )
{
	// Plus d'un joueur?
	if( Projet_Commun_Reseau_Client_Cache_BCacheClient_ObtenirNombreClient( this->m_cacheClient ) > 1 )
		return Projet_Commun_Reseau_Client_Cache_BCacheClient_ObtenirNombreClientMort( this->m_cacheClient ) >= Projet_Commun_Reseau_Client_Cache_BCacheClient_ObtenirNombreClient( this->m_cacheClient ) - 1;
	// Un seul joueur?
	else
		return Projet_Commun_Reseau_Client_Cache_BCacheClient_ObtenirNombreClientMort( this->m_cacheClient ) > 0;
}

/* Gerer la partie (privee) */
void Projet_Serveur_Monde_BMondeServeur_GererPartieInterne( BMondeServeur *this )
{
	// Joueurs
	const BEtatClient *joueur;
	const BEtatClient *joueurVivant[ BNOMBRE_MAXIMUM_JOUEUR ];
	NU32 nombreJoueurVivant = 0;

	// Iterateur
	NU32 i;

	// Fin de la partie?
	if( Projet_Serveur_Monde_BMondeServeur_EstPartieTermineeInterne( this )
		&& this->m_estPartieEnCours )
	{
		// Arret de la partie
		this->m_estPartieEnCours = NFALSE;

		// Compter les joueurs encore en vie
		for( i = 0; i < Projet_Commun_Reseau_Client_Cache_BCacheClient_ObtenirNombreClient( this->m_cacheClient ); i++ )
		{
			// Obtenir joueur
			if( !( joueur = Projet_Commun_Reseau_Client_Cache_BCacheClient_ObtenirClient2( this->m_cacheClient,
				i ) ) )
				continue;

			// Verifier si le joueur est en vie
			if( Projet_Commun_Reseau_Client_BEtatClient_EstEnVie( joueur ) )
			{
				// Ajouter le client
				joueurVivant[ nombreJoueurVivant ] = joueur;

				// Incrementer le nombre de joueurs en vie
				nombreJoueurVivant++;
			}
		}

		// Notifier les clients
		Projet_Serveur_TraitementPacket_NotifierFinPartie( this->m_serveur,
			( nombreJoueurVivant < 1 ?
				BTYPE_FIN_PARTIE_EGALITE
				: BTYPE_FIN_PARTIE_VICTOIRE ),
			( nombreJoueurVivant >= 1 ?
				Projet_Commun_Reseau_Client_BEtatClient_ObtenirIdentifiant( joueurVivant[ 0 ] )
				: NERREUR ) );
	}
}

/* Update */
void Projet_Serveur_Monde_BMondeServeur_Update( BMondeServeur *this )
{
	// Si la partie n'est pas en cours
	if( !this->m_estPartieEnCours )
		// Quitter
		return;

	// En attente de lancement (compte a rebours)
	if( !this->m_estCompteReboursEffectue )
		// Gerer compte a rebours
		Projet_Serveur_Monde_BMondeServeur_GererCompteRebours( this );
	// En jeu
	else
	{
		// Lock etat carte
		Projet_Commun_Carte_Etat_BEtatCarte_Proteger( (BEtatCarte*)this->m_etatCarte );

		// Gerer les bombes
		Projet_Serveur_Monde_BMondeServeur_GererBombeInterne( this );

		// Gerer les bonus
		Projet_Serveur_Monde_BMondeServeur_GererBonusInterne( this );

		// Gerer les joueurs
		Projet_Serveur_Monde_BMondeServeur_GererJoueurInterne( this );

		// Gerer la partie
		Projet_Serveur_Monde_BMondeServeur_GererPartieInterne( this );

		// Unlock etat carte
		Projet_Commun_Carte_Etat_BEtatCarte_NePlusProteger( (BEtatCarte*)this->m_etatCarte );
	}

	// Update monde
	Projet_Commun_Monde_BMonde_Update( (BMonde*)this->m_monde );
}

