#include "../../../../include/Projet/Projet.h"

// -------------------------------------------
// namespace Projet::Serveur::TraitementPacket
// -------------------------------------------

/* Notifier deconnexion joueur (le cache doit etre protege) */
NBOOL Projet_Serveur_TraitementPacket_NotifierDeconnexionJoueur( BServeur *this,
	NClientServeur *client )
{
	// Donnees
	struct BPacketServeurClientDeconnexionJoueur data;

	// Packet
	NPacket *packet;

	// Referencer
	NREFERENCER( this );

	// Notifier aux autres clients
		// Composer
			data.m_identifiant = NLib_Module_Reseau_Serveur_NClientServeur_ObtenirIdentifiant( client );
		// Creer packet
			if( !( packet = Projet_Commun_Reseau_Packet_Creer( BTYPE_PACKET_SERVEUR_CLIENT_DECONNEXION_JOUEUR,
				&data ) ) )
			{
				// Notifier
				NOTIFIER_ERREUR( NERREUR_CONSTRUCTOR_FAILED );

				// Quitter
				return NFALSE;
			}
		// Ajouter packet
			if( !Projet_Serveur_BServeur_EnvoyerPacketTousNoLock( (BServeur*)NLib_Module_Reseau_Serveur_NClientServeur_ObtenirDonneeExterne( client ),
				packet ) )
			{
				// Notifier
				NOTIFIER_ERREUR( NERREUR_STREAM_ERROR );

				// Quitter
				return NFALSE;
			}

	// OK
	return NTRUE;
}

/* Notifier un joueur de son identifiant */
NBOOL Projet_Serveur_TraitementPacket_NotifierIdentifiantJoueur( BServeur *this,
	NClientServeur *client )
{
	// Donnees
	struct BPacketServeurClientConnexionTransmetIdentifiant data;

	// Packet
	NPacket *packet;

	// Referencer
	NREFERENCER( this );

	// Envoyer l'identifiant au client
		// Composer
			data.m_identifiant = NLib_Module_Reseau_Serveur_NClientServeur_ObtenirIdentifiant( client );
		// Creer packet
			if( !( packet = Projet_Commun_Reseau_Packet_Creer( BTYPE_PACKET_SERVEUR_CLIENT_CONNEXION_TRANSMET_IDENTIFIANT,
				&data ) ) )
			{
				// Notifier
				NOTIFIER_ERREUR( NERREUR_CONSTRUCTOR_FAILED );

				// Quitter
				return NFALSE;
			}
		// Ajouter le packet
			if( !NLib_Module_Reseau_Serveur_NClientServeur_AjouterPacket( client,
				packet ) )
			{
				// Notifier
				NOTIFIER_ERREUR( NERREUR_CONSTRUCTOR_FAILED );

				// Quitter
				return NFALSE;
			}

	// OK
	return NTRUE;
}

/* Notifier un joueur des joueurs deja presents (le cache doit etre protege) */
NBOOL Projet_Serveur_TraitementPacket_NotifierJoueurConnecte( BServeur *this,
	NClientServeur *client )
{
	// Iterateur
	NU32 i;

	// Joueur
	const BEtatClient *joueur;

	// Donnees packet
	struct BPacketServeurClientConnexionTransmetIdentifiant data;
	struct BPacketServeurClientReponseInformationsJoueur data2;
	struct BPacketServeurClientDiffuseEtatPret data3;

	// Packet
	NPacket *packet;

	// Diffuser
	for( i = 0; i < Projet_Commun_Reseau_Client_Cache_BCacheClient_ObtenirNombreClient( this->m_cacheClient ); i++ )
	{
		// Obtenir joueur
		if( !( joueur = Projet_Commun_Reseau_Client_Cache_BCacheClient_ObtenirClient2( this->m_cacheClient,
				i ) )
			|| Projet_Commun_Reseau_Client_BEtatClient_ObtenirIdentifiant( joueur ) == NLib_Module_Reseau_Serveur_NClientServeur_ObtenirIdentifiant( client )
			|| !Projet_Commun_Reseau_Client_BEtatClient_EstVerifie( joueur )
			|| !Projet_Commun_Reseau_Client_BEtatClient_ObtenirNom( joueur ) )
			continue;

		// Composer le packet identifiant
		data.m_identifiant = Projet_Commun_Reseau_Client_BEtatClient_ObtenirIdentifiant( joueur );

		// Creer le packet
		if( !( packet = Projet_Commun_Reseau_Packet_Creer( BTYPE_PACKET_SERVEUR_CLIENT_CONNEXION_TRANSMET_IDENTIFIANT,
			&data ) ) )
		{
			// Notifier
			NOTIFIER_ERREUR( NERREUR_CONSTRUCTOR_FAILED );

			// Quitter
			return NFALSE;
		}

		// Ajouter le packet
		if( !NLib_Module_Reseau_Serveur_NClientServeur_AjouterPacket( client,
			packet ) )
		{
			// Notifier
			NOTIFIER_ERREUR( NERREUR_STREAM_ERROR );

			// Quitter
			return NFALSE;
		}

		// Composer le packet details
			// Allouer
				if( !( data2.m_nom = calloc( strlen( Projet_Commun_Reseau_Client_BEtatClient_ObtenirNom( joueur ) ) + 1,
					sizeof( char ) ) ) )
				{
					// Notifier
					NOTIFIER_ERREUR( NERREUR_ALLOCATION_FAILED );

					// Quitter
					return NFALSE;
				}
			// Copier
				// Nom
					memcpy( data2.m_nom,
						Projet_Commun_Reseau_Client_BEtatClient_ObtenirNom( joueur ),
						strlen( Projet_Commun_Reseau_Client_BEtatClient_ObtenirNom( joueur ) ) );
				// Charset
					data2.m_charset = Projet_Commun_Reseau_Client_BEtatClient_ObtenirCharset( joueur );
				// Couleur charset
					data2.m_couleurCharset = Projet_Commun_Reseau_Client_BEtatClient_ObtenirCouleurCharset( joueur );
				// Identifiant
					data2.m_identifiant = Projet_Commun_Reseau_Client_BEtatClient_ObtenirIdentifiant( joueur );

		// Creer le packet
		if( !( packet = Projet_Commun_Reseau_Packet_Creer( BTYPE_PACKET_SERVEUR_CLIENT_REPONSE_INFORMATIONS_JOUEUR,
			&data2 ) ) )
		{
			// Notifier
			NOTIFIER_ERREUR( NERREUR_CONSTRUCTOR_FAILED );

			// Quitter
			return NFALSE;
		}

		// Ajouter le packet
		if( !NLib_Module_Reseau_Serveur_NClientServeur_AjouterPacket( client,
			packet ) )
		{
			// Notifier
			NOTIFIER_ERREUR( NERREUR_STREAM_ERROR );

			// Quitter
			return NFALSE;
		}

		// Composer le packet etat pret
		data3.m_identifiant = Projet_Commun_Reseau_Client_BEtatClient_ObtenirIdentifiant( joueur );
		data3.m_etat = Projet_Commun_Reseau_Client_BEtatClient_EstPret( joueur );

		// Creer le packet
		if( !( packet = Projet_Commun_Reseau_Packet_Creer( BTYPE_PACKET_SERVEUR_CLIENT_DIFFUSE_ETAT_PRET,
			&data3 ) ) )
		{
			// Notifier
			NOTIFIER_ERREUR( NERREUR_CONSTRUCTOR_FAILED );

			// Quitter
			return NFALSE;
		}

		// Envoyer le packet
		if( !NLib_Module_Reseau_Serveur_NClientServeur_AjouterPacket( client,
			packet ) )
		{
			// Notifier
			NOTIFIER_ERREUR( NERREUR_STREAM_ERROR );

			// Quitter
			return NFALSE;
		}
	}

	// OK
	return NTRUE;
}

/* Notifier un joueur de l'etat actuel du serveur */
NBOOL Projet_Serveur_TraitementPacket_NotifierEtatServeur( BServeur *this,
	NClientServeur *client )
{
	// Donnees
	struct BPacketServeurClientDiffuseChangementCarte data;

	// Packet
	NPacket *packet;

	// Composer
	data.m_identifiant = Projet_Commun_Monde_BConfigurationMonde_ObtenirIdentifiantCarte( Projet_Serveur_BServeur_ObtenirConfigurationMonde( this ) );

	// Creer
	if( !( packet = Projet_Commun_Reseau_Packet_Creer( BTYPE_PACKET_SERVEUR_CLIENT_DIFFUSE_CHANGEMENT_CARTE,
		&data ) ) )
	{
		// Notifier
		NOTIFIER_ERREUR( NERREUR_CONSTRUCTOR_FAILED );

		// Quitter
		return NFALSE;
	}

	// Envoyer
	if( !NLib_Module_Reseau_Serveur_NClientServeur_AjouterPacket( client,
		packet ) )
	{
		// Notifier
		NOTIFIER_ERREUR( NERREUR_STREAM_ERROR );

		// Quitter
		return NFALSE;
	}

	// OK
	return NTRUE;
}

/* Notifier un joueur de son kick pour CRC incorrect */
NBOOL Projet_Serveur_TraitementPacket_NotifierJoueurKickCRC( BServeur *this,
	NClientServeur *client )
{
	// Donnees
	struct BPacketServeurClientChecksumIncorrect data;

	// Packet
	NPacket *packet;

	// Referener
	NREFERENCER( this );

	// Composer
	data.m_identifiant = NLib_Module_Reseau_Serveur_NClientServeur_ObtenirIdentifiant( client );

	// Creer le packet
	if( !( packet = Projet_Commun_Reseau_Packet_Creer( BTYPE_PACKET_SERVEUR_CLIENT_CHECKSUM_INCORRECT,
		&data ) ) )
	{
		// Notifier
		NOTIFIER_ERREUR( NERREUR_CONSTRUCTOR_FAILED );

		// Quitter
		return NFALSE;
	}

	// Envoyer le packet
	if( !NLib_Module_Reseau_Serveur_NClientServeur_AjouterPacket( client,
		packet ) )
	{
		// Notifier
		NOTIFIER_ERREUR( NERREUR_STREAM_ERROR );

		// Quitter
		return NFALSE;
	}

	// OK
	return NTRUE;
}

/* Effectuer requete ping */
NBOOL Projet_Serveur_TraitementPacket_EnvoyerRequetePing( BServeur *this,
	NClientServeur *client )
{
	// Donnees
	struct BPacketServeurClientPing data;

	// Packet
	NPacket *packet;

	// Referencer
	NREFERENCER( this );

	// Composer packet
	data.m_identifiant = NLib_Module_Reseau_Serveur_NClientServeur_ObtenirIdentifiant( client );

	// Creer packet
	if( !( packet = Projet_Commun_Reseau_Packet_Creer( BTYPE_PACKET_SERVEUR_CLIENT_PING,
		&data ) ) )
	{
		// Notifier
		NOTIFIER_ERREUR( NERREUR_CONSTRUCTOR_FAILED );

		// Quitter
		return NFALSE;
	}

	// Envoyer packet
	if( !NLib_Module_Reseau_Serveur_NClientServeur_AjouterPacket( client,
		packet ) )
	{
		// Notifier
		NOTIFIER_ERREUR( NERREUR_STREAM_ERROR );

		// Quitter
		return NFALSE;
	}

	// OK
	return NTRUE;
}

/* Notifier changement de carte */
NBOOL Projet_Serveur_TraitementPacket_NotifierChangementCarte( BServeur *this,
	NU32 carte )
{
	// Donnees
	struct BPacketServeurClientDiffuseChangementCarte data;

	// Packet
	NPacket *packet;

	// Composer packet
	data.m_identifiant = carte;

	// Creer packet
	if( !( packet = Projet_Commun_Reseau_Packet_Creer( BTYPE_PACKET_SERVEUR_CLIENT_DIFFUSE_CHANGEMENT_CARTE,
		&data ) ) )
	{
		// Notifier
		NOTIFIER_ERREUR( NERREUR_CONSTRUCTOR_FAILED );

		// Quitter
		return NFALSE;
	}

	// Envoyer packet
	if( !Projet_Commun_Reseau_Client_Cache_BCacheClient_EnvoyerPacketDepuisServeurTous( this->m_cacheClient,
		packet ) )
	{
		// Notifier
		NOTIFIER_ERREUR( NERREUR_STREAM_ERROR );

		// Quitter
		return NFALSE;
	}

	return NTRUE;
}

/* Notifier lancement partie */
NBOOL Projet_Serveur_TraitementPacket_NotifierLancementPartie( BServeur *this )
{
	// Donnees
	struct BPacketServeurClientDiffuseLancement data;

	// Carte
	const BCarte *carte;

	// Taille carte
	const NUPoint *tailleCarte;

	// Cases
	const BCaseEtatCarte **cases;

	// Joueur
	const BEtatClient *joueur;

	// Packet
	NPacket *packet;

	// Iterateurs
	NU32 i,
		j;

	// Obtenir la carte
	if( !( carte = Projet_Commun_Monde_BMonde_ObtenirCarte( this->m_monde ) )
		|| !( tailleCarte = Projet_Commun_Carte_BCarte_ObtenirTaille( carte ) )
		|| !( cases = Projet_Commun_Monde_BMonde_ObtenirCaseEtatCarte( this->m_monde ) ) )
	{
		// Notifier
		NOTIFIER_ERREUR( NERREUR_OBJECT_STATE );

		// Quitter
		return NFALSE;
	}

	// Allouer la memoire
		// 1D
			if( !( data.m_estCaseRemplie = calloc( tailleCarte->x,
				sizeof( NBOOL* ) ) ) )
			{
				// Notifier
				NOTIFIER_ERREUR( NERREUR_ALLOCATION_FAILED );

				// Quitter
				return NFALSE;
			}
		// 2D
			for( i = 0; i < tailleCarte->x; i++ )
				if( !( data.m_estCaseRemplie[ i ] = calloc( tailleCarte->y,
					sizeof( NBOOL ) ) ) )
				{
					// Notifier
					NOTIFIER_ERREUR( NERREUR_ALLOCATION_FAILED );

					// Liberer
					for( j = 0; j < i; j++ )
						NFREE( data.m_estCaseRemplie[ j ] );
					NFREE( data.m_estCaseRemplie );

					// Quitter
					return NFALSE;
				}

	// Copier cases remplie
	for( i = 0; i < tailleCarte->x; i++ )
		for( j = 0; j < tailleCarte->y; j++ )
			data.m_estCaseRemplie[ i ][ j ] = cases[ i ][ j ].m_estRempli;

	// Allouer la memoire
	if( !( data.m_identifiantJoueur = calloc( Projet_Commun_Reseau_Client_Cache_BCacheClient_ObtenirNombreClient( this->m_cacheClient ),
			sizeof( NU32 ) ) )
		|| !( data.m_positionJoueur = calloc( Projet_Commun_Reseau_Client_Cache_BCacheClient_ObtenirNombreClient( this->m_cacheClient ),
			sizeof( NSPoint ) ) ) )
	{
		// Notifier
		NOTIFIER_ERREUR( NERREUR_ALLOCATION_FAILED );

		// Liberer
		NFREE( data.m_identifiantJoueur );
		for( i = 0; i < tailleCarte->x; i++ )
			NFREE( data.m_estCaseRemplie[ i ] );
		NFREE( data.m_estCaseRemplie );

		// Quitter
		return NFALSE;
	}

	// Copier informations joueurs
	for( i = 0; i < Projet_Commun_Reseau_Client_Cache_BCacheClient_ObtenirNombreClient( this->m_cacheClient ); i++ )
	{
		// Obtenir joueur
		if( !( joueur = Projet_Commun_Reseau_Client_Cache_BCacheClient_ObtenirClient2( this->m_cacheClient,
			i ) ) )
		{
			// Notifier
			NOTIFIER_ERREUR( NERREUR_OBJECT_STATE );

			// Liberer
			NFREE( data.m_positionJoueur );
			NFREE( data.m_identifiantJoueur );
			for( i = 0; i < tailleCarte->x; i++ )
				NFREE( data.m_estCaseRemplie[ i ] );
			NFREE( data.m_estCaseRemplie );

			// Quitter
			return NFALSE;
		}

		// Recuperer
		data.m_identifiantJoueur[ i ] = Projet_Commun_Reseau_Client_BEtatClient_ObtenirIdentifiant( joueur );
		data.m_positionJoueur[ i ] = *Projet_Commun_Reseau_Client_BEtatClient_ObtenirPosition( joueur );
	}

	// Enregistrer
	data.m_identifiantCarte = Projet_Commun_Monde_BConfigurationMonde_ObtenirIdentifiantCarte( this->m_configurationMonde );
	data.m_nombreJoueur = Projet_Commun_Reseau_Client_Cache_BCacheClient_ObtenirNombreClient( this->m_cacheClient );
	data.m_tailleCarte = *tailleCarte;

	// Creer le packet
	if( !( packet = Projet_Commun_Reseau_Packet_Creer( BTYPE_PACKET_SERVEUR_CLIENT_DIFFUSE_LANCEMENT,
		&data ) ) )
	{
		// Notifier
		NOTIFIER_ERREUR( NERREUR_CONSTRUCTOR_FAILED );

		// Quitter
		return NFALSE;
	}

	// Envoyer le packet
	if( !Projet_Commun_Reseau_Client_Cache_BCacheClient_EnvoyerPacketDepuisServeurTousNoLock( this->m_cacheClient,
		packet ) )
	{
		// Notifier
		NOTIFIER_ERREUR( NERREUR_STREAM_ERROR );

		// Quitter
		return NFALSE;
	}

	// OK
	return NTRUE;
}

/* Notifier message (privee) */
__ALLOC NPacket *Projet_Serveur_TraitementPacket_CreerPacketMessageClientInterne( const char *message,
	BPoliceMessageClient police,
	NCouleur couleur,
	NU32 dureeAffichage,
	BPoliceMessageClient position,
	NBOOL estDoitAfficherCadre,
	NCouleur couleurCadre )
{
	// Sortie
	__OUTPUT NPacket *packet;

	// Donnee
	struct BPacketServeurClientDiffuseMessageAfficher data;

	// Composer packet
		// Taille message
			data.m_tailleMessage = strlen( message );
		// Allouer la memoire
			if( !( data.m_message = calloc( data.m_tailleMessage,
				sizeof( char ) ) ) )
			{
				// Notifier
				NOTIFIER_ERREUR( NERREUR_ALLOCATION_FAILED );

				// Quitter
				return NULL;
			}
		// Copier
			memcpy( data.m_message,
				message,
				data.m_tailleMessage );
			data.m_couleur = couleur;
			data.m_dureeAffichage = dureeAffichage;
			data.m_police = police;
			data.m_estDoitAfficherCadre = estDoitAfficherCadre;
			data.m_position = position;
			data.m_couleurCadre = couleurCadre;

	// Construire le packet
	if( !( packet = Projet_Commun_Reseau_Packet_Creer( BTYPE_PACKET_SERVEUR_CLIENT_DIFFUSE_MESSAGE_AFFICHER,
		&data ) ) )
	{
		// Notifier
		NOTIFIER_ERREUR( NERREUR_CONSTRUCTOR_FAILED );

		// Liberer
		NFREE( data.m_message );

		// Quitter
		return NULL;
	}

	// OK
	return packet;
}

NBOOL Projet_Serveur_TraitementPacket_NotifierMessageClientUnique( NClientServeur *client,
	const char *message,
	BPoliceMessageClient police,
	NCouleur couleur,
	NU32 dureeAffichage,
	BPositionAffichageMessageClient position,
	NBOOL estDoitAfficherCadre,
	NCouleur couleurCadre )
{
	// Packet
	NPacket *packet;

	// Creer le packet
	if( !( packet = Projet_Serveur_TraitementPacket_CreerPacketMessageClientInterne( message,
		police,
		couleur,
		dureeAffichage,
		position,
		estDoitAfficherCadre,
		couleurCadre ) ) )
	{
		// Notifier
		NOTIFIER_ERREUR( NERREUR_CONSTRUCTOR_FAILED );

		// Quitter
		return NFALSE;
	}

	// Envoyer
	return NLib_Module_Reseau_Serveur_NClientServeur_AjouterPacket( client,
		packet );
}

NBOOL Projet_Serveur_TraitementPacket_NotifierMessageClientTous( BServeur *this,
	const char *message,
	BPoliceMessageClient police,
	NCouleur couleur,
	NU32 dureeAffichage,
	BPositionAffichageMessageClient position,
	NBOOL estDoitAfficherCadre,
	NCouleur couleurCadre )
{
	// Packet
	NPacket *packet;

	// Creer le packet
	if( !( packet = Projet_Serveur_TraitementPacket_CreerPacketMessageClientInterne( message,
		police,
		couleur,
		dureeAffichage,
		position,
		estDoitAfficherCadre,
		couleurCadre ) ) )
	{
		// Notifier
		NOTIFIER_ERREUR( NERREUR_CONSTRUCTOR_FAILED );

		// Quitter
		return NFALSE;
	}

	// Envoyer
	return Projet_Commun_Reseau_Client_Cache_BCacheClient_EnvoyerPacketDepuisServeurTousNoLock( this->m_cacheClient,
		packet );
}

/* Notifier le debut de la partie */
NBOOL Projet_Serveur_TraitementPacket_NotifierDebutPartie( BServeur *this )
{
	// Packet
	NPacket *packet;

	// Donnees
	struct BPacketServeurClientDiffuseDebutPartie data;

	// Construire le packet
	if( !( packet = Projet_Commun_Reseau_Packet_Creer( BTYPE_PACKET_SERVEUR_CLIENT_DIFFUSE_DEBUT_PARTIE,
		&data ) ) )
	{
		// Notifier
		NOTIFIER_ERREUR( NERREUR_CONSTRUCTOR_FAILED );

		// Quitter
		return NFALSE;
	}

	// Envoyer a tous les clients
	return Projet_Commun_Reseau_Client_Cache_BCacheClient_EnvoyerPacketDepuisServeurTousNoLock( this->m_cacheClient,
		packet );
}

/* Notifier un changement de direction de la part d'un joueur */
NBOOL Projet_Serveur_TraitementPacket_NotifierJoueurChangeDirection( BServeur *this,
	const NClientServeur *client,
	NDirection direction )
{
	// Packet
	NPacket *packet;

	// Donnee
	struct BPacketServeurClientDiffuseChangementDirection data;

	// Composer packet
	data.m_direction = direction;
	data.m_identifiant = NLib_Module_Reseau_Serveur_NClientServeur_ObtenirIdentifiant( client );

	// Creer packet
	if( !( packet = Projet_Commun_Reseau_Packet_Creer( BTYPE_PACKET_SERVEUR_CLIENT_DIFFUSE_CHANGEMENT_DIRECTION,
		&data ) ) )
	{
		// Notifier
		NOTIFIER_ERREUR( NERREUR_CONSTRUCTOR_FAILED );

		// Quitter
		return NFALSE;
	}

	// Envoyer packet
	if( !Projet_Commun_Reseau_Client_Cache_BCacheClient_EnvoyerPacketDepuisServeurTous( this->m_cacheClient,
		packet ) )
	{
		// Notifier
		NOTIFIER_ERREUR( NERREUR_STREAM_ERROR );

		// Quitter
		return NFALSE;
	}

	// OK
	return NTRUE;
}

/* Notifier un changement de position de la part d'un joueur */
NBOOL Projet_Serveur_TraitementPacket_NotifierJoueurChangePosition( BServeur *this,
	const NClientServeur *client,
	NSPoint nouvellePosition,
	NDirection direction )
{
	// Packet
	NPacket *packet;

	// Donnee
	struct BPacketServeurClientDiffuseChangementPosition data;

	// Composer packet
	data.m_direction = direction;
	data.m_nouvellePosition = nouvellePosition;
	data.m_identifiant = NLib_Module_Reseau_Serveur_NClientServeur_ObtenirIdentifiant( client );

	// Creer packet
	if( !( packet = Projet_Commun_Reseau_Packet_Creer( BTYPE_PACKET_SERVEUR_CLIENT_DIFFUSE_CHANGEMENT_POSITION,
		&data ) ) )
	{
		// Notifier
		NOTIFIER_ERREUR( NERREUR_CONSTRUCTOR_FAILED );

		// Quitter
		return NFALSE;
	}

	// Envoyer packet
	if( !Projet_Commun_Reseau_Client_Cache_BCacheClient_EnvoyerPacketDepuisServeurTousNoLock( this->m_cacheClient,
		packet ) )
	{
		// Notifier
		NOTIFIER_ERREUR( NERREUR_STREAM_ERROR );

		// Quitter
		return NFALSE;
	}

	// OK
	return NTRUE;
}

/* Notifier un refus de pose de bombe */
NBOOL Projet_Serveur_TraitementPacket_NotifierRefusPoseBombe( const NClientServeur *client )
{
	// Packet
	NPacket *packet;

	// Donnees
	struct BPacketServeurClientRefusePoseBombe data;

	// Composer
	data.m_identifiant = NLib_Module_Reseau_Serveur_NClientServeur_ObtenirIdentifiant( client );

	// Creer le packet
	if( !( packet = Projet_Commun_Reseau_Packet_Creer( BTYPE_PACKET_SERVEUR_CLIENT_REFUSE_POSE_BOMBE,
		&data ) ) )
	{
		// Notifier
		NOTIFIER_ERREUR( NERREUR_CONSTRUCTOR_FAILED );

		// Quitter
		return NFALSE;
	}

	// Envoyer le packet
	if( !NLib_Module_Reseau_Serveur_NClientServeur_AjouterPacket( (NClientServeur*)client,
		packet ) )
	{
		// Notifier
		NOTIFIER_ERREUR( NERREUR_STREAM_ERROR );

		// Quitter
		return NFALSE;
	}

	// OK
	return NTRUE;
}

/* Notifier une pose de bombe */
NBOOL Projet_Serveur_TraitementPacket_NotifierPoseBombe( BServeur *this,
	const NClientServeur *client,
	const NSPoint *position,
	NU32 identifiantBombe )
{
	// Packet
	NPacket *packet;

	// Donnees
	struct BPacketServeurClientDiffusePoseBombe data;

	// Composer le packet
	data.m_identifiantBombe = identifiantBombe;
	data.m_identifiantJoueur = NLib_Module_Reseau_Serveur_NClientServeur_ObtenirIdentifiant( client );
	data.m_position = *position;

	// Creer le packet
	if( !( packet = Projet_Commun_Reseau_Packet_Creer( BTYPE_PACKET_SERVEUR_CLIENT_DIFFUSE_POSE_BOMBE,
		&data ) ) )
	{
		// Notifier
		NOTIFIER_ERREUR( NERREUR_CONSTRUCTOR_FAILED );

		// Quitter
		return NFALSE;
	}

	// Diffuser le packet
	if( !Projet_Serveur_BServeur_EnvoyerPacketTousNoLock( this,
		packet ) )
	{
		// Notifier
		NOTIFIER_ERREUR( NERREUR_STREAM_ERROR );

		// Quitter
		return NFALSE;
	}

	// OK
	return NTRUE;
}

/* Notifier une explosion de bombe */
NBOOL Projet_Serveur_TraitementPacket_NotifierExplosionBombe( BServeur *this,
	NU32 identifiantBombe,
	NU32 puissance,
	NU32 identifiantJoueur )
{
	// Packet
	NPacket *packet;
	
	// Donnees
	struct BPacketServeurClientBombeExplose data;
	
	// Composer packet
	data.m_identifiant = identifiantBombe;
	data.m_puissance = puissance;
	data.m_identifiantJoueur = identifiantJoueur;
	
	// Creer le packet
	if( !( packet = Projet_Commun_Reseau_Packet_Creer( BTYPE_PACKET_SERVEUR_CLIENT_BOMBE_EXPLOSE,
		&data ) ) )
	{
		// Notifier
		NOTIFIER_ERREUR( NERREUR_CONSTRUCTOR_FAILED );

		// Quitter
		return NFALSE;
	}

	// Envoyer le packet
	if( !Projet_Serveur_BServeur_EnvoyerPacketTousNoLock( this,
		packet ) )
	{
		// Notifier
		NOTIFIER_ERREUR( NERREUR_STREAM_ERROR );

		// Quitter
		return NFALSE;
	}

	// OK
	return NTRUE;
}

/* Notifier destruction de bloc rempli */
NBOOL Projet_Serveur_TraitementPacket_NotifierDestructionBlocRempli( BServeur *this,
	NS32 x,
	NS32 y )
{
	// Packet
	NPacket *packet;

	// Donnees
	struct BPacketServeurClientBlocRempliDetruit data;

	// Composer
	NDEFINIR_POSITION( data.m_position,
		x,
		y );

	// Creer le packet
	if( !( packet = Projet_Commun_Reseau_Packet_Creer( BTYPE_PACKET_SERVEUR_CLIENT_BLOC_REMPLI_DETRUIT,
		&data ) ) )
	{
		// Notifier
		NOTIFIER_ERREUR( NERREUR_CONSTRUCTOR_FAILED );

		// Quitter
		return NFALSE;
	}

	// Envoyer le packet
	if( !Projet_Serveur_BServeur_EnvoyerPacketTousNoLock( this,
		packet ) )
	{
		// Notifier
		NOTIFIER_ERREUR( NERREUR_STREAM_ERROR );

		// Quitter
		return NFALSE;
	}

	// OK
	return NTRUE;
}

/* Notifier une apparition de bonus */
NBOOL Projet_Serveur_TraitementPacket_NotifierApparitionBonus( BServeur *this,
	NSPoint position,
	BListeBonus type,
	NU32 dureeBonus,
	NU32 identifiantBonus )
{
	// Packet
	NPacket *packet;

	// Donnees
	struct BPacketServeurClientDiffuseApparitionBonus data;

	// Composer
	data.m_duree = dureeBonus;
	data.m_identifiant = identifiantBonus;
	data.m_position = position;
	data.m_type = type;

	// Creer le packet
	if( !( packet = Projet_Commun_Reseau_Packet_Creer( BTYPE_PACKET_SERVEUR_CLIENT_DIFFUSE_APPARITION_BONUS,
		&data ) ) )
	{
		// Notifier
		NOTIFIER_ERREUR( NERREUR_CONSTRUCTOR_FAILED );

		// Quitter
		return NFALSE;
	}

	// Envoyer le packet
	if( !Projet_Serveur_BServeur_EnvoyerPacketTousNoLock( this,
		packet ) )
	{
		// Notifier
		NOTIFIER_ERREUR( NERREUR_STREAM_ERROR );

		// Quitter
		return NFALSE;
	}

	// OK
	return NTRUE;
}

/* Notifier une disparition de bonus */
NBOOL Projet_Serveur_TraitementPacket_NotifierDisparitionBonus( BServeur *this,
	NU32 identifiantBonus )
{
	// Packet
	NPacket *packet;

	// Donnees
	struct BPacketServeurClientDiffuseDisparitionBonus data;

	// Composer
	data.m_identifiant = identifiantBonus;

	// Creer
	if( !( packet = Projet_Commun_Reseau_Packet_Creer( BTYPE_PACKET_SERVEUR_CLIENT_DIFFUSE_DISPARITION_BONUS,
		&data ) ) )
	{
		// Notifier
		NOTIFIER_ERREUR( NERREUR_CONSTRUCTOR_FAILED );

		// Quitter
		return NFALSE;
	}

	// Envoyer
	if( !Projet_Serveur_BServeur_EnvoyerPacketTousNoLock( this,
		packet ) )
	{
		// Notifier
		NOTIFIER_ERREUR( NERREUR_STREAM_ERROR );

		// Quitter
		return NFALSE;
	}

	// OK
	return NTRUE;
}

/* Notifier une prise de bonus */
NBOOL Projet_Serveur_TraitementPacket_NotifierPriseBonus( const NClientServeur *client,
	NU32 typeBonus )
{
	// Packet
	NPacket *packet;

	// Donnees
	struct BPacketServeurClientDistribueBonus data;

	// Composer
	data.m_typeBonus = typeBonus;
	data.m_identifiant = NLib_Module_Reseau_Serveur_NClientServeur_ObtenirIdentifiant( client );

	// Creer
	if( !( packet = Projet_Commun_Reseau_Packet_Creer( BTYPE_PACKET_SERVEUR_CLIENT_DISTRIBUE_BONUS,
		&data ) ) )
	{
		// Notifier
		NOTIFIER_ERREUR( NERREUR_CONSTRUCTOR_FAILED );

		// Quitter
		return NFALSE;
	}

	// Envoyer
	if( !NLib_Module_Reseau_Serveur_NClientServeur_AjouterPacket( (NClientServeur*)client,
		packet ) )
	{
		// Notifier
		NOTIFIER_ERREUR( NERREUR_STREAM_ERROR );

		// Quitter
		return NFALSE;
	}

	// OK
	return NTRUE;
}

/* Notifier la mort d'un joueur */
NBOOL Projet_Serveur_TraitementPacket_NotifierMortJoueur( BServeur *this,
	NU32 identifiant )
{
	// Packet
	NPacket *packet;

	// Donnees
	struct BPacketServeurClientDiffuseMortJoueur data;

	// Composer
	data.m_identifiantJoueur = identifiant;

	// Creer
	if( !( packet = Projet_Commun_Reseau_Packet_Creer( BTYPE_PACKET_SERVEUR_CLIENT_DIFFUSE_MORT_JOUEUR,
		&data ) ) )
	{
		// Notifier
		NOTIFIER_ERREUR( NERREUR_CONSTRUCTOR_FAILED );

		// Quitter
		return NFALSE;
	}

	// Envoyer
	if( !Projet_Serveur_BServeur_EnvoyerPacketTousNoLock( this,
		packet ) )
	{
		// Notifier
		NOTIFIER_ERREUR( NERREUR_STREAM_ERROR );

		// Quitter
		return NFALSE;
	}

	// OK
	return NTRUE;
}

/* Notifier la fin de la partie */
NBOOL Projet_Serveur_TraitementPacket_NotifierFinPartie( BServeur *this,
	BTypeFinPartie type,
	NU32 identifiant )
{
	// Packet
	NPacket *packet;

	// Donnees
	struct BPacketServeurClientAnnonceFinPartie data;

	// Composer
	data.m_identifiant = identifiant;
	data.m_type = type;

	// Creer le packet
	if( !( packet = Projet_Commun_Reseau_Packet_Creer( BTYPE_PACKET_SERVEUR_CLIENT_ANNONCE_FIN_PARTIE,
		&data ) ) )
	{
		// Notifier
		NOTIFIER_ERREUR( NERREUR_CONSTRUCTOR_FAILED );

		// Quitter
		return NFALSE;
	}

	// Envoyer
	if( !Projet_Serveur_BServeur_EnvoyerPacketTousNoLock( this,
		packet ) )
	{
		// Notifier
		NOTIFIER_ERREUR( NERREUR_STREAM_ERROR );

		// Quitter
		return NFALSE;
	}

	// OK
	return NTRUE;
}

// BTYPE_PACKET_CLIENT_SERVEUR_TRANSMET_INFORMATIONS_JOUEUR
NBOOL Projet_Serveur_TraitementPacket_TraiterTransmetInformationsJoueur( BServeur *this,
	const NClientServeur *client,
	const struct BPacketClientServeurTransmetInformationsJoueur *data )
{
	// Joueur
	const BEtatClient *joueur;

	// Donnees packet
	struct BPacketServeurClientReponseInformationsJoueur data2;

	// Packet
	NPacket *packet;
	
	// Buffer
	char buffer[ 2048 ];

	// Traiter
	if( !Projet_Commun_Reseau_Client_Cache_BCacheClient_EnregistrerDemandePersonnalisation( this->m_cacheClient,
		NLib_Module_Reseau_Serveur_NClientServeur_ObtenirIdentifiant( client ),
		data->m_charset,
		data->m_couleurCharset,
		data->m_nom,
		Projet_Serveur_BServeur_ObtenirRessource( this ) ) )
	{
		// Notifier
		NOTIFIER_ERREUR( NERREUR_STREAM_ERROR );

		// Quitter
		return NFALSE;
	}

	// Proteger le cache
	Projet_Commun_Reseau_Client_Cache_BCacheClient_ProtegerCache( this->m_cacheClient );

	// Obtenir le client
	if( !( joueur = Projet_Commun_Reseau_Client_Cache_BCacheClient_ObtenirClient( this->m_cacheClient,
		NLib_Module_Reseau_Serveur_NClientServeur_ObtenirIdentifiant( client ) ) ) )
	{
		// Notifier
		NOTIFIER_ERREUR( NERREUR_STREAM_ERROR );

		// Ne plus proteger
		Projet_Commun_Reseau_Client_Cache_BCacheClient_NePlusProtegerCache( this->m_cacheClient );

		// Quitter
		return NFALSE;
	}

	// Composer reponse
		// Allouer la memoire
			if( !( data2.m_nom = calloc( strlen( Projet_Commun_Reseau_Client_BEtatClient_ObtenirNom( joueur ) ) + 1,
				sizeof( char ) ) ) )
			{
				// Notifier
				NOTIFIER_ERREUR( NERREUR_ALLOCATION_FAILED );
				
				// Ne plus proteger
				Projet_Commun_Reseau_Client_Cache_BCacheClient_NePlusProtegerCache( this->m_cacheClient );

				// Quitter
				return NFALSE;
			}
		// Copier
			data2.m_charset = Projet_Commun_Reseau_Client_BEtatClient_ObtenirCharset( joueur );
			data2.m_couleurCharset = Projet_Commun_Reseau_Client_BEtatClient_ObtenirCouleurCharset( joueur );
			data2.m_identifiant = NLib_Module_Reseau_Serveur_NClientServeur_ObtenirIdentifiant( client );
			memcpy( data2.m_nom,
				Projet_Commun_Reseau_Client_BEtatClient_ObtenirNom( joueur ),
				strlen( Projet_Commun_Reseau_Client_BEtatClient_ObtenirNom( joueur ) ) );

	// Notifier
		// Composer message
			if( data->m_charset != data2.m_charset
				|| data->m_couleurCharset != data2.m_couleurCharset
				|| strcmp( data->m_nom,
					data2.m_nom ) )
				sprintf( buffer,
					"[SERVEUR] Le client %d change de details (REFUSE) [voulait(%d/%d/\"%s\"), aura(%d/%d/\"%s\").\n",
					NLib_Module_Reseau_Serveur_NClientServeur_ObtenirIdentifiant( client ),
					data->m_charset,
					data->m_couleurCharset,
					data->m_nom,
					data2.m_charset,
					data2.m_couleurCharset,
					data2.m_nom );
			else
				sprintf( buffer,
					"[SERVEUR] Le client %d change de details (%d/%d/\"%s\").\n",
					NLib_Module_Reseau_Serveur_NClientServeur_ObtenirIdentifiant( client ),
					data->m_charset,
					data->m_couleurCharset,
					data->m_nom );
		// Notifier
			NOTIFIER_AVERTISSEMENT_UTILISATEUR( NERREUR_USER,
				buffer,
				0 );

	// Creer packet reponse
	if( !( packet = Projet_Commun_Reseau_Packet_Creer( BTYPE_PACKET_SERVEUR_CLIENT_REPONSE_INFORMATIONS_JOUEUR,
		&data2 ) ) )
	{
		// Notifier
		NOTIFIER_ERREUR( NERREUR_CONSTRUCTOR_FAILED );

		// Ne plus proteger
		Projet_Commun_Reseau_Client_Cache_BCacheClient_NePlusProtegerCache( this->m_cacheClient );

		// Quitter
		return NFALSE;
	}

	// Envoyer packet
	if( !Projet_Commun_Reseau_Client_Cache_BCacheClient_EnvoyerPacketDepuisServeurTousNoLock( this->m_cacheClient,
		packet ) )
	{
		// Notifier
		NOTIFIER_ERREUR( NERREUR_STREAM_ERROR );

		// Ne plus proteger
		Projet_Commun_Reseau_Client_Cache_BCacheClient_NePlusProtegerCache( this->m_cacheClient );

		// Quitter
		return NFALSE;
	}

	// Ne plus proteger
	Projet_Commun_Reseau_Client_Cache_BCacheClient_NePlusProtegerCache( this->m_cacheClient );

	// OK
	return NTRUE;
}

// BTYPE_PACKET_CLIENT_SERVEUR_TRANSMET_CHECKSUM
NBOOL Projet_Serveur_TraitementPacket_TraiterTransmetChecksum( BServeur *this,
	const NClientServeur *client,
	const struct BPacketClientServeurTransmetChecksum *packet )
{
	// Buffer
	char buffer[ 2048 ];

	// Referencer
	NREFERENCER( client );

	// Verifier le checksum
	if( packet->m_checksum != Projet_Serveur_BServeur_ObtenirChecksumRessource( this ) )
	{
		// Notifier
		NOTIFIER_ERREUR( NERREUR_CRC );

		// Quitter
		return NFALSE;
	}

	// Notifier
		// Composer
			sprintf( buffer,
				"[SERVEUR] Le client %d a authentifie ses ressources.",
				NLib_Module_Reseau_Serveur_NClientServeur_ObtenirIdentifiant( client ) );
		// Notifier
			NOTIFIER_AVERTISSEMENT_UTILISATEUR( NERREUR_USER,
				buffer,
				0 );

	// Le client est verifie
	Projet_Commun_Reseau_Client_Cache_BCacheClient_DefinirClientVerifie( this->m_cacheClient,
		NLib_Module_Reseau_Serveur_NClientServeur_ObtenirIdentifiant( client ) );

	// OK
	return NTRUE;
}

// BTYPE_PACKET_CLIENT_SERVEUR_PING
NBOOL Projet_Serveur_TraitementPacket_TraiterPing( BServeur *this,
	const NClientServeur *client,
	const struct BPacketClientServeurTransmetChecksum *data )
{
	// Donnees reponse
	struct BPacketClientServeurPong dataReponse;

	// Packet
	NPacket *packet;

	// Referencer
	NREFERENCER( data );
	NREFERENCER( this );

	// Composer packet
	dataReponse.m_identifiant = NLib_Module_Reseau_Serveur_NClientServeur_ObtenirIdentifiant( client );

	// Creer packet
	if( !( packet = Projet_Commun_Reseau_Packet_Creer( BTYPE_PACKET_SERVEUR_CLIENT_PONG,
		&dataReponse ) ) )
	{
		// Notifier
		NOTIFIER_ERREUR( NERREUR_CONSTRUCTOR_FAILED );

		// Quitter
		return NFALSE;
	}

	// Envoyer packet
	if( !NLib_Module_Reseau_Serveur_NClientServeur_AjouterPacket( (NClientServeur*)client,
		packet ) )
	{
		// Notifier
		NOTIFIER_ERREUR( NERREUR_STREAM_ERROR );

		// Quitter
		return NFALSE;
	}

	// OK
	return NTRUE;
}

// BTYPE_PACKET_CLIENT_SERVEUR_PONG:
NBOOL Projet_Serveur_TraitementPacket_TraiterPong( BServeur *this,
	const NClientServeur *client,
	const struct BPacketClientServeurTransmetChecksum *data )
{
	// Referencer
	NREFERENCER( this );
	NREFERENCER( data );

	// Enregistrer
	if( !NLib_Module_Reseau_NPing_RecevoirReponseRequete( (NPing*)NLib_Module_Reseau_Serveur_NClientServeur_ObtenirPing( client ) ) )
	{
		// Notifier
		NOTIFIER_ERREUR( NERREUR_STREAM_ERROR );

		// Quitter
		return NFALSE;
	}

	// OK
	return NTRUE;
}

// BTYPE_PACKET_CLIENT_SERVEUR_CHANGE_ETAT_PRET
NBOOL Projet_Serveur_TraitementPacket_TraiterChangeEtatPret( BServeur *this,
	const NClientServeur *client,
	const struct BPacketClientServeurChangeEtatPret *data )
{
	// Joueur
	const BEtatClient *joueur;

	// Donnees
	struct BPacketServeurClientDiffuseEtatPret dataReponse;

	// Packet
	NPacket *packet;

	// Buffer
	char buffer[ 2048 ];

	// Referencer
	NREFERENCER( data );

	// Enregistrer modification
	if( !Projet_Commun_Reseau_Client_Cache_BCacheClient_EnregistrerChangementEtatPret( this->m_cacheClient,
		NLib_Module_Reseau_Serveur_NClientServeur_ObtenirIdentifiant( client ) ) )
	{
		// Notifier
		NOTIFIER_ERREUR( NERREUR_OBJECT_STATE );

		// Quitter
		return NFALSE;
	}

	// Proteger
	Projet_Commun_Reseau_Client_Cache_BCacheClient_ProtegerCache( this->m_cacheClient );

	// Recuperer joueur
	if( !( joueur = Projet_Commun_Reseau_Client_Cache_BCacheClient_ObtenirClient( this->m_cacheClient,
		NLib_Module_Reseau_Serveur_NClientServeur_ObtenirIdentifiant( client ) ) ) )
	{
		// Notifier
		NOTIFIER_ERREUR( NERREUR_OBJECT_STATE );

		// Ne plus proteger
		Projet_Commun_Reseau_Client_Cache_BCacheClient_NePlusProtegerCache( this->m_cacheClient );

		// Quitter
		return NFALSE;
	}

	// Composer packet
	dataReponse.m_identifiant = NLib_Module_Reseau_Serveur_NClientServeur_ObtenirIdentifiant( client );
	dataReponse.m_etat = Projet_Commun_Reseau_Client_BEtatClient_EstPret( joueur );

	// Creer packet
	if( !( packet = Projet_Commun_Reseau_Packet_Creer( BTYPE_PACKET_SERVEUR_CLIENT_DIFFUSE_ETAT_PRET,
		&dataReponse ) ) )
	{
		// Notifier
		NOTIFIER_ERREUR( NERREUR_CONSTRUCTOR_FAILED );

		// Ne plus proteger
		Projet_Commun_Reseau_Client_Cache_BCacheClient_NePlusProtegerCache( this->m_cacheClient );

		// Quitter
		return NFALSE;
	}

	// Envoyer packet
	if( !Projet_Commun_Reseau_Client_Cache_BCacheClient_EnvoyerPacketDepuisServeurTousNoLock( this->m_cacheClient,
		packet ) )
	{
		// Notifier
		NOTIFIER_ERREUR( NERREUR_STREAM_ERROR );

		// Ne plus proteger
		Projet_Commun_Reseau_Client_Cache_BCacheClient_NePlusProtegerCache( this->m_cacheClient );

		// Quitter
		return NFALSE;
	}

	// Notifier
		// Composer
			sprintf( buffer,
				"[SERVEUR] Le client %d %s.",
				NLib_Module_Reseau_Serveur_NClientServeur_ObtenirIdentifiant( client ),
					Projet_Commun_Reseau_Client_BEtatClient_EstPret( joueur ) ? "est pret" : "n'est pas pret" );
		// Notifier
			NOTIFIER_AVERTISSEMENT_UTILISATEUR( NERREUR_USER,
				buffer,
				0 );
			
	// Ne plus proteger
	Projet_Commun_Reseau_Client_Cache_BCacheClient_NePlusProtegerCache( this->m_cacheClient );

	// OK
	return NTRUE;
}

// BTYPE_PACKET_CLIENT_SERVEUR_CONFIRME_LANCEMENT
NBOOL Projet_Serveur_TraitementPacket_TraiterConfirmeLancement( BServeur *this,
	const NClientServeur *client,
	const struct BPacketClientServeurConfirmeLancement *data )
{
	// Referencer
	NREFERENCER( data );

	// Verifier etat serveur
	if( this->m_etat != BETAT_SERVEUR_ATTENTE_CONFIRMATION_LANCEMENT )
		// Kicker
		return NFALSE;
	
	// Definir
	if( !Projet_Commun_Reseau_Client_Cache_BCacheClient_EnregistrerConfirmationLancement( this->m_cacheClient,
		NLib_Module_Reseau_Serveur_NClientServeur_ObtenirIdentifiant( client ) ) )
	{
		// Notifier
		NOTIFIER_ERREUR( NERREUR_STREAM_ERROR );

		// Quitter
		return NFALSE;
	}

	// OK
	return NTRUE;
}

// BTYPE_PACKET_CLIENT_SERVEUR_CHANGE_DIRECTION
NBOOL Projet_Serveur_TraitementPacket_TraiterChangeDirection( BServeur *this,
	const NClientServeur *client,
	const struct BPacketClientServeurChangeDirection *data )
{
	// Joueur
	const BEtatClient *joueur;

	// Verifier etat serveur
	if( this->m_etat != BETAT_SERVEUR_EN_JEU )
		// Kicker
		return NFALSE;

	// Lock le cache
	Projet_Commun_Reseau_Client_Cache_BCacheClient_ProtegerCache( this->m_cacheClient );

	// Obtenir le joueur
	if( !( joueur = Projet_Commun_Reseau_Client_Cache_BCacheClient_ObtenirClient( this->m_cacheClient,
		NLib_Module_Reseau_Serveur_NClientServeur_ObtenirIdentifiant( client ) ) ) )
	{
		// Notifier
		NOTIFIER_ERREUR( NERREUR_OBJECT_STATE );

		// Unlock le cache
		Projet_Commun_Reseau_Client_Cache_BCacheClient_NePlusProtegerCache( this->m_cacheClient );

		// Quitter
		return NFALSE;
	}

	// Verifier que le joueur soit bien en vie
	if( !Projet_Commun_Reseau_Client_BEtatClient_EstEnVie( joueur ) )
	{
		// Notifier
		NOTIFIER_AVERTISSEMENT( NERREUR_OBJECT_STATE );

		// Unlock le cache
		Projet_Commun_Reseau_Client_Cache_BCacheClient_NePlusProtegerCache( this->m_cacheClient );

		// Quitter (sans erreur)
		return NTRUE;
	}

	// Unlock le cache
	Projet_Commun_Reseau_Client_Cache_BCacheClient_NePlusProtegerCache( this->m_cacheClient );

	// Enregistrer le changement
	if( !Projet_Commun_Reseau_Client_Cache_BCacheClient_EnregistrerChangementDirection( this->m_cacheClient,
		NLib_Module_Reseau_Serveur_NClientServeur_ObtenirIdentifiant( client ),
		data->m_direction ) )
	{
		// Notifier
		NOTIFIER_ERREUR( NERREUR_STREAM_ERROR );

		// Quitter
		return NFALSE;
	}

	// Transmettre aux clients
	return Projet_Serveur_TraitementPacket_NotifierJoueurChangeDirection( this,
		client,
		data->m_direction );
}

// BTYPE_PACKET_CLIENT_SERVEUR_CHANGE_POSITION
NBOOL Projet_Serveur_TraitementPacket_TraiterChangePosition( BServeur *this,
	const NClientServeur *client,
	const struct BPacketClientServeurChangePosition *data )
{
	// Position
	NSPoint position;

	// Code retour
	__OUTPUT NBOOL codeRetour;

	// Joueur
	BEtatClient *joueur;

	// Carte
	const BCarte *carte;

	// Etat carte
	const BEtatCarte *etatCarte;

	// Obtenir carte/etat carte
	if( !( carte = Projet_Serveur_Monde_BMondeServeur_ObtenirCarte( this->m_mondeServeur ) )
		|| !( etatCarte = Projet_Serveur_Monde_BMondeServeur_ObtenirEtatCarte( this->m_mondeServeur ) ) )
	{
		// Notifier
		NOTIFIER_ERREUR( NERREUR_OBJECT_STATE );

		// Quitter
		return NFALSE;
	}

	// Lock cache
	Projet_Commun_Reseau_Client_Cache_BCacheClient_ProtegerCache( this->m_cacheClient );

	// Obtenir joueur
	if( !( joueur = (BEtatClient*)Projet_Commun_Reseau_Client_Cache_BCacheClient_ObtenirClient( this->m_cacheClient,
		NLib_Module_Reseau_Serveur_NClientServeur_ObtenirIdentifiant( client ) ) ) )
	{
		// Notifier
		NOTIFIER_ERREUR( NERREUR_PARAMETER_ERROR );

		// Unlock cache
		Projet_Commun_Reseau_Client_Cache_BCacheClient_NePlusProtegerCache( this->m_cacheClient );

		// Quitter
		return NFALSE;
	}

	// Obtenir position actuelle
	position = *Projet_Commun_Reseau_Client_BEtatClient_ObtenirPosition( joueur );

	// Calculer position future
	switch( data->m_direction )
	{
		case NHAUT:
			position.y--;
			break;
		case NBAS:
			position.y++;
			break;
		case NGAUCHE:
			position.x--;
			break;
		case NDROITE:
			position.x++;
			break;

		default:
			break;
	}

	// Lock le cache map
	Projet_Commun_Carte_Etat_BEtatCarte_Proteger( (BEtatCarte*)Projet_Commun_Monde_BMonde_ObtenirEtatCarte( this->m_monde ) );

	// Verifier que la position ne sorte pas
	if( !Projet_Commun_Carte_BCarte_EstPositionCorrecte( carte,
			position )
		// Verifier que le deplacement est bien possible
		|| Projet_Commun_Carte_BCarte_ObtenirCases( carte )[ position.x ][ position.y ].m_type == BTYPE_BLOC_SOLIDE
		|| Projet_Commun_Carte_Etat_BEtatCarte_ObtenirCase( etatCarte )[ position.x ][ position.y ].m_estRempli
		|| Projet_Commun_Carte_Etat_BEtatCarte_ObtenirCase( etatCarte )[ position.x ][ position.y ].m_etatBombe != NULL
		|| !Projet_Commun_Reseau_Client_BEtatClient_EstEnVie( joueur ) )
	{
		// Unlock le cache map
		Projet_Commun_Carte_Etat_BEtatCarte_NePlusProteger( (BEtatCarte*)Projet_Commun_Monde_BMonde_ObtenirEtatCarte( this->m_monde ) );

		// Replacer
		Projet_Serveur_TraitementPacket_NotifierJoueurChangePosition( this,
			client,
			*Projet_Commun_Reseau_Client_BEtatClient_ObtenirPosition( joueur ),
			data->m_direction );

		// Unlock cache
		Projet_Commun_Reseau_Client_Cache_BCacheClient_NePlusProtegerCache( this->m_cacheClient );

		// Quitter
		return NTRUE;
	}

	// Unlock le cache map
	Projet_Commun_Carte_Etat_BEtatCarte_NePlusProteger( (BEtatCarte*)Projet_Commun_Monde_BMonde_ObtenirEtatCarte( this->m_monde ) );

	// Enregistrer le changement
	if( !Projet_Commun_Reseau_Client_Cache_BCacheClient_EnregistrerChangementPosition( this->m_cacheClient,
		NLib_Module_Reseau_Serveur_NClientServeur_ObtenirIdentifiant( client ),
		position,
		data->m_direction ) )
	{
		// Notifier
		NOTIFIER_ERREUR( NERREUR_STREAM_ERROR );

		// Unlock cache
		Projet_Commun_Reseau_Client_Cache_BCacheClient_NePlusProtegerCache( this->m_cacheClient );

		// Quitter
		return NFALSE;
	}

	// Transmettre aux clients
	codeRetour = Projet_Serveur_TraitementPacket_NotifierJoueurChangePosition( this,
		client,
		position,
		data->m_direction );

	// Verifier si le client prend un bonus
	Projet_Serveur_Monde_BMondeServeur_TraiterPriseBonus( this->m_mondeServeur,
		client,
		joueur );

	// Unlock cache
	Projet_Commun_Reseau_Client_Cache_BCacheClient_NePlusProtegerCache( this->m_cacheClient );

	// OK?
	return codeRetour;
}

// BTYPE_PACKET_CLIENT_SERVEUR_POSE_BOMBE
NBOOL Projet_Serveur_TraitementPacket_TraiterPoseBombe( BServeur *this,
	const NClientServeur *client,
	const struct BPacketClientServeurPoseBombe *data )
{
	// Position
	NSPoint position;

	// Joueur
	const BEtatClient *joueur;

	// Identifiant bombe
	NU32 identifiantBombe;

	// Referencer
	NREFERENCER( data );

	// Lock le cache client
	Projet_Commun_Reseau_Client_Cache_BCacheClient_ProtegerCache( this->m_cacheClient );

	// Obtenir joueur
	if( !( joueur = Projet_Commun_Reseau_Client_Cache_BCacheClient_ObtenirClient( this->m_cacheClient,
		NLib_Module_Reseau_Serveur_NClientServeur_ObtenirIdentifiant( client ) ) ) )
	{
		// Notifier
		NOTIFIER_ERREUR( NERREUR_STREAM_ERROR );

		// Unlock le cache
		Projet_Commun_Reseau_Client_Cache_BCacheClient_NePlusProtegerCache( this->m_cacheClient );

		// Quitter
		return NFALSE;
	}

	// Verifier le nombre de bombes posees
	if( Projet_Commun_Reseau_Client_BEtatClient_ObtenirNombreBombePosee( joueur ) >= Projet_Commun_Reseau_Client_BEtatClient_ObtenirNombreMaximumBombe( joueur )
		|| !Projet_Commun_Reseau_Client_BEtatClient_EstEnVie( joueur ) )
	{
		// Notifier
		NOTIFIER_AVERTISSEMENT( NERREUR_OBJECT_STATE );

		// Repondre au client
		if( !Projet_Serveur_TraitementPacket_NotifierRefusPoseBombe( client ) )
		{
			// Notifier
			NOTIFIER_ERREUR( NERREUR_STREAM_ERROR );

			// Unlock le cache
			Projet_Commun_Reseau_Client_Cache_BCacheClient_NePlusProtegerCache( this->m_cacheClient );

			// Quitter
			return NFALSE;
		}

		// Unlock le cache
		Projet_Commun_Reseau_Client_Cache_BCacheClient_NePlusProtegerCache( this->m_cacheClient );

		// Quitter
		return NTRUE;
	}

	// Obtenir position
	position = *Projet_Commun_Reseau_Client_BEtatClient_ObtenirPosition( joueur );

	// Poser bombe
	if( ( identifiantBombe = Projet_Serveur_Monde_BMondeServeur_PoserBombe( this->m_mondeServeur,
		position,
		NLib_Module_Reseau_Serveur_NClientServeur_ObtenirIdentifiant( client ) ) ) == NERREUR )
	{
		// Repondre au client
		if( !Projet_Serveur_TraitementPacket_NotifierRefusPoseBombe( client ) )
		{
			// Notifier
			NOTIFIER_ERREUR( NERREUR_STREAM_ERROR );

			// Unlock le cache
			Projet_Commun_Reseau_Client_Cache_BCacheClient_NePlusProtegerCache( this->m_cacheClient );

			// Quitter
			return NFALSE;
		}

		// Unlock le cache
		Projet_Commun_Reseau_Client_Cache_BCacheClient_NePlusProtegerCache( this->m_cacheClient );

		// OK
		return NTRUE;
	}

	// Obtenir joueur
	if( !( joueur = Projet_Commun_Reseau_Client_Cache_BCacheClient_ObtenirClient( this->m_cacheClient,
		NLib_Module_Reseau_Serveur_NClientServeur_ObtenirIdentifiant( client ) ) ) )
	{
		// Notifier
		NOTIFIER_ERREUR( NERREUR_STREAM_ERROR );

		// Unlock le cache
		Projet_Commun_Reseau_Client_Cache_BCacheClient_NePlusProtegerCache( this->m_cacheClient );

		// Quitter
		return NFALSE;
	}

	// Incrementer le nombre de bombes posees
	Projet_Commun_Reseau_Client_BEtatClient_IncrementerNombreBombePosee( (BEtatClient*)joueur );

	// Diffuser pose bombe
	if( !Projet_Serveur_TraitementPacket_NotifierPoseBombe( this,
		client,
		&position,
		identifiantBombe ) )
	{
		// Notifier
		NOTIFIER_ERREUR( NERREUR_STREAM_ERROR );

		// Unlock cache
		Projet_Commun_Reseau_Client_Cache_BCacheClient_NePlusProtegerCache( this->m_cacheClient );

		// Quitter
		return NFALSE;
	}

	// Unlock cache
	Projet_Commun_Reseau_Client_Cache_BCacheClient_NePlusProtegerCache( this->m_cacheClient );

	// OK
	return NTRUE;
}

