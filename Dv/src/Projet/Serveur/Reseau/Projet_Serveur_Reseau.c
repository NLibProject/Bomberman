#include "../../../../include/Projet/Projet.h"

// ---------------------------------
// namespace Projet::Serveur::Reseau
// ---------------------------------

/* Callback connexion client */
__CALLBACK NBOOL Projet_Serveur_Reseau_CallbackConnexionClient( const NClientServeur *client )
{
	// Ajouter
	return Projet_Serveur_BServeur_AjouterClient( (BServeur*)NLib_Module_Reseau_Serveur_NClientServeur_ObtenirDonneeExterne( client ),
		(NClientServeur*)client );
}

/* Callback reception packet */
__CALLBACK NBOOL Projet_Serveur_Reseau_CallbackReceptionPacket( const NClientServeur *client,
	const NPacket *packet )
{
	// Donnees
	char *donnee;

	// Type
	BTypePacket type;

	// Lire
	if( !( donnee = Projet_Commun_Reseau_Lire( packet,
		&type ) ) )
	{
		// Notifier
		NOTIFIER_ERREUR( NERREUR_CONSTRUCTOR_FAILED );

		// Quitter
		return NFALSE;
	}

	// Communiquer au serveur
	if( !Projet_Serveur_BServeur_TraiterPacket( (BServeur*)NLib_Module_Reseau_Serveur_NClientServeur_ObtenirDonneeExterne( client ),
		client,
		type,
		donnee ) )
	{
		// Notifier
		NOTIFIER_ERREUR( NERREUR_STREAM_ERROR );
		
		// Liberer
		Projet_Commun_Reseau_Packet_Donnee_Liberer( donnee,
			type );

		// Quitter
		return NFALSE;
	}

	// Liberer
	Projet_Commun_Reseau_Packet_Donnee_Liberer( donnee,
		type );

	// OK
	return NTRUE;
}

/* Callback deconnexion client */
__CALLBACK NBOOL Projet_Serveur_Reseau_CallbackDeconnexionClient( const NClientServeur *client )
{
	return Projet_Serveur_BServeur_SupprimerClient( (BServeur*)NLib_Module_Reseau_Serveur_NClientServeur_ObtenirDonneeExterne( client ),
		(NClientServeur*)client );
}

