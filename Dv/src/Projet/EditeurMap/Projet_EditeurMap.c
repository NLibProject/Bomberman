#include "../../../include/Projet/Projet.h"

/*
	@author SOARES Lucas
*/

// ----------------------------
// namespace Projet::EditeurMap
// ----------------------------

/* Main editeur map */
NS32 Projet_EditeurMap_Main( void )
{
	// Editeur
	BEditeur *editeur;

	// Initialiser NLib
	if( !NLib_Initialiser( Projet_Commun_Erreur_CallbackNotificationErreur ) )
	{
		// Notifier
		NOTIFIER_ERREUR( NERREUR_INIT_FAILED );

		// Quitter
		return EXIT_FAILURE;
	}

	// Construire l'editeur
	if( !( editeur = Projet_EditeurMap_BEditeur_Construire( ) ) )
	{
		// Notifier
		NOTIFIER_ERREUR( NERREUR_CONSTRUCTOR_FAILED );

		// Quitter
		return EXIT_FAILURE;
	}

	// Editer
	Projet_EditeurMap_BEditeur_Editer( editeur );

	// Detruire l'editeur
	Projet_EditeurMap_BEditeur_Detruire( &editeur );

	// Detruire NLib
	NLib_Detruire( );

	// OK
	return EXIT_SUCCESS;
}

