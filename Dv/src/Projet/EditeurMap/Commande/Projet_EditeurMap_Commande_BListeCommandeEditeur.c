#define PROJET_EDITEURMAP_COMMANDE_BLISTECOMMANDEEDITEUR_INTERNE
#include "../../../../include/Projet/Projet.h"

// --------------------------------------------------------
// enum Projet::EditeurMap::Commande::BListeCommandeEditeur
// --------------------------------------------------------

/* Obtenir commande */
BListeCommandeEditeur Projet_EditeurMap_Commande_BListeCommandeEditeur_Obtenir( const char *commande )
{
	// Sortie
	BListeCommandeEditeur sortie;

	// Si il s'agit d'une saisie de l'identifiant de la commande
	if( ( sortie = strtol( commande,
		NULL,
		10 ) ) != BLISTE_COMMANDE_EDITEUR_AUCUNE )
		return sortie;

	// Chercher
	for( sortie = (BListeCommandeEditeur)0; sortie < BLISTE_COMMANDES_EDITEUR; sortie++ )
		// Verifier
		if( NLib_Chaine_Comparer( BLISTE_COMMANDE_EDITEUR_TEXTE[ sortie ],
			commande,
			NFALSE,
			0 ) )
			return sortie;

	// Introuvable
	return BLISTE_COMMANDE_EDITEUR_AUCUNE;
}

/* Afficher aide */
void Projet_EditeurMap_Commande_BListeCommandeEditeur_AfficherAide( void )
{
	// Iterateur
	NU32 i;

	// Expliciter
	printf( "Liste des commandes:\n\n" );

	// Afficher les commandes
	for( i = BLISTE_COMMANDE_EDITEUR_AUCUNE + 1; i < BLISTE_COMMANDES_EDITEUR; i++ )
		printf( "[%d]\t\"%s\"\t[%s]\n",
			i,
			BLISTE_COMMANDE_EDITEUR_TEXTE[ i ],
			BLISTE_DESCRIPTION_COMMANDE_EDITEUR_TEXTE[ i ] );

	// Taille maximale carte
	printf( "\nTaille maximale carte: %dx%d.\n",
		BTAILLE_MAXIMUM_CARTE.x,
		BTAILLE_MAXIMUM_CARTE.y );

	// Liste des touches
	printf( "\nTouches de raccourcis:\n\n" );
	printf( "s: Definir la position de depart des joueurs.\n" );
	printf( "p: Passer au pinceau.\n" );
	printf( "e: Passer a la gomme.\n" );
	printf( "b: Passer en mode choix de bloc de remplissage.\n" );

	// Retour a la ligne
	puts( "" );
}

