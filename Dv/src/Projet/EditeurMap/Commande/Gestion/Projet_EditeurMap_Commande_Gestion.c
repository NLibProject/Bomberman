#include "../../../../../include/Projet/Projet.h"

// -----------------------------------------------
// namespace Projet::EditeurMap::Commande::Gestion
// -----------------------------------------------

/* Gerer */
NBOOL Projet_EditeurMap_Commande_Gestion_Gerer( BEditeur *editeur )
{
	// Buffer
	char buffer[ 2048 ];

	// Nom carte
	char nom[ 2048 ];

	// Handle
	NU32 handle;

	// Taille carte
	NUPoint taille;

	// Afficher la liste des commandes
	Projet_EditeurMap_Commande_BListeCommandeEditeur_AfficherAide( );

	do
	{
		// Lire
		scanf( "%s",
			buffer );

		// Traiter
		switch( Projet_EditeurMap_Commande_BListeCommandeEditeur_Obtenir( buffer ) )
		{
			case BLISTE_COMMANDE_EDITEUR_QUITTER:
				Projet_EditeurMap_BEditeur_Arreter( editeur );
				break;

			case BLISTE_COMMANDE_EDITEUR_NOUVELLE:
				// Handle
				printf( "Saisissez l'handle de la carte: " );
				scanf( "%u",
					&handle );

				// Nom
				printf( "Saisissez le nom de la carte: " );
				scanf( "%s",
					nom );

				// Taille
				printf( "Saisissez la taille de la carte: " );
				scanf( "%u %u", &taille.x, &taille.y );

				// Demander la creation
				Projet_EditeurMap_BEditeur_CreerCarte( editeur,
					handle,
					nom,
					taille );
				break;

			case BLISTE_COMMANDE_EDITEUR_CHARGER:
				// Demander
				printf( "Saisissez l'handle de la carte a charger: " );
				scanf( "%d",
					&handle );

				// Charger
				Projet_EditeurMap_BEditeur_Charger( editeur,
					handle );
				break;

			case BLISTE_COMMANDE_EDITEUR_SAUVEGARDER:
				Projet_EditeurMap_BEditeur_Sauvegarder( editeur );
				break;

			case BLISTE_COMMANDE_EDITEUR_CHANGER_NOM:
				// Demander
				printf( "Saisissez le nouveau nom: " );
				scanf( "%s",
					nom );

				// Modifier
				Projet_EditeurMap_BEditeur_ChangerNomCarte( editeur,
					nom );
				break;

			case BLISTE_COMMANDE_EDITEUR_CHANGER_TAILLE:
				// Demander
				printf( "Saisissez la nouvelle taille: " );
				scanf( "%d %d",
					&taille.x,
					&taille.y );

				// Modifier
				Projet_EditeurMap_BEditeur_ChangerTailleCarte( editeur,
					taille );
				break;

			case BLISTE_COMMANDE_EDITEUR_INFOS_CARTE:
				Projet_EditeurMap_BEditeur_AfficherInfosCarte( editeur );
				break;

			case BLISTE_COMMANDE_EDITEUR_AIDE:
				Projet_EditeurMap_Commande_BListeCommandeEditeur_AfficherAide( );
				break;

			case BLISTE_COMMANDE_EDITEUR_AUCUNE:
			default:
				NOTIFIER_AVERTISSEMENT_UTILISATEUR( NERREUR_USER,
					"Cette commande est inconnue, tapez aide pour plus d'informations",
					0 );
				break;
		}
	} while( Projet_EditeurMap_BEditeur_EstEnCours( editeur ) );

	// OK
	return NTRUE;
}

