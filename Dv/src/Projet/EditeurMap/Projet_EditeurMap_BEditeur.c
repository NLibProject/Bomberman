#include "../../../include/Projet/Projet.h"

// -----------------------------------
// struct Projet::EditeurMap::BEditeur
// -----------------------------------

/* Update curseur (privee) */
void Projet_EditeurMap_BEditeur_UpdateCurseurInterne( BEditeur *this )
{
	switch( this->m_typeEdition )
	{
		case BTYPE_EDITION_TILESET_GOMME:
			NLib_Module_SDL_DefinirCurseur2( NLib_Module_SDL_Surface_NSurface_ObtenirSurface( this->m_surfaceOutil[ BTYPE_EDITION_TILESET_GOMME ] ) );
			break;

		case BTYPE_EDITION_TILESET_PINCEAU:
			NLib_Module_SDL_DefinirCurseur2( NLib_Module_SDL_Surface_NSurface_ObtenirSurface( this->m_surfaceOutil[ BTYPE_EDITION_TILESET_PINCEAU ] ) );
			break;

		default:
			NLib_Module_SDL_RestaurerCurseur( );
			break;
	}
}

/* Creer les cadres (privee) */
NBOOL Projet_EditeurMap_BEditeur_CreerCadreInterne( __OUTPUT BEditeur *this )
{
	// Iterateurs
	NU32 i = 0, j;

	// Position
	NSPoint position;

	// Taille
	NUPoint taille;

	// Taille horizontale tileset
	NU32 tailleHorizontaleTileset;

	// Calculer la taille horizontale des tilesets
	tailleHorizontaleTileset = BDIMENSION_HORIZONTALE_GRILLE_EDITEUR * BTAILLE_CASE_TILESET.x;

	// Creer
	for( ; i < BLISTE_CADRES; i++ )
	{
		// Definir les parametres
		switch( i )
		{
			case BLISTE_CADRE_CARTE:
				// Position
				NDEFINIR_POSITION( position,
					0,
					0 );

				// Taille
				NDEFINIR_POSITION( taille,
					NLib_Module_SDL_NFenetre_ObtenirResolution( this->m_fenetre )->x - tailleHorizontaleTileset - BEPAISSEUR_CADRE_EDITEUR_MAP,// + BTAILLE_CASE_TILESET.x,
					NLib_Module_SDL_NFenetre_ObtenirResolution( this->m_fenetre )->y - BEPAISSEUR_CADRE_EDITEUR_MAP );
				break;

			case BLISTE_CADRE_TYPE_BLOC:
				// Position
				NDEFINIR_POSITION( position,
					NLib_Module_SDL_NFenetre_ObtenirResolution( this->m_fenetre )->x - tailleHorizontaleTileset,// + BTAILLE_CASE_TILESET.x,
					NLib_Module_SDL_NFenetre_ObtenirResolution( this->m_fenetre )->y - ( NLib_Module_SDL_NFenetre_ObtenirResolution( this->m_fenetre )->y / BDIVIDENDE_TAILLE_TILESET_EDITEUR ) );

				// Taille
				NDEFINIR_POSITION( taille,
					tailleHorizontaleTileset - BEPAISSEUR_CADRE_EDITEUR_MAP,// - BTAILLE_CASE_TILESET.x,
					( NLib_Module_SDL_NFenetre_ObtenirResolution( this->m_fenetre )->y / BDIVIDENDE_TAILLE_TILESET_EDITEUR ) - BEPAISSEUR_CADRE_EDITEUR_MAP );
				break;

			case BLISTE_CADRE_TILESET:
				// Position
				NDEFINIR_POSITION( position,
					NLib_Module_SDL_NFenetre_ObtenirResolution( this->m_fenetre )->x - tailleHorizontaleTileset,// + BTAILLE_CASE_TILESET.x,
					0 );

				// Taille
				NDEFINIR_POSITION( taille,
					tailleHorizontaleTileset - BEPAISSEUR_CADRE_EDITEUR_MAP,// - BTAILLE_CASE_TILESET.x,
					NLib_Module_SDL_NFenetre_ObtenirResolution( this->m_fenetre )->y - ( NLib_Module_SDL_NFenetre_ObtenirResolution( this->m_fenetre )->y / BDIVIDENDE_TAILLE_TILESET_EDITEUR ) - BEPAISSEUR_CADRE_EDITEUR_MAP );
				break;

			default:
				// Position
				NDEFINIR_POSITION( position,
					0,
					0 );

				// Taille
				NDEFINIR_POSITION( taille,
					0,
					0 );
				break;
		}

		// Construire
		if( !( this->m_cadre[ i ] = NLib_Module_SDL_Bouton_NBouton_Construire( position,
			taille,
			(NCouleur){ 0xFF, 0xFF, 0xFF, 0xFF },
			(NCouleur){ 0x00, 0x00, 0x00, 0xFF },
			this->m_fenetre,
			BEPAISSEUR_CADRE_EDITEUR_MAP ) ) )
		{
			// Notifier
			NOTIFIER_ERREUR( NERREUR_CONSTRUCTOR_FAILED );

			// Liberer
			for( j = 0; j < i; j++ )
				NLib_Module_SDL_Bouton_NBouton_Detruire( &this->m_cadre[ j ] );

			// Quitter
			return NFALSE;
		}
	}

	// OK
	return NTRUE;
}

void Projet_EditeurMap_BEditeur_UpdateCadreSelectionBlocInterne( BEditeur *this )
{
	// Iterateur
	NU32 i = 0;

	// Definir la position
	for( ; i < BCOUCHES_CARTE; i++ )
		NLib_Module_SDL_NCadre_DefinirPosition( this->m_cadreSelectionBloc[ i ],
			NLib_Module_SDL_Bouton_NBouton_ObtenirPosition( this->m_cadre[ BLISTE_CADRE_TILESET ] ).x + ( (NS32)this->m_selectionBloc[ i ].x * BTAILLE_CASE_TILESET.x ),
			( (NS32)this->m_selectionBloc[ i ].y * (NS32)BTAILLE_CASE_TILESET.y ) - ( (NS32)this->m_scroll[ BLISTE_CADRE_TILESET ].x * (NS32)BTAILLE_CASE_TILESET.y ) );
}

/* Construire l'editeur */
__ALLOC BEditeur *Projet_EditeurMap_BEditeur_Construire( void )
{
	// Sortie
	__OUTPUT BEditeur *out;

	// Iterateurs
	NU32 i = 0, j;

	// Buffer
	char buffer[ 2048 ];

	// Police
	NPolice *police;

	// Allouer la memoire
	if( !( out = calloc( 1,
		sizeof( BEditeur ) ) ) )
	{
		// Notifier
		NOTIFIER_ERREUR( NERREUR_ALLOCATION_FAILED );

		// Quitter
		return NULL;
	}

	// Construire la fenetre
	if( !( out->m_fenetre = NLib_Module_SDL_NFenetre_Construire( "Editeur map",
		(NUPoint){ 1280, 720 },
		NFALSE ) ) )
	{
		// Notifier
		NOTIFIER_ERREUR( NERREUR_CONSTRUCTOR_FAILED );

		// Liberer
		NFREE( out );

		// Quitter
		return NULL;
	}

	// Construire les ressources
	if( !( out->m_ressource = Projet_Commun_Ressource_BRessource_Construire( out->m_fenetre ) ) )
	{
		// Notifier
		NOTIFIER_ERREUR( NERREUR_CONSTRUCTOR_FAILED );

		// Detruire la fenetre
		NLib_Module_SDL_NFenetre_Detruire( &out->m_fenetre );

		// Liberer
		NFREE( out );

		// Quitter
		return NULL;
	}

	// Zero
	out->m_estEditionEnCours = NFALSE;

	// Construire les cadres
	if( !Projet_EditeurMap_BEditeur_CreerCadreInterne( out ) )
	{
		// Notifier
		NOTIFIER_ERREUR( NERREUR_CONSTRUCTOR_FAILED );

		// Detruire les ressources
		Projet_Commun_Ressource_BRessource_Detruire( &out->m_ressource );

		// Detruire la fenetre
		NLib_Module_SDL_NFenetre_Detruire( &out->m_fenetre );

		// Liberer
		NFREE( out );

		// Quitter
		return NULL;
	}

	// Creer les grilles
		// Tileset
			if( !( out->m_grilleTileset = Projet_EditeurMap_Grille_BGrille_Construire( Projet_Commun_Ressource_BRessource_ObtenirEnsembleTileset( out->m_ressource ),
				Projet_Commun_Ressource_BRessource_ObtenirNombreTileset( out->m_ressource ) ) ) )
			{
				// Notifier
				NOTIFIER_ERREUR( NERREUR_CONSTRUCTOR_FAILED );

				// Detruire les cadres
				for( i = 0; i < BLISTE_CADRES; i++ )
					NLib_Module_SDL_Bouton_NBouton_Detruire( &out->m_cadre[ i ] );

				// Detruire les ressources
				Projet_Commun_Ressource_BRessource_Detruire( &out->m_ressource );

				// Detruire la fenetre
				NLib_Module_SDL_NFenetre_Detruire( &out->m_fenetre );

				// Liberer
				NFREE( out );

				// Quitter
				return NULL;
			}
		// Type bloc
			if( !( out->m_grilleTypeBloc = Projet_EditeurMap_Grille_BGrille_Construire2( BTYPES_BLOC ) ) )
			{
				// Notifier
				NOTIFIER_ERREUR( NERREUR_CONSTRUCTOR_FAILED );

				// Detruire le tileset grille
				Projet_EditeurMap_Grille_BGrille_Detruire( &out->m_grilleTileset );

				// Detruire les cadres
				for( i = 0; i < BLISTE_CADRES; i++ )
					NLib_Module_SDL_Bouton_NBouton_Detruire( &out->m_cadre[ i ] );

				// Detruire les ressources
				Projet_Commun_Ressource_BRessource_Detruire( &out->m_ressource );

				// Detruire la fenetre
				NLib_Module_SDL_NFenetre_Detruire( &out->m_fenetre );

				// Liberer
				NFREE( out );

				// Quitter
				return NULL;
			}

	// Creer les cadres de selection
	for( i = 0; i < BCOUCHES_CARTE; i++ )
		// Creer les cadres de selection
		if( !( out->m_cadreSelectionBloc[ i ] = NLib_Module_SDL_NCadre_Construire3( NLib_Module_SDL_Bouton_NBouton_ObtenirPosition( out->m_cadre[ BLISTE_CADRE_TILESET ] ).x + ( out->m_selectionBloc[ i ].x * BTAILLE_CASE_TILESET.x ),
				( (NS32)out->m_selectionBloc[ i ].y * (NS32)BTAILLE_CASE_TILESET.y ) - ( (NS32)out->m_scroll[ BLISTE_CADRE_TILESET ].x * (NS32)BTAILLE_CASE_TILESET.y ),
				BTAILLE_CASE_TILESET.x,
				BTAILLE_CASE_TILESET.y,
				(NCouleur){ 0xFF, 0x00, 0x00, 0xFF },
				(NCouleur){ 0x00, 0x00, 0x00, 0x00 },
				out->m_fenetre,
				1 ) ) )
		{
			// Notifier
			NOTIFIER_ERREUR( NERREUR_CONSTRUCTOR_FAILED );

			// Detruire la grille
			Projet_EditeurMap_Grille_BGrille_Detruire( &out->m_grilleTileset );
			Projet_EditeurMap_Grille_BGrille_Detruire( &out->m_grilleTypeBloc );

			// Detruire les cadres
			for( j = 0; j < i; j++ )
				NLib_Module_SDL_Bouton_NBouton_Detruire( &out->m_cadre[ i ] );

			// Detruire les ressources
			Projet_Commun_Ressource_BRessource_Detruire( &out->m_ressource );

			// Detruire la fenetre
			NLib_Module_SDL_NFenetre_Detruire( &out->m_fenetre );

			// Liberer
			NFREE( out );

			// Quitter
			return NULL;
		}

	// Creer le cadre de type de bloc
	if( !( out->m_cadreBlocCarte = NLib_Module_SDL_NCadre_Construire3( 0,
		0,
		BTAILLE_CASE_TILESET.x,
		BTAILLE_CASE_TILESET.y,
		(NCouleur){ 0xFF, 0xFF, 0xFF, 0xFF },
		(NCouleur){ 0x00, 0x00, 0x00, 0xFF },
		out->m_fenetre,
		1 ) ) )
	{
		// Notifier
		NOTIFIER_ERREUR( NERREUR_MUTEX );

		// Detruire les cadres
		for( i = 0; i < BLISTE_CADRES; i++ )
			NLib_Module_SDL_Bouton_NBouton_Detruire( &out->m_cadre[ i ] );

		// Detruire la grille
		Projet_EditeurMap_Grille_BGrille_Detruire( &out->m_grilleTileset );
		Projet_EditeurMap_Grille_BGrille_Detruire( &out->m_grilleTypeBloc );

		// Detruire les ressources
		Projet_Commun_Ressource_BRessource_Detruire( &out->m_ressource );

		// Detruire la fenetre
		NLib_Module_SDL_NFenetre_Detruire( &out->m_fenetre );

		// Liberer
		NFREE( out );

		// Quitter
		return NULL;
	}

	// Ouvrir la police
	if( !( police = NLib_Module_SDL_TTF_NPolice_Construire2( Projet_Commun_Ressource_Police_BListePolice_ObtenirLien( BLISTE_POLICE_TREBUCHET ),
		BTAILLE_POLICE_CHOIX_POSITION_DEPART_JOUEUR_EDITEUR,
		(NCouleur){ 0xFF, 0xFF, 0xFF, 0xFF } ) ) )
	{
		// Notifier
		NOTIFIER_ERREUR( NERREUR_CONSTRUCTOR_FAILED );

		// Detruire le cadre type de bloc
		NLib_Module_SDL_NCadre_Detruire( &out->m_cadreBlocCarte );

		// Detruire les cadres
		for( i = 0; i < BLISTE_CADRES; i++ )
			NLib_Module_SDL_Bouton_NBouton_Detruire( &out->m_cadre[ i ] );

		// Detruire la grille
		Projet_EditeurMap_Grille_BGrille_Detruire( &out->m_grilleTileset );
		Projet_EditeurMap_Grille_BGrille_Detruire( &out->m_grilleTypeBloc );

		// Detruire les ressources
		Projet_Commun_Ressource_BRessource_Detruire( &out->m_ressource );

		// Detruire la fenetre
		NLib_Module_SDL_NFenetre_Detruire( &out->m_fenetre );

		// Liberer
		NFREE( out );

		// Quitter
		return NULL;
	}

	// Creer les surfaces
	for( i = 0; i < BNOMBRE_MAXIMUM_JOUEUR; i++ )
	{
		// Creer le texte
		sprintf( buffer,
			"%d",
			i + 1 );

		// Creer la surface
		if( !( out->m_surfacePositionJoueur[ i ] = NLib_Module_SDL_TTF_NPolice_CreerTexte( police,
			out->m_fenetre,
			buffer ) ) )
		{
			// Notifier
			NOTIFIER_ERREUR( NERREUR_CONSTRUCTOR_FAILED );

			// Detruire texte
			for( j = 0; j < i; j++ )
				NLib_Module_SDL_Surface_NSurface_Detruire( &out->m_surfacePositionJoueur[ j ] );

			// Detruire le cadre type de bloc
			NLib_Module_SDL_NCadre_Detruire( &out->m_cadreBlocCarte );

			// Detruire les cadres
			for( i = 0; i < BLISTE_CADRES; i++ )
				NLib_Module_SDL_Bouton_NBouton_Detruire( &out->m_cadre[ i ] );

			// Detruire la grille
			Projet_EditeurMap_Grille_BGrille_Detruire( &out->m_grilleTileset );
			Projet_EditeurMap_Grille_BGrille_Detruire( &out->m_grilleTypeBloc );

			// Detruire les ressources
			Projet_Commun_Ressource_BRessource_Detruire( &out->m_ressource );

			// Detruire la fenetre
			NLib_Module_SDL_NFenetre_Detruire( &out->m_fenetre );

			// Liberer
			NFREE( out );

			// Quitter
			return NULL;
		}
	}

	// Fermer la police
	NLib_Module_SDL_TTF_NPolice_Detruire( &police );

	// Charger curseurs
	for( i = 0; i < BTYPES_EDITION; i++ )
	{
		// Trouver
		switch( i )
		{
			case BTYPE_EDITION_TILESET_GOMME:
				j = BRESSOURCE_EDITEUR_SURFACE_OUTIL_GOMME;
				break;
			case BTYPE_EDITION_TILESET_PINCEAU:
				j = BRESSOURCE_EDITEUR_SURFACE_OUTIL_PINCEAU;
				break;

			default:
				continue;
		}

		// Charger
		if( !( out->m_surfaceOutil[ i ] = NLib_Module_SDL_Surface_NSurface_Construire2( out->m_fenetre,
			Projet_EditeurMap_Ressource_BRessourceEditeur_ObtenirLienVersRessource( j ) ) ) )
		{
			// Notifier
			NOTIFIER_ERREUR( NERREUR_MUTEX );

			// Detruire surface
			for( i = 0; i < BTYPES_EDITION; i++ )
				switch( i )
				{
					case BTYPE_EDITION_TILESET_GOMME:
					case BTYPE_EDITION_TILESET_PINCEAU:
						NLib_Module_SDL_Surface_NSurface_Detruire( &out->m_surfaceOutil[ i ] );
						break;

					default:
						break;
				}

			// Detruire texte
			for( i = 0; i < BNOMBRE_MAXIMUM_JOUEUR; i++ )
				NLib_Module_SDL_Surface_NSurface_Detruire( &out->m_surfacePositionJoueur[ i ] );

			// Detruire le cadre type de bloc
			NLib_Module_SDL_NCadre_Detruire( &out->m_cadreBlocCarte );

			// Detruire les cadres
			for( i = 0; i < BLISTE_CADRES; i++ )
				NLib_Module_SDL_Bouton_NBouton_Detruire( &out->m_cadre[ i ] );

			// Detruire la grille
			Projet_EditeurMap_Grille_BGrille_Detruire( &out->m_grilleTileset );
			Projet_EditeurMap_Grille_BGrille_Detruire( &out->m_grilleTypeBloc );

			// Detruire les ressources
			Projet_Commun_Ressource_BRessource_Detruire( &out->m_ressource );

			// Detruire la fenetre
			NLib_Module_SDL_NFenetre_Detruire( &out->m_fenetre );

			// Liberer
			NFREE( out );

			// Quitter
			return NULL;
		}
	}

	// Est maintenant en cours
	out->m_estEnCours = NTRUE;

	// Lancer le thread de commandes
	if( !( out->m_threadCommande = NLib_Thread_NThread_Construire( (NBOOL ( ___cdecl* )( void* ))Projet_EditeurMap_Commande_Gestion_Gerer,
		out,
		&out->m_estEnCours ) ) )
	{
		// Notifier
		NOTIFIER_ERREUR( NERREUR_CONSTRUCTOR_FAILED );

		// Detruire outils
		for( i = 0; i < BTYPES_EDITION; i++ )
			switch( i )
			{
				case BTYPE_EDITION_TILESET_GOMME:
				case BTYPE_EDITION_TILESET_PINCEAU:
					NLib_Module_SDL_Surface_NSurface_Detruire( &out->m_surfaceOutil[ i ] );
					break;

				default:
					break;
			}

		// Detruire texte
		for( i = 0; i < BNOMBRE_MAXIMUM_JOUEUR; i++ )
			NLib_Module_SDL_Surface_NSurface_Detruire( &out->m_surfacePositionJoueur[ i ] );

		// Detruire le cadre type de bloc
		NLib_Module_SDL_NCadre_Detruire( &out->m_cadreBlocCarte );

		// Detruire les cadres
		for( i = 0; i < BLISTE_CADRES; i++ )
			NLib_Module_SDL_Bouton_NBouton_Detruire( &out->m_cadre[ i ] );

		// Detruire la grille
		Projet_EditeurMap_Grille_BGrille_Detruire( &out->m_grilleTileset );
		Projet_EditeurMap_Grille_BGrille_Detruire( &out->m_grilleTypeBloc );

		// Detruire les ressources
		Projet_Commun_Ressource_BRessource_Detruire( &out->m_ressource );

		// Detruire la fenetre
		NLib_Module_SDL_NFenetre_Detruire( &out->m_fenetre );

		// Liberer
		NFREE( out );

		// Quitter
		return NULL;
	}

	// Definir
	Projet_EditeurMap_BEditeur_UpdateCurseurInterne( out );

	// OK
	return out;
}

/* Detruire l'editeur */
void Projet_EditeurMap_BEditeur_Detruire( BEditeur **this )
{
	// Iterateur
	NU32 i;

	// Detruire le thread
	NLib_Thread_NThread_Detruire( &(*this)->m_threadCommande );

	// Detruire outils
	for( i = 0; i < BTYPES_EDITION; i++ )
		switch( i )
		{
			case BTYPE_EDITION_TILESET_GOMME:
			case BTYPE_EDITION_TILESET_PINCEAU:
				NLib_Module_SDL_Surface_NSurface_Detruire( &(*this)->m_surfaceOutil[ i ] );
				break;

			default:
				break;
		}

	// Detruire texte
	for( i = 0; i < BNOMBRE_MAXIMUM_JOUEUR; i++ )
		NLib_Module_SDL_Surface_NSurface_Detruire( &(*this)->m_surfacePositionJoueur[ i ] );

	// Detruire le cadre type de bloc
	NLib_Module_SDL_NCadre_Detruire( &(*this)->m_cadreBlocCarte );

	// Detruire les cadres
	for( i = 0; i < BLISTE_CADRES; i++ )
		NLib_Module_SDL_Bouton_NBouton_Detruire( &(*this)->m_cadre[ i ] );

	// Detruire les grilles
	Projet_EditeurMap_Grille_BGrille_Detruire( &(*this)->m_grilleTileset );
	Projet_EditeurMap_Grille_BGrille_Detruire( &(*this)->m_grilleTypeBloc );

	// Detruire les elements lies a l'edition
	if( (*this)->m_estEditionEnCours )
	{
		// Detruire la carte
		Projet_Commun_Carte_BCarte_Detruire( &(*this)->m_carte );

		// Detruire la cadre entourant la carte
		NLib_Module_SDL_NCadre_Detruire( &(*this)->m_cadreCarte );
	}

	// Detruire les ressources
	Projet_Commun_Ressource_BRessource_Detruire( &(*this)->m_ressource );

	// Detruire la fenetre
	NLib_Module_SDL_NFenetre_Detruire( &(*this)->m_fenetre );

	// Liberer
	NFREE( (*this) );
}

/* Afficher la grille tileset (privee) */
void Projet_EditeurMap_BEditeur_AfficherGrilleTilesetInterne( const BEditeur *this )
{
	// Iterateurs
	NU32 i = 0, j;

	// Rectangle
	NSRect rectangle;

	// Tileset
	BTileset *tileset;

	// Position
	NSPoint position;

	// Case de la grille
	const BCaseGrille **cases;

	// Limite y d'affichage
	NU32 limiteYAffichage;

	// Base affichage
	NSPoint baseAffichage;

	// Copier la base d'affichage
	NDEFINIR_POSITION( baseAffichage,
		NLib_Module_SDL_Bouton_NBouton_ObtenirPosition( this->m_cadre[ BLISTE_CADRE_TILESET ] ).x,
		NLib_Module_SDL_Bouton_NBouton_ObtenirPosition( this->m_cadre[ BLISTE_CADRE_TILESET ] ).y );

	// Definir le clip rect
		// Position
			NDEFINIR_POSITION( rectangle.m_position,
				NLib_Module_SDL_Bouton_NBouton_ObtenirPosition( this->m_cadre[ BLISTE_CADRE_TILESET ] ).x,
				NLib_Module_SDL_Bouton_NBouton_ObtenirPosition( this->m_cadre[ BLISTE_CADRE_TILESET ] ).y );
		// Taille
			NDEFINIR_POSITION( rectangle.m_taille,
				rectangle.m_position.x + NLib_Module_SDL_Bouton_NBouton_ObtenirTaille( this->m_cadre[ BLISTE_CADRE_TILESET ] ).x,
				rectangle.m_position.y + NLib_Module_SDL_Bouton_NBouton_ObtenirTaille( this->m_cadre[ BLISTE_CADRE_TILESET ] ).y );
		// Definir
			NLib_Module_SDL_NFenetre_DefinirClipRect( this->m_fenetre,
				rectangle );

	// Peindre en noir
	NLib_Module_SDL_NFenetre_Nettoyer2( this->m_fenetre,
		0x00,
		0x00,
		0x00,
		0xFF );

	// Calculer la limite d'affichage
	limiteYAffichage = this->m_scroll[ BLISTE_CADRE_TILESET ].x + NLib_Module_SDL_Bouton_NBouton_ObtenirTaille( this->m_cadre[ BLISTE_CADRE_TILESET ] ).y / BTAILLE_CASE_TILESET.y;
	limiteYAffichage += ( limiteYAffichage >= Projet_EditeurMap_Grille_BGrille_ObtenirTaille( this->m_grilleTileset ).y ) ? 0 : 1;

	// Recuperer les cases
	cases = Projet_EditeurMap_Grille_BGrille_ObtenirCase( this->m_grilleTileset );

	// Afficher
	for( ; i < Projet_EditeurMap_Grille_BGrille_ObtenirTaille( this->m_grilleTileset ).x; i++ )
		for( j = this->m_scroll[ BLISTE_CADRE_TILESET ].x; j < limiteYAffichage; j++ )
		{
			// Verifier
			if( cases[ i ][ j ].m_idTileset != BBLOC_CARTE_AUCUN )
			{
				// Obtenir le tileset
				tileset = Projet_Commun_Ressource_BRessource_ObtenirTileset( this->m_ressource,
					cases[ i ][ j ].m_idTileset );

				// Definir la position
				NDEFINIR_POSITION( position,
					baseAffichage.x + ( i * BTAILLE_CASE_TILESET.x ),
					baseAffichage.y + ( ( j - this->m_scroll[ BLISTE_CADRE_TILESET ].x ) * BTAILLE_CASE_TILESET.y ) );

				// Afficher
				Projet_Commun_Carte_Tileset_BTileset_Afficher( tileset,
					cases[ i ][ j ].m_position,
					position,
					1 );
			}
		}

	// Afficher les cadres de selection
	for( i = 0; i < BCOUCHES_CARTE; i++ )
		NLib_Module_SDL_NCadre_Dessiner( this->m_cadreSelectionBloc[ i ] );

	// Supprimer le rectangle
	NLib_Module_SDL_NFenetre_SupprimerClipRect( this->m_fenetre );
}

/* Afficher la grille type de blocs (privee) */
void Projet_EditeurMap_BEditeur_AfficherGrilleTypeBlocInterne( BEditeur *this )
{
	// Rectangle
	NSRect rectangle;

	// Cases grille
	const BCaseGrille **cases;

	// Base affichage
	NSPoint baseAffichage;

	// Iterateurs
	NU32 x,
		y;

	// Copier la base d'affichage
	NDEFINIR_POSITION( baseAffichage,
		NLib_Module_SDL_Bouton_NBouton_ObtenirPosition( this->m_cadre[ BLISTE_CADRE_TYPE_BLOC ] ).x,
		NLib_Module_SDL_Bouton_NBouton_ObtenirPosition( this->m_cadre[ BLISTE_CADRE_TYPE_BLOC ] ).y );

	// Definir le clip rect
		// Position
			NDEFINIR_POSITION( rectangle.m_position,
				NLib_Module_SDL_Bouton_NBouton_ObtenirPosition( this->m_cadre[ BLISTE_CADRE_TYPE_BLOC ] ).x,
				NLib_Module_SDL_Bouton_NBouton_ObtenirPosition( this->m_cadre[ BLISTE_CADRE_TYPE_BLOC ] ).y );
		// Taille
			NDEFINIR_POSITION( rectangle.m_taille,
				rectangle.m_position.x + NLib_Module_SDL_Bouton_NBouton_ObtenirTaille( this->m_cadre[ BLISTE_CADRE_TYPE_BLOC ] ).x,
				rectangle.m_position.y + NLib_Module_SDL_Bouton_NBouton_ObtenirTaille( this->m_cadre[ BLISTE_CADRE_TYPE_BLOC ] ).y );
		// Definir
			NLib_Module_SDL_NFenetre_DefinirClipRect( this->m_fenetre,
				rectangle );

	// Peindre en noir
	NLib_Module_SDL_NFenetre_Nettoyer2( this->m_fenetre,
		0x00,
		0x00,
		0x00,
		0xFF );

	// Obtenir les cases
	cases = Projet_EditeurMap_Grille_BGrille_ObtenirCase( this->m_grilleTypeBloc );

	// Afficher
	for( x = 0; x < Projet_EditeurMap_Grille_BGrille_ObtenirTaille( this->m_grilleTypeBloc ).x; x++ )
		for( y = 0; y < Projet_EditeurMap_Grille_BGrille_ObtenirTaille( this->m_grilleTypeBloc ).y; y++ )
		{
			// Definir la position
			NLib_Module_SDL_NCadre_DefinirPosition( this->m_cadreBlocCarte,
				baseAffichage.x + x * BTAILLE_CASE_TILESET.x,
				baseAffichage.y + y * BTAILLE_CASE_TILESET.y );

			// Definir la couleur
			NLib_Module_SDL_NCadre_DefinirCouleur( this->m_cadreBlocCarte,
				0xFF,
				0xFF,
				0xFF,
				0xFF );
			NLib_Module_SDL_NCadre_DefinirCouleurFond( this->m_cadreBlocCarte,
				BCOULEUR_TYPE_BLOC_EDITEUR[ cases[ x ][ y ].m_typeBloc ].r,
				BCOULEUR_TYPE_BLOC_EDITEUR[ cases[ x ][ y ].m_typeBloc ].g,
				BCOULEUR_TYPE_BLOC_EDITEUR[ cases[ x ][ y ].m_typeBloc ].b,
				BCOULEUR_TYPE_BLOC_EDITEUR[ cases[ x ][ y ].m_typeBloc ].a );

			// Afficher
			if( cases[ x ][ y ].m_typeBloc < BTYPES_BLOC )
				NLib_Module_SDL_NCadre_Dessiner( this->m_cadreBlocCarte );
		}

	// Afficher le bloc de selection
		// Definir la position
			NLib_Module_SDL_NCadre_DefinirPosition( this->m_cadreBlocCarte,
				baseAffichage.x + this->m_selectionTypeBloc.x * BTAILLE_CASE_TILESET.x,
				baseAffichage.y + this->m_selectionTypeBloc.y * BTAILLE_CASE_TILESET.y );
		// Definir la couleur
			NLib_Module_SDL_NCadre_DefinirCouleur( this->m_cadreBlocCarte,
				0xFF,
				0x00,
				0x00,
				0xFF );
			NLib_Module_SDL_NCadre_DefinirCouleurFond( this->m_cadreBlocCarte,
				0x00,
				0x00,
				0x00,
				0x00 );
		// Dessiner
			NLib_Module_SDL_NCadre_Dessiner( this->m_cadreBlocCarte );
		// Restaurer la couleur
			NLib_Module_SDL_NCadre_DefinirCouleur( this->m_cadreBlocCarte,
				0xFF,
				0xFF,
				0xFF,
				0xFF );

	// Restaurer le clip rect
	NLib_Module_SDL_NFenetre_SupprimerClipRect( this->m_fenetre );
}

/* Mettre a jour l'editeur (privee) */
void Projet_EditeurMap_BEditeur_MettreAJourInterne( BEditeur *this )
{
	// Iterateur
	NU32 i = 0;

	// Evenement
	SDL_Event *e;

	// Maximum scroll
	NU32 maximumScroll;

	// Case de la grille
	BCaseGrille caseGrille;

	// Position
	NSPoint position;
	NUPoint position2;

	// Obtenir evenement
	e = NLib_Module_SDL_NFenetre_ObtenirEvenement( this->m_fenetre );

	// Gerer les cadres
	for( i = 0; i < BLISTE_CADRES; i++ )
	{
		// Mettre a jour
		NLib_Module_SDL_Bouton_NBouton_Update( this->m_cadre[ i ],
			&this->m_positionSouris,
			NLib_Module_SDL_NFenetre_ObtenirEvenement( this->m_fenetre ) );

		// Gerer
		switch( i )
		{
			case BLISTE_CADRE_CARTE:
				// On sort si la map n'existe pas
				if( !this->m_estEditionEnCours )
					break;

				// Gerer le cadre
				if( NLib_Module_SDL_Bouton_NBouton_ObtenirEtat( this->m_cadre[ i ] ) == NETAT_BOUTON_PRESSE )
				{
					// Calculer la case de la pression
					NDEFINIR_POSITION( position,
						this->m_positionSouris.x / BTAILLE_CASE_TILESET.x,
						this->m_positionSouris.y / BTAILLE_CASE_TILESET.y );

					// Si on est dans la carte
					if( Projet_Commun_Carte_BCarte_EstPositionCorrecte( this->m_carte,
						position ) )
						switch( NLib_Module_SDL_Bouton_NBouton_ObtenirBoutonActivation( this->m_cadre[ i ] ) )
						{
							case SDL_BUTTON_LEFT:
							case SDL_BUTTON_RIGHT:
								switch( this->m_typeEdition )
								{
									case BTYPE_EDITION_TILESET_GOMME:
										// Supprimer position tileset
										NDEFINIR_POSITION( ((BBloc**)Projet_Commun_Carte_BCarte_ObtenirCases( this->m_carte ))[ position.x ][ position.y ].m_positionTileset[ NLib_Module_SDL_Bouton_NBouton_ObtenirBoutonActivation( this->m_cadre[ i ] ) == SDL_BUTTON_RIGHT ],
											BBLOC_CARTE_AUCUN,
											BBLOC_CARTE_AUCUN );

										// Supprimer tileset
										((BBloc**)Projet_Commun_Carte_BCarte_ObtenirCases( this->m_carte ))[ position.x ][ position.y ].m_tileset[ NLib_Module_SDL_Bouton_NBouton_ObtenirBoutonActivation( this->m_cadre[ i ] ) == SDL_BUTTON_RIGHT ] = BBLOC_CARTE_AUCUN;
										break;

									case BTYPE_EDITION_TILESET_PINCEAU:
									case BTYPE_EDITION_TYPE_BLOC:
										// Copier case grille
										caseGrille = Projet_EditeurMap_Grille_BGrille_ObtenirCase( this->m_typeEdition == BTYPE_EDITION_TILESET_PINCEAU ?
												this->m_grilleTileset
												: this->m_grilleTypeBloc )
											[ this->m_typeEdition == BTYPE_EDITION_TILESET_PINCEAU ?
												( this->m_selectionBloc[ NLib_Module_SDL_Bouton_NBouton_ObtenirBoutonActivation( this->m_cadre[ i ] ) == SDL_BUTTON_RIGHT ].x )
												: this->m_selectionTypeBloc.x ]
											[ this->m_typeEdition == BTYPE_EDITION_TILESET_PINCEAU ?
												( this->m_selectionBloc[ NLib_Module_SDL_Bouton_NBouton_ObtenirBoutonActivation( this->m_cadre[ i ] ) == SDL_BUTTON_RIGHT ].y )
												: this->m_selectionTypeBloc.y ];

										// Suivant le type d'edition
										switch( this->m_typeEdition )
										{
											case BTYPE_EDITION_TILESET_PINCEAU:
												// Definir tileset
												( (BBloc**)Projet_Commun_Carte_BCarte_ObtenirCases( this->m_carte ) )[ position.x ][ position.y ].m_tileset[ NLib_Module_SDL_Bouton_NBouton_ObtenirBoutonActivation( this->m_cadre[ i ] ) == SDL_BUTTON_RIGHT ] = caseGrille.m_idTileset;

												// Definir position
												NDEFINIR_POSITION( ( (BBloc**)Projet_Commun_Carte_BCarte_ObtenirCases( this->m_carte ) )[ position.x ][ position.y ].m_positionTileset[ NLib_Module_SDL_Bouton_NBouton_ObtenirBoutonActivation( this->m_cadre[ i ] ) == SDL_BUTTON_RIGHT ],
													caseGrille.m_position.x,
													caseGrille.m_position.y );
												break;

											case BTYPE_EDITION_TYPE_BLOC:
												// Definir le type de bloc
												if( NLib_Module_SDL_Bouton_NBouton_ObtenirBoutonActivation( this->m_cadre[ i ] ) == SDL_BUTTON_LEFT )
													( (BBloc**)Projet_Commun_Carte_BCarte_ObtenirCases( this->m_carte ) )[ position.x ][ position.y ].m_type = caseGrille.m_typeBloc;
												break;

											default:
												break;
										}
										break;

									case BTYPE_EDITION_POSITION_DEPART:
										// Bouton gauche
										if( NLib_Module_SDL_Bouton_NBouton_ObtenirBoutonActivation( this->m_cadre[ i ] ) == SDL_BUTTON_LEFT )
										{
											// Definir
											Projet_Commun_Carte_BCarte_DefinirPositionDepart( this->m_carte,
												this->m_choixPositionDepartJoueurChoix,
												position );

											// Position suivante
											if( this->m_choixPositionDepartJoueurChoix < BNOMBRE_MAXIMUM_JOUEUR - 1 )
												this->m_choixPositionDepartJoueurChoix++;
											else
												this->m_choixPositionDepartJoueurChoix = 0;

											// Remettre a zero
											NLib_Module_SDL_Bouton_NBouton_RemettreEtatAZero( this->m_cadre[ i ] );
										}
										break;

									default:
										break;
								}
								break;


							case SDL_BUTTON_MIDDLE:
								switch( this->m_typeEdition )
								{
									case BTYPE_EDITION_TILESET_PINCEAU:
										// Recuperer la position dans le tileset
										position2 = Projet_EditeurMap_Grille_ChercherPositionCaseTileset( this->m_grilleTileset,
											Projet_Commun_Carte_BCarte_ObtenirCases( this->m_carte )[ position.x ][ position.y ].m_tileset[ BCOUCHE_CARTE_UN ],
											Projet_Commun_Carte_BCarte_ObtenirCases( this->m_carte )[ position.x ][ position.y ].m_positionTileset[ BCOUCHE_CARTE_UN ] );

										// Recuperer le bloc
										if( position2.x != BBLOC_CARTE_AUCUN
											&& position2.y != BBLOC_CARTE_AUCUN )
											this->m_selectionBloc[ BCOUCHE_CARTE_UN ] = position2;
										break;

									case BTYPE_EDITION_TYPE_BLOC:
										// Recuperer la position dans le tileset
										position2 = Projet_EditeurMap_Grille_ChercherPositionCaseTypeBloc( this->m_grilleTypeBloc,
											Projet_Commun_Carte_BCarte_ObtenirCases( this->m_carte )[ position.x ][ position.y ].m_type );

										// Recuperer le bloc
										if( position2.x != BBLOC_CARTE_AUCUN
											&& position2.y != BBLOC_CARTE_AUCUN )
											this->m_selectionTypeBloc = position2;
										break;

									default:
										break;
								}

								// Update
								Projet_EditeurMap_BEditeur_UpdateCadreSelectionBlocInterne( this );
								break;
						}
				}
				break;

			case BLISTE_CADRE_TYPE_BLOC:
				// Si clic sur le cadre
				if( NLib_Module_SDL_Bouton_NBouton_ObtenirEtat( this->m_cadre[ BLISTE_CADRE_TYPE_BLOC ] ) == NETAT_BOUTON_PRESSE )
				{
					// Calculer la case du clic
					NDEFINIR_POSITION( position,
						Projet_EditeurMap_Grille_BGrille_ObtenirTaille( this->m_grilleTypeBloc ).x - ( ( NLib_Module_SDL_NFenetre_ObtenirResolution( this->m_fenetre )->x - (NU32)this->m_positionSouris.x ) / BTAILLE_CASE_TILESET.x ) - 1,
						( NLib_Module_SDL_Bouton_NBouton_ObtenirPosition( this->m_cadre[ BLISTE_CADRE_TYPE_BLOC ] ).y - this->m_positionSouris.y ) / BTAILLE_CASE_TILESET.y );

					// Corriger la selection
					NDEFINIR_POSITION( this->m_selectionTypeBloc,
						( position.x >= (NS32)Projet_EditeurMap_Grille_BGrille_ObtenirTaille( this->m_grilleTypeBloc ).x - 1 ?
							(NS32)Projet_EditeurMap_Grille_BGrille_ObtenirTaille( this->m_grilleTypeBloc ).x - 1
							: position.x ),
						( position.y >= (NS32)Projet_EditeurMap_Grille_BGrille_ObtenirTaille( this->m_grilleTypeBloc ).y - 1 ?
							(NS32)Projet_EditeurMap_Grille_BGrille_ObtenirTaille( this->m_grilleTypeBloc ).y - 1
							: position.y ) );

					// Type d'edition bloc
					this->m_typeEdition = BTYPE_EDITION_TYPE_BLOC;

					// Mettre a jour curseur
					Projet_EditeurMap_BEditeur_UpdateCurseurInterne( this );
				}
				break;

			case BLISTE_CADRE_TILESET:
				switch( e->type )
				{
					// Deplacement molette
					case SDL_MOUSEWHEEL:
						if( NLib_Module_SDL_Bouton_NBouton_ObtenirEtat( this->m_cadre[ i ] ) == NETAT_BOUTON_SURVOLE )
						{
							// Calculer le maximum pour le scrolling
							maximumScroll = (NS32)Projet_EditeurMap_Grille_BGrille_ObtenirTaille( this->m_grilleTileset ).y
								- ( NLib_Module_SDL_Bouton_NBouton_ObtenirTaille( this->m_cadre[ i ] ).y / BTAILLE_CASE_TILESET.y );

							// On veut monter
							if( e->wheel.y < 0 )
							{
								if( (NS32)this->m_scroll[ BLISTE_CADRE_TILESET ].x - e->wheel.y >= (NS32)maximumScroll )
									this->m_scroll[ BLISTE_CADRE_TILESET ].x = maximumScroll;
								else
									this->m_scroll[ BLISTE_CADRE_TILESET ].x -= e->wheel.y;
							}
							// On veut descendre
							else
							{
								if( (NS32)this->m_scroll[ BLISTE_CADRE_TILESET ].x - e->wheel.y <= 0 )
									this->m_scroll[ BLISTE_CADRE_TILESET ].x = 0;
								else
									this->m_scroll[ BLISTE_CADRE_TILESET ].x -= e->wheel.y;
							}

							// Mettre a jour la position
							Projet_EditeurMap_BEditeur_UpdateCadreSelectionBlocInterne( this );
						}
						break;

					default:
						break;
				}

				// Pression sur le cadre
				if( NLib_Module_SDL_Bouton_NBouton_ObtenirEtat( this->m_cadre[ i ] ) == NETAT_BOUTON_PRESSE )
				{
					// Si le bouton correspond a un bouton de choix de couche
					switch( NLib_Module_SDL_Bouton_NBouton_ObtenirBoutonActivation( this->m_cadre[ i ] ) )
					{
						case SDL_BUTTON_LEFT:
						case SDL_BUTTON_RIGHT:
							// Recuperer la case
							NDEFINIR_POSITION( position,
								Projet_EditeurMap_Grille_BGrille_ObtenirTaille( this->m_grilleTileset ).x - ( ( NLib_Module_SDL_NFenetre_ObtenirResolution( this->m_fenetre )->x - (NU32)this->m_positionSouris.x ) / BTAILLE_CASE_TILESET.x ) - 1,
								( (NS32)this->m_scroll[ BLISTE_CADRE_TILESET ].x * (NS32)BTAILLE_CASE_TILESET.y + (NU32)this->m_positionSouris.y ) / BTAILLE_CASE_TILESET.y );

							// Arranger la selection
							NDEFINIR_POSITION( this->m_selectionBloc[ NLib_Module_SDL_Bouton_NBouton_ObtenirBoutonActivation( this->m_cadre[ i ] ) == SDL_BUTTON_RIGHT ],
								( position.x >= (NS32)BDIMENSION_HORIZONTALE_GRILLE_EDITEUR - 1 ?
									(NS32)BDIMENSION_HORIZONTALE_GRILLE_EDITEUR - 1
									: position.x ),
								( position.y >= (NS32)Projet_EditeurMap_Grille_BGrille_ObtenirTaille( this->m_grilleTileset ).y - 1 ?
									(NS32)Projet_EditeurMap_Grille_BGrille_ObtenirTaille( this->m_grilleTileset ).y - 1
									: position.y ) );

							// Recreer le cadre de selection
							Projet_EditeurMap_BEditeur_UpdateCadreSelectionBlocInterne( this );
							break;

						default:
							break;
					}

					// Changer de mode si on est pas en mode bloc de remplissage
					switch( this->m_typeEdition )
					{
						case BTYPE_EDITION_BLOC_REMPLISSAGE:
							// Recuperer la case de la grille
							caseGrille = Projet_EditeurMap_Grille_BGrille_ObtenirCase( this->m_grilleTileset )
								[ this->m_selectionBloc[ BCOUCHE_CARTE_UN ].x ]
								[ this->m_selectionBloc[ BCOUCHE_CARTE_UN ].y ];

							// Enregistrer
							Projet_Commun_Carte_BCarte_DefinirBlocRemplissage( this->m_carte,
								caseGrille.m_idTileset,
								caseGrille.m_position );
							break;
						case BTYPE_EDITION_BLOC_BORDURE:
							// Recuperer la case de la grille
							caseGrille = Projet_EditeurMap_Grille_BGrille_ObtenirCase( this->m_grilleTileset )
								[ this->m_selectionBloc[ BCOUCHE_CARTE_UN ].x ]
								[ this->m_selectionBloc[ BCOUCHE_CARTE_UN ].y ];

							// Enregistrer
							Projet_Commun_Carte_BCarte_DefinirBlocBordure( this->m_carte,
								caseGrille.m_idTileset,
								caseGrille.m_position );
							break;

						default:
							this->m_typeEdition = BTYPE_EDITION_TILESET_PINCEAU;
							break;
					}

					// Mettre a jour curseur
					Projet_EditeurMap_BEditeur_UpdateCurseurInterne( this );
				}
				break;

			default:
				break;
		}
	}

	// Mettre a jour les ressources
	Projet_Commun_Ressource_BRessource_Update( this->m_ressource );
}

/* Afficher la carte (privee) */
void Projet_EditeurMap_BEditeur_AfficherCarteInterne( BEditeur *this )
{
	// Iterateurs
	NU32 i = 0,
		j;

	// Case survolee
	NUPoint caseSurvolee;

	// Type bloc
	BTypeBloc typeBloc;

	// Afficher carte
	for( ; i < BCOUCHES_CARTE; i++ )
	{
		// Afficher couche
		Projet_Commun_Carte_Affichage_Afficher( this->m_carte,
			i,
			Projet_Commun_Ressource_BRessource_ObtenirEnsembleTileset( this->m_ressource ),
			(NSPoint){ 0, 0 },
			1 );

		// Afficher supplement
		switch( this->m_typeEdition )
		{
			case BTYPE_EDITION_BLOC_REMPLISSAGE:
				if( i == BCOUCHE_CARTE_UN )
					Projet_Commun_Carte_Affichage_AfficherRemplissageComplet( this->m_carte,
						Projet_Commun_Ressource_BRessource_ObtenirEnsembleTileset( this->m_ressource ),
						(NSPoint){ 0, 0 },
						1 );
				break;

			default:
				break;
		}
	}

	// Afficher les supplements
	switch( this->m_typeEdition )
	{
		default:
		case BTYPE_EDITION_BLOC_BORDURE:
		case BTYPE_EDITION_BLOC_REMPLISSAGE:
		case BTYPE_EDITION_TILESET_PINCEAU:
			break;

		case BTYPE_EDITION_POSITION_DEPART:
			break;

		case BTYPE_EDITION_TYPE_BLOC:
			// Afficher
			for( i = 0; i < Projet_Commun_Carte_BCarte_ObtenirTaille( this->m_carte )->x; i++ )
				for( j = 0; j < Projet_Commun_Carte_BCarte_ObtenirTaille( this->m_carte )->y; j++ )
				{
					// Recuperer le type du bloc
					typeBloc = Projet_Commun_Carte_BCarte_ObtenirCases( this->m_carte )[ i ][ j ].m_type;

					// Definir la couleur
					NLib_Module_SDL_NCadre_DefinirCouleur( this->m_cadreBlocCarte,
						BCOULEUR_TYPE_BLOC_EDITEUR[ typeBloc ].r,
						BCOULEUR_TYPE_BLOC_EDITEUR[ typeBloc ].g,
						BCOULEUR_TYPE_BLOC_EDITEUR[ typeBloc ].b,
						BCOULEUR_TYPE_BLOC_EDITEUR[ typeBloc ].a );
					NLib_Module_SDL_NCadre_DefinirCouleurFond( this->m_cadreBlocCarte,
						BCOULEUR_TYPE_BLOC_EDITEUR[ typeBloc ].r,
						BCOULEUR_TYPE_BLOC_EDITEUR[ typeBloc ].g,
						BCOULEUR_TYPE_BLOC_EDITEUR[ typeBloc ].b,
						BCOULEUR_TYPE_BLOC_EDITEUR[ typeBloc ].a );

					// Definir la position
					NLib_Module_SDL_NCadre_DefinirPosition( this->m_cadreBlocCarte,
						i * BTAILLE_CASE_TILESET.x,
						j * BTAILLE_CASE_TILESET.y );

					// Dessiner
					NLib_Module_SDL_NCadre_Dessiner( this->m_cadreBlocCarte );
				}

			// Restaurer les contours
			NLib_Module_SDL_NCadre_DefinirCouleur( this->m_cadreBlocCarte,
				0xFF,
				0xFF,
				0xFF,
				0xFF );
			break;
	}

	// Si on edite une carte
	if( this->m_estEditionEnCours )
		// Afficher la case survolee
		if( NLib_Module_SDL_Bouton_NBouton_ObtenirEtat( this->m_cadre[ BLISTE_CADRE_CARTE ] ) == NETAT_BOUTON_SURVOLE )
		{
			// Calculer la case survolee
			NDEFINIR_POSITION( caseSurvolee,
				(NU32)this->m_positionSouris.x / BTAILLE_CASE_TILESET.x,
				(NU32)this->m_positionSouris.y / BTAILLE_CASE_TILESET.y );

			// Si la case survolee appartient a la carte
			if( caseSurvolee.x < Projet_Commun_Carte_BCarte_ObtenirTaille( this->m_carte )->x
				&& caseSurvolee.y < Projet_Commun_Carte_BCarte_ObtenirTaille( this->m_carte )->y )
			{
				// Definir la position
				NLib_Module_SDL_NCadre_DefinirPosition( this->m_cadreBlocCarte,
					caseSurvolee.x * BTAILLE_CASE_TILESET.x,
					caseSurvolee.y * BTAILLE_CASE_TILESET.y );

				// Definir la couleur
				NLib_Module_SDL_NCadre_DefinirCouleur( this->m_cadreBlocCarte,
					0xFF,
					0x00,
					0x00,
					0xFF );
				NLib_Module_SDL_NCadre_DefinirCouleurFond( this->m_cadreBlocCarte,
					0x00,
					0x00,
					0x00,
					0x00 );

				// Dessiner
				NLib_Module_SDL_NCadre_Dessiner( this->m_cadreBlocCarte );
			}
		}

	// Afficher le cadre de contour
	NLib_Module_SDL_NCadre_Dessiner( this->m_cadreCarte );

	// Afficher les positions de depart
	if( this->m_typeEdition == BTYPE_EDITION_POSITION_DEPART )
	{
		// Definir la couleur
		NLib_Module_SDL_NCadre_DefinirCouleur( this->m_cadreBlocCarte,
			0xFF,
			0xFF,
			0xFF,
			0xFF );
		NLib_Module_SDL_NCadre_DefinirCouleurFond( this->m_cadreBlocCarte,
			0x00,
			0x00,
			0x00,
			0x00 );

		for( i = 0; i < BNOMBRE_MAXIMUM_JOUEUR; i++ )
		{
			// Definir position
			NLib_Module_SDL_NCadre_DefinirPosition( this->m_cadreBlocCarte,
				Projet_Commun_Carte_BCarte_ObtenirPositionDepart( this->m_carte,
					i )->x * BTAILLE_CASE_TILESET.x,
				Projet_Commun_Carte_BCarte_ObtenirPositionDepart( this->m_carte,
					i )->y * BTAILLE_CASE_TILESET.y );

			// Dessiner
			NLib_Module_SDL_NCadre_Dessiner( this->m_cadreBlocCarte );

			// Afficher la surface correspondante
				// Definir position
					NLib_Module_SDL_Surface_NSurface_DefinirPosition( this->m_surfacePositionJoueur[ i ],
						( Projet_Commun_Carte_BCarte_ObtenirPositionDepart( this->m_carte,
							i )->x * BTAILLE_CASE_TILESET.x ) + ( BTAILLE_CASE_TILESET.x / 2 - NLib_Module_SDL_Surface_NSurface_ObtenirTaille( this->m_surfacePositionJoueur[ i ] )->x / 2 ),
						Projet_Commun_Carte_BCarte_ObtenirPositionDepart( this->m_carte,
							i )->y * BTAILLE_CASE_TILESET.y  + ( BTAILLE_CASE_TILESET.y / 2 - NLib_Module_SDL_Surface_NSurface_ObtenirTaille( this->m_surfacePositionJoueur[ i ] )->y / 2 ) );
				// Afficher
					NLib_Module_SDL_Surface_NSurface_Afficher( this->m_surfacePositionJoueur[ i ] );
		}
	}
}

/* Afficher l'editeur (privee) */
void Projet_EditeurMap_BEditeur_AfficherInterne( BEditeur *this )
{
	// Iterateur
	NU32 i;

	// Nettoyer la fenetre
	NLib_Module_SDL_NFenetre_Nettoyer( this->m_fenetre );

	// Afficher les cadres
	for( i = 0; i < BLISTE_CADRES; i++ )
		NLib_Module_SDL_Bouton_NBouton_Dessiner( this->m_cadre[ i ] );

	// Afficher la grille tileset
	Projet_EditeurMap_BEditeur_AfficherGrilleTilesetInterne( this );

	// Afficher la grille type de blocs
	Projet_EditeurMap_BEditeur_AfficherGrilleTypeBlocInterne( this );

	// Afficher la carte
	if( this->m_estEditionEnCours )
		Projet_EditeurMap_BEditeur_AfficherCarteInterne( this );

	// Mettre a jour l'ecran
	NLib_Module_SDL_NFenetre_Update( this->m_fenetre );
}

/* Editeur */
NBOOL Projet_EditeurMap_BEditeur_Editer( BEditeur *this )
{
	// Evenement
	SDL_Event *e;

	// Recuperer l'handle de l'evenement
	e = NLib_Module_SDL_NFenetre_ObtenirEvenement( this->m_fenetre );

	// Charger
	if( !Projet_EditeurMap_BEditeur_Charger( this,
		1 ) )
		// Creer une map de test
		Projet_EditeurMap_BEditeur_CreerCarte( this,
			(NU32)~0 - 1,
			"Premiere",
			(NUPoint){ 17, 17 } );

	// Boucle principale
	do
	{
		// Vider le type
		e->type = SDL_FIRSTEVENT;

		// Recuperer evenement
		SDL_PollEvent( NLib_Module_SDL_NFenetre_ObtenirEvenement( this->m_fenetre ) );

		// Traiter evenement
		switch( e->type )
		{
			case SDL_QUIT:
				break;

			case SDL_MOUSEMOTION:
				NDEFINIR_POSITION( this->m_positionSouris,
					e->motion.x,
					e->motion.y );
				break;

			case SDL_KEYDOWN:
				switch( e->key.keysym.sym )
				{
					// Changer la position de depart des joueurs/arreter la musique
					case SDLK_s:
						if( Projet_Commun_Ressource_BRessource_EstLectureMusique( this->m_ressource ) )
							// Arreter la musique
							Projet_Commun_Ressource_BRessource_ArreterMusique( this->m_ressource );
						else
						{
							// Enregistrer
							this->m_typeEdition = BTYPE_EDITION_POSITION_DEPART;
							this->m_choixPositionDepartJoueurChoix = 0;

							// Mettre a jour curseur
							Projet_EditeurMap_BEditeur_UpdateCurseurInterne( this );
						}
						break;

					// Passer en mode gomme
					case SDLK_e:
						// Enregistrer
						this->m_typeEdition = BTYPE_EDITION_TILESET_GOMME;

						// Mettre a jour curseur
						Projet_EditeurMap_BEditeur_UpdateCurseurInterne( this );
						break;

					// Passer en mode pinceau
					case SDLK_p:
						// Enregistrer
						this->m_typeEdition = BTYPE_EDITION_TILESET_PINCEAU;

						// Mettre a jour curseur
						Projet_EditeurMap_BEditeur_UpdateCurseurInterne( this );
						break;

					case SDLK_b:
						// Enregistrer
						this->m_typeEdition = ( this->m_typeEdition == BTYPE_EDITION_BLOC_REMPLISSAGE ) ?
							BTYPE_EDITION_BLOC_BORDURE
							: BTYPE_EDITION_BLOC_REMPLISSAGE;

						// Mettre a jour curseur
						Projet_EditeurMap_BEditeur_UpdateCurseurInterne( this );
						break;

					case SDLK_m:
						// Si on ne lit pas
						if( !Projet_Commun_Ressource_BRessource_EstLectureMusique( this->m_ressource ) )
							Projet_Commun_Ressource_BRessource_LireMusique( this->m_ressource,
								Projet_Commun_Carte_BCarte_ObtenirMusique( this->m_carte ) );
						// On lit deja
						else
						{
							// Definir nouvelle musique
							Projet_Commun_Carte_BCarte_DefinirMusique( this->m_carte,
								( Projet_Commun_Carte_BCarte_ObtenirMusique( this->m_carte ) + 1 >= BLISTE_MUSIQUES ) ?
									(BListeMusique)0
									: Projet_Commun_Carte_BCarte_ObtenirMusique( this->m_carte ) + 1 );

							// Lire musique
							Projet_Commun_Ressource_BRessource_LireMusique( this->m_ressource,
								Projet_Commun_Carte_BCarte_ObtenirMusique( this->m_carte ) );
						}

						// Afficher la musique
						printf( "Musique en cours: %d\n", Projet_Commun_Carte_BCarte_ObtenirMusique( this->m_carte ) );
						break;
					default:
						break;
				}
				break;

			default:
				break;
		}

		// Mettre a jour
		Projet_EditeurMap_BEditeur_MettreAJourInterne( this );

		// Afficher
		Projet_EditeurMap_BEditeur_AfficherInterne( this );
	} while( this->m_estEnCours );

	// OK
	return NTRUE;
}

/* Preparer l'editeur apres la fin de la creation de la carte (privee) */
void Projet_EditeurMap_BEditeur_FinaliserPreparationEditeurCarteInterne( BEditeur *this )
{
	// Liberer
	if( this->m_cadreCarte != NULL )
		NLib_Module_SDL_NCadre_Detruire( &this->m_cadreCarte );

	// Creer le cadre entourant la carte
	if( !( this->m_cadreCarte = NLib_Module_SDL_NCadre_Construire3( 0,
		0,
		Projet_Commun_Carte_BCarte_ObtenirTaille( this->m_carte )->x * BTAILLE_CASE_TILESET.x,
		Projet_Commun_Carte_BCarte_ObtenirTaille( this->m_carte )->y * BTAILLE_CASE_TILESET.y,
		(NCouleur){ 0xFF, 0x00, 0x00, 0xFF },
		(NCouleur){ 0x00, 0x00, 0x00, 0x00 },
		this->m_fenetre,
		1 ) ) )
	{
		// Notifier
		NOTIFIER_ERREUR( NERREUR_CONSTRUCTOR_FAILED );

		// Detruire la carte
		Projet_Commun_Carte_BCarte_Detruire( &this->m_carte );

		// Quitter
		return;
	}

	// On est desormais en cours d'edition
	this->m_estEditionEnCours = NTRUE;
}

/* Creer map */
void Projet_EditeurMap_BEditeur_CreerCarte( BEditeur *this,
	NU32 handle,
	const char *nom,
	NUPoint taille )
{
	// Liberer
	if( this->m_estEditionEnCours )
	{
		// Detruire ancienne map
		Projet_Commun_Carte_BCarte_Detruire( &this->m_carte );

		// Detruire cadre
		NLib_Module_SDL_NCadre_Detruire( &this->m_cadreCarte );

		// Plus d'edition
		this->m_estEditionEnCours = NFALSE;
	}

	// Creer
	if( !( this->m_carte = Projet_Commun_Carte_BCarte_Construire( handle,
		nom,
		taille ) ) )
	{
		// Notifier
		NOTIFIER_ERREUR( NERREUR_CONSTRUCTOR_FAILED );

		// Quitter
		return;
	}

	// Preparer l'editeur
	Projet_EditeurMap_BEditeur_FinaliserPreparationEditeurCarteInterne( this );
}

/* Creer lien vers carte editeur (privee) */
void Projet_EditeurMap_BEditeur_CreerLienVersCarteEditeurInterne( __OUTPUT char lien[ MAX_PATH ],
	NU32 handle )
{
	// Creer le lien
	sprintf( lien,
		"%s/%s%d.%s",
		BREPERTOIRE_CARTE_EDITEUR,
		BBASE_NOM_FICHIER_CARTE,
		handle,
		BEXTENSION_FICHIER_CARTE );
}

/* Charger */
NBOOL Projet_EditeurMap_BEditeur_Charger( BEditeur *this,
	NU32 handle )
{
	// Lien
	char lien[ MAX_PATH ];

	// Creer le lien
	Projet_EditeurMap_BEditeur_CreerLienVersCarteEditeurInterne( lien,
		handle );

	// Liberer
	if( this->m_estEditionEnCours )
	{
		// Detruire ancienne map
		Projet_Commun_Carte_BCarte_Detruire( &this->m_carte );

		// Detruire cadre
		NLib_Module_SDL_NCadre_Detruire( &this->m_cadreCarte );

		// Plus d'edition
		this->m_estEditionEnCours = NFALSE;
	}

	// Charger
	if( !( this->m_carte = Projet_Commun_Carte_BCarte_Construire2( lien ) ) )
	{
		// Notifier
		NOTIFIER_ERREUR( NERREUR_CONSTRUCTOR_FAILED );

		// Quitter
		return NFALSE;
	}

	// Preparer l'editeur
	Projet_EditeurMap_BEditeur_FinaliserPreparationEditeurCarteInterne( this );

	// OK
	return NTRUE;
}

/* Sauvegarder */
NBOOL Projet_EditeurMap_BEditeur_Sauvegarder( const BEditeur *this )
{
	// Lien
	char lien[ MAX_PATH ];

	// Verifier si edition en cours
	if( !this->m_estEditionEnCours )
	{
		// Notifier
		NOTIFIER_AVERTISSEMENT( NERREUR_OBJECT_STATE );

		// Quitter
		return NFALSE;
	}

	// Creer le dossier
	NLib_Module_Repertoire_CreerRepertoire( BREPERTOIRE_CARTE_EDITEUR );

	// Creer le lien
	Projet_EditeurMap_BEditeur_CreerLienVersCarteEditeurInterne( lien,
		Projet_Commun_Carte_BCarte_ObtenirHandle( this->m_carte ) );

	// OK
	return Projet_Commun_Carte_BCarte_Sauvegarder( this->m_carte,
		lien );
}

/* Arreter l'edition */
void Projet_EditeurMap_BEditeur_Arreter( BEditeur *this )
{
	this->m_estEnCours = NFALSE;
}

/* Changer nom carte */
void Projet_EditeurMap_BEditeur_ChangerNomCarte( BEditeur *this,
	const char *nom )
{
	// Verifier
	if( !this->m_estEditionEnCours )
		return;

	// Modifier
	Projet_Commun_Carte_BCarte_DefinirNom( this->m_carte,
		nom );
}

/* Changer taille carte */
void Projet_EditeurMap_BEditeur_ChangerTailleCarte( BEditeur *this,
	NUPoint taille )
{
	// Verifier
	if( !this->m_estEditionEnCours )
		return;

	// Modifier
	Projet_Commun_Carte_BCarte_DefinirTaille( this->m_carte,
		taille );

	// Recreer le cadre
	Projet_EditeurMap_BEditeur_FinaliserPreparationEditeurCarteInterne( this );
}

/* Afficher informations sur la carte */
void Projet_EditeurMap_BEditeur_AfficherInfosCarte( const BEditeur *this )
{
	if( this->m_estEditionEnCours )
	{
		// Afficher
		printf( "Carte %d \"%s\" de taille %dx%d\n",
			Projet_Commun_Carte_BCarte_ObtenirHandle( this->m_carte ),
			Projet_Commun_Carte_BCarte_ObtenirNom( this->m_carte ),
			Projet_Commun_Carte_BCarte_ObtenirTaille( this->m_carte )->x,
			Projet_Commun_Carte_BCarte_ObtenirTaille( this->m_carte )->y );
	}
	else
		// Afficher
		printf( "Aucune carte en cours d'edition.\n" );
}

/* Est en cours? */
NBOOL Projet_EditeurMap_BEditeur_EstEnCours( const BEditeur *this )
{
	return this->m_estEnCours;
}

