#include "../../../../../include/Projet/Projet.h"

// -----------------------------------------------------------
// struct Projet::Client::Monde::Deplacement::BEtatDeplacement
// -----------------------------------------------------------

/* Construire */
__ALLOC BEtatDeplacement *Projet_Client_Monde_Deplacement_BEtatDeplacement_Construire( const struct BClient *client )
{
	// Sortie
	__OUTPUT BEtatDeplacement *out;

	// Allouer la memoire
	if( !( out = calloc( 1,
		sizeof( BEtatDeplacement ) ) ) )
	{
		// Notifier
		NOTIFIER_ERREUR( NERREUR_ALLOCATION_FAILED );

		// Quitter
		return NULL;
	}

	// Enregistrer
	out->m_client = client;

	// OK
	return out;
}

/* Detruire */
void Projet_Client_Monde_Deplacement_BEtatDeplacement_Detruire( BEtatDeplacement **this )
{
	// Liberer
	NFREE( (*this) );
}

/* Deplacer */
void Projet_Client_Monde_Deplacement_BEtatDeplacement_Deplacer( BEtatDeplacement *this,
	NDirection direction )
{
	// Joueur
	const BEtatClient *joueur;

	// Proteger cache client
	Projet_Client_BClient_ProtegerCacheClient( (BClient*)this->m_client );

	// Obtenir joueur
	if( !( joueur = Projet_Client_BClient_ObtenirJoueurCourant( (BClient*)this->m_client ) ) )
	{
		// Notifier
		NOTIFIER_ERREUR( NERREUR_OBJECT_STATE );

		// Ne plus proteger cache client
		Projet_Client_BClient_NePlusProtegerCacheClient( (BClient*)this->m_client );

		// Quitter
		return;
	}

	// Verifier etat
	if( !Projet_Commun_Reseau_Client_BEtatClient_EstEnVie( joueur ) )
	{
		// Ne plus proteger cache client
		Projet_Client_BClient_NePlusProtegerCacheClient( (BClient*)this->m_client );

		// Quitter
		return;
	}

	// Ne plus proteger cache client
	Projet_Client_BClient_NePlusProtegerCacheClient( (BClient*)this->m_client );

	// Verifier etat
	if( direction == this->m_direction
		&& this->m_estEnCours )
		return;

	// Enregistrer
	this->m_tempsDebutPressionTouche = NLib_Temps_ObtenirTick( );
	this->m_direction = direction;
	this->m_estEnCours = NTRUE;
}

/* Stopper deplacement */
void Projet_Client_Monde_Deplacement_BEtatDeplacement_StopperDeplacement( BEtatDeplacement *this,
	NDirection direction )
{
	// Arreter
	if( this->m_direction != direction
		|| !this->m_estEnCours )
		return;

	// Il s'agit d'un changement de direction?
	if( NLib_Temps_ObtenirTick( ) - this->m_tempsDebutPressionTouche <= BTEMPS_AVANT_CHANGEMENT_DIRECTION_DEPLACEMENT_CLIENT )
		// Envoyer packet changement de direction
		Projet_Client_TraitementPacket_EnvoyerChangementDirection( (BClient*)this->m_client,
			this->m_direction );

	// Arreter
	this->m_estEnCours = NFALSE;
}

/* Reponse deplacement serveur */
void Projet_Client_Monde_Deplacement_BEtatDeplacement_EnregistrerReponseDeplacementServeur( BEtatDeplacement *this )
{
	this->m_estAttenteReponseDeplacement = NFALSE;
}

/* Est en attente de la reponse pour le deplacement? */
NBOOL Projet_Client_Monde_Deplacement_BEtatDeplacement_EstAttenteReponseServeur( const BEtatDeplacement *this )
{
	return this->m_estAttenteReponseDeplacement;
}

/* Update */
void Projet_Client_Monde_Deplacement_BEtatDeplacement_Update( BEtatDeplacement *this )
{
	// Etat carte
	const BEtatCarte *etatCarte;

	// Est deplacement impossible?
	NBOOL estDeplacementImpossible = NFALSE;

	// Carte
	const BCarte *carte;

	// Cases etat
	const BCaseEtatCarte **caseEtat;

	// Joueur courant
	const BEtatClient *joueur;

	// Future position
	NSPoint futurePosition;

	// Verifier etat
	if( !this->m_estEnCours
		|| this->m_estAttenteReponseDeplacement )
		return;

	// Obtenir etat carte
	if( !( etatCarte = Projet_Client_BClient_ObtenirEtatCarte( this->m_client ) )
		|| !( carte = Projet_Commun_Carte_Etat_BEtatCarte_ObtenirCarte( etatCarte ) ) )
		return;

	// Proteger le cache
	Projet_Client_BClient_ProtegerCacheClient( (BClient*)this->m_client );

	// Obtenir joueur
	if( !( joueur = Projet_Client_BClient_ObtenirJoueurCourant( this->m_client ) ) )
	{
		Projet_Client_BClient_NePlusProtegerCacheClient( (BClient*)this->m_client );
		return;
	}

	// Verifier qu'il ne soit pas en deplacement
	if( Projet_Commun_Reseau_Client_BEtatDeplacementClient_EstDeplacementActif( Projet_Commun_Reseau_Client_BEtatClient_ObtenirEtatDeplacement( joueur ) ) )
	{
		Projet_Client_BClient_NePlusProtegerCacheClient( (BClient*)this->m_client );
		return;
	}

	// Copier position
	futurePosition = *Projet_Commun_Reseau_Client_BEtatClient_ObtenirPosition( joueur );

	// Calculer future position
	switch( this->m_direction )
	{
		case NHAUT:
			futurePosition.y--;
			break;
		case NBAS:
			futurePosition.y++;
			break;
		case NGAUCHE:
			futurePosition.x--;
			break;
		case NDROITE:
			futurePosition.x++;
			break;

		default:
			break;
	}

	// Obtenir cases
	if( !( caseEtat = Projet_Commun_Carte_Etat_BEtatCarte_ObtenirCase( etatCarte ) ) )
	{
		// Notifier
		NOTIFIER_ERREUR( NERREUR_OBJECT_STATE );

		// Ne plus proteger
		Projet_Client_BClient_NePlusProtegerCacheClient( (BClient*)this->m_client );

		// Quitter
		return;
	}

	// Verifier que le joueur puisse passer
		// Depasse les limites de la carte?
			if( !Projet_Commun_Carte_BCarte_EstPositionCorrecte( carte,
				futurePosition ) )
				estDeplacementImpossible = NTRUE;
		// Va vers une case avec colision/remplie
			else if( caseEtat[ futurePosition.x ][ futurePosition.y ].m_estRempli
				|| Projet_Commun_Carte_BCarte_ObtenirCases( carte )[ futurePosition.x ][ futurePosition.y ].m_type == BTYPE_BLOC_SOLIDE )
				estDeplacementImpossible = NTRUE;
			else if( caseEtat[ futurePosition.x ][ futurePosition.y ].m_etatBombe != NULL )
				estDeplacementImpossible = NTRUE;

	// Impossible d'aller a cet endroit
	if( estDeplacementImpossible )
	{
		// Changement de direction si differente
		if( Projet_Commun_Reseau_Client_BEtatClient_ObtenirDirection( joueur ) != this->m_direction )
			Projet_Client_TraitementPacket_EnvoyerChangementDirection( (BClient*)this->m_client,
				this->m_direction );

		// Arret du deplacement
		this->m_estEnCours = NFALSE;

		// Unlock cache
		Projet_Client_BClient_NePlusProtegerCacheClient( (BClient*)this->m_client );

		// Quitter
		return;
	}

	// Il s'agit d'un deplacement (et pas d'un changement de direction)
	if( Projet_Commun_Reseau_Client_BEtatClient_ObtenirDirection( joueur ) == this->m_direction
		|| NLib_Temps_ObtenirTick( ) - this->m_tempsDebutPressionTouche > BTEMPS_AVANT_CHANGEMENT_DIRECTION_DEPLACEMENT_CLIENT )
	{
		// Envoyer packet changement de position
		Projet_Client_TraitementPacket_EnvoyerChangementPosition( (BClient*)this->m_client,
			this->m_direction );

		// On attend la reponse
		this->m_estAttenteReponseDeplacement = NTRUE;

		// Demarrer le mouvement
		Projet_Commun_Reseau_Client_BEtatDeplacementClient_Deplacer( (BEtatDeplacementClient*)Projet_Commun_Reseau_Client_BEtatClient_ObtenirEtatDeplacement( joueur ),
			*Projet_Commun_Reseau_Client_BEtatClient_ObtenirPosition( joueur ),
			this->m_direction );

		// Enregistrer temps
		this->m_tempsDebutPressionTouche = NLib_Temps_ObtenirTick( );
	}

	// Ne plus proteger le cache
	Projet_Client_BClient_NePlusProtegerCacheClient( (BClient*)this->m_client );
}

