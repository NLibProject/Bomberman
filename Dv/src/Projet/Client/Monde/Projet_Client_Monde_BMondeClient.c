#include "../../../../include/Projet/Projet.h"

// ------------------------------------------
// struct Projet::Client::Monde::BMondeClient
// ------------------------------------------

/* Construire */
__ALLOC BMondeClient *Projet_Client_Monde_BMondeClient_Construire( struct BClient *client )
{
	// Sortie
	__OUTPUT BMondeClient *out;

	// Allouer la memoire
	if( !( out = calloc( 1,
		sizeof( BMondeClient ) ) ) )
	{
		// Notifier
		NOTIFIER_ERREUR( NERREUR_ALLOCATION_FAILED );

		// Quitter
		return NULL;
	}
	
	// Enregistrer
	if( !( out->m_client = client )
		|| !( out->m_monde = Projet_Client_BClient_ObtenirMonde( client ) )
		|| !( out->m_carte = Projet_Commun_Monde_BMonde_ObtenirCarte( out->m_monde ) )
		|| !( out->m_cacheClient = Projet_Client_BClient_ObtenirCacheClient( out->m_client ) ) )
	{
		// Notifier
		NOTIFIER_ERREUR( NERREUR_OBJECT_STATE );

		// Liberer
		NFREE( out );

		// Quitter
		return NULL;
	}

	// Construire configuration affichage
	if( !( out->m_configurationAffichage = Projet_Client_Monde_BConfigurationAffichageMondeClient_Construire( Projet_Client_BClient_ObtenirFenetre( client ),
		out->m_carte ) ) )
	{
		// Notifier
		NOTIFIER_ERREUR( NERREUR_CONSTRUCTOR_FAILED );

		// Liberer
		NFREE( out );

		// Quitter
		return NULL;
	}

	// Construire gestion message
	if( !( out->m_message = Projet_Client_Monde_Message_BMessageMondeClient_Construire( Projet_Client_BClient_ObtenirFenetre( client ) ) ) )
	{
		// Notifier
		NOTIFIER_ERREUR( NERREUR_CONSTRUCTOR_FAILED );

		// Detruire
		Projet_Client_Monde_BConfigurationAffichageMondeClient_Detruire( &out->m_configurationAffichage );
		NFREE( out );

		// Quitter
		return NULL;
	}

	// Construire l'etat deplacement
	if( !( out->m_etatDeplacement = Projet_Client_Monde_Deplacement_BEtatDeplacement_Construire( out->m_client ) ) )
	{
		// Notifier
		NOTIFIER_ERREUR( NERREUR_CONSTRUCTOR_FAILED );

		// Detruire
		Projet_Client_Monde_Message_BMessageMondeClient_Detruire( &out->m_message );
		Projet_Client_Monde_BConfigurationAffichageMondeClient_Detruire( &out->m_configurationAffichage );
		NFREE( out );

		// Quitter
		return NULL;
	}

	// Construire l'etat de pose de bombe
	if( !( out->m_etatPoseBombe = Projet_Client_Monde_Bombe_BEtatPoseBombe_Construire( out->m_client ) ) )
	{
		// Notifier
		NOTIFIER_ERREUR( NERREUR_CONSTRUCTOR_FAILED );

		// Detruire
		Projet_Client_Monde_Deplacement_BEtatDeplacement_Detruire( &out->m_etatDeplacement );
		Projet_Client_Monde_Message_BMessageMondeClient_Detruire( &out->m_message );
		Projet_Client_Monde_BConfigurationAffichageMondeClient_Detruire( &out->m_configurationAffichage );
		NFREE( out );

		// Quitter
		return NULL;
	}

	// OK
	return out;
}

/* Detruire */
void Projet_Client_Monde_BMondeClient_Detruire( BMondeClient **this )
{
	// Detruire etat pose bombe
	Projet_Client_Monde_Bombe_BEtatPoseBombe_Detruire( &(*this)->m_etatPoseBombe );

	// Detruire etat deplacement
	Projet_Client_Monde_Deplacement_BEtatDeplacement_Detruire( &(*this)->m_etatDeplacement );

	// Detruire message
	Projet_Client_Monde_Message_BMessageMondeClient_Detruire( &(*this)->m_message );

	// Detruire configuration affichage
	Projet_Client_Monde_BConfigurationAffichageMondeClient_Detruire( &(*this)->m_configurationAffichage );

	// Liberer
	NFREE( *this );
}

/* Actualiser (privee) */
void Projet_Client_Monde_BMondeClient_ActualiserInterne( BMondeClient *this )
{
	// Iterateur
	NU32 i = 0;

	// Nettoyer ecran
	NLib_Module_SDL_NFenetre_Nettoyer( (NFenetre*)Projet_Client_BClient_ObtenirFenetre( this->m_client ) );

	// Remplissage
	Projet_Commun_Carte_Affichage_AfficherBordure( this->m_carte,
		Projet_Commun_Ressource_BRessource_ObtenirEnsembleTileset( Projet_Client_BClient_ObtenirRessource( this->m_client ) ),
		*Projet_Client_Monde_BConfigurationAffichageMondeClient_ObtenirBaseAffichage( this->m_configurationAffichage ),
		Projet_Client_Monde_BConfigurationAffichageMondeClient_ObtenirFacteurZoom( this->m_configurationAffichage ),
		Projet_Client_BClient_ObtenirFenetre( this->m_client ) );

	// Dessiner carte
	for( ; i < BCOUCHES_CARTE; i++ )
	{
		// Couche
		Projet_Commun_Carte_Affichage_Afficher( this->m_carte,
			i,
			Projet_Commun_Ressource_BRessource_ObtenirEnsembleTileset( Projet_Client_BClient_ObtenirRessource( this->m_client ) ),
			*Projet_Client_Monde_BConfigurationAffichageMondeClient_ObtenirBaseAffichage( this->m_configurationAffichage ),
			Projet_Client_Monde_BConfigurationAffichageMondeClient_ObtenirFacteurZoom( this->m_configurationAffichage ) );

		// Elements supplementaires
		switch( i )
		{
			case BCOUCHE_CARTE_UN:
				// Blocs remplis
				Projet_Commun_Carte_Affichage_AfficherRemplissageSchema( this->m_carte,
					Projet_Commun_Ressource_BRessource_ObtenirEnsembleTileset( Projet_Client_BClient_ObtenirRessource( this->m_client ) ),
					*Projet_Client_Monde_BConfigurationAffichageMondeClient_ObtenirBaseAffichage( this->m_configurationAffichage ),
					Projet_Client_Monde_BConfigurationAffichageMondeClient_ObtenirFacteurZoom( this->m_configurationAffichage ),
					Projet_Commun_Monde_BMonde_ObtenirEtatCarte( this->m_monde ) );

				// Animations explosion
				Projet_Commun_Carte_Affichage_AfficherExplosion( this->m_carte,
					*Projet_Client_Monde_BConfigurationAffichageMondeClient_ObtenirBaseAffichage( this->m_configurationAffichage ),
					Projet_Client_Monde_BConfigurationAffichageMondeClient_ObtenirFacteurZoom( this->m_configurationAffichage ),
					Projet_Commun_Monde_BMonde_ObtenirEtatCarte( this->m_monde ) );

				// Bombes
				Projet_Commun_Carte_Affichage_AfficherBombe( this->m_carte,
					Projet_Commun_Ressource_BRessource_ObtenirEnsembleAnimation( Projet_Client_BClient_ObtenirRessource( this->m_client ) ),
					*Projet_Client_Monde_BConfigurationAffichageMondeClient_ObtenirBaseAffichage( this->m_configurationAffichage ),
					Projet_Client_Monde_BConfigurationAffichageMondeClient_ObtenirFacteurZoom( this->m_configurationAffichage ),
					Projet_Commun_Monde_BMonde_ObtenirEtatCarte( this->m_monde ) );

				// Bonus
				Projet_Commun_Carte_Affichage_AfficherBonus( this->m_carte,
					Projet_Commun_Ressource_BRessource_ObtenirBonus( Projet_Client_BClient_ObtenirRessource( this->m_client ) ),
					*Projet_Client_Monde_BConfigurationAffichageMondeClient_ObtenirBaseAffichage( this->m_configurationAffichage ),
					Projet_Client_Monde_BConfigurationAffichageMondeClient_ObtenirFacteurZoom( this->m_configurationAffichage ),
					Projet_Commun_Monde_BMonde_ObtenirEtatCarte( this->m_monde ) );

				// Personnages
				Projet_Commun_Personnage_Affichage_Afficher( (BCacheClient*)this->m_cacheClient,
					Projet_Client_Monde_Deplacement_BEtatDeplacement_EstAttenteReponseServeur( this->m_etatDeplacement ),
					Projet_Commun_Ressource_BRessource_ObtenirEnsemblePersonnage( Projet_Client_BClient_ObtenirRessource( this->m_client ) ),
					*Projet_Client_Monde_BConfigurationAffichageMondeClient_ObtenirBaseAffichage( this->m_configurationAffichage ),
					Projet_Client_Monde_BConfigurationAffichageMondeClient_ObtenirFacteurZoom( this->m_configurationAffichage ) );
				break;

			default:
				break;
		}
	}

	// Afficher message
	Projet_Client_Monde_Message_BMessageMondeClient_Afficher( this->m_message );

	// Actualiser ecran
	NLib_Module_SDL_NFenetre_Update( (NFenetre*)Projet_Client_BClient_ObtenirFenetre( this->m_client ) );
}

/* Mettre a jour (privee) */
void Projet_Client_Monde_BMondeClient_MettreAJourInterne( BMondeClient *this,
	SDL_Event *e )
{
	// Touche
	BTouche touche;

	// Client
	BClient *client = this->m_client;

	// Verifier etat connexion
	if( !Projet_Client_BClient_EstConnecte( client )
		|| Projet_Client_BClient_ObtenirNombreJoueur( client ) < BNOMBRE_MINIMUM_JOUEUR )
	{
		// Arreter
		this->m_estEnCours = NFALSE;
		this->m_codeRetour = BCODE_RETOUR_MONDE_CLIENT_RETOUR_MENU_PRINCIPAL;

		// Sortir
		return;
	}

	// Si partie en cours
	if( this->m_estPartieEnCours )
	{
		// Traiter les evenements
		switch( e->type )
		{
			case SDL_KEYDOWN:
				switch( ( touche = Projet_Client_BClient_ObtenirToucheInverse( client,
					e->key.keysym.sym ) ) )
				{
					case BTOUCHE_HAUT:
					case BTOUCHE_BAS:
					case BTOUCHE_GAUCHE:
					case BTOUCHE_DROITE:
						Projet_Client_Monde_Deplacement_BEtatDeplacement_Deplacer( this->m_etatDeplacement,
							(BTouche)( touche - BTOUCHE_HAUT ) );
						break;

					case BTOUCHE_BOMBE:
						Projet_Client_Monde_Bombe_BEtatPoseBombe_PoserBombe( this->m_etatPoseBombe );
						break;

					default:
						break;
				}
				break;

			case SDL_KEYUP:
				switch( ( touche = Projet_Client_BClient_ObtenirToucheInverse( client,
					e->key.keysym.sym ) ) )
				{
					case BTOUCHE_HAUT:
					case BTOUCHE_BAS:
					case BTOUCHE_GAUCHE:
					case BTOUCHE_DROITE:
						Projet_Client_Monde_Deplacement_BEtatDeplacement_StopperDeplacement( this->m_etatDeplacement,
							(BTouche)( touche - BTOUCHE_HAUT ) );
						break;

					case BTOUCHE_BOMBE:
						Projet_Client_Monde_Bombe_BEtatPoseBombe_FinirPoseBombe( this->m_etatPoseBombe );
						break;

					default:
						break;
				}
				break;

			default:
				break;
		}
	}

	// Mettre a jour le monde
	Projet_Commun_Monde_BMonde_Update( (BMonde*)Projet_Client_BClient_ObtenirMonde( client ) );

	// Mettre a jour deplacement
	Projet_Client_Monde_Deplacement_BEtatDeplacement_Update( this->m_etatDeplacement );

	// Mettre a jour message
	Projet_Client_Monde_Message_BMessageMondeClient_Update( this->m_message );

	// Update client
	Projet_Client_BClient_MettreAJour( this->m_client );
}

/* Lancer */
BCodeRetourMondeClient Projet_Client_Monde_BMondeClient_Lancer( BMondeClient *this )
{
	// Client
	BClient *client = this->m_client;

	// Evenement
	SDL_Event *e;

	// Lancer
	this->m_estEnCours = NTRUE;
	this->m_codeRetour = BCODE_RETOUR_MONDE_CLIENT_RETOUR_MENU_PRINCIPAL;

	// Lancer la musique
	Projet_Commun_Ressource_BRessource_LireMusique( Projet_Client_BClient_ObtenirRessource( this->m_client ),
		Projet_Commun_Carte_BCarte_ObtenirMusique( this->m_carte ) );

	// Confirmer le lancement
	if( !Projet_Client_TraitementPacket_EnvoyerLancementEffectue( client ) )
	{
		// Notifier
		NOTIFIER_ERREUR( NERREUR_STREAM_ERROR );

		// Quitter
		return BCODE_RETOUR_MONDE_CLIENT_RETOUR_MENU_PRINCIPAL;
	}

	// Obtenir evenement
	e = NLib_Module_SDL_NFenetre_ObtenirEvenement( Projet_Client_BClient_ObtenirFenetre( client ) );

	// Joueur
	do
	{
		// Mettre a jour evenements
		SDL_PollEvent( e );

		// Analyser evenement
		switch( e->type )
		{
			case SDL_WINDOWEVENT:
				switch( e->window.event )
				{
					case SDL_WINDOWEVENT_CLOSE:
						// Plus en cours
						this->m_estEnCours = NFALSE;

						// On veut quitter
						this->m_codeRetour = BCODE_RETOUR_MONDE_CLIENT_QUITTER;
						break;

					default:
						break;
				}
				break;

			case SDL_KEYDOWN:
				switch( e->key.keysym.sym )
				{
					case SDLK_ESCAPE:
						// Plus en cours
						this->m_estEnCours = NFALSE;

						// Retour au menu principal
						this->m_codeRetour = BCODE_RETOUR_MONDE_CLIENT_RETOUR_MENU_PRINCIPAL;
						break;

					default:
						break;
				}
				break;

			default:
				break;
		}

		// Mettre a jour
		Projet_Client_Monde_BMondeClient_MettreAJourInterne( this,
			e );

		// Actualiser
		Projet_Client_Monde_BMondeClient_ActualiserInterne( this );
	} while( this->m_estEnCours );

	// Quitter
	return this->m_codeRetour;
}

/* Demarrer la partie (sur ordre du serveur */
void Projet_Client_Monde_BMondeClient_DemarrerPartie( BMondeClient *this )
{
	this->m_estPartieEnCours = NTRUE;
}

/* Afficher un message */
NBOOL Projet_Client_Monde_BMondeClient_AfficherMessage( BMondeClient *this,
	const char *message,
	BPoliceMessageClient police,
	NU32 duree,
	NCouleur couleur,
	BPositionAffichageMessageClient position,
	NBOOL estDoitAfficherCadre,
	NCouleur couleurCadre )
{
	return Projet_Client_Monde_Message_BMessageMondeClient_DonnerMessage( this->m_message,
		message,
		duree,
		police,
		couleur,
		position,
		estDoitAfficherCadre,
		couleurCadre );
}

/* Enregistrer reponse suite a un deplacement */
void Projet_Client_Monde_BMondeClient_EnregistrerReponseServeurDeplacement( BMondeClient *this )
{
	Projet_Client_Monde_Deplacement_BEtatDeplacement_EnregistrerReponseDeplacementServeur( this->m_etatDeplacement );
}

/* Enregistrer reponse pose bombe */
void Projet_Client_Monde_BMondeClient_EnregistrerReponseServeurPoseBombe( BMondeClient *this )
{
	Projet_Client_Monde_Bombe_BEtatPoseBombe_EnregistrerReponseServeur( this->m_etatPoseBombe );
}

/* Poser bombe */
NBOOL Projet_Client_Monde_BMondeClient_PoserBombe( BMondeClient *this,
	NSPoint position,
	NU32 identifiantBombe,
	NU32 identifiantJoueur )
{
	// Joueur
	BEtatClient *joueur;

	// Liberer si il s'agit du client courant
	if( identifiantJoueur == Projet_Commun_Reseau_Client_Cache_BCacheClient_ObtenirIdentifiantClientCourant( this->m_cacheClient ) )
		Projet_Client_Monde_BMondeClient_EnregistrerReponseServeurPoseBombe( this );

	// Verifier parametres
	if( !Projet_Commun_Carte_BCarte_EstPositionCorrecte( this->m_carte,
		position ) )
	{
		// Notifier
		NOTIFIER_ERREUR( NERREUR_PARAMETER_ERROR );

		// Quitter
		return NFALSE;
	}

	// Lock le cache
	Projet_Commun_Reseau_Client_Cache_BCacheClient_ProtegerCache( (BCacheClient*)this->m_cacheClient );

	// Obtenir le joueur
	if( !( joueur = (BEtatClient*)Projet_Commun_Reseau_Client_Cache_BCacheClient_ObtenirClient( (BCacheClient*)this->m_cacheClient,
		identifiantJoueur ) ) )
	{
		// Notifier
		NOTIFIER_ERREUR( NERREUR_OBJECT_STATE );

		// Unlock le cache
		Projet_Commun_Reseau_Client_Cache_BCacheClient_NePlusProtegerCache( (BCacheClient*)this->m_cacheClient );

		// Quitter
		return NFALSE;
	}

	// Incrementer nombre de bombes posees
	Projet_Commun_Reseau_Client_BEtatClient_IncrementerNombreBombePosee( joueur );

	// Unlock le cache
	Projet_Commun_Reseau_Client_Cache_BCacheClient_NePlusProtegerCache( (BCacheClient*)this->m_cacheClient );

	// Poser
	return ( Projet_Commun_Carte_Etat_BEtatCarte_PoserBombe2( (BEtatCarte*)Projet_Commun_Monde_BMonde_ObtenirEtatCarte( this->m_monde ),
		position,
		identifiantJoueur,
		0,
		0,
		0,
		Projet_Commun_Ressource_BRessource_ObtenirEnsembleAnimation( Projet_Client_BClient_ObtenirRessource( this->m_client ) ),
		identifiantBombe ) != NERREUR );
}

/* Poser bonus */
NBOOL Projet_Client_Monde_BMondeClient_PoserBonus( BMondeClient *this,
	NSPoint position,
	BListeBonus type,
	NU32 duree,
	NU32 identifiant )
{
	// Verifier parametres
	if( !Projet_Commun_Carte_BCarte_EstPositionCorrecte( this->m_carte,
		position ) )
	{
		// Notifier
		NOTIFIER_ERREUR( NERREUR_PARAMETER_ERROR );

		// Quitter
		return NFALSE;
	}

	// Poser
	return Projet_Commun_Carte_Etat_BEtatCarte_PoserBonus2( (BEtatCarte*)Projet_Commun_Monde_BMonde_ObtenirEtatCarte( this->m_monde ),
		position,
		type,
		duree,
		NLib_Temps_ObtenirTick( ),
		identifiant );
}

/* Supprimer bonus */
NBOOL Projet_Client_Monde_BMondeClient_SupprimerBonus( BMondeClient *this,
	NU32 identifiant )
{
	// Supprimer le bonus
	return Projet_Commun_Carte_Etat_BEtatCarte_DisparaitreBonus( (BEtatCarte*)Projet_Commun_Monde_BMonde_ObtenirEtatCarte( this->m_monde ),
		identifiant );
}

/* Lire son bonus */
void Projet_Client_Monde_BMondeClient_JouerSonBonus( BMondeClient *this,
	NBOOL estJoueurCourant,
	BListeBonus bonus )
{
	// Effet sonore a lancer
	BListeEffetSonore effet;

	// Referencer
	NREFERENCER( estJoueurCourant );

	// Analyser le bonus
	switch( bonus )
	{
		case BLISTE_BONUS_AUGMENTE_PUISSANCE:
			effet = BLISTE_EFFET_SONORE_BM_POWERUP1;
			break;
		case BLISTE_BONUS_DIMINUE_PUISSANCE:
			effet = BLISTE_EFFET_SONORE_BM_POWERDOWN1;
			break;
		case BLISTE_BONUS_PUISSANCE_MAXIMALE:
			effet = BLISTE_EFFET_SONORE_BM_POWERUP2;
			break;
		case BLISTE_BONUS_AUGMENTE_NOMBRE_BOMBE:
			effet = BLISTE_EFFET_SONORE_BM_POWERUP3;
			break;

		default:
			effet = BLISTE_EFFET_SONORE_BM_JEU_PICKUP;
			break;
	}

	// Lire le son
	Projet_Client_BClient_JouerEffetSonore( this->m_client,
		effet );
}

/* Tuer un joueur */
NBOOL Projet_Client_Monde_BMondeClient_TuerJoueur( BMondeClient *this,
	NU32 identifiant )
{
	// Joueur
	BEtatClient *joueur;

	// Proteger le cache
	Projet_Commun_Reseau_Client_Cache_BCacheClient_ProtegerCache( (BCacheClient*)this->m_cacheClient );

	// Obtenir joueur
	if( !( joueur = (BEtatClient*)Projet_Commun_Reseau_Client_Cache_BCacheClient_ObtenirClient( this->m_cacheClient,
		identifiant ) ) )
	{
		// Notifier
		NOTIFIER_ERREUR( NERREUR_OBJECT_STATE );

		// Ne plus proteger le cache
		Projet_Commun_Reseau_Client_Cache_BCacheClient_NePlusProtegerCache( (BCacheClient*)this->m_cacheClient );

		// Quitter
		return NFALSE;
	}

	// Definir etat mort
	Projet_Commun_Reseau_Client_BEtatClient_DefinirEstEnVie( joueur,
		NFALSE );

	// Jouer son de mort
	Projet_Client_BClient_JouerEffetSonore( this->m_client,
		BEFFET_SONORE_MORT_CLIENT );

	// Activer l'animation de mort
	Projet_Commun_Reseau_Client_BEtatClient_LancerAnimationMort( joueur );

	// Ne plus proteger le cache
	Projet_Commun_Reseau_Client_Cache_BCacheClient_NePlusProtegerCache( (BCacheClient*)this->m_cacheClient );

	// OK
	return NTRUE;
}

/* Arreter la partie */
void Projet_Client_Monde_BMondeClient_ArreterPartie( BMondeClient *this,
	BTypeFinPartie type,
	NU32 identifiantVainqueur )
{
	// Buffer
	char buffer[ 4096 ] = { 0, };

	// Joueur
	const BEtatClient *joueur;

	// Arreter la partie
	this->m_estPartieEnCours = NFALSE;

	// Enregistrer
	this->m_typeFinPartie = type;
	this->m_identifiantVainqueur = identifiantVainqueur;

	// Vider le message
	memset( buffer,
		0,
		4096 );

	// Creer le message
	switch( type )
	{
		case BTYPE_FIN_PARTIE_VICTOIRE:
			// Lock le cache
			Projet_Commun_Reseau_Client_Cache_BCacheClient_ProtegerCache( (BCacheClient*)this->m_cacheClient );

			// Obtenir le joueur
			if( !( joueur = Projet_Commun_Reseau_Client_Cache_BCacheClient_ObtenirClient( this->m_cacheClient,
				identifiantVainqueur ) ) )
			{
				// Notifier
				NOTIFIER_ERREUR( NERREUR_STREAM_ERROR );

				// Unlock
				Projet_Commun_Reseau_Client_Cache_BCacheClient_NePlusProtegerCache( (BCacheClient*)this->m_cacheClient );

				// Quitter
				return;
			}

			// Concevoir le message
			sprintf( buffer,
				BMESSAGE_VICTOIRE_FIN_PARTIE_CLIENT,
				Projet_Commun_Reseau_Client_BEtatClient_ObtenirNom( joueur ) );

			// Unlock le cache
			Projet_Commun_Reseau_Client_Cache_BCacheClient_NePlusProtegerCache( (BCacheClient*)this->m_cacheClient );
			break;

		case BTYPE_FIN_PARTIE_EGALITE:
			// Concevoir le message
			sprintf( buffer,
				BMESSAGE_EGALITE_FIN_PARTIE_CLIENT );
			break;

		default:
			sprintf( buffer,
				BMESSAGE_INCONNU_FIN_PARTIE_CLIENT );
			break;
	}

	// Transmettre a la gestion des messages
	Projet_Client_Monde_Message_BMessageMondeClient_DonnerMessageFinPartie( this->m_message,
		buffer );
}

