#include "../../../../../include/Projet/Projet.h"

// ---------------------------------------------------
// struct Projet::Client::Monde::Bombe::BEtatPoseBombe
// ---------------------------------------------------

/* Construire */
__ALLOC BEtatPoseBombe *Projet_Client_Monde_Bombe_BEtatPoseBombe_Construire( const struct BClient *client )
{
	// Sortie
	__OUTPUT BEtatPoseBombe *out;

	// Allouer la memoire
	if( !( out = calloc( 1,
		sizeof( BEtatPoseBombe ) ) ) )
	{
		// Notifier
		NOTIFIER_ERREUR( NERREUR_ALLOCATION_FAILED );

		// Quitter
		return NULL;
	}

	// Enregistrer
	out->m_client = (struct BClient*)client;

	// Zero
	out->m_estAttenteReponseServeur = NFALSE;
	out->m_estPoseLock = NFALSE;

	// OK
	return out;
}

/* Detruire */
void Projet_Client_Monde_Bombe_BEtatPoseBombe_Detruire( BEtatPoseBombe **this )
{
	// Liberer
	NFREE( *this );
}

/* Poser bombe */
NBOOL Projet_Client_Monde_Bombe_BEtatPoseBombe_PoserBombe( BEtatPoseBombe *this )
{
	// Joueur
	const BEtatClient *joueur;

	// Proteger cache client
	Projet_Client_BClient_ProtegerCacheClient( this->m_client );

	// Obtenir joueur
	if( !( joueur = Projet_Client_BClient_ObtenirJoueurCourant( this->m_client ) ) )
	{
		// Notifier
		NOTIFIER_ERREUR( NERREUR_OBJECT_STATE );

		// Ne plus proteger cache client
		Projet_Client_BClient_NePlusProtegerCacheClient( this->m_client );

		// Quitter
		return NFALSE;
	}

	// Verifier etat
	if( !Projet_Commun_Reseau_Client_BEtatClient_EstEnVie( joueur ) )
	{
		// Ne plus proteger cache client
		Projet_Client_BClient_NePlusProtegerCacheClient( this->m_client );

		// Quitter
		return NFALSE;
	}

	// Ne plus proteger cache client
	Projet_Client_BClient_NePlusProtegerCacheClient( this->m_client );

	// Verifier etat
	if( this->m_estAttenteReponseServeur
		|| this->m_estPoseLock )
		return NFALSE;

	// Poser
	if( !Projet_Client_TraitementPacket_EnvoyerPoseBombe( this->m_client ) )
		return NFALSE;

	// On lock la pose
	this->m_estPoseLock = NTRUE;

	// On attend desormais une reponse du serveur
	return ( this->m_estAttenteReponseServeur = NTRUE );
}

/* Finir de poser bombe */
void Projet_Client_Monde_Bombe_BEtatPoseBombe_FinirPoseBombe( BEtatPoseBombe *this )
{
	this->m_estPoseLock = NFALSE;
}

/* Enregister reponse serveur */
void Projet_Client_Monde_Bombe_BEtatPoseBombe_EnregistrerReponseServeur( BEtatPoseBombe *this )
{
	this->m_estAttenteReponseServeur = NFALSE;
}

