#include "../../../../include/Projet/Projet.h"

// ----------------------------------------------------------------
// struct Projet::Client::Monde::BConfigurationAffichageMondeClient
// ----------------------------------------------------------------

/* Construire */
__ALLOC BConfigurationAffichageMondeClient *Projet_Client_Monde_BConfigurationAffichageMondeClient_Construire( const NFenetre *fenetre,
	const BCarte *carte )
{
	// Sortie
	__OUTPUT BConfigurationAffichageMondeClient *out;

	// Allouer la memoire
	if( !( out = calloc( 1,
		sizeof( BConfigurationAffichageMondeClient ) ) ) )
	{
		// Notifier
		NOTIFIER_ERREUR( NERREUR_ALLOCATION_FAILED );

		// Quitter
		return NULL;
	}

	// Zoom demarre a 1
	out->m_zoom = 1;

	// Chercher le bon facteur de zoom
	while( Projet_Commun_Carte_Affichage_ObtenirTailleAAfficher( carte,
			out->m_zoom ).x < NLib_Module_SDL_NFenetre_ObtenirResolution( fenetre )->x
		&& Projet_Commun_Carte_Affichage_ObtenirTailleAAfficher( carte,
			out->m_zoom ).y < NLib_Module_SDL_NFenetre_ObtenirResolution( fenetre )->y )
		out->m_zoom++;

	// On diminue le zoom de 1
	if( out->m_zoom > 1 )
		out->m_zoom--;

	// Centrer
	NDEFINIR_POSITION( out->m_baseAffichage,
		( NLib_Module_SDL_NFenetre_ObtenirResolution( fenetre )->x / 2 ) - ( Projet_Commun_Carte_Affichage_ObtenirTailleAAfficher( carte,
			out->m_zoom ).x / 2 ),
		( NLib_Module_SDL_NFenetre_ObtenirResolution( fenetre )->y / 2 ) - ( Projet_Commun_Carte_Affichage_ObtenirTailleAAfficher( carte,
			out->m_zoom ).y / 2 ) );

	// OK
	return out;
}

/* Detruire */
void Projet_Client_Monde_BConfigurationAffichageMondeClient_Detruire( BConfigurationAffichageMondeClient **this )
{
	// Liberer
	NFREE( *this );
}

/* Obtenir base affichage */
const NSPoint *Projet_Client_Monde_BConfigurationAffichageMondeClient_ObtenirBaseAffichage( const BConfigurationAffichageMondeClient *this )
{
	return &this->m_baseAffichage;
}

/* Obtenir facteur de zoom */
NU32 Projet_Client_Monde_BConfigurationAffichageMondeClient_ObtenirFacteurZoom( const BConfigurationAffichageMondeClient *this )
{
	return this->m_zoom;
}

