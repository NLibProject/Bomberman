#include "../../../../../include/Projet/Projet.h"

// ----------------------------------------------------------
// struct Projet::Client::Monde::Message::BMessageMondeClient
// ----------------------------------------------------------

/* Construire */
__ALLOC BMessageMondeClient *Projet_Client_Monde_Message_BMessageMondeClient_Construire( const NFenetre *fenetre )
{
	// Sortie
	__OUTPUT BMessageMondeClient *out;

	// Iterateurs
	NU32 i,
		j;

	// Allouer la memoire
	if( !( out = calloc( 1,
		sizeof( BMessageMondeClient ) ) ) )
	{
		// Notifier
		NOTIFIER_ERREUR( NERREUR_ALLOCATION_FAILED );

		// Quitter
		return NULL;
	}

	// Creer le mutex
	if( !( out->m_mutex = NLib_Mutex_NMutex_Construire( ) ) )
	{
		// Notifier
		NOTIFIER_ERREUR( NERREUR_CONSTRUCTOR_FAILED );

		// Liberer
		NFREE( out );

		// Quitter
		return NULL;
	}

	// Creer le cadre
	if( !( out->m_cadre = NLib_Module_SDL_NCadre_Construire( (NSPoint){ 0, 0 },
		(NUPoint){ 0, 0 },
		BCOULEUR_CONTOUR_CADRE_MESSAGE_CLIENT,
		(NCouleur){ 0x00, 0x00, 0x00, 0x00 },
		fenetre,
		BEPAISSEUR_CONTOUR_CADRE_MESSAGE_CLIENT ) ) )
	{
		// Notifier
		NOTIFIER_ERREUR( NERREUR_CONSTRUCTOR_FAILED );

		// Liberer
		NLib_Mutex_NMutex_Detruire( &out->m_mutex );
		NFREE( out );

		// Quitter
		return NULL;
	}
	
	// Creer les polices
	for( i = 0; i < BPOLICES_MESSAGE_CLIENT; i++ )
		if( !( out->m_police[ i ] = NLib_Module_SDL_TTF_NPolice_Construire2( Projet_Commun_Ressource_Police_BListePolice_ObtenirLien( Projet_Commun_Reseau_Client_Message_BPoliceMessageClient_ObtenirPolice( i ) ),
			Projet_Commun_Reseau_Client_Message_BPoliceMessageClient_ObtenirTaille( i ),
			(NCouleur){ 0xFF, 0xFF, 0xFF, 0xFF } ) ) )
		{
			// Notifier
			NOTIFIER_ERREUR( NERREUR_CONSTRUCTOR_FAILED );

			// Detruire
			NLib_Module_SDL_NCadre_Detruire( &out->m_cadre );
			for( j = 0; j < i; j++ )
				NLib_Module_SDL_TTF_NPolice_Detruire( &out->m_police[ j ] );
			NLib_Mutex_NMutex_Detruire( &out->m_mutex );
			NFREE( out );

			// Quitter
			return NULL;
		}

	// Enregistrer
	out->m_fenetre = fenetre;

	// OK
	return out;
}

/* Detruire */
void Projet_Client_Monde_Message_BMessageMondeClient_Detruire( BMessageMondeClient **this )
{
	// Iterateur
	NU32 i = 0;

	// Detruire cadre
	NLib_Module_SDL_NCadre_Detruire( &(*this)->m_cadre );

	// Detruire messages
	NFREE( (*this)->m_messageActuel );
	NFREE( (*this)->m_messageFinPartie );

	// Detruire polices
	for( ; i < BPOLICES_MESSAGE_CLIENT; i++ )
		NLib_Module_SDL_TTF_NPolice_Detruire( &(*this)->m_police[ i ] );

	// Detruire surfaces
	NLib_Module_SDL_Surface_NSurface_Detruire( &(*this)->m_surfaceMessage );
	NLib_Module_SDL_Surface_NSurface_Detruire( &(*this)->m_surfaceMessageFinPartie );

	// Detruire mutex
	NLib_Mutex_NMutex_Detruire( &(*this)->m_mutex );

	// Liberer
	NFREE( (*this) );
}

/* Update */
void Projet_Client_Monde_Message_BMessageMondeClient_Update( BMessageMondeClient *this )
{
	// Lock mutex
	NLib_Mutex_NMutex_Lock( this->m_mutex );

	// On doit afficher le message de fin
	if( this->m_estDoitAfficherFinPartie )
	{
		// Le message n'est pas encore cree
		if( !this->m_estMessageFinPartieCree )
		{
			// Liberer
			NLib_Module_SDL_Surface_NSurface_Detruire( &this->m_surfaceMessageFinPartie );

			// Construire
			if( !( this->m_surfaceMessageFinPartie = NLib_Module_SDL_TTF_NPolice_CreerTexte( this->m_police[ BPOLICE_MESSAGE_CLIENT_FIN_PARTIE ],
				this->m_fenetre,
				this->m_messageFinPartie ) ) )
			{
				// Notifier
				NOTIFIER_ERREUR( NERREUR_CONSTRUCTOR_FAILED );

				// Unlock mutex
				NLib_Mutex_NMutex_Unlock( this->m_mutex );

				// Quitter
				return;
			}

			// Definir position
			NLib_Module_SDL_Surface_NSurface_DefinirPosition( this->m_surfaceMessageFinPartie,
				( NLib_Module_SDL_NFenetre_ObtenirResolution( this->m_fenetre )->x / 2 ) - ( NLib_Module_SDL_Surface_NSurface_ObtenirTaille( this->m_surfaceMessageFinPartie )->x / 2 ),
				( NLib_Module_SDL_NFenetre_ObtenirResolution( this->m_fenetre )->y / 2 ) - ( NLib_Module_SDL_Surface_NSurface_ObtenirTaille( this->m_surfaceMessageFinPartie )->y / 2 ) );

			// Definir color mod
			NLib_Module_SDL_Surface_NSurface_DefinirModificationCouleur( this->m_surfaceMessageFinPartie,
				BCOULEUR_MESSAGE_FIN_PARTIE_CLIENT.r,
				BCOULEUR_MESSAGE_FIN_PARTIE_CLIENT.g,
				BCOULEUR_MESSAGE_FIN_PARTIE_CLIENT.b );

			// Definir alpha mod
			NLib_Module_SDL_Surface_NSurface_DefinirModificationAlpha( this->m_surfaceMessageFinPartie,
				BCOULEUR_MESSAGE_FIN_PARTIE_CLIENT.a );

			// Enregister
			this->m_estMessageFinPartieCree = NTRUE;
		}
	}

	// Verifier
	if( !this->m_estDoitAfficher )
	{
		// Unlock mutex
		NLib_Mutex_NMutex_Unlock( this->m_mutex );

		// Quitter
		return;
	}

	// Si le message est deja cree
	if( this->m_estMessageCree )
	{
		// Depassement de la duree d'affichage?
		if( NLib_Temps_ObtenirTick( ) - this->m_tempsDebutAffichage >= this->m_dureeAffichageActuelle )
		{
			// Liberer ancien texte
			NLib_Module_SDL_Surface_NSurface_Detruire( &this->m_surfaceMessage );

			// Stopper l'affichage
			this->m_estDoitAfficher = NFALSE;
		}
		// On est toujours dans le creneau de duree
		else
			// Incrementer alpha
			if( this->m_alpha < 0xFF )
				this->m_alpha += BVITESSE_INCREMENTATION_ALPHA_MESSAGE_CLIENT;
	}
	// On doit creer le message
	else
	{
		// Liberer ancien texte
		NLib_Module_SDL_Surface_NSurface_Detruire( &this->m_surfaceMessage );

		// Creer texte
		if( !( this->m_surfaceMessage = NLib_Module_SDL_TTF_NPolice_CreerTexte( this->m_police[ this->m_policeActuelle ],
			this->m_fenetre,
			this->m_messageActuel ) ) )
		{
			// Notifier
			NOTIFIER_ERREUR( NERREUR_CONSTRUCTOR_FAILED );

			// Unlock mutex
			NLib_Mutex_NMutex_Unlock( this->m_mutex );

			// Quitter
			return;
		}

		// Definir position
		if( this->m_estDoitAfficherCadre )
		{
			// Definir couleur cadre
			NLib_Module_SDL_NCadre_DefinirCouleurFond( this->m_cadre,
				this->m_couleurCadre.r,
				this->m_couleurCadre.g,
				this->m_couleurCadre.b,
				this->m_couleurCadre.a );

			// Configurer taille cadre
			NLib_Module_SDL_NCadre_DefinirTaille( this->m_cadre,
				(NU32)( (NS32)NLib_Module_SDL_Surface_NSurface_ObtenirTaille( this->m_surfaceMessage )->x + BPADDING_CADRE_MESSAGE_CLIENT[ NGAUCHE ] + BPADDING_CADRE_MESSAGE_CLIENT[ NDROITE ] ),
				(NU32)( (NS32)NLib_Module_SDL_Surface_NSurface_ObtenirTaille( this->m_surfaceMessage )->y + BPADDING_CADRE_MESSAGE_CLIENT[ NHAUT ] + BPADDING_CADRE_MESSAGE_CLIENT[ NBAS ] ) );
			
			// Definir position cadre
			switch( this->m_position )
			{
				default:
				case BPOSITION_AFFICHAGE_MESSAGE_CLIENT_CENTRE:
					NLib_Module_SDL_NCadre_DefinirPosition( this->m_cadre,
						( NLib_Module_SDL_NFenetre_ObtenirResolution( this->m_fenetre )->x / 2 ) - ( NLib_Module_SDL_NCadre_ObtenirTaille( this->m_cadre )->x / 2 ),
						( NLib_Module_SDL_NFenetre_ObtenirResolution( this->m_fenetre )->y / 2 ) - ( NLib_Module_SDL_NCadre_ObtenirTaille( this->m_cadre )->y / 2 ) );
					break;

				case BPOSITION_AFFICHAGE_MESSAGE_CLIENT_HAUT:
					NLib_Module_SDL_NCadre_DefinirPosition( this->m_cadre,
						( NLib_Module_SDL_NFenetre_ObtenirResolution( this->m_fenetre )->x / 2 ) - ( NLib_Module_SDL_NCadre_ObtenirTaille( this->m_cadre )->x / 2 ),
						BMARGE_VERTICALE_AFFICHAGE_TEXTE_MESSAGE_CLIENT );
					break;
			}

			// Centre texte dans cadre
			NLib_Module_SDL_Surface_NSurface_DefinirPosition( this->m_surfaceMessage,
				NLib_Module_SDL_NCadre_ObtenirPosition( this->m_cadre )->x + ( ( NLib_Module_SDL_NCadre_ObtenirTaille( this->m_cadre )->x / 2 ) - ( NLib_Module_SDL_Surface_NSurface_ObtenirTaille( this->m_surfaceMessage )->x / 2 ) ),
				NLib_Module_SDL_NCadre_ObtenirPosition( this->m_cadre )->y + ( ( NLib_Module_SDL_NCadre_ObtenirTaille( this->m_cadre )->y / 2 ) - ( NLib_Module_SDL_Surface_NSurface_ObtenirTaille( this->m_surfaceMessage )->y / 2 ) ) );
		}
		else
			switch( this->m_position )
			{
				default:
				case BPOSITION_AFFICHAGE_MESSAGE_CLIENT_CENTRE:
					NLib_Module_SDL_Surface_NSurface_DefinirPosition( this->m_surfaceMessage,
						( NLib_Module_SDL_NFenetre_ObtenirResolution( this->m_fenetre )->x / 2 ) - ( NLib_Module_SDL_Surface_NSurface_ObtenirTaille( this->m_surfaceMessage )->x / 2 ),
						( NLib_Module_SDL_NFenetre_ObtenirResolution( this->m_fenetre )->y / 2 ) - ( NLib_Module_SDL_Surface_NSurface_ObtenirTaille( this->m_surfaceMessage )->y / 2 ) );
					break;

				case BPOSITION_AFFICHAGE_MESSAGE_CLIENT_HAUT:
					NLib_Module_SDL_Surface_NSurface_DefinirPosition( this->m_surfaceMessage,
						( NLib_Module_SDL_NFenetre_ObtenirResolution( this->m_fenetre )->x / 2 ) - ( NLib_Module_SDL_Surface_NSurface_ObtenirTaille( this->m_surfaceMessage )->x / 2 ),
						BMARGE_VERTICALE_AFFICHAGE_TEXTE_MESSAGE_CLIENT );
					break;
			}

		
		// Definir color mod
		NLib_Module_SDL_Surface_NSurface_DefinirModificationCouleur( this->m_surfaceMessage,
			this->m_couleurActuelle.r,
			this->m_couleurActuelle.g,
			this->m_couleurActuelle.b );

		// Enregistrer
		this->m_tempsDebutAffichage = NLib_Temps_ObtenirTick( );

		// Liberer texte
		NFREE( this->m_messageActuel );

		// Message cree
		this->m_estMessageCree = NTRUE;

		// Zero
		this->m_alpha = 0;
	}

	// Unlock mutex
	NLib_Mutex_NMutex_Unlock( this->m_mutex );
}

/* Donner un message a afficher */
NBOOL Projet_Client_Monde_Message_BMessageMondeClient_DonnerMessage( BMessageMondeClient *this,
	const char *message,
	NU32 dureeAffichage,
	BPoliceMessageClient police,
	NCouleur couleur,
	BPositionAffichageMessageClient position,
	NBOOL estDoitAfficherCadre,
	NCouleur couleurCadre )
{	
	// Verifier
	if( !message
		|| strlen( message ) <= 0 )
	{
		// Notifier
		NOTIFIER_AVERTISSEMENT( NERREUR_PARAMETER_ERROR );

		// Quitter (On ignore simplement le message)
		return NTRUE;
	}

	// Lock mutex
	NLib_Mutex_NMutex_Lock( this->m_mutex );

	// Liberer ancien message
	NFREE( this->m_messageActuel );

	// Allouer la memoire
	if( !( this->m_messageActuel = calloc( strlen( message ) + 1,
		sizeof( char ) ) ) )
	{
		// Notifier
		NOTIFIER_ERREUR( NERREUR_ALLOCATION_FAILED );

		// Unlock mutex
		NLib_Mutex_NMutex_Unlock( this->m_mutex );

		// Quitter
		return NFALSE;
	}

	// Enregistrer
	this->m_dureeAffichageActuelle = dureeAffichage;
	this->m_couleurActuelle = couleur;
	this->m_policeActuelle = police;
	this->m_position = position;
	this->m_estDoitAfficherCadre = estDoitAfficherCadre;
	this->m_couleurCadre = couleurCadre;
	memcpy( this->m_messageActuel,
		message,
		strlen( message ) );

	// Message non cree pour l'instant
	this->m_estMessageCree = NFALSE;

	// On demarre l'affichage
	this->m_estDoitAfficher = NTRUE;

	// Unlock
	NLib_Mutex_NMutex_Unlock( this->m_mutex );

	// OK
	return NTRUE;
}

/* Donner un message de fin de partie */
NBOOL Projet_Client_Monde_Message_BMessageMondeClient_DonnerMessageFinPartie( BMessageMondeClient *this,
	const char *message )
{
	// Lock mutex
	NLib_Mutex_NMutex_Lock( this->m_mutex );

	// Liberer ancien message
	NFREE( this->m_messageFinPartie );

	// Allouer la memoire
	if( !( this->m_messageFinPartie = calloc( strlen( message ) + 1,
		sizeof( char ) ) ) )
	{
		// Notifier
		NOTIFIER_ERREUR( NERREUR_ALLOCATION_FAILED );

		// Unlock le mutex
		NLib_Mutex_NMutex_Unlock( this->m_mutex );

		// Quitter
		return NFALSE;
	}

	// Enregistrer
	this->m_estMessageFinPartieCree = NFALSE;
	this->m_estDoitAfficherFinPartie = NTRUE;
	memcpy( this->m_messageFinPartie,
		message,
		strlen( message ) );

	// Unlock mutex
	NLib_Mutex_NMutex_Unlock( this->m_mutex );

	// OK
	return NTRUE;
}

/* Afficher */
void Projet_Client_Monde_Message_BMessageMondeClient_Afficher( const BMessageMondeClient *this )
{
	// Lock mutex
	NLib_Mutex_NMutex_Lock( this->m_mutex );

	// Si on doit afficher le message de fin de partie
	if( this->m_estDoitAfficherFinPartie
		&& this->m_estMessageFinPartieCree )
		NLib_Module_SDL_Surface_NSurface_Afficher( this->m_surfaceMessageFinPartie );

	// Verifier si il doit y avoir un affichage
	if( !this->m_estDoitAfficher
		|| !this->m_estMessageCree )
	{
		// Unlock mutex
		NLib_Mutex_NMutex_Unlock( this->m_mutex );

		// Quitter
		return;
	}

	// Afficher cadre
	if( this->m_estDoitAfficherCadre )
		NLib_Module_SDL_NCadre_Dessiner( this->m_cadre );

	// Changer alpha
	NLib_Module_SDL_Surface_NSurface_DefinirModificationAlpha( this->m_surfaceMessage,
		(NU8)this->m_alpha );

	// Afficher
	NLib_Module_SDL_Surface_NSurface_Afficher( this->m_surfaceMessage );

	// Unlock mutex
	NLib_Mutex_NMutex_Unlock( this->m_mutex );
}

