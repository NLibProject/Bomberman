#include "../../../include/Projet/Projet.h"

// ------------------------
// namespace Projet::Client
// ------------------------

/* Main client */
NS32 Projet_Client_Main( void )
{
	// Client
	BClient *client;

	// Retour client
	NS32 retourClient;

	// Initialiser NLib
	NLib_Initialiser( Projet_Commun_Erreur_CallbackNotificationErreur );

	// Construire le client
	if( !( client = Projet_Client_BClient_Construire( ) ) )
	{
		// Notifier
		NOTIFIER_ERREUR( NERREUR_CONSTRUCTOR_FAILED );

		// Quitter
		return EXIT_FAILURE;
	}

	// Lancer le client
	retourClient = Projet_Client_BClient_Lancer( client )
		? EXIT_SUCCESS
		: EXIT_FAILURE;

	// Detruire le client
	Projet_Client_BClient_Detruire( &client );

	// Detruire NLib
	NLib_Detruire( );

	// OK
	return retourClient;
}

