#include "../../../../../include/Projet/Projet.h"

// ------------------------------------------------------
// struct Projet::Client::Menu::Rejoindre::BMenuRejoindre
// ------------------------------------------------------

/* Changer etat pret (privee) */
void Projet_Client_Menu_Rejoindre_BMenuRejoindre_ChangerEtatPretInterne( BMenuRejoindre *this )
{
	// Envoyer requete
	Projet_Client_TraitementPacket_EnvoyerChangementEtatPret( this->m_client );
}

/* Changer nom (privee) */
void Projet_Client_Menu_Rejoindre_BMenuRejoindre_ChangerNomInterne( BMenuRejoindre *this,
	const char *nom )
{
	// Joueur
	BEtatClient *joueur;

	// Obtenir joueur
	if( !( joueur = (BEtatClient*)Projet_Client_BClient_ObtenirJoueurCourant( this->m_client ) ) )
		return;

	// Envoyer demande
	Projet_Client_TraitementPacket_EnvoyerInformationsJoueur( this->m_client,
		nom,
		Projet_Commun_Reseau_Client_BEtatClient_ObtenirCharset( joueur ),
		Projet_Commun_Reseau_Client_BEtatClient_ObtenirCouleurCharset( joueur ) );
}

/* Changer charset/couleur (privee) */
void Projet_Client_Menu_Rejoindre_BMenuRejoindre_ChangerCharsetInterne( BMenuRejoindre *this )
{
	// Joueur
	BEtatClient *joueur;

	// Charset
	NU32 charset;

	// Obtenir joueur
	if( !( joueur = (BEtatClient*)Projet_Client_BClient_ObtenirJoueurCourant( this->m_client ) ) )
		return;

	// Recuperer charset
	charset = Projet_Commun_Reseau_Client_BEtatClient_ObtenirCharset( joueur ) ;

	// Passer au charset suivant
	if( charset < Projet_Commun_Ressource_BRessource_ObtenirNombrePersonnage( Projet_Client_BClient_ObtenirRessource( this->m_client ) ) - 1 )
		charset++;
	else
		charset = 0;

	// Envoyer demande
	Projet_Client_TraitementPacket_EnvoyerInformationsJoueur( this->m_client,
		Projet_Commun_Reseau_Client_BEtatClient_ObtenirNom( joueur ),
		charset,
		0 );
}

void Projet_Client_Menu_Rejoindre_BMenuRejoindre_ChangerCouleurCharsetInterne( BMenuRejoindre *this )
{
	// Joueur
	BEtatClient *joueur;

	// Couleur charset
	NU32 couleurCharset;

	// Obtenir joueur
	if( !( joueur = (BEtatClient*)Projet_Client_BClient_ObtenirJoueurCourant( this->m_client ) ) )
		return;

	// Recuperer charset
	couleurCharset = Projet_Commun_Reseau_Client_BEtatClient_ObtenirCouleurCharset( joueur ) ;

	// Passer au charset suivant
	if( couleurCharset < Projet_Commun_Personnage_BPersonnage_ObtenirNombreCouleur( Projet_Commun_Ressource_BRessource_ObtenirPersonnage( Projet_Client_BClient_ObtenirRessource( this->m_client ),
		Projet_Commun_Reseau_Client_BEtatClient_ObtenirCharset( joueur ) ) ) - 1 )
		couleurCharset++;
	else
		couleurCharset = 0;

	// Envoyer demande
	Projet_Client_TraitementPacket_EnvoyerInformationsJoueur( this->m_client,
		Projet_Commun_Reseau_Client_BEtatClient_ObtenirNom( joueur ),
		Projet_Commun_Reseau_Client_BEtatClient_ObtenirCharset( joueur ),
		couleurCharset );
}

/* Update cadre carte */
void Projet_Client_Menu_Rejoindre_BMenuRejoindre_UpdateCadreCarte( BMenuRejoindre *this )
{
	// Position
	NSPoint positionCarte,
		positionLancer;

	// Taille
	NUPoint tailleCarte;

	// Carte
	const BCarte *carte;

	// Obtenir la carte
	if( !( carte = Projet_Client_BClient_ObtenirCarte( this->m_client,
		Projet_Commun_Monde_BConfigurationMonde_ObtenirIdentifiantCarte( Projet_Client_BClient_ObtenirConfigurationMonde( this->m_client ) ) ) ) )
		return;

	// Definir taille
	NDEFINIR_POSITION( tailleCarte,
		Projet_Commun_Carte_Affichage_ObtenirTailleAAfficher( carte,
			1 ).x,
		Projet_Commun_Carte_Affichage_ObtenirTailleAAfficher( carte,
			1 ).y );

	// Definir position
	NDEFINIR_POSITION( positionCarte,
		(NS32)NLib_Module_SDL_NFenetre_ObtenirResolution( this->m_fenetre )->x - ( (NS32)( NLib_Module_SDL_NFenetre_ObtenirResolution( this->m_fenetre )->x - BTAILLE_CADRE_JOUEUR_MENU_REJOINDRE.x - BMARGING_CADRE_JOUEUR_MENU_REJOINDRE[ NGAUCHE ] - BMARGING_CADRE_JOUEUR_MENU_REJOINDRE[ NDROITE ] ) / 2
			+ ( (NU32)tailleCarte.x + (NS32)BMARGING_CADRE_CARTE_MENU_REJOINDRE[ NDROITE ] ) / 2 ),
		(NS32)( NLib_Module_SDL_NFenetre_ObtenirResolution( this->m_fenetre )->y / 2 ) - (NS32)( tailleCarte.y / 2 ) );

	// Mettre a jour cadre
	NLib_Module_SDL_Bouton_NBouton_DefinirPosition( this->m_cadreCarte,
		positionCarte );
	NLib_Module_SDL_Bouton_NBouton_DefinirTaille( this->m_cadreCarte,
		tailleCarte );

	// Definir position
	NDEFINIR_POSITION( positionLancer,
		positionCarte.x + ( tailleCarte.x / 2 ) - ( ( NLib_Module_SDL_Bouton_NBouton_ObtenirTaille( this->m_boutonLancer ).x + BMARGING_BOUTON_LANCER_MENU_REJOINDRE[ NGAUCHE ] + BMARGING_BOUTON_LANCER_MENU_REJOINDRE[ NDROITE ] ) / 2 ),
		positionCarte.y + tailleCarte.y + BMARGING_BOUTON_LANCER_MENU_REJOINDRE[ NHAUT ] );

	// Mettre a jour bouton lancer
	NLib_Module_SDL_Bouton_NBouton_DefinirPosition( this->m_boutonLancer,
		positionLancer );

	// Centrer texte lancer
	NLib_Module_SDL_Surface_NSurface_DefinirPosition( this->m_surfaceTexteLancer,
		NLib_Module_SDL_Bouton_NBouton_ObtenirPosition( this->m_boutonLancer ).x + ( ( NLib_Module_SDL_Bouton_NBouton_ObtenirTaille( this->m_boutonLancer ).x / 2 ) - ( NLib_Module_SDL_Surface_NSurface_ObtenirTaille( this->m_surfaceTexteLancer )->x / 2 ) ),
		NLib_Module_SDL_Bouton_NBouton_ObtenirPosition( this->m_boutonLancer ).y + ( ( NLib_Module_SDL_Bouton_NBouton_ObtenirTaille( this->m_boutonLancer ).y / 2 ) - ( NLib_Module_SDL_Surface_NSurface_ObtenirTaille( this->m_surfaceTexteLancer )->y / 2 ) ) );
}

void Projet_Client_Menu_Rejoindre_BMenuRejoindre_ChangerCarteInterne( BMenuRejoindre *this )
{
	// Identifiant carte
	NU32 identifiantCarte;

	// Obtenir identifiant
	identifiantCarte = Projet_Commun_Monde_BConfigurationMonde_ObtenirIdentifiantCarte( Projet_Client_BClient_ObtenirConfigurationMonde( this->m_client ) );

	// Notifier les clients
	Projet_Serveur_TraitementPacket_NotifierChangementCarte( Projet_Client_BClient_ObtenirServeur( this->m_client ),
		( identifiantCarte < Projet_Client_BClient_ObtenirNombreCarte( this->m_client ) - 1 ) ?
			identifiantCarte + 1
			: 0 );
}

/* Actualiser (privee) */
void Projet_Client_Menu_Rejoindre_BMenuRejoindre_ActualiserInterne( BMenuRejoindre *this )
{
	// Iterateur
	NU32 i;

	// Index
	NU32 index;

	// Position
	NSPoint position;

	// Joueur
	const BEtatClient *joueur;

	// Nettoyer la fenetre
	NLib_Module_SDL_NFenetre_Nettoyer( this->m_fenetre );

	// Afficher etoiles
	for( i = 0; i < BNOMBRE_COUCHES_ETOILES_MENU; i++ )
		Projet_Client_Menu_BFondEtoileMenu_Afficher( this->m_etoile[ i ] );

	// Suivant l'etape
	switch( this->m_etape )
	{
		case BETAPE_MENU_REJOINDRE_CONNEXION:
			// Afficher adresse
			if( this->m_surfaceAdresseConnexion != NULL )
				NLib_Module_SDL_Surface_NSurface_Afficher( this->m_surfaceAdresseConnexion );

			// Definir couleur cadre adresse
			if( NLib_Module_SDL_Saisie_NSaisieSDL_EstEnCours( this->m_saisieAdresseConnexion ) )
				NLib_Module_SDL_Bouton_NBouton_DefinirCouleur( this->m_boutonAdresse,
					BCOULEUR_CONTOUR_SAISIE_CADRE_ADRESSE_MENU_REJOINDRE );
			else
				NLib_Module_SDL_Bouton_NBouton_DefinirCouleur( this->m_boutonAdresse,
					BCOULEUR_CONTOUR_CADRE_ADRESSE_MENU_REJOINDRE );

			// Afficher cadre adresse
			NLib_Module_SDL_Bouton_NBouton_Dessiner( this->m_boutonAdresse );

			// Afficher titre
			NLib_Module_SDL_Surface_NSurface_Afficher( this->m_surfaceTitreConnexion );

			// Afficher cadre validation
			NLib_Module_SDL_Bouton_NBouton_Dessiner( this->m_boutonValidationConnexion );

			// Afficher texte validation
			NLib_Module_SDL_Surface_NSurface_Afficher( this->m_surfaceValidationConnexion );
			break;

		case BETAPE_MENU_REJOINDRE_ATTENTE_REPONSE_CONNEXION:
			break;

		case BETAPE_MENU_REJOINDRE_CHOIX_DETAILS:
			// Obtenir index courant
			index = Projet_Client_BClient_ObtenirIndexJoueurCourant( this->m_client );

			// Proteger cache
			Projet_Client_BClient_ProtegerCacheClient( this->m_client );

			// Personnages
			for( i = 0; i < Projet_Client_BClient_ObtenirNombreJoueur( this->m_client ); i++ )
			{
				// Obtenir joueur
				if( !( joueur = Projet_Client_BClient_ObtenirJoueur( this->m_client,
					i ) ) )
					continue;

				// Cadre
				NLib_Module_SDL_NCadre_Dessiner( this->m_cadreJoueur[ i ] );

				// Definir position
				Projet_Commun_Personnage_BPersonnage_DefinirPosition( Projet_Commun_Ressource_BRessource_ObtenirPersonnage( this->m_ressource,
						Projet_Commun_Reseau_Client_BEtatClient_ObtenirCharset( joueur ) ),
					NLib_Module_SDL_Bouton_NBouton_ObtenirPosition( this->m_boutonPersonnage[ i ] ) );

				// Dessiner
				if( index == i )
					// Cadre personnage courant
					NLib_Module_SDL_Bouton_NBouton_Dessiner( this->m_boutonPersonnage[ i ] );

				// Personnage
				Projet_Commun_Personnage_BPersonnage_AfficherCharset( Projet_Commun_Ressource_BRessource_ObtenirPersonnage( this->m_ressource,
						Projet_Commun_Reseau_Client_BEtatClient_ObtenirCharset( joueur ) ),
					NBAS,
					Projet_Commun_Reseau_Client_BEtatClient_ObtenirCouleurCharset( joueur ),
					1 );

				// Cadre personnage/nom
				if( index == i )
				{
					// Changer couleur cadre nom
					if( this->m_estSaisieNomPersonnage )
						NLib_Module_SDL_Bouton_NBouton_DefinirCouleur( this->m_boutonNomPersonnage[ i ],
							BCOULEUR_BOUTON_CHOIX_NOM_CONTOUR_SAISIE_MENU_REJOINDRE );
					else
						NLib_Module_SDL_Bouton_NBouton_DefinirCouleur( this->m_boutonNomPersonnage[ i ],
							BCOULEUR_BOUTON_CHOIX_NOM_CONTOUR_MENU_REJOINDRE );

					// Cadre nom personnage
					NLib_Module_SDL_Bouton_NBouton_Dessiner( this->m_boutonNomPersonnage[ i ] );
				}

				// Bouton pret
					// Definir couleur
						switch( Projet_Commun_Reseau_Client_BEtatClient_EstPret( joueur ) )
						{
							default:
							case BETAT_PRET_PAS_PRET:
								// Definir couleur
								NLib_Module_SDL_Bouton_NBouton_DefinirCouleur( this->m_boutonPret[ i ],
									BCOULEUR_BOUTON_PRET_PAS_PRET_CONTOUR_MENU_REJOINDRE );
								NLib_Module_SDL_Bouton_NBouton_DefinirCouleurFond( this->m_boutonPret[ i ],
									BCOULEUR_BOUTON_PRET_PAS_PRET_FOND_MENU_REJOINDRE );
								break;

							case BETAT_PRET_PRET:
								// Definir couleur
								NLib_Module_SDL_Bouton_NBouton_DefinirCouleur( this->m_boutonPret[ i ],
									BCOULEUR_BOUTON_PRET_PRET_CONTOUR_MENU_REJOINDRE );
								NLib_Module_SDL_Bouton_NBouton_DefinirCouleurFond( this->m_boutonPret[ i ],
									BCOULEUR_BOUTON_PRET_PRET_FOND_MENU_REJOINDRE );
								break;
						}
					// Afficher
						NLib_Module_SDL_Bouton_NBouton_Dessiner( this->m_boutonPret[ i ] );

				// Texte pret
					// Placer
						NLib_Module_SDL_Surface_NSurface_DefinirPosition( this->m_surfaceTextePret[ Projet_Commun_Reseau_Client_BEtatClient_EstPret( joueur ) ],
							NLib_Module_SDL_Bouton_NBouton_ObtenirPosition( this->m_boutonPret[ i ] ).x + ( ( NLib_Module_SDL_Bouton_NBouton_ObtenirTaille( this->m_boutonPret[ i ] ).x / 2 ) - ( NLib_Module_SDL_Surface_NSurface_ObtenirTaille( this->m_surfaceTextePret[ Projet_Commun_Reseau_Client_BEtatClient_EstPret( joueur ) ] )->x / 2 ) ),
							NLib_Module_SDL_Bouton_NBouton_ObtenirPosition( this->m_boutonPret[ i ] ).y + ( ( NLib_Module_SDL_Bouton_NBouton_ObtenirTaille( this->m_boutonPret[ i ] ).y / 2 ) - ( NLib_Module_SDL_Surface_NSurface_ObtenirTaille( this->m_surfaceTextePret[ Projet_Commun_Reseau_Client_BEtatClient_EstPret( joueur ) ] )->y / 2 ) ) );
					// Afficher
						NLib_Module_SDL_Surface_NSurface_Afficher( this->m_surfaceTextePret[ Projet_Commun_Reseau_Client_BEtatClient_EstPret( joueur ) ] );

				// Nom
				if( this->m_surfaceNomPersonnage[ i ] != NULL )
					NLib_Module_SDL_Surface_NSurface_Afficher( this->m_surfaceNomPersonnage[ i ] );
			}

			// Ne plus proteger cache
			Projet_Client_BClient_NePlusProtegerCacheClient( this->m_client );

			// Afficher carte
				// Definir la position
					position = NLib_Module_SDL_Bouton_NBouton_ObtenirPosition( this->m_cadreCarte );
				// Carte
					for( i = 0; i < BCOUCHES_CARTE; i++ )
						Projet_Commun_Carte_Affichage_Afficher( Projet_Client_BClient_ObtenirCarte( this->m_client,
								Projet_Commun_Monde_BConfigurationMonde_ObtenirIdentifiantCarte( Projet_Client_BClient_ObtenirConfigurationMonde( this->m_client ) ) ),
							i,
							Projet_Commun_Ressource_BRessource_ObtenirEnsembleTileset( Projet_Client_BClient_ObtenirRessource( this->m_client ) ),
							position,
							1 );
				// Cadre contour
					if( Projet_Client_BClient_EstHote( this->m_client ) )
						NLib_Module_SDL_Bouton_NBouton_Dessiner( this->m_cadreCarte );

			// Afficher bouton lancement
			if( Projet_Client_BClient_EstPretLancer( this->m_client ) )
			{
				// Bouton
					NLib_Module_SDL_Bouton_NBouton_Dessiner( this->m_boutonLancer );
				// Texte
					NLib_Module_SDL_Surface_NSurface_Afficher( this->m_surfaceTexteLancer );
			}
			break;

		default:
			break;
	}

	// Actualiser
	NLib_Module_SDL_NFenetre_Update( this->m_fenetre );
}

/* Update nom joueur */
void Projet_Client_Menu_Rejoindre_BMenuRejoindre_UpdateNomJoueur( BMenuRejoindre *this,
	NBOOL estForcerUpdate )
{
	// Iterateur
	NU32 i;

	// Joueur
	const BEtatClient *joueur;

	// Nom
	const char *nom;

	// Creer surfaces nom
	if( estForcerUpdate
		|| this->m_estDoitUpdateNomJoueur
		|| this->m_identifiantDerniereModification != Projet_Client_BClient_ObtenirIdentifiantDerniereModificationCacheClient( this->m_client ) )
	{
		// Liberer
		for( i = 0; i < BNOMBRE_MAXIMUM_JOUEUR; i++ )
			NLib_Module_SDL_Surface_NSurface_Detruire( &this->m_surfaceNomPersonnage[ i ] );

		// Proteger cache
		Projet_Client_BClient_ProtegerCacheClient( this->m_client );

		// Creer surfaces
		for( i = 0; i < Projet_Client_BClient_ObtenirNombreJoueur( this->m_client ); i++ )
		{
			// Obtenir joueur
			if( !( joueur = Projet_Client_BClient_ObtenirJoueur( this->m_client,
				i ) ) )
				break;

			// Creer pour saisie
			if( i == Projet_Client_BClient_ObtenirIndexJoueurCourant( this->m_client ) )
			{
				// Si saisie en cours
				if( this->m_estSaisieNomPersonnage )
					nom = NLib_Module_SDL_Saisie_NSaisieSDL_ObtenirTexte( this->m_saisieNomPersonnage );
				else
					nom = Projet_Commun_Reseau_Client_BEtatClient_ObtenirNom( joueur );
			}
			else
				nom = Projet_Commun_Reseau_Client_BEtatClient_ObtenirNom( joueur );

			// Creer texte
			if( nom == NULL
				|| !( this->m_surfaceNomPersonnage[ i ] = NLib_Module_SDL_TTF_NPolice_CreerTexte( this->m_policeNomJoueur,
					this->m_fenetre,
					nom ) ) )
				continue;

			// Centrer texte
			NLib_Module_SDL_Surface_NSurface_DefinirPosition( this->m_surfaceNomPersonnage[ i ],
				NLib_Module_SDL_Bouton_NBouton_ObtenirPosition( this->m_boutonNomPersonnage[ i ] ).x + ( ( NLib_Module_SDL_Bouton_NBouton_ObtenirTaille( this->m_boutonNomPersonnage[ i ] ).x / 2 ) - ( NLib_Module_SDL_Surface_NSurface_ObtenirTaille( this->m_surfaceNomPersonnage[ i ] )->x / 2 ) ),
				NLib_Module_SDL_Bouton_NBouton_ObtenirPosition( this->m_boutonNomPersonnage[ i ] ).y + ( ( NLib_Module_SDL_Bouton_NBouton_ObtenirTaille( this->m_boutonNomPersonnage[ i ] ).y / 2 ) - ( NLib_Module_SDL_Surface_NSurface_ObtenirTaille( this->m_surfaceNomPersonnage[ i ] )->y / 2 ) ) );
		}

		// Enregistrer identifiant
		this->m_identifiantDerniereModification = Projet_Client_BClient_ObtenirIdentifiantDerniereModificationCacheClient( this->m_client );

		// Ne plus proteger cache
		Projet_Client_BClient_NePlusProtegerCacheClient( this->m_client );

		// Plus d'update necessaire
		this->m_estDoitUpdateNomJoueur = NFALSE;
	}
}

/* Update minimal (client+reseau) (privee) */
void Projet_Client_Menu_Rejoindre_BMenuRejoindre_UpdateMinimalInterne( BMenuRejoindre *this )
{
	// Iterateur
	NU32 i = 0;

	// Update client
	Projet_Client_BClient_MettreAJour( this->m_client );

	// Mettre a jour etoiles
	for( ; i < BNOMBRE_COUCHES_ETOILES_MENU; i++ )
		Projet_Client_Menu_BFondEtoileMenu_Update( (BFondEtoileMenu*)this->m_etoile[ i ] );

	// Gerer etat connexion
	switch( this->m_etape )
	{
		case BETAPE_MENU_REJOINDRE_ATTENTE_REPONSE_CONNEXION:
		case BETAPE_MENU_REJOINDRE_CHOIX_DETAILS:
			// Verifier etat connexion
			if( !Projet_Client_BClient_EstConnecte( this->m_client ) )
			{
				// N'est plus en cours
				this->m_estEnCours = NFALSE;

				// Retour au menu principal
				this->m_codeRetour = BCODE_MENU_REJOINDRE_RETOUR_MENU_PRINCIPAL;

				// Quitter
				return;
			}
			break;

		default:
			break;
	}

	// Mettre a jours noms
	Projet_Client_Menu_Rejoindre_BMenuRejoindre_UpdateNomJoueur( this,
		NFALSE );
}

/* Callback gestion saisie (privee) */
__CALLBACK void Projet_Client_Menu_Rejoindre_BMenuRejoindre_CallbackGestionSaisieInterne( BMenuRejoindre *this )
{
	// Update
	Projet_Client_Menu_Rejoindre_BMenuRejoindre_UpdateMinimalInterne( this );
}

/* Callback update chaine saisie nom (privee) */
__CALLBACK NBOOL Projet_Client_Menu_Rejoindre_BMenuRejoindre_CallbackUpdateChaineSaisieNomJoueurInterne( BMenuRejoindre *this )
{
	// Update
	Projet_Client_Menu_Rejoindre_BMenuRejoindre_UpdateNomJoueur( this,
		NTRUE );

	// OK
	return NTRUE;
}

/* Creer cadres joueurs (privee) */
NBOOL Projet_Client_Menu_Rejoindre_BMenuRejoindre_CreerCadreJoueurInterne( BMenuRejoindre *this )
{
	// Iterateurs
	NU32 i,
		j;

	// Position
	NSPoint position;

	// Taille
	NUPoint taille;

	// Taille totale
	NUPoint tailleTotale;

	// Police
	NPolice *police;

	// Calculer la taille totale
	NDEFINIR_POSITION( tailleTotale,
		BTAILLE_CADRE_JOUEUR_MENU_REJOINDRE.x + BPADDING_CADRE_JOUEUR_MENU_REJOINDRE[ NGAUCHE ] + BPADDING_CADRE_JOUEUR_MENU_REJOINDRE[ NDROITE ],
		( BTAILLE_CADRE_JOUEUR_MENU_REJOINDRE.y + BPADDING_CADRE_JOUEUR_MENU_REJOINDRE[ NHAUT ] + BPADDING_CADRE_JOUEUR_MENU_REJOINDRE[ NBAS ] ) * BNOMBRE_MAXIMUM_JOUEUR
			+ ( BMARGING_CADRE_JOUEUR_MENU_REJOINDRE[ NHAUT ] + BMARGING_CADRE_JOUEUR_MENU_REJOINDRE[ NBAS ] ) * ( BNOMBRE_MAXIMUM_JOUEUR - 1 ) );

	// Charger police
	if( !( this->m_policeNomJoueur = NLib_Module_SDL_TTF_NPolice_Construire2( Projet_Commun_Ressource_Police_BListePolice_ObtenirLien( BPOLICE_SAISIE_NOM_JOUEUR_MENU_REJOINDRE ),
		BTAILLE_POLICE_SAISIE_NOM_JOUEUR_MENU_REJOINDRE,
		BCOULEUR_POLICE_SAISIE_NOM_JOUEUR_MENU_REJOINDRE ) ) )
	{
		// Notifier
		NOTIFIER_ERREUR( NERREUR_CONSTRUCTOR_FAILED );

		// Quitter
		return NFALSE;
	}

	// Construire
	for( i = 0; i < BNOMBRE_MAXIMUM_JOUEUR; i++ )
	{
		// Calculer la position
		NDEFINIR_POSITION( position,
			BMARGING_CADRE_JOUEUR_MENU_REJOINDRE[ NGAUCHE ],
			( ( (NS32)NLib_Module_SDL_NFenetre_ObtenirResolution( this->m_fenetre )->y / 2 ) - ( (NS32)tailleTotale.y / 2 ) )
				+ ( i * BTAILLE_CADRE_JOUEUR_MENU_REJOINDRE.y )
				+ i * ( BPADDING_CADRE_JOUEUR_MENU_REJOINDRE[ NHAUT ] + BPADDING_CADRE_JOUEUR_MENU_REJOINDRE[ NBAS ]
					+ BMARGING_CADRE_JOUEUR_MENU_REJOINDRE[ NHAUT ] + BMARGING_CADRE_JOUEUR_MENU_REJOINDRE[ NBAS ] ) );

		// Enregistrer taille
		NDEFINIR_POSITION( taille,
			BTAILLE_CADRE_JOUEUR_MENU_REJOINDRE.x + BPADDING_CADRE_JOUEUR_MENU_REJOINDRE[ NGAUCHE ] + BPADDING_CADRE_JOUEUR_MENU_REJOINDRE[ NDROITE ],
			BTAILLE_CADRE_JOUEUR_MENU_REJOINDRE.y + BPADDING_CADRE_JOUEUR_MENU_REJOINDRE[ NHAUT ] + BPADDING_CADRE_JOUEUR_MENU_REJOINDRE[ NBAS ] );

		// Construire cadre joueur
		if( !( this->m_cadreJoueur[ i ] = NLib_Module_SDL_NCadre_Construire( position,
			taille,
			BCOULEUR_CADRE_PERSONNAGE_CONTOUR_MENU_REJOINDRE,
			BCOULEUR_CADRE_PERSONNAGE_FOND_MENU_REJOINDRE,
			this->m_fenetre,
			BEPAISSEUR_CADRE_JOUEUR_MENU_REJOINDRE ) ) )
		{
			// Notifier
			NOTIFIER_ERREUR( NERREUR_CONSTRUCTOR_FAILED );

			// Liberer
			for( j = 0; j < i; j++ )
				NLib_Module_SDL_NCadre_Detruire( &this->m_cadreJoueur[ j ] );
			NLib_Module_SDL_TTF_NPolice_Detruire( &this->m_policeNomJoueur );

			// Quitter
			return NFALSE;
		}

		// Definir taille
		NDEFINIR_POSITION( taille,
			BTAILLE_CHARSET.x,
			BTAILLE_CHARSET.y );

		// Definir position
		NDEFINIR_POSITION( position,
			NLib_Module_SDL_NCadre_ObtenirPosition( this->m_cadreJoueur[ i ] )->x + ( ( NLib_Module_SDL_NCadre_ObtenirTaille( this->m_cadreJoueur[ i ] )->x / 2 ) - ( taille.x / 2 ) ),
				NLib_Module_SDL_NCadre_ObtenirPosition( this->m_cadreJoueur[ i ] )->y + BMARGE_BOUTON_CHOIX_PERSONNAGE_MENU_REJOINDRE );

		// Construire
		if( !( this->m_boutonPersonnage[ i ] = NLib_Module_SDL_Bouton_NBouton_Construire( position,
			taille,
			BCOULEUR_BOUTON_CHOIX_PERSONNAGE_CONTOUR_MENU_REJOINDRE,
			BCOULEUR_BOUTON_CHOIX_PERSONNAGE_FOND_MENU_REJOINDRE,
			this->m_fenetre,
			BEPAISSEUR_BOUTON_CHOIX_PERSONNAGE_MENU_REJOINDRE ) ) )
		{
			// Notifier
			NOTIFIER_ERREUR( NERREUR_CONSTRUCTOR_FAILED );

			// Liberer
			for( j = 0; j < i + 1; j++ )
				NLib_Module_SDL_NCadre_Detruire( &this->m_cadreJoueur[ j ] );
			for( j = 0; j < i; j++ )
				NLib_Module_SDL_Bouton_NBouton_Detruire( &this->m_boutonPersonnage[ j ] );
			NLib_Module_SDL_TTF_NPolice_Detruire( &this->m_policeNomJoueur );

			// Quitter
			return NFALSE;
		}

		// Definir taille
		NDEFINIR_POSITION( taille,
			NLib_Module_SDL_NCadre_ObtenirTaille( this->m_cadreJoueur[ i ] )->x - ( BPADDING_BOUTON_NOM_JOUEUR_MENU_REJOINDRE[ NGAUCHE ] + BPADDING_BOUTON_NOM_JOUEUR_MENU_REJOINDRE[ NDROITE ] ),
			NLib_Module_SDL_TTF_NPolice_CalculerTailleTexte( this->m_policeNomJoueur,
				"Exemple" ).y + BPADDING_BOUTON_NOM_JOUEUR_MENU_REJOINDRE[ NHAUT ] + BPADDING_BOUTON_NOM_JOUEUR_MENU_REJOINDRE[ NBAS ] );

		// Definir position
		NDEFINIR_POSITION( position,
			NLib_Module_SDL_NCadre_ObtenirPosition( this->m_cadreJoueur[ i ] )->x + ( ( NLib_Module_SDL_NCadre_ObtenirTaille( this->m_cadreJoueur[ i ] )->x / 2 ) - ( taille.x / 2 ) ),
			NLib_Module_SDL_NCadre_ObtenirPosition( this->m_cadreJoueur[ i ] )->y + NLib_Module_SDL_NCadre_ObtenirTaille( this->m_cadreJoueur[ i ] )->y - taille.y - BMARGING_BOUTON_NOM_JOUEUR_MENU_REJOINDRE[ NBAS ] );

		// Construire bouton nom joueur
		if( !( this->m_boutonNomPersonnage[ i ] = NLib_Module_SDL_Bouton_NBouton_Construire( position,
			taille,
			BCOULEUR_BOUTON_CHOIX_NOM_CONTOUR_MENU_REJOINDRE,
			BCOULEUR_BOUTON_CHOIX_NOM_FOND_MENU_REJOINDRE,
			this->m_fenetre,
			BEPAISSEUR_BOUTON_CHOIX_NOM_MENU_REJOINDRE ) ) )
		{
			// Notifier
			NOTIFIER_ERREUR( NERREUR_CONSTRUCTOR_FAILED );

			// Liberer
			for( j = 0; j < i + 1; j++ )
			{
				NLib_Module_SDL_NCadre_Detruire( &this->m_cadreJoueur[ j ] );
				NLib_Module_SDL_Bouton_NBouton_Detruire( &this->m_boutonPersonnage[ j ] );
			}
			for( j = 0; j < i; j++ )
				NLib_Module_SDL_Bouton_NBouton_Detruire( &this->m_boutonNomPersonnage[ j ] );
			NLib_Module_SDL_TTF_NPolice_Detruire( &this->m_policeNomJoueur );

			// Quitter
			return NFALSE;
		}
	}

	// Creer saisie
	if( !( this->m_saisieNomPersonnage = NLib_Module_SDL_Saisie_NSaisieSDL_Construire( NMODE_SAISIE_SDL_CHIFFRES | NMODE_SAISIE_SDL_LETTRES,
		BTAILLE_MAXIMALE_NOM_JOUEUR,
		SDLK_RETURN,
		(void ( ___cdecl* )( void* ))Projet_Client_Menu_Rejoindre_BMenuRejoindre_CallbackGestionSaisieInterne,
		(void ( ___cdecl* )( void* ))Projet_Client_Menu_Rejoindre_BMenuRejoindre_ActualiserInterne,
		(NBOOL ( ___cdecl* )( void* ))Projet_Client_Menu_Rejoindre_BMenuRejoindre_CallbackUpdateChaineSaisieNomJoueurInterne,
		this,
		this,
		this,
		NULL ) ) )
	{
		// Notifier
		NOTIFIER_ERREUR( NERREUR_CONSTRUCTOR_FAILED );

		// Liberer
		for( i = 0; i < BNOMBRE_MAXIMUM_JOUEUR; i++ )
		{
			NLib_Module_SDL_NCadre_Detruire( &this->m_cadreJoueur[ i ] );
			NLib_Module_SDL_Bouton_NBouton_Detruire( &this->m_boutonPersonnage[ i ] );
			NLib_Module_SDL_Bouton_NBouton_Detruire( &this->m_boutonNomPersonnage[ i ] );
		}
		NLib_Module_SDL_TTF_NPolice_Detruire( &this->m_policeNomJoueur );

		// Quitter
		return NFALSE;
	}

	// Creer bouton pret
	for( i = 0; i < BNOMBRE_MAXIMUM_JOUEUR; i++ )
	{
		// Definir taille
		NDEFINIR_POSITION( taille,
			BTAILLE_BOUTON_PRET_MENU_REJOINDRE.x,
			NLib_Module_SDL_NCadre_ObtenirTaille( this->m_cadreJoueur[ i ] )->y );

		// Definir position
		NDEFINIR_POSITION( position,
			NLib_Module_SDL_NCadre_ObtenirPosition( this->m_cadreJoueur[ i ] )->x + NLib_Module_SDL_NCadre_ObtenirTaille( this->m_cadreJoueur[ i ] )->x + BMARGING_BOUTON_PRET_MENU_REJOINDRE[ NGAUCHE ],
			NLib_Module_SDL_NCadre_ObtenirPosition( this->m_cadreJoueur[ i ] )->y + ( ( NLib_Module_SDL_NCadre_ObtenirTaille( this->m_cadreJoueur[ i ] )->y / 2 ) - ( taille.y / 2 ) ) );

		// Construire
		if( !( this->m_boutonPret[ i ] = NLib_Module_SDL_Bouton_NBouton_Construire( position,
			taille,
			BCOULEUR_BOUTON_PRET_PAS_PRET_CONTOUR_MENU_REJOINDRE,
			BCOULEUR_BOUTON_PRET_PAS_PRET_FOND_MENU_REJOINDRE,
			this->m_fenetre,
			BEPAISSEUR_BOUTON_PRET_MENU_REJOINDRE ) ) )
		{
			// Notifier
			NOTIFIER_ERREUR( NERREUR_CONSTRUCTOR_FAILED );

			// Liberer
			for( j = 0; j < i; j++ )
				NLib_Module_SDL_Bouton_NBouton_Detruire( &this->m_boutonPret[ j ] );
			NLib_Module_SDL_Saisie_NSaisieSDL_Detruire( &this->m_saisieNomPersonnage );
			for( i = 0; i < BNOMBRE_MAXIMUM_JOUEUR; i++ )
			{
				NLib_Module_SDL_NCadre_Detruire( &this->m_cadreJoueur[ i ] );
				NLib_Module_SDL_Bouton_NBouton_Detruire( &this->m_boutonPersonnage[ i ] );
				NLib_Module_SDL_Bouton_NBouton_Detruire( &this->m_boutonNomPersonnage[ i ] );
			}
			NLib_Module_SDL_TTF_NPolice_Detruire( &this->m_policeNomJoueur );

			// Quitter
			return NFALSE;
		}
	}

	// Creer police
	if( !( police = NLib_Module_SDL_TTF_NPolice_Construire2( Projet_Commun_Ressource_Police_BListePolice_ObtenirLien( BPOLICE_BOUTON_PRET_MENU_REJOINDRE ),
		BTAILLE_POLICE_BOUTON_PRET_MENU_REJOINDRE,
		BCOULEUR_POLICE_BOUTON_PRET_MENU_REJOINDRE ) ) )
	{
		// Notifier
		NOTIFIER_ERREUR( NERREUR_CONSTRUCTOR_FAILED );

		// Liberer
		for( j = 0; j < i; j++ )
			NLib_Module_SDL_Surface_NSurface_Detruire( &this->m_surfaceTextePret[ j ] );
		NLib_Module_SDL_TTF_NPolice_Detruire( &police );
		NLib_Module_SDL_Saisie_NSaisieSDL_Detruire( &this->m_saisieNomPersonnage );
		for( i = 0; i < BNOMBRE_MAXIMUM_JOUEUR; i++ )
		{
			NLib_Module_SDL_Bouton_NBouton_Detruire( &this->m_boutonPret[ i ] );
			NLib_Module_SDL_NCadre_Detruire( &this->m_cadreJoueur[ i ] );
			NLib_Module_SDL_Bouton_NBouton_Detruire( &this->m_boutonPersonnage[ i ] );
			NLib_Module_SDL_Bouton_NBouton_Detruire( &this->m_boutonNomPersonnage[ i ] );
		}
		NLib_Module_SDL_TTF_NPolice_Detruire( &this->m_policeNomJoueur );

		// Quitter
		return NFALSE;
	}

	// Creer texte pret
	for( i = 0; i < BETATS_PRET; i++ )
		if( !( this->m_surfaceTextePret[ i ] = NLib_Module_SDL_TTF_NPolice_CreerTexte( police,
			this->m_fenetre,
			Projet_Commun_Reseau_Client_BEtatPret_ObtenirNomEtat( i ) ) ) )
		{
			// Notifier
			NOTIFIER_ERREUR( NERREUR_CONSTRUCTOR_FAILED );

			// Liberer
			for( j = 0; j < i; j++ )
				NLib_Module_SDL_Surface_NSurface_Detruire( &this->m_surfaceTextePret[ j ] );
			NLib_Module_SDL_TTF_NPolice_Detruire( &police );
			NLib_Module_SDL_Saisie_NSaisieSDL_Detruire( &this->m_saisieNomPersonnage );
			for( i = 0; i < BNOMBRE_MAXIMUM_JOUEUR; i++ )
			{
				NLib_Module_SDL_Bouton_NBouton_Detruire( &this->m_boutonPret[ i ] );
				NLib_Module_SDL_NCadre_Detruire( &this->m_cadreJoueur[ i ] );
				NLib_Module_SDL_Bouton_NBouton_Detruire( &this->m_boutonPersonnage[ i ] );
				NLib_Module_SDL_Bouton_NBouton_Detruire( &this->m_boutonNomPersonnage[ i ] );
			}
			NLib_Module_SDL_TTF_NPolice_Detruire( &this->m_policeNomJoueur );

			// Quitter
			return NFALSE;
		}

	// Detruire police
	NLib_Module_SDL_TTF_NPolice_Detruire( &police );

	// Construire police texte lancement
	if( !( police = NLib_Module_SDL_TTF_NPolice_Construire2( Projet_Commun_Ressource_Police_BListePolice_ObtenirLien( BPOLICE_TEXTE_BOUTON_LANCER_MENU_REJOINDRE ),
		BTAILLE_POLICE_TEXTE_BOUTON_LANCER_MENU_REJOINDRE,
		BCOULEUR_POLICE_TEXTE_BOUTON_LANCER_MENU_REJOINDRE ) ) )
	{
		// Notifier
		NOTIFIER_ERREUR( NERREUR_CONSTRUCTOR_FAILED );

		// Liberer
		for( i = 0; i < BETATS_PRET; i++ )
			NLib_Module_SDL_Surface_NSurface_Detruire( &this->m_surfaceTextePret[ i ] );
		NLib_Module_SDL_Saisie_NSaisieSDL_Detruire( &this->m_saisieNomPersonnage );
		for( i = 0; i < BNOMBRE_MAXIMUM_JOUEUR; i++ )
		{
			NLib_Module_SDL_Bouton_NBouton_Detruire( &this->m_boutonPret[ i ] );
			NLib_Module_SDL_NCadre_Detruire( &this->m_cadreJoueur[ i ] );
			NLib_Module_SDL_Bouton_NBouton_Detruire( &this->m_boutonPersonnage[ i ] );
			NLib_Module_SDL_Bouton_NBouton_Detruire( &this->m_boutonNomPersonnage[ i ] );
		}
		NLib_Module_SDL_TTF_NPolice_Detruire( &this->m_policeNomJoueur );

		// Quitter
		return NFALSE;
	}

	// Creer texte lancement
	if( !( this->m_surfaceTexteLancer = NLib_Module_SDL_TTF_NPolice_CreerTexte( police,
		this->m_fenetre,
		BTEXTE_LANCER_MENU_REJOINDRE ) ) )
	{
		// Notifier
		NOTIFIER_ERREUR( NERREUR_CONSTRUCTOR_FAILED );

		// Liberer
		NLib_Module_SDL_TTF_NPolice_Detruire( &police );
		for( i = 0; i < BETATS_PRET; i++ )
			NLib_Module_SDL_Surface_NSurface_Detruire( &this->m_surfaceTextePret[ i ] );
		NLib_Module_SDL_Saisie_NSaisieSDL_Detruire( &this->m_saisieNomPersonnage );
		for( i = 0; i < BNOMBRE_MAXIMUM_JOUEUR; i++ )
		{
			NLib_Module_SDL_Bouton_NBouton_Detruire( &this->m_boutonPret[ i ] );
			NLib_Module_SDL_NCadre_Detruire( &this->m_cadreJoueur[ i ] );
			NLib_Module_SDL_Bouton_NBouton_Detruire( &this->m_boutonPersonnage[ i ] );
			NLib_Module_SDL_Bouton_NBouton_Detruire( &this->m_boutonNomPersonnage[ i ] );
		}
		NLib_Module_SDL_TTF_NPolice_Detruire( &this->m_policeNomJoueur );

		// Quitter
		return NFALSE;
	}

	// Liberer police
	NLib_Module_SDL_TTF_NPolice_Detruire( &police );


	// Construire bouton lancer
		// Definir taille
			NDEFINIR_POSITION( taille,
				NLib_Module_SDL_Surface_NSurface_ObtenirTaille( this->m_surfaceTexteLancer )->x + BPADDING_BOUTON_LANCER_MENU_REJOINDRE[ NGAUCHE ] + BPADDING_BOUTON_LANCER_MENU_REJOINDRE[ NDROITE ],
				NLib_Module_SDL_Surface_NSurface_ObtenirTaille( this->m_surfaceTexteLancer )->y + BPADDING_BOUTON_LANCER_MENU_REJOINDRE[ NHAUT ] + BPADDING_BOUTON_LANCER_MENU_REJOINDRE[ NBAS ] );
		// Definir position
			NDEFINIR_POSITION( position,
				0,
				0 );
		// Construire
			if( !( this->m_boutonLancer = NLib_Module_SDL_Bouton_NBouton_Construire( position,
				taille,
				BCOULEUR_BOUTON_CONTOUR_LANCER_MENU_REJOINDRE,
				BCOULEUR_BOUTON_FOND_LANCER_MENU_REJOINDRE,
				this->m_fenetre,
				BEPAISSEUR_BOUTON_LANCER_MENU_REJOINDRE ) ) )
			{
				// Notifier
				NOTIFIER_ERREUR( NERREUR_CONSTRUCTOR_FAILED );

				// Liberer
				NLib_Module_SDL_Surface_NSurface_Detruire( &this->m_surfaceTexteLancer );
				for( i = 0; i < BETATS_PRET; i++ )
					NLib_Module_SDL_Surface_NSurface_Detruire( &this->m_surfaceTextePret[ i ] );
				NLib_Module_SDL_Saisie_NSaisieSDL_Detruire( &this->m_saisieNomPersonnage );
				for( i = 0; i < BNOMBRE_MAXIMUM_JOUEUR; i++ )
				{
					NLib_Module_SDL_Bouton_NBouton_Detruire( &this->m_boutonPret[ i ] );
					NLib_Module_SDL_NCadre_Detruire( &this->m_cadreJoueur[ i ] );
					NLib_Module_SDL_Bouton_NBouton_Detruire( &this->m_boutonPersonnage[ i ] );
					NLib_Module_SDL_Bouton_NBouton_Detruire( &this->m_boutonNomPersonnage[ i ] );
				}
				NLib_Module_SDL_TTF_NPolice_Detruire( &this->m_policeNomJoueur );

				// Quitter
				return NFALSE;
			}

	// OK
	return NTRUE;
}

/* Update taille cadre adresse (privee) */
void Projet_Client_Menu_Rejoindre_BMenuRejoindre_UpdateCadreAdresseEtValidationInterne( BMenuRejoindre *this )
{
	// Taille texte
	NUPoint tailleTexte;

	// Position
	NSPoint position;

	// Taille
	NUPoint taille;

	// Verifier
	if( !this->m_adresseConnexion )
		taille = BTAILLE_MINIMALE_CADRE_ADRESSE_MENU_REJOINDRE;
	else
	{
		// Obtenir la taille du texte
		tailleTexte = NLib_Module_SDL_TTF_NPolice_CalculerTailleTexte( this->m_policeAdresse,
			this->m_adresseConnexion );

		// Definir taille
		NDEFINIR_POSITION( taille,
			tailleTexte.x + BMARGING_CADRE_ADRESSE_MENU_REJOINDRE[ NGAUCHE ] + BMARGING_CADRE_ADRESSE_MENU_REJOINDRE[ NDROITE ],
			tailleTexte.y + BMARGING_CADRE_ADRESSE_MENU_REJOINDRE[ NHAUT ] + BMARGING_CADRE_ADRESSE_MENU_REJOINDRE[ NBAS ] );

		// Definir
		NLib_Module_SDL_Bouton_NBouton_DefinirTaille( this->m_boutonAdresse,
			taille );
	}

	// Calculer position centre
	NDEFINIR_POSITION( position,
		( NLib_Module_SDL_NFenetre_ObtenirResolution( this->m_fenetre )->x / 2 ) - taille.x / 2,
		( NLib_Module_SDL_NFenetre_ObtenirResolution( this->m_fenetre )->y / 2 ) - taille.y / 2 );

	// Definir
	NLib_Module_SDL_Bouton_NBouton_DefinirPosition( this->m_boutonAdresse,
		position );

	// Placer bouton validation
		// Definir la taille
			NDEFINIR_POSITION( taille,
				NLib_Module_SDL_Surface_NSurface_ObtenirTaille( this->m_surfaceValidationConnexion )->x + BPADDING_CADRE_VALIDATION_CONNEXION_MENU_REJOINDRE[ NGAUCHE ] + BPADDING_CADRE_VALIDATION_CONNEXION_MENU_REJOINDRE[ NDROITE ],
				NLib_Module_SDL_Surface_NSurface_ObtenirTaille( this->m_surfaceValidationConnexion )->y + BPADDING_CADRE_VALIDATION_CONNEXION_MENU_REJOINDRE[ NHAUT ] + BPADDING_CADRE_VALIDATION_CONNEXION_MENU_REJOINDRE[ NBAS ] );
		// Definir la police
			NDEFINIR_POSITION( position,
				( NLib_Module_SDL_NFenetre_ObtenirResolution( this->m_fenetre )->x / 2 ) - ( taille.x / 2 ),
				NLib_Module_SDL_Bouton_NBouton_ObtenirPosition( this->m_boutonAdresse ).y + NLib_Module_SDL_Bouton_NBouton_ObtenirTaille( this->m_boutonAdresse ).y + BMARGING_CADRE_VALIDATION_CONNEXION_MENU_REJOINDRE[ NHAUT ] );
		//	Enregistrer
			NLib_Module_SDL_Bouton_NBouton_DefinirPosition( this->m_boutonValidationConnexion,
				position );
			NLib_Module_SDL_Bouton_NBouton_DefinirTaille( this->m_boutonValidationConnexion,
				taille );

	// Placer texte
		// Connexion
			NLib_Module_SDL_Surface_NSurface_DefinirPosition( this->m_surfaceValidationConnexion,
				NLib_Module_SDL_Bouton_NBouton_ObtenirPosition( this->m_boutonValidationConnexion ).x + ( NLib_Module_SDL_Bouton_NBouton_ObtenirTaille( this->m_boutonValidationConnexion ).x / 2 - NLib_Module_SDL_Surface_NSurface_ObtenirTaille( this->m_surfaceValidationConnexion )->x / 2 ),
				NLib_Module_SDL_Bouton_NBouton_ObtenirPosition( this->m_boutonValidationConnexion ).y + ( NLib_Module_SDL_Bouton_NBouton_ObtenirTaille( this->m_boutonValidationConnexion ).y / 2 - NLib_Module_SDL_Surface_NSurface_ObtenirTaille( this->m_surfaceValidationConnexion )->y / 2 ) );
		// Titre
			NLib_Module_SDL_Surface_NSurface_DefinirPosition( this->m_surfaceTitreConnexion,
				NLib_Module_SDL_Bouton_NBouton_ObtenirPosition( this->m_boutonAdresse ).x + ( NLib_Module_SDL_Bouton_NBouton_ObtenirTaille( this->m_boutonAdresse ).x / 2 - NLib_Module_SDL_Surface_NSurface_ObtenirTaille( this->m_surfaceTitreConnexion )->x / 2 ),
				NLib_Module_SDL_Bouton_NBouton_ObtenirPosition( this->m_boutonAdresse ).y - NLib_Module_SDL_Surface_NSurface_ObtenirTaille( this->m_surfaceTitreConnexion )->y - BMARGE_TITRE_CONNEXION_MENU_REJOINDRE );
		// Adresse
			if( this->m_surfaceAdresseConnexion != NULL )
				NLib_Module_SDL_Surface_NSurface_DefinirPosition( this->m_surfaceAdresseConnexion,
					NLib_Module_SDL_Bouton_NBouton_ObtenirPosition( this->m_boutonAdresse ).x + ( NLib_Module_SDL_Bouton_NBouton_ObtenirTaille( this->m_boutonAdresse ).x / 2 - NLib_Module_SDL_Surface_NSurface_ObtenirTaille( this->m_surfaceAdresseConnexion )->x / 2 ),
					NLib_Module_SDL_Bouton_NBouton_ObtenirPosition( this->m_boutonAdresse ).y + ( NLib_Module_SDL_Bouton_NBouton_ObtenirTaille( this->m_boutonAdresse ).y / 2 - NLib_Module_SDL_Surface_NSurface_ObtenirTaille( this->m_surfaceAdresseConnexion )->y / 2 ) );

}

/* Callback update chaine saisie (privee) */
__CALLBACK NBOOL Projet_Client_Menu_Rejoindre_BMenuRejoindre_CallbackUpdateChaineSaisieAdresseInterne( BMenuRejoindre *this )
{
	// Liberer
	NLib_Module_SDL_Surface_NSurface_Detruire( &this->m_surfaceAdresseConnexion );

	// Creer texte
	if( !( this->m_surfaceAdresseConnexion = NLib_Module_SDL_TTF_NPolice_CreerTexte( this->m_policeAdresse,
		this->m_fenetre,
		this->m_adresseConnexion ) ) )
	{
		// Notifier
		NOTIFIER_AVERTISSEMENT( NERREUR_CONSTRUCTOR_FAILED );

		// Quitter
		return NFALSE;
	}

	// Update position
	Projet_Client_Menu_Rejoindre_BMenuRejoindre_UpdateCadreAdresseEtValidationInterne( this );

	// OK
	return NTRUE;
}

/* Creer elements connexion (privee) */
NBOOL Projet_Client_Menu_Rejoindre_BMenuRejoindre_CreerElementConnexionInterne( BMenuRejoindre *this )
{
	// Police
	NPolice *police;

	// Position
	NSPoint position;

	// Taille
	NUPoint taille;

	// Definir taille
	NDEFINIR_POSITION( taille,
		BTAILLE_MINIMALE_CADRE_ADRESSE_MENU_REJOINDRE.x,
		BTAILLE_MINIMALE_CADRE_ADRESSE_MENU_REJOINDRE.y );

	// Definir position
	NDEFINIR_POSITION( position,
		( NLib_Module_SDL_NFenetre_ObtenirResolution( this->m_fenetre )->x / 2 ) - ( taille.x / 2 ),
		( NLib_Module_SDL_NFenetre_ObtenirResolution( this->m_fenetre )->y / 2 ) - ( taille.y / 2 ) );

	// Creer le cadre autour de l'adresse
	if( !( this->m_boutonAdresse = NLib_Module_SDL_Bouton_NBouton_Construire( position,
		taille,
		BCOULEUR_CONTOUR_CADRE_ADRESSE_MENU_REJOINDRE,
		BCOULEUR_FOND_CADRE_ADRESSE_MENU_REJOINDRE,
		this->m_fenetre,
		BEPAISSEUR_CADRE_ADRESSE_MENU_REJOINDRE ) ) )
	{
		// Notifier
		NOTIFIER_ERREUR( NERREUR_CONSTRUCTOR_FAILED );

		// Quitter
		return NFALSE;
	}

	// Creer la police
	if( !( police = NLib_Module_SDL_TTF_NPolice_Construire2( Projet_Commun_Ressource_Police_BListePolice_ObtenirLien( BPOLICE_TEXTE_VALIDATION_CONNEXION_MENU_REJOINDRE ),
		BTAILLE_POLICE_TEXTE_VALIDATION_CONNEXION_MENU_REJOINDRE,
		BCOULEUR_POLICE_TEXTE_VALIDATION_CONNEXION_MENU_REJOINDRE ) ) )
	{
		// Notifier
		NOTIFIER_ERREUR( NERREUR_CONSTRUCTOR_FAILED );

		// Detruire
		NLib_Module_SDL_Bouton_NBouton_Detruire( &this->m_boutonAdresse );

		// Quitter
		return NFALSE;
	}

	// Creer le texte
	if( !( this->m_surfaceValidationConnexion = NLib_Module_SDL_TTF_NPolice_CreerTexte( police,
		this->m_fenetre,
		BTEXTE_CONNEXION_MENU_REJOINDRE ) ) )
	{
		// Notifier
		NOTIFIER_ERREUR( NERREUR_CONSTRUCTOR_FAILED );

		// Detruire
		NLib_Module_SDL_TTF_NPolice_Detruire( &police );
		NLib_Module_SDL_Bouton_NBouton_Detruire( &this->m_boutonAdresse );

		// Quitter
		return NFALSE;
	}

	// Fermer la police
	NLib_Module_SDL_TTF_NPolice_Detruire( &police );

	// Construire la police
	if( !( police = NLib_Module_SDL_TTF_NPolice_Construire2( Projet_Commun_Ressource_Police_BListePolice_ObtenirLien( BPOLICE_TEXTE_TITRE_CONNEXION_MENU_REJOINDRE ),
		BTAILLE_POLICE_TEXTE_TITRE_CONNEXION_MENU_REJOINDRE,
		BCOULEUR_POLICE_TEXTE_TITRE_CONNEXION_MENU_REJOINDRE ) ) )
	{
		// Notifier
		NOTIFIER_ERREUR( NERREUR_CONSTRUCTOR_FAILED );

		// Detruire
		NLib_Module_SDL_Surface_NSurface_Detruire( &this->m_surfaceValidationConnexion );
		NLib_Module_SDL_Bouton_NBouton_Detruire( &this->m_boutonAdresse );

		// Quitter
		return NFALSE;
	}

	// Creer la surface
	if( !( this->m_surfaceTitreConnexion = NLib_Module_SDL_TTF_NPolice_CreerTexte( police,
		this->m_fenetre,
		BTEXTE_TITRE_CONNEXION_MENU_REJOINDRE ) ) )
	{
		// Notifier
		NOTIFIER_ERREUR( NERREUR_CONSTRUCTOR_FAILED );

		// Detruire
		NLib_Module_SDL_TTF_NPolice_Detruire( &police );
		NLib_Module_SDL_Surface_NSurface_Detruire( &this->m_surfaceValidationConnexion );
		NLib_Module_SDL_Bouton_NBouton_Detruire( &this->m_boutonAdresse );

		// Quitter
		return NFALSE;
	}

	// Fermer la police
	NLib_Module_SDL_TTF_NPolice_Detruire( &police );

	// Definir la taille
	NDEFINIR_POSITION( taille,
		NLib_Module_SDL_Surface_NSurface_ObtenirTaille( this->m_surfaceValidationConnexion )->x + BPADDING_CADRE_VALIDATION_CONNEXION_MENU_REJOINDRE[ NGAUCHE ] + BPADDING_CADRE_VALIDATION_CONNEXION_MENU_REJOINDRE[ NDROITE ],
		NLib_Module_SDL_Surface_NSurface_ObtenirTaille( this->m_surfaceValidationConnexion )->y + BPADDING_CADRE_VALIDATION_CONNEXION_MENU_REJOINDRE[ NHAUT ] + BPADDING_CADRE_VALIDATION_CONNEXION_MENU_REJOINDRE[ NBAS ] );

	// Definir la police
	NDEFINIR_POSITION( position,
		( NLib_Module_SDL_NFenetre_ObtenirResolution( this->m_fenetre )->x / 2 ) - ( taille.x / 2 ),
		NLib_Module_SDL_Bouton_NBouton_ObtenirPosition( this->m_boutonAdresse ).y + BMARGING_CADRE_VALIDATION_CONNEXION_MENU_REJOINDRE[ NHAUT ] );

	// Creer le cadre validation
	if( !( this->m_boutonValidationConnexion = NLib_Module_SDL_Bouton_NBouton_Construire( position,
		taille,
		BCOULEUR_CONTOUR_CADRE_VALIDATION_MENU_REJOINDRE,
		BCOULEUR_FOND_CADRE_VALIDATION_MENU_REJOINDRE,
		this->m_fenetre,
		BEPAISSEUR_CADRE_VALIDATION_MENU_REJOINDRE ) ) )
	{
		// Notifier
		NOTIFIER_ERREUR( NERREUR_CONSTRUCTOR_FAILED );

		// Detruire
		NLib_Module_SDL_Surface_NSurface_Detruire( &this->m_surfaceTitreConnexion );
		NLib_Module_SDL_Surface_NSurface_Detruire( &this->m_surfaceValidationConnexion );
		NLib_Module_SDL_Bouton_NBouton_Detruire( &this->m_boutonAdresse );

		// Quitter
		return NFALSE;
	}

	// Creer la police pour l'adresse
	if( !( this->m_policeAdresse = NLib_Module_SDL_TTF_NPolice_Construire2( Projet_Commun_Ressource_Police_BListePolice_ObtenirLien( BPOLICE_TEXTE_ADRESSE_MENU_REJOINDRE ),
		BTAILLE_POLICE_TEXTE_ADRESSE_MENU_REJOINDRE,
		BCOULEUR_POLICE_TEXTE_ADRESSE_MENU_REJOINDRE ) ) )
	{
		// Notifier
		NOTIFIER_ERREUR( NERREUR_CONSTRUCTOR_FAILED );

		// Detruire
		NLib_Module_SDL_Surface_NSurface_Detruire( &this->m_surfaceTitreConnexion );
		NLib_Module_SDL_Surface_NSurface_Detruire( &this->m_surfaceValidationConnexion );
		NLib_Module_SDL_Bouton_NBouton_Detruire( &this->m_boutonAdresse );

		// Quitter
		return NFALSE;
	}

	// Composer ip/port defaut
		// Allouer
			if( !( this->m_adresseConnexion = calloc( NLIB_TAILLE_MAXIMALE_CHAINE_IP_PORT + 1,
				sizeof( char ) ) ) )
			{
				// Notifier
				NOTIFIER_ERREUR( NERREUR_ALLOCATION_FAILED );

				// Detruire
				NLib_Module_SDL_TTF_NPolice_Detruire( &this->m_policeAdresse );
				NLib_Module_SDL_Surface_NSurface_Detruire( &this->m_surfaceTitreConnexion );
				NLib_Module_SDL_Surface_NSurface_Detruire( &this->m_surfaceValidationConnexion );
				NLib_Module_SDL_Bouton_NBouton_Detruire( &this->m_boutonAdresse );

				// Quitter
				return NFALSE;
			}
		// Composer
			sprintf( this->m_adresseConnexion,
				"%s:%d",
				NLIB_ADRESSE_IP_LOCALE,
				Projet_Client_Configuration_BConfiguration_ObtenirPort( Projet_Client_BClient_ObtenirConfiguration( this->m_client ) ) );

	// Creer l'instance de saisie
	if( !( this->m_saisieAdresseConnexion = NLib_Module_SDL_Saisie_NSaisieSDL_Construire( NMODE_SAISIE_SDL_CHIFFRES | NMODE_SAISIE_SDL_CARACTERES_SPECIAUX,
		NLIB_TAILLE_MAXIMALE_CHAINE_IP_PORT,
		SDLK_RETURN,
		(void ( ___cdecl* )( void* ))Projet_Client_Menu_Rejoindre_BMenuRejoindre_CallbackGestionSaisieInterne,
		(void ( ___cdecl* )( void* ))Projet_Client_Menu_Rejoindre_BMenuRejoindre_ActualiserInterne,
		(NBOOL ( ___cdecl* )( void* ))Projet_Client_Menu_Rejoindre_BMenuRejoindre_CallbackUpdateChaineSaisieAdresseInterne,
		this,
		this,
		this,
		this->m_adresseConnexion ) ) )
	{
		// Notifier
		NOTIFIER_ERREUR( NERREUR_CONSTRUCTOR_FAILED );

		// Detruire
		NFREE( this->m_adresseConnexion );
		NLib_Module_SDL_TTF_NPolice_Detruire( &this->m_policeAdresse );
		NLib_Module_SDL_Surface_NSurface_Detruire( &this->m_surfaceTitreConnexion );
		NLib_Module_SDL_Surface_NSurface_Detruire( &this->m_surfaceValidationConnexion );
		NLib_Module_SDL_Bouton_NBouton_Detruire( &this->m_boutonAdresse );

		// Quitter
		return NFALSE;
	}

	// Creer surface adresse
	if( !( this->m_surfaceAdresseConnexion = NLib_Module_SDL_TTF_NPolice_CreerTexte( this->m_policeAdresse,
		this->m_fenetre,
		this->m_adresseConnexion ) ) )
	{
		// Notifier
		NOTIFIER_ERREUR( NERREUR_CONSTRUCTOR_FAILED );

		// Detruire
		NFREE( this->m_adresseConnexion );
		NLib_Module_SDL_Saisie_NSaisieSDL_Detruire( &this->m_saisieAdresseConnexion );
		NLib_Module_SDL_TTF_NPolice_Detruire( &this->m_policeAdresse );
		NLib_Module_SDL_Surface_NSurface_Detruire( &this->m_surfaceTitreConnexion );
		NLib_Module_SDL_Surface_NSurface_Detruire( &this->m_surfaceValidationConnexion );
		NLib_Module_SDL_Bouton_NBouton_Detruire( &this->m_boutonAdresse );

		// Quitter
		return NFALSE;
	}

	// Reorganiser cadres
	Projet_Client_Menu_Rejoindre_BMenuRejoindre_UpdateCadreAdresseEtValidationInterne( this );

	// OK
	return NTRUE;
}

/* Construire */
__ALLOC BMenuRejoindre *Projet_Client_Menu_Rejoindre_BMenuRejoindre_Construire( const NFenetre *fenetre,
	const struct BClient *client,
	const BFondEtoileMenu *etoile[ BNOMBRE_COUCHES_ETOILES_MENU ] )
{
	// Sortie
	__OUTPUT BMenuRejoindre *out;

	// Iterateur
	NU32 i;

	// Allouer la memoire
	if( !( out = calloc( 1,
		sizeof( BMenuRejoindre ) ) ) )
	{
		// Notifier
		NOTIFIER_ERREUR( NERREUR_ALLOCATION_FAILED );

		// Quitter
		return NULL;
	}

	// Enregistrer
	out->m_fenetre = (NFenetre*)fenetre;
	out->m_client = (struct BClient*)client;
	out->m_ressource = Projet_Client_BClient_ObtenirRessource( client );
	out->m_etoile = etoile;

	// Creer le cadre carte
	if( !( out->m_cadreCarte = NLib_Module_SDL_Bouton_NBouton_Construire( (NSPoint){ 0, 0 },
		(NUPoint){ 0, 0 },
		(NCouleur){ 0xFF, 0xFF, 0xFF, 0xFF },
		(NCouleur){ 0x00, 0x00, 0x00, 0x00 },
		out->m_fenetre,
		2 ) ) )
	{
		// Notifier
		NOTIFIER_ERREUR( NERREUR_CONSTRUCTOR_FAILED );

		// Liberer
		NFREE( out );

		// Quitter
		return NULL;
	}

	// Creer les cadres personnages
	if( !Projet_Client_Menu_Rejoindre_BMenuRejoindre_CreerCadreJoueurInterne( out ) )
	{
		// Notifier
		NOTIFIER_ERREUR( NERREUR_CONSTRUCTOR_FAILED );

		// Liberer
		NLib_Module_SDL_Bouton_NBouton_Detruire( &out->m_cadreCarte );
		NFREE( out );

		// Quitter
		return NULL;
	}

	// Creer elements connexion
	if( !Projet_Client_Menu_Rejoindre_BMenuRejoindre_CreerElementConnexionInterne( out ) )
	{
		// Notifier
		NOTIFIER_ERREUR( NERREUR_CONSTRUCTOR_FAILED );

		// Liberer
		for( i = 0; i < BNOMBRE_MAXIMUM_JOUEUR; i++ )
			NLib_Module_SDL_NCadre_Detruire( &out->m_cadreJoueur[ i ] );
		for( i = 0; i < BNOMBRE_MAXIMUM_JOUEUR; i++ )
			NLib_Module_SDL_Bouton_NBouton_Detruire( &out->m_boutonPersonnage[ i ] );
		NLib_Module_SDL_Bouton_NBouton_Detruire( &out->m_cadreCarte );
		NFREE( out );

		// Quitter
		return NULL;
	}

	// Zero
	out->m_identifiantDerniereModification = 0;

	// Update cadre carte
	Projet_Client_Menu_Rejoindre_BMenuRejoindre_UpdateCadreCarte( out );

	// OK
	return out;
}

/* Detruire */
void Projet_Client_Menu_Rejoindre_BMenuRejoindre_Detruire( BMenuRejoindre **this )
{
	// Iterateur
	NU32 i;

	// Detruire cadres
	for( i = 0; i < BNOMBRE_MAXIMUM_JOUEUR; i++ )
	{
		NLib_Module_SDL_NCadre_Detruire( &(*this)->m_cadreJoueur[ i ] );
		NLib_Module_SDL_Bouton_NBouton_Detruire( &(*this)->m_boutonPret[ i ] );
		NLib_Module_SDL_Bouton_NBouton_Detruire( &(*this)->m_boutonPersonnage[ i ] );
		NLib_Module_SDL_Bouton_NBouton_Detruire( &(*this)->m_boutonNomPersonnage[ i ] );
	}

	// Detruire boutons
	NLib_Module_SDL_Bouton_NBouton_Detruire( &(*this)->m_cadreCarte );
	NLib_Module_SDL_Bouton_NBouton_Detruire( &(*this)->m_boutonAdresse );
	NLib_Module_SDL_Bouton_NBouton_Detruire( &(*this)->m_boutonValidationConnexion );
	NLib_Module_SDL_Bouton_NBouton_Detruire( &(*this)->m_boutonLancer );

	// Detruire saisie
		// Connexion
			NLib_Module_SDL_Surface_NSurface_Detruire( &(*this)->m_surfaceValidationConnexion );
			NLib_Module_SDL_Saisie_NSaisieSDL_Detruire( &(*this)->m_saisieAdresseConnexion );
			NLib_Module_SDL_Surface_NSurface_Detruire( &(*this)->m_surfaceAdresseConnexion );
			NLib_Module_SDL_Surface_NSurface_Detruire( &(*this)->m_surfaceTitreConnexion );
		// Nom joueur
			NLib_Module_SDL_Saisie_NSaisieSDL_Detruire( &(*this)->m_saisieNomPersonnage );

	// Detruire police
	NLib_Module_SDL_TTF_NPolice_Detruire( &(*this)->m_policeAdresse );
	NLib_Module_SDL_TTF_NPolice_Detruire( &(*this)->m_policeNomJoueur );

	// Surface
	for( i = 0; i < BNOMBRE_MAXIMUM_JOUEUR; i++ )
		NLib_Module_SDL_Surface_NSurface_Detruire( &(*this)->m_surfaceNomPersonnage[ i ] );
	for( i = 0; i < BETATS_PRET; i++ )
		NLib_Module_SDL_Surface_NSurface_Detruire( &(*this)->m_surfaceTextePret[ i ] );
	NLib_Module_SDL_Surface_NSurface_Detruire( &(*this)->m_surfaceTexteLancer );

	// Detruire texte
	NFREE( (*this)->m_adresseConnexion );

	// Liberer
	NFREE( (*this) );
}

/* Connecter */
NBOOL Projet_Client_Menu_Rejoindre_BMenuRejoindre_ConnecterInterne( BMenuRejoindre *this,
	const char *ip,
	NU32 port )
{
	// Se connecter
	if( !Projet_Client_BClient_Connecter( this->m_client,
		ip,
		port ) )
	{
		// Notifier
		NOTIFIER_ERREUR( NERREUR_UNABLE_TO_CONNECT );

		// Quitter
		return NFALSE;
	}

	// Changer etape
	this->m_etape = BETAPE_MENU_REJOINDRE_ATTENTE_REPONSE_CONNEXION;

	// Enregistrer temps debut connexion
	this->m_tempsDebutTentativeConnexion = NLib_Temps_ObtenirTick( );

	// OK
	return NTRUE;
}

NBOOL Projet_Client_Menu_Rejoindre_BMenuRejoindre_ConnecterInterne2( BMenuRejoindre *this )
{
	// Curseur
	NU32 curseur = 0;

	// Cha�ne
	const char *chaine;

	// IP
	char *ip;

	// Port
	NU32 port;

	// Resultat
	__OUTPUT NBOOL resultat;

	// Buffer
	char buffer[ 2048 ];

	// Recuperer chaine saisie
	if( !( chaine = NLib_Module_SDL_Saisie_NSaisieSDL_ObtenirTexte( this->m_saisieAdresseConnexion ) ) )
	{
		// Notifier
		NOTIFIER_ERREUR( NERREUR_OBJECT_STATE );

		// Quitter
		return NFALSE;
	}

	// Lire ip
	if( !( ip = NLib_Chaine_LireJusqua( chaine,
		':',
		&curseur,
		NFALSE ) ) )
	{
		// Notifier
		NOTIFIER_ERREUR( NERREUR_PARAMETER_ERROR );

		// Quitter
		return NFALSE;
	}

	// Lire port
	if( ( port = NLib_Chaine_LireNombreNonSigne( chaine,
		&curseur,
		NFALSE ) ) == NERREUR )
	{
		// Notifier
		NOTIFIER_ERREUR( NERREUR_PARAMETER_ERROR );

		// Liberer
		NFREE( ip );

		// Quitter
		return NFALSE;
	}

	// Notifier
		// Composer
			sprintf( buffer,
				"Tentative de connexion a \"%s\" sur le port %d...\n",
				ip,
				port );
		// Notifier
			NOTIFIER_AVERTISSEMENT_UTILISATEUR( NERREUR_USER,
				buffer,
				0 );

	// Connecter
	resultat = Projet_Client_Menu_Rejoindre_BMenuRejoindre_ConnecterInterne( this,
		ip,
		port );

	// Liberer
	NFREE( ip );

	// OK?
	return resultat;
}

/* Mettre a jour evenement (privee) */
void Projet_Client_Menu_Rejoindre_BMenuRejoindre_MettreAJourInterne( BMenuRejoindre *this )
{
	// Iterateur
	NU32 i;

	// Index
	NU32 index;

	// Joueur
	const BEtatClient *joueur;

	// Update minimal
	Projet_Client_Menu_Rejoindre_BMenuRejoindre_UpdateMinimalInterne( this );

	// Gerer etape
	switch( this->m_etape )
	{
		case BETAPE_MENU_REJOINDRE_CONNEXION:
			// Update boutons
			NLib_Module_SDL_Bouton_NBouton_Update( this->m_boutonAdresse,
				&this->m_positionSouris,
				NLib_Module_SDL_NFenetre_ObtenirEvenement( this->m_fenetre ) );
			NLib_Module_SDL_Bouton_NBouton_Update( this->m_boutonValidationConnexion,
				&this->m_positionSouris,
				NLib_Module_SDL_NFenetre_ObtenirEvenement( this->m_fenetre ) );

			// Si clic bouton adresse
			if( NLib_Module_SDL_Bouton_NBouton_ObtenirEtat( this->m_boutonAdresse ) == NETAT_BOUTON_PRESSE )
			{
				// Lancer saisie
				if( NLib_Module_SDL_Saisie_NSaisieSDL_Saisir( this->m_saisieAdresseConnexion,
					NLib_Module_SDL_NFenetre_ObtenirEvenement( this->m_fenetre ),
					&this->m_estEnCours,
					NFALSE,
					&this->m_adresseConnexion ) != NRETOUR_SAISIE_SDL_QUITTER )
					this->m_estEnCours = NTRUE;

				// Remettre a zero
				NLib_Module_SDL_Bouton_NBouton_RemettreEtatAZero( this->m_boutonAdresse );
			}

			// Si clic bouton connexion
			if( NLib_Module_SDL_Bouton_NBouton_ObtenirEtat( this->m_boutonValidationConnexion ) == NETAT_BOUTON_PRESSE )
			{
				// Se connecter
				Projet_Client_Menu_Rejoindre_BMenuRejoindre_ConnecterInterne2( this );

				// Remettre a zero
				NLib_Module_SDL_Bouton_NBouton_RemettreEtatAZero( this->m_boutonValidationConnexion );
			}
			break;

		case BETAPE_MENU_REJOINDRE_ATTENTE_REPONSE_CONNEXION:
			// Verifier timer
			if( NLib_Temps_ObtenirTick( ) - this->m_tempsDebutTentativeConnexion >= BTIMEOUT_CONNEXION_REJOINDRE )
			{
				// Retour a l'etape initiale
				this->m_etape = BETAPE_MENU_REJOINDRE_ATTENTE_REPONSE_CONNEXION;

				// Notifier
				NOTIFIER_AVERTISSEMENT( NERREUR_TIMEOUT );

				// Quitter etape
				break;
			}

			// Proteger cache
			Projet_Client_BClient_ProtegerCacheClient( this->m_client );

			// Si on a bien re�u la confirmation du joueur de la part du serveur
			if( Projet_Client_BClient_ObtenirNombreJoueur( this->m_client ) >= 1 )
			{
				// Envoyer le choix par defaut au serveur
				if( !Projet_Client_TraitementPacket_EnvoyerInformationsJoueur( this->m_client,
					Projet_Client_Configuration_BConfiguration_ObtenirNomDefaut( Projet_Client_BClient_ObtenirConfiguration( this->m_client ) ),
					Projet_Client_Configuration_BConfiguration_ObtenirCharsetDefaut( Projet_Client_BClient_ObtenirConfiguration( this->m_client ) ),
					Projet_Client_Configuration_BConfiguration_ObtenirCouleurCharsetDefaut( Projet_Client_BClient_ObtenirConfiguration( this->m_client ) ) ) )
				{
					// Erreur
					this->m_codeRetour = BCODE_MENU_REJOINDRE_RETOUR_MENU_PRINCIPAL;

					// Quitter
					this->m_estEnCours = NFALSE;
				}
				// Passer a l'etape suivante
				else
					// Etape suivante
					this->m_etape = BETAPE_MENU_REJOINDRE_CHOIX_DETAILS;
			}

			// Ne plus proteger cache
			Projet_Client_BClient_NePlusProtegerCacheClient( this->m_client );
			break;

		case BETAPE_MENU_REJOINDRE_CHOIX_DETAILS:
			// Mettre a jour boutons
				// Carte
					NLib_Module_SDL_Bouton_NBouton_Update( this->m_cadreCarte,
						&this->m_positionSouris,
						NLib_Module_SDL_NFenetre_ObtenirEvenement( this->m_fenetre ) );
				// Personnage
					for( i = 0; i < Projet_Client_BClient_ObtenirNombreJoueur( this->m_client ); i++ )
					{
						// Personnage
						NLib_Module_SDL_Bouton_NBouton_Update( this->m_boutonPersonnage[ i ],
							&this->m_positionSouris,
							NLib_Module_SDL_NFenetre_ObtenirEvenement( this->m_fenetre ) );

						// Nom personnage
						NLib_Module_SDL_Bouton_NBouton_Update( this->m_boutonNomPersonnage[ i ],
							&this->m_positionSouris,
							NLib_Module_SDL_NFenetre_ObtenirEvenement( this->m_fenetre ) );

						// Pret
						NLib_Module_SDL_Bouton_NBouton_Update( this->m_boutonPret[ i ],
							&this->m_positionSouris,
							NLib_Module_SDL_NFenetre_ObtenirEvenement( this->m_fenetre ) );
					}
				// Lancer
					NLib_Module_SDL_Bouton_NBouton_Update( this->m_boutonLancer,
						&this->m_positionSouris,
						NLib_Module_SDL_NFenetre_ObtenirEvenement( this->m_fenetre ) );

			// Proteger cache
			Projet_Client_BClient_ProtegerCacheClient( this->m_client );

			// Obtenir index
			if( ( index = Projet_Client_BClient_ObtenirIndexJoueurCourant( this->m_client ) ) != NERREUR )
			{
				// Si clic sur bouton pret
				if( NLib_Module_SDL_Bouton_NBouton_ObtenirEtat( this->m_boutonPret[ index ] ) == NETAT_BOUTON_PRESSE )
				{
					// Effectuer changement etat
					Projet_Client_Menu_Rejoindre_BMenuRejoindre_ChangerEtatPretInterne( this );

					// Remettre a zero
					NLib_Module_SDL_Bouton_NBouton_RemettreEtatAZero( this->m_boutonPret[ index ] );
				}

				// Obtenir joueur
				if( !( joueur = Projet_Client_BClient_ObtenirJoueurCourant( this->m_client ) ) )
				{
					// Ne plus proteger cache
					Projet_Client_BClient_NePlusProtegerCacheClient( this->m_client );
					break;
				}

				// Verifier que le joueur ne soit pas pret
				if( !( Projet_Commun_Reseau_Client_BEtatClient_EstPret( joueur ) ) )
				{
					// Si clic sur carte
					if( NLib_Module_SDL_Bouton_NBouton_ObtenirEtat( this->m_cadreCarte ) == NETAT_BOUTON_PRESSE
						&& Projet_Client_BClient_EstHote( this->m_client ) )
					{
						// Changer carte
						Projet_Client_Menu_Rejoindre_BMenuRejoindre_ChangerCarteInterne( this );

						// Remettre bouton a zero
						NLib_Module_SDL_Bouton_NBouton_RemettreEtatAZero( this->m_cadreCarte );
					}

					// Si clic sur charset
					if( NLib_Module_SDL_Bouton_NBouton_ObtenirEtat( this->m_boutonPersonnage[ index ] ) == NETAT_BOUTON_PRESSE )
					{
						// Changer charset/couleur
						switch( NLib_Module_SDL_Bouton_NBouton_ObtenirBoutonActivation( this->m_boutonPersonnage[ index ] ) )
						{
							case SDL_BUTTON_LEFT:
								Projet_Client_Menu_Rejoindre_BMenuRejoindre_ChangerCharsetInterne( this );
								break;
							case SDL_BUTTON_RIGHT:
								Projet_Client_Menu_Rejoindre_BMenuRejoindre_ChangerCouleurCharsetInterne( this );
								break;

							default:
								 break;
						}

						// Remettre a zero
						NLib_Module_SDL_Bouton_NBouton_RemettreEtatAZero( this->m_boutonPersonnage[ index ] );
					}

					// Si clic sur nom
					if( NLib_Module_SDL_Bouton_NBouton_ObtenirEtat( this->m_boutonNomPersonnage[ index ] ) == NETAT_BOUTON_PRESSE )
					{
						switch( NLib_Module_SDL_Bouton_NBouton_ObtenirBoutonActivation( this->m_boutonNomPersonnage[ index ] ) )
						{
							case SDL_BUTTON_LEFT:
								// Notifier saisie
								this->m_estSaisieNomPersonnage = NTRUE;

								// Obtenir joueur
								if( !( joueur = Projet_Client_BClient_ObtenirJoueurCourant( this->m_client ) ) )
								{
									this->m_estSaisieNomPersonnage = NFALSE;
									break;
								}

								// Donner nom actuel
								NLib_Module_SDL_Saisie_NSaisieSDL_DefinirTexte( this->m_saisieNomPersonnage,
									Projet_Commun_Reseau_Client_BEtatClient_ObtenirNom( joueur ) );

								// Update surface
								Projet_Client_Menu_Rejoindre_BMenuRejoindre_UpdateNomJoueur( this,
									NTRUE );

								// Ne plus proteger cache
								Projet_Client_BClient_NePlusProtegerCacheClient( this->m_client );

								// Activer la saisie
								switch( NLib_Module_SDL_Saisie_NSaisieSDL_Saisir( this->m_saisieNomPersonnage,
									NLib_Module_SDL_NFenetre_ObtenirEvenement( this->m_fenetre ),
									&this->m_estEnCours,
									NFALSE,
									NULL ) )
								{
									case NRETOUR_SAISIE_SDL_OK:
										// On continue
										this->m_estEnCours = NTRUE;

										// Proteger le cache
										Projet_Client_BClient_ProtegerCacheClient( this->m_client );

										// Enregistrer la modification
										if( NLib_Module_SDL_Saisie_NSaisieSDL_ObtenirTexte( this->m_saisieNomPersonnage ) != NULL
											&& strlen( NLib_Module_SDL_Saisie_NSaisieSDL_ObtenirTexte( this->m_saisieNomPersonnage ) ) > 0 )
											Projet_Client_Menu_Rejoindre_BMenuRejoindre_ChangerNomInterne( this,
												NLib_Module_SDL_Saisie_NSaisieSDL_ObtenirTexte( this->m_saisieNomPersonnage ) );
										else
											this->m_estDoitUpdateNomJoueur = NTRUE;

										// Ne plus proteger
										Projet_Client_BClient_NePlusProtegerCacheClient( this->m_client );
										break;

									case NRETOUR_SAISIE_SDL_ECHAP:
										// Forcer mise a jour
										this->m_estDoitUpdateNomJoueur = NTRUE;
										break;

									case NRETOUR_SAISIE_SDL_QUITTER:
										// Plus en cours
										this->m_estEnCours = NFALSE;

										// Fermer
										this->m_codeRetour = BCODE_MENU_REJOINDRE_QUITTER;
										break;

									default:
										break;
								}

								// Plus de saisie en cours
								this->m_estSaisieNomPersonnage = NFALSE;
								break;

							default:
								 break;
						}

						// Proteger le cache
						Projet_Client_BClient_ProtegerCacheClient( this->m_client );

						// Remettre a zero
						NLib_Module_SDL_Bouton_NBouton_RemettreEtatAZero( this->m_boutonNomPersonnage[ index ] );
					}
				}
			}

			// Ne plus proteger cache
			Projet_Client_BClient_NePlusProtegerCacheClient( this->m_client );

			// Si on est pret a lancer
			if( Projet_Client_BClient_EstPretLancer( this->m_client ) )
				// Verifier bouton lancer
				if( NLib_Module_SDL_Bouton_NBouton_ObtenirEtat( this->m_boutonLancer ) == NETAT_BOUTON_PRESSE )
				{
					// Changer etat serveur
					Projet_Serveur_BServeur_ValiderLancementClient( Projet_Client_BClient_ObtenirServeur( this->m_client ) );

					// Remettre a zero le bouton
					NLib_Module_SDL_Bouton_NBouton_RemettreEtatAZero( this->m_boutonLancer );
				}
			break;

		default:
			break;
	}

	// Update nom
	Projet_Client_Menu_Rejoindre_BMenuRejoindre_UpdateNomJoueur( this,
		NFALSE );
}

/* Lancer */
BCodeMenuRejoindre Projet_Client_Menu_Rejoindre_BMenuRejoindre_Lancer( BMenuRejoindre *this )
{
	// Evenement
	SDL_Event *e;

	// Recuperer evenement
	e = NLib_Module_SDL_NFenetre_ObtenirEvenement( this->m_fenetre );

	// Lancer musique
	Projet_Commun_Ressource_BRessource_LireMusique( this->m_ressource,
		BMUSIQUE_MENU_REJOINDRE );

	// Est en cours!
	this->m_estEnCours = NTRUE;

	// Boucle principale
	do
	{
		// Effacer evenement
		memset( e,
			0,
			sizeof( SDL_Event ) );

		// Mettre a jour evenement
		SDL_PollEvent( e );

		// Analyser
		switch( e->type )
		{
			case SDL_WINDOWEVENT:
				if( e->window.event == SDL_WINDOWEVENT_CLOSE )
					this->m_estEnCours = NFALSE;
				break;

			case SDL_MOUSEMOTION:
				// Enregistrer position souris
				NDEFINIR_POSITION( this->m_positionSouris,
					e->motion.x,
					e->motion.y );
				break;

			case SDL_KEYDOWN:
				switch( e->key.keysym.sym )
				{
					case SDLK_ESCAPE:
						// Plus en cours
						this->m_estEnCours = NFALSE;

						// Retour au menu principal
						this->m_codeRetour = BCODE_MENU_REJOINDRE_RETOUR_MENU_PRINCIPAL;
						break;

					default:
						break;
				}
				break;

			default:
				break;
		}

		// Mettre a jour
		Projet_Client_Menu_Rejoindre_BMenuRejoindre_MettreAJourInterne( this );

		// Actualiser
		if( this->m_estEnCours )
			Projet_Client_Menu_Rejoindre_BMenuRejoindre_ActualiserInterne( this );
	} while( this->m_estEnCours );

	// OK
	return this->m_codeRetour;
}

BCodeMenuRejoindre Projet_Client_Menu_Rejoindre_BMenuRejoindre_LancerHote( BMenuRejoindre *this )
{
	// Se connecter
	if( !Projet_Client_Menu_Rejoindre_BMenuRejoindre_ConnecterInterne( this,
		NLIB_ADRESSE_IP_LOCALE,
		Projet_Client_Configuration_BConfiguration_ObtenirPort( Projet_Client_BClient_ObtenirConfiguration( this->m_client ) ) ) )
		return BCODE_MENU_REJOINDRE_RETOUR_MENU_PRINCIPAL;

	// Lancer
	return Projet_Client_Menu_Rejoindre_BMenuRejoindre_Lancer( this );
}

/* Remettre a zero */
void Projet_Client_Menu_Rejoindre_BMenuRejoindre_RemettreAZero( BMenuRejoindre *this )
{
	// Iterateur
	NU32 i = 0;

	// Vider
	for( ; i < BNOMBRE_MAXIMUM_JOUEUR; i++ )
		NLib_Module_SDL_Surface_NSurface_Detruire( &this->m_surfaceNomPersonnage[ i ] );

	// Remettre a l'etape initiale
	this->m_etape = (BEtapeMenuRejoindre)0;
}

/* Valider le lancement de la partie */
void Projet_Client_Menu_Rejoindre_BMenuRejoindre_ValiderLancement( BMenuRejoindre *this )
{
	// On lance le jeu
	this->m_codeRetour = BCODE_MENU_REJOINDRE_LANCER;

	// Quitter le menu
	this->m_estEnCours = NFALSE;
}

