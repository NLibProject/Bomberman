#include "../../../../../include/Projet/Projet.h"

// ----------------------------------------------
// struct Projet::Client::Menu::Creer::BMenuCreer
// ----------------------------------------------

/* Construire */
__ALLOC BMenuCreer *Projet_Client_Menu_Creer_BMenuCreer_Construire( const NFenetre *fenetre )
{
	// Sortie
	__OUTPUT BMenuCreer *out;

	// Allouer la memoire
	if( !( out = calloc( 1,
		sizeof( BMenuCreer ) ) ) )
	{
		// Notifier
		NOTIFIER_ERREUR( NERREUR_ALLOCATION_FAILED );

		// Quitter
		return NULL;
	}

	// Enregistrer
	out->m_fenetre = (NFenetre*)fenetre;

	// OK
	return out;
}

/* Detruire */
void Projet_Client_Menu_Creer_BMenuCreer_Detruire( BMenuCreer **this )
{
	// Liberer
	NFREE( (*this) );
}

/* Lancer */
BCodeMenuCreer Projet_Client_Menu_Creer_BMenuCreer_Lancer( BMenuCreer *this )
{
	// Referencer
	NREFERENCER( this );

	// OK
	return BCODE_MENU_CREER_LANCER;
}

