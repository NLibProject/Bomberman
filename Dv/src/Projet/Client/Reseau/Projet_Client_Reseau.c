#include "../../../../include/Projet/Projet.h"

// --------------------------------
// namespace Projet::Client::Reseau
// --------------------------------

/* Callback reception packet */
__CALLBACK NBOOL Projet_Client_Reseau_CallbackReceptionPacket( const NPacket *packet,
	const NClient *client )
{
	// Donnees
	char *donnee;

	// Type
	BTypePacket type;

	// Client bomberman
	void *clientBomberman;

	// Lire
	if( !( clientBomberman = NLib_Module_Reseau_Client_NClient_ObtenirDataUtilisateur( client ) )
		|| !( donnee = Projet_Commun_Reseau_Lire( packet,
			&type ) ) )
	{
		// Notifier
		NOTIFIER_ERREUR( NERREUR_CONSTRUCTOR_FAILED );

		// Quitter
		return NFALSE;
	}

	// Communiquer au client
	if( !Projet_Client_BClient_TraiterPacket( clientBomberman,
		donnee,
		type ) )
		return NFALSE;

	// Liberer
	Projet_Commun_Reseau_Packet_Donnee_Liberer( donnee,
		type );

	// OK
	return NTRUE;
}

/* Callback deconnexion */
NBOOL Projet_Client_Reseau_CallbackDeconnexion( const NClient *client )
{
	// Client bomberman
	void *clientBomberman;

	// Obtenir le client
	if( !( clientBomberman = NLib_Module_Reseau_Client_NClient_ObtenirDataUtilisateur( client ) ) )
	{
		// Notifier
		NOTIFIER_ERREUR( NERREUR_OBJECT_STATE );

		// Quitter
		return NFALSE;
	}

	// Traiter
	Projet_Client_BClient_TraiterDeconnexion( clientBomberman );

	// OK
	return NTRUE;
}

