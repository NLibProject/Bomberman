#include "../../../include/Projet/Projet.h"

// ------------------------------
// struct Projet::Client::BClient
// ------------------------------

/* Detruire si erreur initialisation (privee) */
void Projet_Client_BClient_DetruireConstructionInterne( BClient *this,
	NU32 etape )
{
	// Iterateur
	NU32 i;

	// Detruire
	switch( etape )
	{
		default:

		// Detruire ping
		case BETAPE_CONSTRUCTION_CLIENT_PING:
			NLib_Module_Reseau_NPing_Detruire( &this->m_ping );

		// Detruire cache clients
		case BETAPE_CONSTRUCTION_CLIENT_CACHE_CLIENT:
			Projet_Commun_Reseau_Client_Cache_BCacheClient_Detruire( &this->m_cacheClient );

		// Detruire menus
		case BETAPE_CONSTRUCTION_CLIENT_MENU_REJOINDRE:
			Projet_Client_Menu_Rejoindre_BMenuRejoindre_Detruire( &this->m_menuRejoindre );

		case BETAPE_CONSTRUCTION_CLIENT_MENU_CREER:
			Projet_Client_Menu_Creer_BMenuCreer_Detruire( &this->m_menuCreer );

		case BETAPE_CONSTRUCTION_CLIENT_MENU_PRINCIPAL:
			Projet_Client_Menu_Principal_BMenuPrincipal_Detruire( &this->m_menuPrincipal );

		case BETAPE_CONSTRUCTION_CLIENT_ETOILE:
			for( i = 0; i < BNOMBRE_COUCHES_ETOILES_MENU; i++ )
				Projet_Client_Menu_BFondEtoileMenu_Detruire( &this->m_etoileMenu[ i ] );

		// Detruire ressources
		case BETAPE_CONSTRUCTION_CLIENT_RESSOURCE:
			Projet_Commun_Ressource_BRessource_Detruire( &this->m_ressource );

		// Detruire ensemble cartes
		case BETAPE_CONSTRUCTION_CLIENT_ENSEMBLE_CARTE:
			Projet_Commun_Carte_Ensemble_BEnsembleCarte_Detruire( &this->m_ensembleCarte );

		// Detruire la configuration monde
		case BETAPE_CONSTRUCTION_CLIENT_CONFIGURATION_MONDE:
			Projet_Commun_Monde_BConfigurationMonde_Detruire( &this->m_configurationMonde );

		// Detruire fenetre
		case BETAPE_CONSTRUCTION_CLIENT_FENETRE:
			NLib_Module_SDL_NFenetre_Detruire( &this->m_fenetre );

		// Detruire configuration
		case BETAPE_CONSTRUCTION_CLIENT_CONFIGURATION:
			Projet_Client_Configuration_BConfiguration_Detruire( &this->m_configuration );

		// Liberer
		case BETAPE_CONSTRUCTION_CLIENT_THIS:
			NFREE( this );
			break;
	}
}

#define ERREUR_CONSTRUCTION( etape ) \
	{ \
		/* Notifier */ \
		NOTIFIER_ERREUR( NERREUR_CONSTRUCTOR_FAILED ); \
 \
		/* Liberer */ \
		Projet_Client_BClient_DetruireConstructionInterne( out, \
			etape ); \
 \
		/* Quitter */ \
		return NULL; \
	}

/* Construire */
__ALLOC BClient *Projet_Client_BClient_Construire( void )
{
	// Sortie
	__OUTPUT BClient *out;

	// Iterateurs
	NU32 i,
		j;

	// Allouer la memoire
	if( !( out = calloc( 1,
		sizeof( BClient ) ) ) )
	{
		// Notifier
		NOTIFIER_ERREUR( NERREUR_CONSTRUCTOR_FAILED );

		// Quitter
		return NULL;
	}

	// Charger la configuration
	if( !( out->m_configuration = Projet_Client_Configuration_BConfiguration_Construire( BLIEN_FICHIER_CONFIGURATION_CLIENT ) ) )
		ERREUR_CONSTRUCTION( BETAPE_CONSTRUCTION_CLIENT_THIS );

	// Creer la fenetre
	if( !( out->m_fenetre = NLib_Module_SDL_NFenetre_Construire( BTITRE_FENETRE_CLIENT,
		*Projet_Client_Configuration_BConfiguration_ObtenirResolution( out->m_configuration ),
		NFALSE ) ) )
		ERREUR_CONSTRUCTION( BETAPE_CONSTRUCTION_CLIENT_CONFIGURATION );

	// Construire la configuration du monde
	if( !( out->m_configurationMonde = Projet_Commun_Monde_BConfigurationMonde_Construire( ) ) )
		ERREUR_CONSTRUCTION( BETAPE_CONSTRUCTION_CLIENT_FENETRE );

	// Construire ensemble carte
	if( !( out->m_ensembleCarte = Projet_Commun_Carte_Ensemble_BEnsembleCarte_Construire( BREPERTOIRE_ENSEMBLE_CARTE ) ) )
		ERREUR_CONSTRUCTION( BETAPE_CONSTRUCTION_CLIENT_CONFIGURATION_MONDE );

	// Charger les ressources
	if( !( out->m_ressource = Projet_Commun_Ressource_BRessource_Construire( out->m_fenetre ) ) )
		ERREUR_CONSTRUCTION( BETAPE_CONSTRUCTION_CLIENT_ENSEMBLE_CARTE );

	// Construire fond etoile
	for( i = 0; i < BNOMBRE_COUCHES_ETOILES_MENU; i++ )
		if( !( out->m_etoileMenu[ i ] = Projet_Client_Menu_BFondEtoileMenu_Construire( out->m_fenetre,
			BDELAI_ENTRE_UPDATE_ETOILES_MENU[ i ],
			BCOULEUR_ETOILES_MENU[ i ] ) ) )
		{
			// Liberer
			for( j = 0; j < i; j++ )
				Projet_Client_Menu_BFondEtoileMenu_Detruire( &out->m_etoileMenu[ j ] );

			// Quitter
			ERREUR_CONSTRUCTION( BETAPE_CONSTRUCTION_CLIENT_RESSOURCE );
		}

	// Construire les menus
		// Principal
			if( !( out->m_menuPrincipal = Projet_Client_Menu_Principal_BMenuPrincipal_Construire( out->m_fenetre,
				(const BFondEtoileMenu**)out->m_etoileMenu ) ) )
				ERREUR_CONSTRUCTION( BETAPE_CONSTRUCTION_CLIENT_ETOILE );
		// Creer
			if( !( out->m_menuCreer = Projet_Client_Menu_Creer_BMenuCreer_Construire( out->m_fenetre ) ) )
				ERREUR_CONSTRUCTION( BETAPE_CONSTRUCTION_CLIENT_MENU_PRINCIPAL );
		// Rejoindre
			if( !( out->m_menuRejoindre = Projet_Client_Menu_Rejoindre_BMenuRejoindre_Construire( out->m_fenetre,
				out,
				(const BFondEtoileMenu**)out->m_etoileMenu ) ) )
				ERREUR_CONSTRUCTION( BETAPE_CONSTRUCTION_CLIENT_MENU_CREER );

	// Construire le cache client
	if( !( out->m_cacheClient = Projet_Commun_Reseau_Client_Cache_BCacheClient_Construire( out->m_ressource ) ) )
		ERREUR_CONSTRUCTION( BETAPE_CONSTRUCTION_CLIENT_MENU_REJOINDRE );

	// Construire ping
	if( !( out->m_ping = NLib_Module_Reseau_NPing_Construire( ) ) )
		ERREUR_CONSTRUCTION( BETAPE_CONSTRUCTION_CLIENT_CACHE_CLIENT );

	// Zero
	out->m_estHote = NFALSE;

	// Est maintenant en cours
	out->m_estEnCours = NTRUE;

	// OK
	return out;
}

#undef ERREUR_CONSTRUCTION

/* Detruire */
void Projet_Client_BClient_Detruire( BClient **this )
{
	// Detruire client
	if( (*this)->m_client != NULL )
		NLib_Module_Reseau_Client_NClient_Detruire( &(*this)->m_client );

	// Detruire serveur
	if( (*this)->m_estHote )
		Projet_Serveur_BServeur_Detruire( &(*this)->m_serveur );

	// Detruire monde
	if( (*this)->m_monde != NULL )
		Projet_Commun_Monde_BMonde_Detruire( &(*this)->m_monde );

	// Detruire
	Projet_Client_BClient_DetruireConstructionInterne( *this,
		BETAPES_CONSTRUCTION_CLIENT );

	// Dissocier adresse
	NDISSOCIER_ADRESSE( *this );
}

/* Lancer le serveur (privee) */
NBOOL Projet_Client_BClient_DemarrerServeurInterne( BClient *this )
{
	// Creer le serveur
	if( !( this->m_serveur = Projet_Serveur_BServeur_Construire( Projet_Client_Configuration_BConfiguration_ObtenirPort( this->m_configuration ),
		this->m_configurationMonde ) ) )
	{
		// Notifier
		NOTIFIER_ERREUR( NERREUR_CONSTRUCTOR_FAILED );

		// Quitter
		return NFALSE;
	}

	// Lancer le serveur
	if( !Projet_Serveur_BServeur_Lancer( this->m_serveur ) )
	{
		// Notifier
		NOTIFIER_ERREUR( NERREUR_CONSTRUCTOR_FAILED );

		// Liberer
		Projet_Serveur_BServeur_Detruire( &this->m_serveur );

		// Quitter
		return NFALSE;
	}

	// OK
	return this->m_estHote = NTRUE;
}

/* Lancer le client */
NBOOL Projet_Client_BClient_Lancer( BClient *this )
{
	// Doit lancer le jeu?
	NBOOL estDoitLancer;

	// Boucle principale
	do
	{
		// Jouer musique menu
		Projet_Client_BClient_JouerMusique( this,
			BLISTE_MUSIQUE_AC_RAIN );

		// On ne doit pas lancer le jeu
		estDoitLancer = NFALSE;

		// Deconnecter
		if( this->m_client != NULL )
			NLib_Module_Reseau_Client_NClient_Detruire( &this->m_client );

		// Si on etait h�te auparavant
		if( this->m_estHote )
		{
			// On ne l'est plus
			this->m_estHote = NFALSE;

			// Detruire
			Projet_Serveur_BServeur_Detruire( &this->m_serveur );

			// Vider cache
				// Detruire
					if( this->m_cacheClient != NULL )
						Projet_Commun_Reseau_Client_Cache_BCacheClient_Detruire( &this->m_cacheClient );
				// Reconstruire
					if( !( this->m_cacheClient = Projet_Commun_Reseau_Client_Cache_BCacheClient_Construire( this->m_ressource ) ) )
					{
						// Notifier
						NOTIFIER_ERREUR( NERREUR_CONSTRUCTOR_FAILED );

						// Quitter
						return NFALSE;
					}
		}

		// Remettre a zero menu rejoindre
		Projet_Client_Menu_Rejoindre_BMenuRejoindre_RemettreAZero( this->m_menuRejoindre );

		// Si on avait un monde en stock
		if( this->m_monde != NULL )
			Projet_Commun_Monde_BMonde_Detruire( &this->m_monde );

		// Menu principal
		switch( Projet_Client_Menu_Principal_BMenuPrincipal_Lancer( this->m_menuPrincipal ) )
		{
			case BCODE_MENU_PRINCIPAL_MENU_CREER:
				// Lancer
				switch( Projet_Client_Menu_Creer_BMenuCreer_Lancer( this->m_menuCreer ) )
				{
					case BCODE_MENU_CREER_LANCER:
						// Lancer le serveur
						if( !Projet_Client_BClient_DemarrerServeurInterne( this ) )
						{
							// Notifier
							NOTIFIER_ERREUR( NERREUR_SERVER_CLOSED );

							// Sortir
							break;
						}

						// Aller dans le menu rejoindre
						switch( Projet_Client_Menu_Rejoindre_BMenuRejoindre_LancerHote( this->m_menuRejoindre ) )
						{
							default:
							case BCODE_MENU_REJOINDRE_RETOUR_MENU_PRINCIPAL:
								break;

							case BCODE_MENU_REJOINDRE_LANCER:
								estDoitLancer = NTRUE;
								break;

							case BCODE_MENU_REJOINDRE_QUITTER:
								this->m_estEnCours = NFALSE;
								break;
						}
						break;

					case BCODE_MENU_CREER_RETOUR_MENU_PRINCIPAL:
						break;

					case BCODE_MENU_CREER_QUITTER:
						// Quitter
						this->m_estEnCours = NFALSE;
						break;

					default:
						break;
				}
				break;

			case BCODE_MENU_PRINCIPAL_MENU_REJOINDRE:
				// Lancer
				switch( Projet_Client_Menu_Rejoindre_BMenuRejoindre_Lancer( this->m_menuRejoindre ) )
				{
					default:
					case BCODE_MENU_REJOINDRE_RETOUR_MENU_PRINCIPAL:
						break;

					case BCODE_MENU_REJOINDRE_LANCER:
						estDoitLancer = NTRUE;
						break;

					case BCODE_MENU_REJOINDRE_QUITTER:
						this->m_estEnCours = NFALSE;
						break;
				}
				break;

			case BCODE_MENU_PRINCIPAL_QUITTER:
				this->m_estEnCours = NFALSE;
				break;

			default:
				break;
		}

		// Si on doit lancer le jeu
		if( estDoitLancer )
		{
			// Construire le monde client
			if( !( this->m_mondeClient = Projet_Client_Monde_BMondeClient_Construire( this ) ) )
			{
				// Notifier
				NOTIFIER_ERREUR( NERREUR_CONSTRUCTOR_FAILED );

				// Retourner au menu principal
				continue;
			}

			// Lancer le monde
			switch( Projet_Client_Monde_BMondeClient_Lancer( this->m_mondeClient ) )
			{
				default:
				case BCODE_RETOUR_MONDE_CLIENT_RETOUR_MENU_PRINCIPAL:
					break;

				case BCODE_RETOUR_MONDE_CLIENT_QUITTER:
					this->m_estEnCours = NFALSE;
					break;
			}

			// Detruire le monde client
			Projet_Client_Monde_BMondeClient_Detruire( &this->m_mondeClient );
		}
	} while( this->m_estEnCours );

	// OK
	return NTRUE;
}

/* Mettre a jour */
NBOOL Projet_Client_BClient_MettreAJour( BClient *this )
{
	// Est doit deconnecter?
	NBOOL estDoitDeconnecter = NFALSE;

	// Mettre a jour les ressources
	Projet_Commun_Ressource_BRessource_Update( this->m_ressource );

	// Si on est connecte
	if( Projet_Client_BClient_EstConnecte( this ) )
	{
		// Mettre a jour le cache
		Projet_Commun_Reseau_Client_Cache_BCacheClient_Update( this->m_cacheClient );

		// Verifier l'etat du ping
		if( NLib_Module_Reseau_NPing_EstDoitEffectuerRequete( this->m_ping ) )
		{
			// Envoyer requete
			if( Projet_Client_TraitementPacket_EnvoyerRequetePing( this ) )
				NLib_Module_Reseau_NPing_EffectuerRequete( this->m_ping );
			else
				estDoitDeconnecter = NTRUE;
		}
		else
			if( NLib_Module_Reseau_NPing_EstTimeout( this->m_ping ) )
			{
				// On s'auto kick
				estDoitDeconnecter = NTRUE;

				// Notifier
				NOTIFIER_ERREUR( NERREUR_TIMEOUT );
			}

		// Verifier etat connexion
		if( NLib_Module_Reseau_Client_NClient_EstErreur( this->m_client ) )
			estDoitDeconnecter = NTRUE;

		// Si on doit deconnecter...
		if( estDoitDeconnecter )
			// Deconnecter
			NLib_Module_Reseau_Client_NClient_Deconnecter( this->m_client );
	}

	// OK
	return NTRUE;
}

/* Se connecter */
NBOOL Projet_Client_BClient_Connecter( BClient *this,
	const char *ip,
	NU32 port )
{
	// Verifier etat connexion
	if( this->m_client != NULL )
		NLib_Module_Reseau_Client_NClient_Detruire( &this->m_client );

	// Construire
	if( !( this->m_client = NLib_Module_Reseau_Client_NClient_Construire( ip,
		port,
		Projet_Client_Reseau_CallbackReceptionPacket,
		Projet_Client_Reseau_CallbackDeconnexion,
		this,
		0,
		NTYPE_CLIENT_TCP,
		NMETHODE_TRANSMISSION_PACKETIO_NPROJECT ) ) )
	{
		// Notifier
		NOTIFIER_ERREUR( NERREUR_CONSTRUCTOR_FAILED );

		// Quitter
		return NFALSE;
	}

	// Se connecter
	if( !NLib_Module_Reseau_Client_NClient_Connecter( this->m_client ) )
	{
		// Notifier
		NOTIFIER_ERREUR( NERREUR_UNABLE_TO_CONNECT );

		// Liberer
		NLib_Module_Reseau_Client_NClient_Detruire( &this->m_client );

		// Quitter
		return NFALSE;
	}

	// Remettre a zero le ping
	NLib_Module_Reseau_NPing_MettreAZero( this->m_ping );

	// OK
	return NTRUE;
}

/* Se deconnecter */
NBOOL Projet_Client_BClient_Deconnecter( BClient *this )
{
	// Si on est connecte
	if( Projet_Client_BClient_EstConnecte( this ) )
		return NLib_Module_Reseau_Client_NClient_Deconnecter( this->m_client );

	// Aucun besoin de deconnecter
	return NTRUE;
}

/* Obtenir configuration */
const BConfiguration *Projet_Client_BClient_ObtenirConfiguration( const BClient *this )
{
	return this->m_configuration;
}

/* Obtenir nombre carte */
NU32 Projet_Client_BClient_ObtenirNombreCarte( const BClient *this )
{
	return Projet_Commun_Carte_Ensemble_BEnsembleCarte_ObtenirNombre( this->m_ensembleCarte );
}

/* Obtenir nombre joueur */
NU32 Projet_Client_BClient_ObtenirNombreJoueur( const BClient *this )
{
	return Projet_Commun_Reseau_Client_Cache_BCacheClient_ObtenirNombreClient( this->m_cacheClient );
}

/* Obtenir carte */
const BCarte *Projet_Client_BClient_ObtenirCarte( const BClient *this,
	NU32 id )
{
	return Projet_Commun_Carte_Ensemble_BEnsembleCarte_Obtenir( this->m_ensembleCarte,
		id );
}

/* Obtenir etat carte */
const BEtatCarte *Projet_Client_BClient_ObtenirEtatCarte( const BClient *this )
{
	// Verifier etat
	if( !this->m_monde )
		return NULL;

	// OK
	return Projet_Commun_Monde_BMonde_ObtenirEtatCarte( this->m_monde );
}

/* Obtenir configuration monde */
BConfigurationMonde *Projet_Client_BClient_ObtenirConfigurationMonde( BClient *this )
{
	return this->m_configurationMonde;
}

/* Obtenir ressources */
const BRessource *Projet_Client_BClient_ObtenirRessource( const BClient *this )
{
	return this->m_ressource;
}

/* Obtenir fenetre */
const NFenetre *Projet_Client_BClient_ObtenirFenetre( const BClient *this )
{
	return this->m_fenetre;
}

/* Obtenir joueur (le cache client doit etre protege) */
const BEtatClient *Projet_Client_BClient_ObtenirJoueur( const BClient *this,
	NU32 index )
{
	return Projet_Commun_Reseau_Client_Cache_BCacheClient_ObtenirClient2( this->m_cacheClient,
		index );
}

const BEtatClient *Projet_Client_BClient_ObtenirJoueurCourant( const BClient *this )
{
	return Projet_Commun_Reseau_Client_Cache_BCacheClient_ObtenirClientCourant( this->m_cacheClient );
}

/* Obtenir l'identifiant du joueur courant */
NU32 Projet_Client_BClient_ObtenirIndexJoueurCourant( const BClient *this )
{
	return Projet_Commun_Reseau_Client_Cache_BCacheClient_ObtenirIndexClientCourant( this->m_cacheClient );
}

/* Proteger cache clients */
NBOOL Projet_Client_BClient_ProtegerCacheClient( BClient *this )
{
	return Projet_Commun_Reseau_Client_Cache_BCacheClient_ProtegerCache( this->m_cacheClient );
}

/* Ne plus proteger le cache clients */
NBOOL Projet_Client_BClient_NePlusProtegerCacheClient( BClient *this )
{
	return Projet_Commun_Reseau_Client_Cache_BCacheClient_NePlusProtegerCache( this->m_cacheClient );
}

/* Obtenir identifiant joueur courant */
NU32 Projet_Client_BClient_ObtenirIdentifiantJoueurCourant( const BClient *this )
{
	return Projet_Commun_Reseau_Client_Cache_BCacheClient_ObtenirIdentifiantClientCourant( this->m_cacheClient );
}

/* Obtenir touche */
SDL_Keycode Projet_Client_BClient_ObtenirTouche( const BClient *this,
	BTouche touche )
{
	return Projet_Client_Configuration_Touche_BConfigurationTouche_Obtenir( Projet_Client_Configuration_BConfiguration_ObtenirConfigurationTouche( this->m_configuration ),
		touche );
}
BTouche Projet_Client_BClient_ObtenirToucheInverse( const BClient *this,
	SDL_Keycode touche )
{
	return Projet_Client_Configuration_Touche_BConfigurationTouche_ObtenirInverse( Projet_Client_Configuration_BConfiguration_ObtenirConfigurationTouche( this->m_configuration ),
		touche );
}

/* Obtenir monde */
const BMonde *Projet_Client_BClient_ObtenirMonde( const BClient *this )
{
	return this->m_monde;
}

/* Obtenir cache clients */
const BCacheClient *Projet_Client_BClient_ObtenirCacheClient( const BClient *this )
{
	return this->m_cacheClient;
}

/* Est connecte? */
NBOOL Projet_Client_BClient_EstConnecte( const BClient *this )
{
	return ( this->m_client != NULL )
		&& NLib_Module_Reseau_Client_NClient_EstConnecte( this->m_client );
}

/* Est h�te? */
NBOOL Projet_Client_BClient_EstHote( const BClient *this )
{
	return this->m_estHote;
}

/* Est pret a lancer */
NBOOL Projet_Client_BClient_EstPretLancer( const BClient *this )
{
	// Iterateur
	NU32 i;

	// Joueur
	const BEtatClient *joueur;

	// Verifier si h�te
	if( !this->m_estHote )
		return NFALSE;

	// Proteger le cache
	Projet_Commun_Reseau_Client_Cache_BCacheClient_ProtegerCache( this->m_cacheClient );

	// Verifier si on a au moins 2 joueurs
	if( Projet_Commun_Reseau_Client_Cache_BCacheClient_ObtenirNombreClient( this->m_cacheClient ) < BNOMBRE_MINIMUM_JOUEUR )
	{
		// Ne plus proteger le cache
		Projet_Commun_Reseau_Client_Cache_BCacheClient_NePlusProtegerCache( this->m_cacheClient );

		// Quitter
		return NFALSE;
	}

	// Verifier si les joueurs sont prets
	for( i = 0; i < Projet_Commun_Reseau_Client_Cache_BCacheClient_ObtenirNombreClient( this->m_cacheClient ); i++ )
		// Obtenir joueur
		if( !( joueur = Projet_Commun_Reseau_Client_Cache_BCacheClient_ObtenirClient2( this->m_cacheClient,
				i ) )
			|| !Projet_Commun_Reseau_Client_BEtatClient_EstPret( joueur ) )
		{
			// Ne plus proteger le cache
			Projet_Commun_Reseau_Client_Cache_BCacheClient_NePlusProtegerCache( this->m_cacheClient );

			// Quitter
			return NFALSE;
		}

	// Ne plus proteger
	Projet_Commun_Reseau_Client_Cache_BCacheClient_NePlusProtegerCache( this->m_cacheClient );

	// OK
	return NTRUE;
}

/* Jouer musique */
NBOOL Projet_Client_BClient_JouerMusique( const BClient *this,
	BListeMusique musique )
{
	return Projet_Commun_Ressource_BRessource_LireMusique( this->m_ressource,
		musique );
}

/* Lire effet sonore */
void Projet_Client_BClient_JouerEffetSonore( const BClient *this,
	BListeEffetSonore effet )
{
	Projet_Commun_Ressource_BRessource_LireEffetSonore( this->m_ressource,
		effet );
}

/* Arreter musique */
NBOOL Projet_Client_BClient_ArreterMusique( const BClient *this )
{
	return Projet_Commun_Ressource_BRessource_ArreterMusique( this->m_ressource );
}

/* Obtenir checksum ressources */
NU32 Projet_Client_BClient_ObtenirChecksumRessource( const BClient *this )
{
	return Projet_Commun_Ressource_BRessource_ObtenirChecksum( this->m_ressource )
		+ Projet_Commun_Carte_Ensemble_BEnsembleCarte_ObtenirChecksum( this->m_ensembleCarte );
}

/* Obtenir le serveur */
BServeur *Projet_Client_BClient_ObtenirServeur( BClient *this )
{
	return this->m_serveur;
}

/* Traiter un packet */
NBOOL Projet_Client_BClient_TraiterPacket( BClient *this,
	void *donnee,
	BTypePacket type )
{
	// Traiter
	switch( type )
	{
		case BTYPE_PACKET_SERVEUR_CLIENT_CONNEXION_TRANSMET_IDENTIFIANT:
			return Projet_Client_TraitementPacket_TraiterConnexionTransmetIdentifiant( this,
				donnee );

		case BTYPE_PACKET_SERVEUR_CLIENT_REPONSE_INFORMATIONS_JOUEUR:
			return Projet_Client_TraitementPacket_TraiterReponseInformationsJoueur( this,
				donnee );

		case BTYPE_PACKET_SERVEUR_CLIENT_DECONNEXION_JOUEUR:
			return Projet_Client_TraitementPacket_TraiterDeconnexionJoueur( this,
				donnee );

		case BTYPE_PACKET_SERVEUR_CLIENT_CHECKSUM_INCORRECT:
			return Projet_Client_TraitementPacket_TraiterChecksumIncorrect( this,
				donnee );

		case BTYPE_PACKET_SERVEUR_CLIENT_PING:
			return Projet_Client_TraitementPacket_TraiterPing( this,
				donnee );
		case BTYPE_PACKET_SERVEUR_CLIENT_PONG:
			return Projet_Client_TraitementPacket_TraiterPong( this,
				donnee );

		case BTYPE_PACKET_SERVEUR_CLIENT_DIFFUSE_ETAT_PRET:
			return Projet_Client_TraitementPacket_TraiterDiffuseEtatPret( this,
				donnee );

		case BTYPE_PACKET_SERVEUR_CLIENT_DIFFUSE_CHANGEMENT_CARTE:
			return Projet_Client_TraitementPacket_TraiterDiffuseChangementCarte( this,
				donnee );

		case BTYPE_PACKET_SERVEUR_CLIENT_DIFFUSE_LANCEMENT:
			return Projet_Client_TraitementPacket_TraiterDiffuseLancement( this,
				donnee );

		case BTYPE_PACKET_SERVEUR_CLIENT_DIFFUSE_MESSAGE_AFFICHER:
			return Projet_Client_TraitementPacket_TraiterDiffuseMessageAfficher( this,
				donnee );

		case BTYPE_PACKET_SERVEUR_CLIENT_DIFFUSE_DEBUT_PARTIE:
			return Projet_Client_TraitementPacket_TraiterDiffuseDebutPartie( this,
				donnee );

		case BTYPE_PACKET_SERVEUR_CLIENT_DIFFUSE_CHANGEMENT_DIRECTION:
			return Projet_Client_TraitementPacket_TraiterDiffuseChangementDirection( this,
				donnee );
		case BTYPE_PACKET_SERVEUR_CLIENT_DIFFUSE_CHANGEMENT_POSITION:
			return Projet_Client_TraitementPacket_TraiterDiffuseChangementPosition( this,
				donnee );

		case BTYPE_PACKET_SERVEUR_CLIENT_REFUSE_POSE_BOMBE:
			return Projet_Client_TraitementPacket_TraiterRefusePoseBombe( this,
				donnee );
		case BTYPE_PACKET_SERVEUR_CLIENT_DIFFUSE_POSE_BOMBE:
			return Projet_Client_TraitementPacket_TraiterDiffusePoseBombe( this,
				donnee );
		case BTYPE_PACKET_SERVEUR_CLIENT_BOMBE_EXPLOSE:
			return Projet_Client_TraitementPacket_TraiterBombeExplose( this,
				donnee );

		case BTYPE_PACKET_SERVEUR_CLIENT_BLOC_REMPLI_DETRUIT:
			return Projet_Client_TraitementPacket_TraiterBlocRempliDetruit( this,
				donnee );

		case BTYPE_PACKET_SERVEUR_CLIENT_DIFFUSE_APPARITION_BONUS:
			return Projet_Client_TraitementPacket_TraiterDiffuseApparitionBonus( this,
				donnee );
		case BTYPE_PACKET_SERVEUR_CLIENT_DIFFUSE_DISPARITION_BONUS:
			return Projet_Client_TraitementPacket_TraiterDiffuseDisparitionBonus( this,
				donnee );
		case BTYPE_PACKET_SERVEUR_CLIENT_DISTRIBUE_BONUS:
			return Projet_Client_TraitementPacket_TraiterDistribueBonus( this,
				donnee );

		case BTYPE_PACKET_SERVEUR_CLIENT_DIFFUSE_MORT_JOUEUR:
			return Projet_Client_TraitementPacket_TraiterDiffuseMortJoueur( this,
				donnee );

		case BTYPE_PACKET_SERVEUR_CLIENT_ANNONCE_FIN_PARTIE:
			return Projet_Client_TraitementPacket_TraiterAnnonceFinPartie( this,
				donnee );

		default:
			// Notifier
			NOTIFIER_ERREUR( NERREUR_UNKNOWN_PACKET );

			// Quitter
			return NFALSE;
	}
}

/* Traiter la deconnexion */
NBOOL Projet_Client_BClient_TraiterDeconnexion( BClient *this )
{
	// Detruire le cache
	Projet_Commun_Reseau_Client_Cache_BCacheClient_Vider( this->m_cacheClient );

	// OK
	return NTRUE;
}

/* Envoyer un packet */
NBOOL Projet_Client_BClient_EnvoyerPacket( BClient *this,
	__WILLBEOWNED NPacket *packet )
{
	// Verifier
	if( !Projet_Client_BClient_EstConnecte( this ) )
	{
		// Notifier
		NOTIFIER_ERREUR( NERREUR_OBJECT_STATE );

		return NFALSE;
	}

	// Envoyer
	return NLib_Module_Reseau_Client_NClient_AjouterPacket( this->m_client,
		packet );
}

/* Obtenir identifiant derniere modification */
NU32 Projet_Client_BClient_ObtenirIdentifiantDerniereModificationCacheClient( BClient *this )
{
	return Projet_Commun_Reseau_Client_Cache_BCacheClient_ObtenirIdentifiantDerniereModification( this->m_cacheClient );
}

