#include "../../../../include/Projet/Projet.h"

// ----------------------------------------------------
// struct Projet::Client::Configuration::BConfiguration
// ----------------------------------------------------

/* Construire */
__ALLOC BConfiguration *Projet_Client_Configuration_BConfiguration_Construire( const char *lien )
{
	// Sortie
	__OUTPUT BConfiguration *out;

	// Fichier
	NFichierClef *fichier;

	// Clefs
	char **clefs;

	// Iterateur
	NU32 i;

	// Curseur
	NU32 curseur;

	// Obtenir les clefs
	if( !( clefs = Projet_Client_Configuration_BListeProprieteConfiguration_ObtenirEnsembleClef( ) ) )
	{
		// Notifier
		NOTIFIER_ERREUR( NERREUR_CONSTRUCTOR_FAILED );

		// Quitter
		return NULL;
	}

	// Construire le fichier
	if( !( fichier = NLib_Fichier_Clef_NFichierClef_Construire( lien,
		(const char**)clefs,
		BLISTE_PROPRIETES_CONFIGURATION ) ) )
	{
		// Notifier
		NOTIFIER_ERREUR( NERREUR_CONSTRUCTOR_FAILED );

		// Liberer
		for( i = 0; i < BLISTE_PROPRIETES_CONFIGURATION; i++ )
			NFREE( clefs[ i ] );
		NFREE( clefs );

		// Quitter
		return NULL;
	}

	// Liberer les clefs
	for( i = 0; i < BLISTE_PROPRIETES_CONFIGURATION; i++ )
		NFREE( clefs[ i ] );
	NFREE( clefs );

	// Allouer la memoire
	if( !( out = calloc( 1,
		sizeof( BConfiguration ) ) ) )
	{
		// Notifier
		NOTIFIER_ERREUR( NERREUR_ALLOCATION_FAILED );

		// Fermer le fichier
		NLib_Fichier_Clef_NFichierClef_Detruire( &fichier );

		// Quitter
		return NULL;
	}

	// Lire
	for( i = 0; i < BLISTE_PROPRIETES_CONFIGURATION; i++ )
		switch( i )
		{
			case BLISTE_PROPRIETE_CONFIGURATION_RESOLUTION:
				// Curseur a zero
				curseur = 0;

				// Lire resolution
				NDEFINIR_POSITION( out->m_resolution,
					NLib_Chaine_LireNombreNonSigne( NLib_Fichier_Clef_NFichierClef_ObtenirValeur( fichier,
							i ),
						&curseur,
						NFALSE ),
					NLib_Chaine_LireNombreNonSigne( NLib_Fichier_Clef_NFichierClef_ObtenirValeur( fichier,
							i ),
						&curseur,
						NFALSE ) );
				break;

			case BLISTE_PROPRIETE_CONFIGURATION_PORT:
				out->m_port = NLib_Fichier_Clef_NFichierClef_ObtenirValeur2( fichier,
					i );
				break;

			case BLISTE_PROPRIETE_CONFIGURATION_NOM_DEFAUT:
				out->m_nomDefaut = NLib_Fichier_Clef_NFichierClef_ObtenirCopieValeur( fichier,
					i );
				break;

			case BLISTE_PROPRIETE_CONFIGURATION_CHARSET_DEFAUT:
				out->m_charsetDefaut = NLib_Fichier_Clef_NFichierClef_ObtenirValeur2( fichier,
					i );
				break;

			case BLISTE_PROPRIETE_CONFIGURATION_COULEUR_CHARSET_DEFAUT:
				out->m_couleurCharsetDefaut = NLib_Fichier_Clef_NFichierClef_ObtenirValeur2( fichier,
					i );
				break;

			default:
				break;
		}

	// Construire la configuration des touches
	if( !( out->m_touche = Projet_Client_Configuration_Touche_BConfigurationTouche_Construire2( fichier ) ) )
	{
		// Notifier
		NOTIFIER_ERREUR( NERREUR_CONSTRUCTOR_FAILED );

		// Liberer
		NFREE( out->m_nomDefaut );
		NFREE( out );

		// Quitter
		return NULL;
	}

	// Fermer le fichier
	NLib_Fichier_Clef_NFichierClef_Detruire( &fichier );

	// OK
	return out;
}

/* Detruire */
void Projet_Client_Configuration_BConfiguration_Detruire( BConfiguration **this )
{
	// Detruire configuration touches
	Projet_Client_Configuration_Touche_BConfigurationTouche_Detruire( &(*this)->m_touche );

	// Liberer nom
	NFREE( (*this)->m_nomDefaut );

	// Liberer
	NFREE( (*this) );
}

/* Obtenir la resolution */
const NUPoint *Projet_Client_Configuration_BConfiguration_ObtenirResolution( const BConfiguration *this )
{
	return &this->m_resolution;
}

/* Obtenir le port */
NU32 Projet_Client_Configuration_BConfiguration_ObtenirPort( const BConfiguration *this )
{
	return this->m_port;
}

/* Obtenir le nom par defaut */
const char *Projet_Client_Configuration_BConfiguration_ObtenirNomDefaut( const BConfiguration *this )
{
	return this->m_nomDefaut;
}

/* Obtenir le charset defaut */
NU32 Projet_Client_Configuration_BConfiguration_ObtenirCharsetDefaut( const BConfiguration *this )
{
	return this->m_charsetDefaut;
}

/* Obtenir couleur charset defaut */
NU32 Projet_Client_Configuration_BConfiguration_ObtenirCouleurCharsetDefaut( const BConfiguration *this )
{
	return this->m_couleurCharsetDefaut;
}

/* Obtenir configuration touches */
BConfigurationTouche *Projet_Client_Configuration_BConfiguration_ObtenirConfigurationTouche( const BConfiguration *this )
{
	return this->m_touche;
}
