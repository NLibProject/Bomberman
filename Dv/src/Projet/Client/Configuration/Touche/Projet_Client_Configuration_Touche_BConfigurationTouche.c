#include "../../../../../include/Projet/Projet.h"

// ------------------------------------------------------------------
// struct Projet::Client::Configuration::Touche::BConfigurationTouche
// ------------------------------------------------------------------

/* Construire */
__ALLOC BConfigurationTouche *Projet_Client_Configuration_Touche_BConfigurationTouche_Construire( void )
{
	// Sortie
	__OUTPUT BConfigurationTouche *out;

	// Iterateur
	NU32 i;

	// Allouer la memoire
	if( !( out = calloc( 1,
		sizeof( BConfigurationTouche ) ) ) )
	{
		// Notifier
		NOTIFIER_ERREUR( NERREUR_ALLOCATION_FAILED );

		// Quitter
		return NULL;
	}

	// Enregistrer valeurs par defaut
	for( i = 0; i < BTOUCHES; i++ )
		out->m_touche[ i ] = Projet_Client_Configuration_Touche_BTouche_ObtenirToucheDefaut( (BTouche)i );

	// OK
	return out;
}

__ALLOC BConfigurationTouche *Projet_Client_Configuration_Touche_BConfigurationTouche_Construire2( NFichierClef *fichier )
{
	// Sortie
	__OUTPUT BConfigurationTouche *out;

	// Iterateur
	NU32 i;

	// Propriete
	NU32 propriete = 0;

	// Allouer la memoire
	if( !( out = calloc( 1,
		sizeof( BConfigurationTouche ) ) ) )
	{
		// Notifier
		NOTIFIER_ERREUR( NERREUR_ALLOCATION_FAILED );

		// Quitter
		return NULL;
	}
	
	// Enregistrer valeurs
	for( i = 0; i < BTOUCHES; i++ )
	{
		// Mapper propriete
		switch( i )
		{
			case BTOUCHE_HAUT:
				propriete = BLISTE_PROPRIETE_CONFIGURATION_TOUCHE_HAUT;
				break;
			case BTOUCHE_BAS:
				propriete = BLISTE_PROPRIETE_CONFIGURATION_TOUCHE_BAS;
				break;
			case BTOUCHE_GAUCHE:
				propriete = BLISTE_PROPRIETE_CONFIGURATION_TOUCHE_GAUCHE;
				break;
			case BTOUCHE_DROITE:
				propriete = BLISTE_PROPRIETE_CONFIGURATION_TOUCHE_DROITE;
				break;

			case BTOUCHE_BOMBE:
				propriete = BLISTE_PROPRIETE_CONFIGURATION_TOUCHE_BOMBE;
				break;

			default:
				break;
		}
	
		// Enregistrer
		if( !( out->m_touche[ i ] = (NS32)NLib_Fichier_Clef_NFichierClef_ObtenirValeur2( fichier,
			propriete ) ) )
		{
			// Notifier
			NOTIFIER_ERREUR( NERREUR_FILE );

			// Liberer
			NFREE( out );

			// Quitter
			return NULL;
		}
	}

	// OK
	return out;
}

/* Detruire */
void Projet_Client_Configuration_Touche_BConfigurationTouche_Detruire( BConfigurationTouche **this )
{
	NFREE( (*this) );
}

/* Obtenir touche */
SDL_Keycode Projet_Client_Configuration_Touche_BConfigurationTouche_Obtenir( const BConfigurationTouche *this,
	BTouche touche )
{
	return this->m_touche[ touche ];
}

BTouche Projet_Client_Configuration_Touche_BConfigurationTouche_ObtenirInverse( const BConfigurationTouche *this,
	SDL_Keycode touche )
{
	// Sortie
	__OUTPUT NU32 i = 0;

	// Chercher
	for( ; i < BTOUCHES; i++ )
		if( this->m_touche[ i ] == touche )
			return (BTouche)i;

	// Introuvable
	return BTOUCHE_AUCUNE;
}

